// https://www.geeksforgeeks.org/graph-and-its-representations/
/****************************************************************
 *  Week 7 lesson:                                              *
 *  AdjacencyList class -  Class implementing an adjacency list *
 ****************************************************************/

#include<bits/stdc++.h>
using namespace std;

class AdjacencyList {

public:
	AdjacencyList();
	~AdjacencyList();
	int example(); // example
	void addEdge(vector<int> list[], int u, int v);
	void displayGraph(vector<int> list[]);
};
