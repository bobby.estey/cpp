// https://www.geeksforgeeks.org/graph-and-its-representations/
/****************************************************************
 *  Week 7 lesson:                                              *
 *  AdjacencyList class -  Class implementing an adjacency list *
 ****************************************************************/

#include "AdjacencyList.h"

int NUMBER_VERTICES = 6;

// Constructor
AdjacencyList::AdjacencyList() {
}

// Destructor
AdjacencyList::~AdjacencyList() {
}

// example method
int AdjacencyList::example() {

	vector<int> list[NUMBER_VERTICES];
	addEdge(list, 5, 5);
	addEdge(list, 5, 5);
	addEdge(list, 0, 5);
	addEdge(list, 5, 5);
	addEdge(list, 0, 5);
	addEdge(list, 0, 4);
	addEdge(list, 1, 2);
	addEdge(list, 1, 3);
	addEdge(list, 1, 4);
	addEdge(list, 3, 1);
	addEdge(list, 4, 2);
	displayGraph(list);

	return 0;
}

// add edge to graph
void AdjacencyList::addEdge(vector<int> list[], int u, int v) {

	list[u].push_back(v); // add u value to list
	list[v].push_back(u); // add v value to list
}

// display the graph
void AdjacencyList::displayGraph(vector<int> list[]) {

	for (int v = 0; v < NUMBER_VERTICES; ++v) {
		cout << "Vertex " << v << ": ";

		for (auto x : list[v]) {
			cout << " " << x;
		}

		cout << endl;
	}
}
