// https://www.tutorialspoint.com/cplusplus-program-to-implement-adjacency-matrix
/********************************************************************
 *  Week 7 lesson:                                                  *
 *  AdjacencyMatrix class -  Class implementing an adjacency matrix *
 ********************************************************************/

#include<iostream>
using namespace std;

class AdjacencyMatrix {

public:
	AdjacencyMatrix();
	~AdjacencyMatrix();
	int example(); // example
	void addEdge(int u, int v);
	void displayGraph(int v);
};
