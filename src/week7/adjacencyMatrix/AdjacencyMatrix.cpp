// https://www.tutorialspoint.com/cplusplus-program-to-implement-adjacency-matrix
/********************************************************************
 *  Week 7 lesson:                                                  *
 *  AdjacencyMatrix class -  Class implementing an adjacency matrix *
 ********************************************************************/

#include "AdjacencyMatrix.h"

int matrix[20][20]; //the adjacency matrix initially 0

// Constructor
AdjacencyMatrix::AdjacencyMatrix() {
}

// Destructor
AdjacencyMatrix::~AdjacencyMatrix() {
}

// example method
int AdjacencyMatrix::example() {

	addEdge(0, 0);
	addEdge(1, 1);
	addEdge(2, 2);
	addEdge(3, 3);
	addEdge(4, 4);

	addEdge(4, 0);
	addEdge(3, 1);
	addEdge(2, 2);
	addEdge(1, 3);
	addEdge(0, 4);

	displayGraph(5);

	return 0;
}

// add edge to graph
void AdjacencyMatrix::addEdge(int u, int v) {

	matrix[u][v] = 1; // add v value to matrix
	matrix[v][u] = 1; // add u value to matrix
}

// display the graph
void AdjacencyMatrix::displayGraph(int v) {

	int i, j;
	for (i = 0; i < v; i++) {
		for (j = 0; j < v; j++) {
			cout << matrix[i][j] << " ";
		}
		cout << endl;
	}
}
