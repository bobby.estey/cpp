/***************************************
 * Week 2 lab:  STL List Example class *
 *                                     *
 * Class definition a STL List example *
 ***************************************/
class STLListExample {

public:
	STLListExample();  // Constructor
	~STLListExample();  // Destructor
	int example(); // example
};
