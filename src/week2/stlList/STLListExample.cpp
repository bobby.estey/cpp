/*************************************
 * Week 2 lab:  STLListExample class *
 *                                   *
 * Class implementing an STL List    *
 *************************************/

#include "STLListExample.h"

#include <iostream>
#include <list>

using namespace std;

/*
 * No argument constructor.
 */
STLListExample::STLListExample() {
	cout << "STLListExample Constructor" << endl;
}

// Destructor. Deallocates the dynamic array list
STLListExample::~STLListExample() {
}

// STLListExample
int STLListExample::example() {
	cout << "STLListExample.example()" << endl;

	list<int> numbers;

	for (int i = 0; i < 10; i++)
		numbers.push_back(rand() % 100);

	while (!numbers.empty()) {
		int x = numbers.front();
		cout << x << " ";

		numbers.pop_front();
	}

	cout << endl;

	return 0;
}
