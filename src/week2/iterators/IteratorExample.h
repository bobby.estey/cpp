/***************************************
 * Week 1 lab:  Iterator Example class *
 *                                     *
 * Class definition a iterator example *
 ***************************************/
class IteratorExample {

public:
	IteratorExample();  // Constructor
	~IteratorExample();  // Destructor
	int example(); // example
};
