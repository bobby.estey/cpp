/**************************************
 * Week 2 lab:  IteratorExample class *
 *                                    *
 * Class implementing an iterator     *
 **************************************/

#include "IteratorExample.h"

#include <iostream>
#include <list>

using namespace std;

/*
 * No argument constructor.
 */
IteratorExample::IteratorExample() {
	cout << "IteratorExample Constructor" << endl;
}

// Destructor. Deallocates the dynamic array list
IteratorExample::~IteratorExample() {
}

// IteratorExample
int IteratorExample::example() {
	cout << "IteratorExample.example()" << endl;
	list<int> numbers;

	for (int i=0; i<10; i++)
		numbers.push_back(rand()%100);

	list<int>::iterator it;

	for (it = numbers.begin(); it!=numbers.end(); ++it)
	{
		cout << *it << " ";
	}

	cout << endl;

	return 0;
}
