/******************************************
 * Week 2 lab:  Recursion class           *
 *                                        *
 * Class implementing a Recursion Example *
 ******************************************/

#include <iostream>
using namespace std;

int factorial(int);

int main() {
    int number;
    int answer;

    cout << "Enter a positive integer: ";
    cin >> number;

    answer = factorial(number);
    cout << "Factorial of " << number << " = " << answer;
    return 0;
}

int factorial(int number) {
    if (number > 1) {
        return number * factorial(number - 1);
    } else {
        return 1;
    }
}