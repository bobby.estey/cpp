/************************************************
 * Week 2 lab:  DoublyLinkedList class          *
 *                                              *
 * Class implementing a simple DoublyLinkedList *
 ************************************************/

/*
 * Linked list node.
 */
struct Node2 {
	int info;		// element stored in this node
	Node2 *previous;    // link to previous node
	Node2 *next;		// link to next node
};

/*
 * Class implementing a linked list.
 */
class DoublyLinkedList {
public:
	DoublyLinkedList();
	~DoublyLinkedList();
	int example(); // example
	bool isEmpty();
	void display();
	void add(int);
	void remove(int);
	void addEnd(int);
	void displayInReverse();

private:
	Node2 *first;	// pointer to header (dummy) node
	Node2 *last;	// pointer to last node
};
