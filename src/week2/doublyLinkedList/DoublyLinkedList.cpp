/************************************************
 * Week 2 lab:  DoublyLinkedList class          *
 *                                              *
 * Class implementing a simple DoublyLinkedList *
 ************************************************/

#include "DoublyLinkedList.h"

#include <iostream>

using namespace std;

/*
 * Initializes the list to empty creating a dummy header node.
 */
DoublyLinkedList::DoublyLinkedList() {
	first = new Node2;
	last = new Node2;
	first->previous = NULL;
	first->next = last;
	last->previous = first;
	last->next = NULL;
}

/*
 * Destructor. Deallocates all the nodes of the linked list,
 * including the header node.
 */
DoublyLinkedList::~DoublyLinkedList() {
	Node2 *temp;

	while (first != NULL) {
		temp = first;
		first = first->next;
		delete temp;
	}
}

int DoublyLinkedList::example() {
	cout << "DoublyLinkedList.example()" << endl;

	DoublyLinkedList myList;
	int x;

	//Add 5 random numbers to list
	for (int i = 0; i < 5; i++) {
		myList.add(rand() % 20);
	}

	cout << "1 - Display the list elements" << endl;
	cout << "2 - Is it empty?" << endl;
	cout << "3 - Add element to the head" << endl;
	cout << "4 - Delete element" << endl;
	cout << "5 - Add element to the end" << endl;
	cout << "6 - Display the list elements in reverse" << endl;
	cout << "0 - Exit" << endl;

	int option;

	//Loop to test the DoublyLinkedList class methods
	do {
		cout << endl << "Enter your choice: ";
		cin >> option;

		switch (option) {
		case 1:
			cout << "List elements: ";
			myList.display();
			break;

		case 2:
			if (myList.isEmpty())
				cout << "List is empty" << endl;
			else
				cout << "List is not empty" << endl;
			break;

		case 3:
			cout << "Enter an element to add at the beginning of the list: ";
			cin >> x;
			myList.add(x);
			break;

		case 4:
			cout << "Enter an element to delete from the list: ";
			cin >> x;
			myList.remove(x);
			break;

		case 5:
			cout << "Enter an element to add at the end of the list: ";
			cin >> x;
			myList.addEnd(x);
			break;

		case 6:
			cout << "List elements in reverse: ";
			myList.displayInReverse();
			break;

		case 0:
			cout << "All done!" << endl;
			break;

		default:
			cout << "Invalid choice!" << endl;
		}

	} while (option != 0);

	return 0;
}

/*
 * Determines whether the list is empty.
 *
 * Returns true if the list is empty, false otherwise.
 */
bool DoublyLinkedList::isEmpty() {
	return first->next == NULL;
}

/*
 * Prints the list elements.
 */
void DoublyLinkedList::display() {
	Node2 *current = first->next;

	while (current != NULL) {
		cout << current->info << " ";
		current = current->next;
	}

	cout << endl;
}

/*
 * Adds the element x to the beginning of the list.
 *
 * x: element to be added to the list.
 */
void DoublyLinkedList::add(int x) {
	Node2 *p = new Node2;

	p->info = x;
	p->next = first->next;

	first->next = p;
}

/*
 * Removes the first occurrence of x from the list. If x is not found,
 * the list remains unchanged.
 *
 * x: element to be removed from the list.
 */
void DoublyLinkedList::remove(int x) {
	Node2 *old = first->next, *p = first;

	//Finding the address of the node before the one to be deleted
	bool found = false;
	while (old != NULL && !found) {
		if (old->info == x)
			found = true;
		else {
			p = old;
			old = p->next;
		}
	}

	//if x is in the list, remove it.
	if (found) {
		p->next = old->next;
		delete old;
	}
}

/*
 * Adds the element x to the end of the list.
 *
 * x: element to be added to the end of list.
 */
void DoublyLinkedList::addEnd(int x) {
	Node2 *newNode = new Node2;
	Node2 *previousNode = last->previous;

	previousNode->next = newNode;

	newNode->info = x;
	newNode->next = last;
	newNode->previous = previousNode;
	newNode->next = last;

	last->previous = newNode;
}

/*
 * Prints the list elements in reverse.
 */
void DoublyLinkedList::displayInReverse() {

	// had to put last->previous because of how the Class was designed, the proper line would be:
	// Node *current = last;
	Node2 *current = last->previous;

	while (current != NULL) {
		cout << current->info << " ";
		current = current->previous;
	}

	cout << endl;
}
