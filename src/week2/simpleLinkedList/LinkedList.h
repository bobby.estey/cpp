/******************************************
 * Week 2 lab:  LinkedList class          *
 *                                        *
 * Class implementing a simple LinkedList *
 ******************************************/

/*
* Linked list node.
*/
struct Node
{
	int info;		//element stored in this node
	Node *next;		//link to next node
};

/*
* Class implementing a linked list.
*/
class LinkedList
{
public:
	LinkedList();
	~LinkedList();
	int example(); // example
	bool isEmpty();
	void display();
	void add(int);
	void remove(int);

private:
	Node *first;	//pointer to header (dummy) node
};
