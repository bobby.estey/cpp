/******************************************
 * Week 2 lab:  LinkedList class          *
 *                                        *
 * Class implementing a simple LinkedList *
 ******************************************/

#include <iostream>
#include "LinkedList.h"

using namespace std;

/*
 * Initializes the list to empty creating a dummy header node.
 */
LinkedList::LinkedList() {
	first = new Node;
	first->next = NULL;
}

/*
 * Destructor. Deallocates all the nodes of the linked list,
 * including the header node.
 */
LinkedList::~LinkedList() {
	Node *temp;

	while (first != NULL) {
		temp = first;
		first = first->next;
		delete temp;
	}
}

int LinkedList::example() {
	cout << "LinkedList.example()" << endl;

	LinkedList myList;
	int x;

	//Add 5 random numbers to list
	for (int i = 0; i < 5; i++) {
		myList.add(rand() % 20);
	}

	cout << "1 - Display the list elements" << endl;
	cout << "2 - Is it empty?" << endl;
	cout << "3 - Add element" << endl;
	cout << "4 - Delete element" << endl;
	cout << "5 - Exit" << endl;

	int option;

	//Loop to test the LinkedList class methods
	do {
		cout << endl << "Enter your choice: ";
		cin >> option;

		switch (option) {
		case 1:
			cout << "List elements: ";
			myList.display();
			break;

		case 2:
			if (myList.isEmpty())
				cout << "List is empty" << endl;
			else
				cout << "List is not empty" << endl;
			break;

		case 3:
			cout << "Enter an element to add at the beginning of the list: ";
			cin >> x;
			myList.add(x);
			break;

		case 4:
			cout << "Enter an element to delete from the list: ";
			cin >> x;
			myList.remove(x);
			break;

		case 5:
			cout << "All done!" << endl;
			break;

		default:
			cout << "Invalid choice!" << endl;
		}

	} while (option != 5);

	return 0;
}

/*
 * Determines whether the list is empty.
 *
 * Returns true if the list is empty, false otherwise.
 */
bool LinkedList::isEmpty() {
	return first->next == NULL;
}

/*
 * Prints the list elements.
 */
void LinkedList::display() {
	Node *current = first->next;

	while (current != NULL) {
		cout << current->info << " ";
		current = current->next;
	}

	cout << endl;
}

/*
 * Adds the element x to the beginning of the list.
 *
 * x: element to be added to the list.
 */
void LinkedList::add(int x) {
	Node *p = new Node;

	p->info = x;
	p->next = first->next;

	first->next = p;
}

/*
 * Removes the first occurrence of x from the list. If x is not found,
 * the list remains unchanged.
 *
 * x: element to be removed from the list.
 */
void LinkedList::remove(int x) {
	Node *old = first->next, *p = first;

	//Finding the address of the node before the one to be deleted
	bool found = false;
	while (old != NULL && !found) {
		if (old->info == x)
			found = true;
		else {
			p = old;
			old = p->next;
		}
	}

	//if x is in the list, remove it.
	if (found) {
		p->next = old->next;
		delete old;
	}
}
