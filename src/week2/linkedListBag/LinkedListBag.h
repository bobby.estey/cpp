/****************************************
 * Week 2 lab: LinkedListBag class      *
 *                                      *
 * Class implementing a Linked List Bag *
 ****************************************/
/*
 * Linked list node.
 */
struct Node3 {
	char info;		// element stored in this node
	Node3 *previous;    // link to previous node
	Node3 *next;		// link to next node
};

/*
 * Class implementing a linked list.
 */
class LinkedListBag {
public:
	LinkedListBag();
	~LinkedListBag();
	int example(); // example
	bool isEmpty();
	void display();
	void add(char);
	void remove(char);
	void addEnd(char);
	void displayInReverse();

private:
	Node3 *first;	// pointer to header (dummy) node
	Node3 *last;	// pointer to last node
};
