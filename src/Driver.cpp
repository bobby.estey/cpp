/********************************
 * Driver class                 *
 *                              *
 * Class implementing a program *
 ********************************/

#include <cstdlib>

#include "week1/arraylist/ArrayList.h"
#include "week1/vector/VectorExample.h"

#include "week2/simpleLinkedList/LinkedList.h"
#include "week2/stlList/STLListExample.h"
#include "week2/iterators/IteratorExample.h"
#include "week2/doublyLinkedList/DoublyLinkedList.h"
#include "week2/linkedListBag/LinkedListBag.h"

#include "week3/stack/Stack.h"
#include "week3/queue/Queue.h"

#include "week4/factorial/Factorial.h"
#include "week4/minimum/Minimum.h"
#include "week4/sorting/Sorting.h"
#include "week4/sortingAlgorithms/SortingAlgorithms.h"
#include "week4/minimum3/Minimum3.h"
#include "week4/harmonic/Harmonic.h"

#include "week5/searchAlgorithms/SearchAlgorithms.h"

#include "week6/binarySearchTree/BinarySearchTree.h"

#include "week7/adjacencyMatrix/AdjacencyMatrix.h"
#include "week7/adjacencyList/AdjacencyList.h"

#include "final/Hashtable.h"
#include "final/SpellChecker.h"

using namespace std;

int main() {

//	ArrayList arrayList; // week 1
//	VectorExample vectorExample; // week 1

//	LinkedList linkedList;  linkedList.example(); // week 2
//	STLListExample stlListExample;  stlListExample.example(); // week 2
//	IteratorExample iteratorExample;  iteratorExample.example(); // week 2
//	DoublyLinkedList doublyLinkedList;  doublyLinkedList.example();  // week 2
//	LinkedListBag linkedListBag;  linkedListBag.example(); // week 2

//	Stack stack; stack.example(); // week 3
//	Queue queue; queue.example(); // week 3

//	Factorial factorial; factorial.example(); // week 4
//	Minimum minimum; minimum.example(); // week 4
//	Sorting sorting; sorting.example(); // week 4
//	SortingAlgorithms sortingAlgorithms; sortingAlgorithms.example(); // week 4
//  Minimum3 minimum3; minimum3.example(); // week 4
//  Harmonic harmonic; harmonic.example(); // week 4

//	SearchAlgorithms searchAlgorithms;	searchAlgorithms.example();  // week 5

//	BinarySearchTree binarySearchTree;	binarySearchTree.example();  // week 6

//	AdjacencyList adjacencyList; adjacencyList.example();  // week 7
//	AdjacencyMatrix adjacencyMatrix; adjacencyMatrix.example();  // week 7

//	Hashtable hashtable(5);	hashtable.example();  // final
//	HashtableString hashtableString(5);	hashtableString.example();  // final
// 	SpellChecker spellChecker; spellChecker.example(); // final

	return 0;
}
