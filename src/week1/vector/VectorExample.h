/*************************************
 * Week 1 lab:  Vector Example class *
 *                                   *
 * Class definition a vector example *
 *************************************/
class VectorExample {

public:
	VectorExample();  // Constructor
	~VectorExample();  // Destructor
	void example(); // example
};
