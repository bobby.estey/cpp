/***************************************
 * Week 1 lab:  Vector Example class   *
 *                                     *
 * Class implementing a vector example *
 **************************************/

#include "VectorExample.h"

#include <iostream>
#include <vector>

using namespace std;

VectorExample::VectorExample() {
	cout << "VectorExample Constructor" << endl;
	example();
}

VectorExample::~VectorExample() {
	cout << "VectorExample Destructor" << endl;
}

void VectorExample::example() {
	cout << "VectorExample.example()" << endl;

	vector<int> v;  // vector of integers
	vector<string> s;  // vector of strings

	// add 10 random values to the vector
	for (int i = 0; i < 10; i++) {

		// get the random number generated and append to the integer vector
		v.push_back(rand() % 100);
	}

	// add the word go and continue to the string vector
	s.push_back("go");
	s.push_back("continue");

	// displays the integer vector
	cout << "integers: ";
	for (int i = 0; i < v.size(); i++) {
		cout << v[i] << " ";
	}

	cout << endl;

	// displays the string vector
	cout << "strings: ";
	for (int i = 0; i < s.size(); i++) {
		cout << s[i] << " ";
	}

	cout << endl;
}
