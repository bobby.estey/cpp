/*******************************************
 * Week 1 lab:  a simple ArrayList class   *
 *                                         *
 * Class definition of an array based list *
 *******************************************/

class ArrayList {

public:
	ArrayList(); // Constructor
	~ArrayList(); // Destructor
	void example(); // example
	bool isEmpty(); // returns if list is empty
	void display(); // displays contents in the list
	void add(int); // add element to the list
	void removeAt(int); // remove element from the list
	void expand(); // expand a list
	int count(int); // return the number of matching integers

private:
	int SIZE;	//size of the array that stores the list items
	int *list;	//array to store the list items
	int *list2; // array 2 (double size) to store the list items
	int length;	//amount of elements in the list
};
