/******************************************
 * Week 1 lab:  a simple ArrayList class  *
 *                                        *
 * Class implementing an array based list *
 ******************************************/

#include "ArrayList.h"

#include <iostream>

using namespace std;

/*
 * No argument constructor. Sets length to 0, initializing the list as an empty
 * list. Default size of array is 20.
 */
ArrayList::ArrayList() {
	cout << "ArrayList Constructor" << endl;

	SIZE = 20;
	list = new int[SIZE];
	length = 0;

	example();
}

// Destructor. Deallocates the dynamic array list
ArrayList::~ArrayList() {
	cout << "ArrayList Destructor" << endl;

	delete[] list;
	list = NULL;
}

// ArrayList example
void ArrayList::example() {
	cout << "ArrayList.example()" << endl;

	if (!isEmpty()) {
		removeAt(0);
	} else {
		add(-1);
	}

	//the list contains -1
	display();

	for (int i = 0; i < 5; i++) {
		add(rand() % 100);		//add some random numbers to the list
	}

	//the list contains 6 numbers; the first one is -1
	display();

	if (!isEmpty()) {
		removeAt(0);
	} else {
		add(-1);
	}

	//the list contains 5 numbers; -1 has been removed
	display();

	add(14);
	display();
	add(16);
	display();
}

// Returns true if the list is empty, false otherwise
bool ArrayList::isEmpty() {
	return length == 0;
}

// Displays the list elements
void ArrayList::display() {
	for (int i = 0; i < length; i++)
		cout << list[i] << " ";
	cout << endl;
}

// Appends the element x to the end of the list. List length is increased by 1
void ArrayList::add(int x) {
	if (length == SIZE) {
		cout << "Insertion Error: list is full" << endl;
		expand();
	} else {
		list[length] = x;
		length++;
	}
}

/*
 * Removes the element at the given location from the list. List length is
 * decreased by 1.
 *
 * position: location of the item to be removed
 */
void ArrayList::removeAt(int position) {
	if (position < 0 || position >= length) {
		cout << "Removal Error: invalid position" << endl;
	} else {
		for (int i = position; i < length - 1; i++)
			list[i] = list[i + 1];
		length--;
	}
}

// Doubles the array list size
void ArrayList::expand() {

	list2 = new int[SIZE * 2];  // creates a list twice the size

	// list2 = list;  // why doesn't this work

	for (int i = 0; i < SIZE; i++) { // copies the data from original list to new list
		list2[i] = list[i];
	}

	list = list2; // list now points to the new list
	SIZE *= 2; // double the size of the list

	// list2 cleared and memory released back
	delete[] list2;
	list2 = NULL;
}

// returns the number of matches in a List
int ArrayList::count(int x) {

	int count = 0;

	if (length > 0) {
		for (int i = 0; i < length; i++) {

			// if element in list matches integer passed in then increment the count
			if (list[i] == x) {
				count++;
			}
		}
	}
	return count;
}
