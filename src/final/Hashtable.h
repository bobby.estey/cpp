// https://www.geeksforgeeks.org/c-program-hashing-chaining/
// CPP program to implement hashing with chaining
#include<bits/stdc++.h>
using namespace std;

class Hashtable {
	int BUCKET;    // No. of buckets

	// Pointer to an array containing buckets
	list<int> *table;
public:
	Hashtable(int V);  // Constructor

	void example();

	// inserts a key into hash table
	void insertItem(int x);

	// deletes a key from hash table
	void deleteItem(int key);

	// hash function to map values to key
	int hashFunction(int x) {
		return (x % BUCKET);
	}

	void displayHash();
};
