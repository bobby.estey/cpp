// https://www.geeksforgeeks.org/c-program-hashing-chaining/
// CPP program checks a hashtable for correctly words

#include "SpellChecker.h"
#include <fstream>
#include <iostream>
#include <algorithm>
#include <ctype.h>

using namespace std;

//lower case alphabets.
char lower_alpha[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
		'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
		'z' };

// Constructor
SpellChecker::SpellChecker() {
}

// Destructor
SpellChecker::~SpellChecker() {
}

void SpellChecker::example() {
	cout << "SpellChecker.example()" << endl;

	HashtableString dictionary(500);
	SpellChecker SpChk;
	ifstream file;
	string word, input, filename;
	int count = 0, choice = 0;

	// opening dictionary file
	file.open("./src/final/Dictionary.txt");
	if (file.fail()) {
		cout << "Error: Failed to open file";
		return;
	}

	// extracting words into the hashtable
	while (file >> word) {
		dictionary.insertItem(word);
		count++;
	}
	file.close();
	cout << "Successfully loaded " << count << " words into the dictionary"
			<< endl;

	do {
		cout << "\tEnter A Selection" << endl;
		cout << "1: Check Spelling\n2: Quit" << endl;
		cin >> choice;
		// User menu
		switch (choice) {
		case 1:

			cout << "Enter a word to spell check..." << endl;
			cin.ignore();
			getline(cin, input);
			SpChk.checkSpelling(&dictionary, input);
			break;
		case 2:
			cout << "Come back again soon :)";
			break;
		default:
			cout << "Error: Invalid selection, try again";
		}
	} while (choice != 2);

}

// Spell checking function
void SpellChecker::checkSpelling(HashtableString *table, string input) {
	int len, flen, correct = 0;
	len = input.length();
	for (int i = 0; i < len; i++)
		input[i] = tolower(input[i]);

	if (table->findItem(input)) {
		correct = 1;
		cout << endl << "Spelling is correct" << endl;
	} else if (!table->findItem(input)) {
		int missing = 0, extra = 0, mixed = 0, incorrect = 0, exchanged = 0;
		cout << endl
				<< "Spelling is wrong. Possible right spellings are given below:- "
				<< endl << endl;
		missing = missingCharacter(table, input);
		extra = extraCharacter(table, input);
		mixed = mixedExtraMissing(table, input);
		incorrect = incorrectArrangement(table, input);
		exchanged = exchangedCharacters(table, input);
		if (missing == 0 && extra == 0 && mixed == 0 && incorrect == 0
				&& exchanged == 0) {
			cout << endl << "No such word exist" << endl;
		}
	}
	cout << endl << "Press any key to continue..." << endl << endl;
//    _getch();
//    system("cls");
}

//function to show the correct spelling if arrangement of word is incorrect.
int SpellChecker::incorrectArrangement(HashtableString *table, string input) {
	int found = 0;
	if (!table->findItem(input)) {
		string Xinput = input, Ninput, permutations, Tinput;
		int len = Xinput.size();
		for (int i = 1; i < Xinput.length(); i++)
			Ninput.push_back(input[i]);
		Xinput.resize(1);
		sort(Ninput.begin(), Ninput.end());
		Tinput = Xinput + Ninput;
		if (table->findItem(Tinput)) {
			found = 1;
			cout << Tinput << endl;

		}
	}
	return found;
}

//function to show correct spelling if exchanged character is present in the given word
int SpellChecker::exchangedCharacters(HashtableString *table, string input) {
	string Xinput;
	int found = 0;
	if (!table->findItem(input)) {
		int len = input.size();
		for (int i = len - 1; i >= 0; i--) {
			Xinput = input;
			Xinput[i] = lower_alpha[0];
			for (int j = 0; j < 26; j++) {
				if (table->findItem(Xinput)) {
					found = 1;
					cout << Xinput << endl;
					break;
				}
				Xinput[i] = lower_alpha[j];
			}
			if (found == 1)
				break;
			else
				continue;
		}
	}
	return found;
}

//function to show correct spelling when there is a missing character in the given word.
int SpellChecker::missingCharacter(HashtableString *table, string input) {
	string Xinput, Tinput, Ninput;
	int found = 0;
	if (!table->findItem(input)) {
		for (int i = 0; i < 26; i++) {
			int len = input.size();
			Xinput = input;
			Xinput.resize(len + 1, 'a');
			Xinput[len] = lower_alpha[i];
			len = Xinput.size();
			if (!table->findItem(Xinput)) {
				for (int j = 1; j <= len; j++)
					Ninput.push_back(Xinput[j]);
				Xinput.resize(1);
				sort(Ninput.begin(), Ninput.end());
				Tinput = Xinput + Ninput;
				if (table->findItem(Tinput)) {
					found = 1;
					cout << Tinput << endl;
					break;
				}
			}
			if (found == 1)
				break;
		}

	}
	return found;
}

//function to show correct spelling of there is an extra character in given word.
int SpellChecker::extraCharacter(HashtableString *table, string input) {
	string Xinput, Ninput, Tinput;
	int found = 0;
	if (!table->findItem(input)) {
		int len = input.size();
		for (int i = 1; i < len; i++) {
			Xinput = input;
			Xinput.erase(Xinput.begin() + i);
			if (table->findItem(Xinput)) {
				found = 1;
				cout << Xinput << endl;
				break;
			}

		}
	}
	return found;
}

//function to show right spelling when given word has wrong extra character and right one is missing.
int SpellChecker::mixedExtraMissing(HashtableString *table, string input) {
	string Xinput;
	int found = 0;
	if (!table->findItem(input)) {
		int len = input.size();
		for (int i = 1; i < len; i++) {
			for (int j = 0; j < 26; j++) {
				Xinput = input;
				Xinput.erase(Xinput.begin() + i);
				Xinput.resize(len, 'a');
				Xinput[len - 1] = lower_alpha[j];
				sort(Xinput.begin() + 1, Xinput.end());
				if (table->findItem(Xinput)) {
					found = 1;
					cout << Xinput << endl;
					break;
				}
			}
			if (found == 1)
				break;
		}
	}
	return found;
}
