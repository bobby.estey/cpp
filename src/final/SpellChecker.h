// https://www.geeksforgeeks.org/c-program-hashing-chaining/
// CPP program checks a hashtable for correctly words
#pragma once
# include "HashtableString.h"

using namespace std;

class SpellChecker {

public:

	SpellChecker();  // Constructor
	~SpellChecker(); // Destructor
	void example();
	void checkSpelling(HashtableString *table, string input);

private:
	// Private functions called by checkSpelling function
	int exchangedCharacters(HashtableString *table, string input);
	int incorrectArrangement(HashtableString *table, string input);
	int extraCharacter(HashtableString *table, string input);
	int missingCharacter(HashtableString *table, string input);
	int mixedExtraMissing(HashtableString *table, string input);
};
