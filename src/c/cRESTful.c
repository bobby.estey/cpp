/*
 ============================================================================
 Name        : cRESTful.c
 Author      :
 Version     :
 Copyright   : Your copyright notice
 Description : Basic REST Call, Ansi-style

 Link        : cc cRESTful.c -lcurl
 Execute     : ./a.out
 Output      : JSON

 Reference   : https://gist.github.com/p120ph37/71fb340f7a0d38fa5443
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>

int main(int argc, char *argv[]) {

	CURL *curl = curl_easy_init();  // initialize curl object
	char url[] = "http://localhost:6464/users";  // endpoint
    struct curl_slist* headers = NULL;  // header object
	CURLcode response;  // JSON response

	if (curl) {

		curl_easy_setopt(curl, CURLOPT_URL, "http://localhost:6464/users");
	    curl_easy_setopt(curl, CURLOPT_HTTPAUTH, (long)CURLAUTH_BASIC);
	    curl_easy_setopt(curl, CURLOPT_USERNAME, "cv64");
	    curl_easy_setopt(curl, CURLOPT_PASSWORD, "password");

		response = curl_easy_perform(curl);
		curl_easy_cleanup(curl);
	}

	return 0;
}
