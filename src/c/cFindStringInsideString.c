#include <stdio.h>
#include <string.h>

int main() {
	char string[] = "robert rob bob bobby";
	char search[] = "ob";
	char *ptr = strstr(string, search);

	if (ptr != NULL) {
		printf("'%s' contains '%s'\n", string, search);
	} else {
		printf("'%s' doesn't contain '%s'\n", string, search);
	}

	return 0;
}
