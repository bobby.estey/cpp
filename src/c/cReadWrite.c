/*
 ============================================================================
 Name        : cReadWrite.c
 Description : Basic C Read / Write Program

 Link        : cc cReadWrite.c
 Execute     : ./a.out
 Output      :

 Reference   :
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>

int main(int argc, char *argv[]) {

    FILE* properties = fopen("readWrite.properties", "r");
    char record[11] = { '\0' };

    while(fgets(record, 11, properties) != NULL) {
    	printf("\nrecord:~%s~\n", record);


    }

	return 0;
}
