#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	char src[40];
	char dest1[100];
	char *dest2;

	strcpy(src, "This is tutorialspoint.com");
	int soLength = strlen(src);
	strncpy(dest1, src, 10);

	printf("dest1:~%s~\n", dest1);

	dest2 = (char *)malloc(soLength);
	strcpy(dest2, src);
	printf("dest2:~%s~", dest2);

	return (0);
}
