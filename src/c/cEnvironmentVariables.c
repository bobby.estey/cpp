// https://www.geeksforgeeks.org/c-program-print-environment-variables/
#include <stdio.h>
#include <stdlib.h>

// Most of the C compilers support a third parameter to main which
// store all environment variables
int main(int argc, char *argv[], char *envp[]) {
	int i = 0;
	char *username = getenv("USERNAME");

	// print all environment variables
	for (i = 0; envp[i] != NULL; i++) {
		printf("%s\n", envp[i]);
	}
	printf("\n");

	// print specific environment variable
	if (username) {
		printf("Environment Variable Found: ~%s~\n", username);
	} else {
		printf("Environment Variable Not Found: ~%s~\n", username);
	}

	return 0;
}
