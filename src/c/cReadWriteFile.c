// https://codeforwin.org/c-programming/c-program-read-and-display-file-contents

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RECORD_SIZE 100

int main() {
	FILE *filePointer;
	char record[RECORD_SIZE];
	int totalRead = 0;
	char filename[] = "readWrite.properties\0";
	int debug = 0;
	char my_path[] = "";

	filePointer = fopen(filename, "r");

	if (filePointer == NULL) {
		printf("Unable to open file: %s\n", filename);
		exit(EXIT_FAILURE);
	}

	while (fgets(record, RECORD_SIZE, filePointer) != NULL) {

		totalRead = strlen(record);

		// Trim new line character from last if exists
		record[totalRead - 1] =
				record[totalRead - 1] == '\n' ? '\0' : record[totalRead - 1];

		printf("record: ~%s~\n\n", record);

		if (strncmp(record, "debug", 5) == 0) {
			printf("debug - a: ~%s~\n\n", record);

			if (strcmp(record, "debug=0") == 0) {
				printf("debug - b: ~%s~\n\n", record);
				debug = 0;
			} else {
				printf("debug - c: ~%s~\n\n", record);
				debug = 1;
			}
		}

		if (strncmp(record, "my_path", 7) == 0) {
			printf("my_path - a: ~%s~\n\n", record);
			strcpy(my_path, record);
		}
	}

	fclose(filePointer);

	printf("debug -  f: ~%d~\n\n", debug);
	printf("my_path -f: ~%s~\n\n", my_path);

	return 0;
}
