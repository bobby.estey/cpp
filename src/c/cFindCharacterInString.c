#include <stdio.h>
#include <string.h>

int main() {

	//                         1         2         3         4         5
	//               012345678901234567890123456789012345678901234567890
	char string[] = "finding first and last occurrence of a character";
	char alphabet[] = "abcdefghijklmnopqrstuvwxyz";
	int length = strlen(string);

	printf("String to search: %s\n", string);
	printf("Length of stringing: %d\n", length);
	printf("Char: first last\n");

	for (int i = 0; i < strlen(alphabet); i++) {
		char *position_ptr = strchr(string, alphabet[i]);
		char *r_position_ptr = strchr(string, alphabet[i]);

		int position = (position_ptr == NULL ? -1 : position_ptr - string);
		int r_position = (r_position_ptr == NULL ? -1 : r_position_ptr - string);

		printf("%4c: %4d %4d\n", alphabet[i], position, r_position);
	}

	return 0;
}
