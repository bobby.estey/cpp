/*******************************************
 * Week 4 lab:  Sorting class              *
 *                                         *
 * Class implementing an array based list. *
 * Bubblesort and quicksort algorithms     *
 * are implemented also.                   *
 *******************************************/
#include <iostream>
#include "Sorting.h"
#include <time.h>

using namespace std;

/*
 * Default constructor. Sets length to 0, initializing the list as an empty
 * list. Default size of array is 20.
 */
Sorting::Sorting() {
	SIZE = 20;
	list = new int[SIZE];
	length = 0;
}

/*
 * Destructor. Deallocates the dynamic array list.
 */
Sorting::~Sorting() {
	delete[] list;
	list = NULL;
}

int Sorting::example() {
	srand((unsigned)time(0));

	//creating a list of integers
	Sorting numbersCopy1, numbersCopy2;


    //filling the list with random integers
    for (int i = 0; i<10; i++)
	{
		int number = rand()%100;
		numbersCopy1.add(number);
		numbersCopy2.add(number);
	}

    //printing the list
    cout << "Original list of numbers:" << endl <<"\t";
    numbersCopy1.display();

    //testing bubblesort
    cout << endl << "Bubble-sorted list of numbers:" << endl <<"\t";
    numbersCopy1.bubbleSort();
    numbersCopy1.display();

    //testing quicksort
    cout << endl << "Quick-sorted list of numbers:" << endl <<"\t";
	numbersCopy2.quicksort();
    numbersCopy2.display();

	return 0;
}

/*
 * Determines whether the list is empty.
 *
 * Returns true if the list is empty, false otherwise.
 */
bool Sorting::isEmpty() {
	return length == 0;
}

/*
 * Prints the list elements.
 */
void Sorting::display() {
	for (int i = 0; i < length; i++)
		cout << list[i] << " ";
	cout << endl;
}

/*
 * Adds the element x to the end of the list. List length is increased by 1.
 *
 * x: element to be added to the list
 */
void Sorting::add(int x) {
	if (length == SIZE) {
		cout << "Insertion Error: list is full" << endl;
	} else {
		list[length] = x;
		length++;
	}
}

/*
 * Removes the element at the given location from the list. List length is
 * decreased by 1.
 *
 * pos: location of the item to be removed
 */
void Sorting::removeAt(int pos) {
	if (pos < 0 || pos >= length) {
		cout << "Removal Error: invalid position" << endl;
	} else {
		for (int i = pos; i < length - 1; i++)
			list[i] = list[i + 1];
		length--;
	}
}

/*
 * Bubble-sorts this Sorting
 */
void Sorting::bubbleSort() {
	for (int i = 0; i < length - 1; i++)
		for (int j = 0; j < length - i - 1; j++)
			if (list[j] > list[j + 1]) {
				//swap list[j] and list[j+1]
				int temp = list[j];
				list[j] = list[j + 1];
				list[j + 1] = temp;
			}
}

/*
 * Quick-sorts this Sorting.
 */
void Sorting::quicksort() {
	quicksort(0, length - 1);
}

/*
 * Recursive quicksort algorithm.
 *
 * begin: initial index of sublist to be quick-sorted.
 * end: last index of sublist to be quick-sorted.
 */
void Sorting::quicksort(int begin, int end) {
	int temp;
	int pivot = findPivotLocation(begin, end);

	// swap list[pivot] and list[end]
	temp = list[pivot];
	list[pivot] = list[end];
	list[end] = temp;

	pivot = end;

	int i = begin, j = end - 1;

	bool iterationCompleted = false;
	while (!iterationCompleted) {
		while (list[i] < list[pivot])
			i++;
		while ((j >= 0) && (list[pivot] < list[j]))
			j--;

		if (i < j) {
			//swap list[i] and list[j]
			temp = list[i];
			list[i] = list[j];
			list[j] = temp;

			i++;
			j--;
		} else
			iterationCompleted = true;
	}

	//swap list[i] and list[pivot]
	temp = list[i];
	list[i] = list[pivot];
	list[pivot] = temp;

	if (begin < i - 1)
		quicksort(begin, i - 1);
	if (i + 1 < end)
		quicksort(i + 1, end);
}

/*
 * Computes the pivot location.
 */
int Sorting::findPivotLocation(int b, int e) {
	return (b + e) / 2;
}
