/********************************
 * Week 4 lab:  Minimum class   *
 *                              *
 * Class implementing a Minimum *
 ********************************/
class Minimum3 {

public:
	Minimum3();	// Constructor
	~Minimum3();  // Destructor
	int example(); // example
	int minimum3(int[], int);
};
