/**********************************
 * Week 4 lab:  Factorial class   *
 *                                *
 * Class implementing a Factorial *
 **********************************/
class Factorial {

public:
	Factorial();	// Constructor
	~Factorial();  // Destructor
	int example(); // example
	int factorial(int);
};
