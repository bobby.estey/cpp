/**********************************
 * Week 4 lab:  Factorial class   *
 *                                *
 * Class implementing a Factorial *
 **********************************/
#include "Factorial.h"

#include <iostream>

using namespace std;

// No argument constructor initializes the Factorial
Factorial::Factorial() {
}

// Destructor. Deallocates the stack
Factorial::~Factorial() {
}

// Factorial Driver example
int Factorial::example() {
	int n;

	cout << "Enter a number: ";
	cin >> n;

	if (n > 0)
		cout << n << "!= " << factorial(n) << endl;
	else
		cout << "Input Error!" << endl;

	return 0;
}

/*
 * Returns the factorial of n
 */
int Factorial::factorial(int n) {
	if (n == 1) {
		return 1;
	} else {
		return n * factorial(n - 1);
	}
}
