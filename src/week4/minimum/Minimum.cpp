/********************************
 * Week 4 lab:  Minimum class   *
 *                              *
 * Class implementing a Minimum *
 ********************************/
#include "Minimum.h"

#include <iostream>

using namespace std;

// No argument constructor initializes the Minimum
Minimum::Minimum() {
}

// Destructor. Deallocates the stack
Minimum::~Minimum() {
}

// Queue Driver example
int Minimum::example() {
	int a[10];

	for (int i = 0; i < 10; i++) {
		a[i] = rand() % 100;
		cout << a[i] << " ";
	}

	cout << endl << "Min =  " << minimum(a, 10) << endl;

	return 0;
}

/*
 * Returns the factorial of n
 */
int Minimum::minimum(int a[], int n) {
	int min = a[0];

	for (int i = 1; i < n; i++)
		if (min > a[i])
			min = a[i];

	return min;
}
