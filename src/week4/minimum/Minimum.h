/********************************
 * Week 4 lab:  Minimum class   *
 *                              *
 * Class implementing a Minimum *
 ********************************/
class Minimum {

public:
	Minimum();	// Constructor
	~Minimum();  // Destructor
	int example(); // example
	int minimum(int[], int);
};
