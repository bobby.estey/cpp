/*********************************
 * Week 4 lab:  Harmonic class   *
 *                               *
 * Class implementing a Harmonic *
 *********************************/
class Harmonic {

public:
	Harmonic();	// Constructor
	~Harmonic();  // Destructor
	int example(); // example
	double harmonic(double, double);
};
