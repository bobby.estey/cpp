/*********************************
 * Week 4 lab:  Harmonic class   *
 *                               *
 * Class implementing a Harmonic *
 *********************************/
#include <iostream>
#include "Harmonic.h"

using namespace std;

// No argument constructor initializes the Harmonic
Harmonic::Harmonic() {
}

// Destructor. Deallocates the stack
Harmonic::~Harmonic() {
}

// Harmonic Driver example
int Harmonic::example() {
	double n;

	cout << "Enter a number: ";
	cin >> n;

	if (n > 0)
		cout << n << "harmonic = " << harmonic(n, 0) << endl;
	else
		cout << "Input Error!" << endl;

	return 0;
}

/*
 * Returns the harmonic of n
 * https://www.dreamincode.net/forums/topic/329767-harmonic-series-recursion-problem/
 */
double Harmonic::harmonic(double n, double result) {
	if (n <= 0) {
		return result;
	} else {
		result += (1 / n);
		return harmonic((n - 1), result);
	}
}
