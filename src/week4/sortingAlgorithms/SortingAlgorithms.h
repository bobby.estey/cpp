/****************************************************
 *  Week 4 lesson:                                  *
 *  SortingAlgorithms class with sorting algorithms *
 ****************************************************/

/*
 * Class implementing an array based list. Bubblesort and quicksort algorithms
 * are implemented also.
 */
class SortingAlgorithms {

public:
	SortingAlgorithms();
	~SortingAlgorithms();
	int example(); // example
	double timer();
	bool isEmpty();
	void display();
	void add(int);
	void removeAt(int);
	void bubbleSort();
	void quicksort(bool);

private:
	void quicksort(int, int, bool);
	int findPivotLocation(int, int, bool);
	int SIZE;		//size of the array that stores the list items
	int *list;		//array to store the list items
	int length;	//amount of elements in the list
	int operations; // the number of operations
};
