/****************************************************
 *  Week 4 lesson:                                   *
 *   SortingAlgorithms class with sorting algorithms *
 *****************************************************/

#include <iostream>

#include <chrono>
#include "SortingAlgorithms.h"

using namespace std;

/*
 * Default constructor. Sets length to 0, initializing the list as an empty
 * list.
 */
SortingAlgorithms::SortingAlgorithms() {
	SIZE = 10;
	list = new int[SIZE];
	length = 0;
	operations = 0;
}

/*
 * Destructor. Deallocates the dynamic array list.
 */
SortingAlgorithms::~SortingAlgorithms() {
	delete[] list;
	list = NULL;
}

// SortingAlgorithms Driver example
int SortingAlgorithms::example() {

	srand((unsigned) time(0));

	//creating a list of integers
	SortingAlgorithms numbersCopy1;

	//filling the list with random integers

	/*
	 for (int i = 0; i < SIZE; i++) {
	 int number = rand() % SIZE;
	 numbersCopy1.add(number);
	 }
	 */

	numbersCopy1.add(1);
	numbersCopy1.add(2);
	numbersCopy1.add(3);
	numbersCopy1.add(4);
	numbersCopy1.add(5);
	numbersCopy1.add(6);
	numbersCopy1.add(7);
	numbersCopy1.add(8);
	numbersCopy1.add(9);
	numbersCopy1.add(10);

	/*
	 numbersCopy1.add(17);
	 numbersCopy1.add(17);
	 numbersCopy1.add(10);
	 numbersCopy1.add(14);
	 numbersCopy1.add(18);
	 numbersCopy1.add(11);
	 numbersCopy1.add(3);
	 numbersCopy1.add(16);
	 numbersCopy1.add(2);
	 numbersCopy1.add(13);
	 numbersCopy1.add(16);
	 */

	//printing the list
	cout << "Original list of numbers:" << endl << "\t";
	numbersCopy1.display();
	numbersCopy1.timer();
	numbersCopy1.display();

	return 0;
}

double SortingAlgorithms::timer() {
	auto start = chrono::steady_clock::now();

	//add code to time here
	//bubbleSort();
	quicksort(false);
	//quicksort(true);

	auto end = chrono::steady_clock::now();
	chrono::duration<double> timeElapsed = chrono::duration_cast<
			chrono::duration<double>>(end - start);
	cout << "Code duration: " << timeElapsed.count() << " seconds" << endl;

	return timeElapsed.count();
}

/*
 * Determines whether the list is empty.
 *
 * Returns true if the list is empty, false otherwise.
 */
bool SortingAlgorithms::isEmpty() {
	return length == 0;
}

/*
 * Prints the list elements.
 */
void SortingAlgorithms::display() {
	for (int i = 0; i < length; i++)
		cout << list[i] << " ";
	cout << endl;
}

/*
 * Adds the element x to the end of the list. List length is increased by 1.
 *
 * x: element to be added to the list
 */
void SortingAlgorithms::add(int x) {
	if (length == SIZE) {
		cout << "Insertion Error: list is full" << endl;
	} else {
		list[length] = x;
		length++;
	}
}

/*
 * Removes the element at the given location from the list. List length is
 * decreased by 1.
 *
 * pos: location of the item to be removed
 */
void SortingAlgorithms::removeAt(int pos) {
	if (pos < 0 || pos >= length) {
		cout << "Removal Error: invalid position" << endl;
	} else {
		for (int i = pos; i < length - 1; i++)
			list[i] = list[i + 1];
		length--;
	}
}

/*
 * Bubble-sorts this SortingAlgorithms
 */
void SortingAlgorithms::bubbleSort() {
	for (int i = 0; i < length - 1; i++) {
		for (int j = 0; j < length - i - 1; j++) {

			operations++;

			if (list[j] > list[j + 1]) {
				//swap list[j] and list[j+1]
				int temp = list[j];
				list[j] = list[j + 1];
				list[j + 1] = temp;

				operations += 4;
			}
		}
	}

	cout << endl << "operations: " << operations << endl;
}

/*
 * Quick-sorts this SortingAlgorithms.
 */
void SortingAlgorithms::quicksort(bool random) {
	quicksort(0, length - 1, random);
}

/*
 * Recursive quicksort algorithm.
 *
 * begin: initial index of sublist to be quick-sorted.
 * end: last index of sublist to be quick-sorted.
 */
void SortingAlgorithms::quicksort(int begin, int end, bool random) {
	int temp;
	int pivot = findPivotLocation(begin, end, random);

	// swap list[pivot] and list[end]
	temp = list[pivot];
	list[pivot] = list[end];
	list[end] = temp;

	pivot = end;

	int i = begin, j = end - 1;

	bool iterationCompleted = false;

	operations += 6;

	while (!iterationCompleted) {
		while (list[i] < list[pivot]) {
			i++;
			operations++;
		}
		while ((j >= 0) && (list[pivot] < list[j])) {
			j--;
			operations++;
		}

		if (i < j) {
			//swap list[i] and list[j]
			temp = list[i];
			list[i] = list[j];
			list[j] = temp;

			i++;
			j--;

			operations += 6;

		} else {
			iterationCompleted = true;

			operations++;
		}

		cout << endl << "operations: " << operations << endl;
	}

	//swap list[i] and list[pivot]
	temp = list[i];
	list[i] = list[pivot];
	list[pivot] = temp;

	operations += 3;

	if (begin < i - 1) {
		quicksort(begin, i - 1, random);
		operations++;
	}

	if (i + 1 < end) {
		quicksort(i + 1, end, random);
		operations++;
	}
}

/*
 * Computes the pivot location.
 */
int SortingAlgorithms::findPivotLocation(int b, int e, bool random) {

	operations++;

	if (random) {
		return rand() % e;
	} else {
		return (b + e) / 2;
	}
}
