#include "MyException.h"

// Constructor passed in a message
MyException::MyException(const string& err) {
    this->errorMessage = err;
}

// function returning the message
string MyException::getMessage() const {
    return this->errorMessage;
}