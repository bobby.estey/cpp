#pragma once
#include <string>
using namespace std;

class MyException {

private:
	string errorMessage;

public:
	MyException(const string &err);
	string getMessage() const;
};
