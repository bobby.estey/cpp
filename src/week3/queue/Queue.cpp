/******************************
 * Week 3 lab:  Queue class   *
 *                            *
 * Class implementing a Queue *
 ******************************/

#include "Queue.h"
#include "../exceptions/MyException.h"

#include <iostream>

using namespace std;

// No argument constructor initializes the stack
Queue::Queue() {
	front = 0;
	back = SIZE - 1;
	count = 0;
}

// Destructor. Deallocates the stack
Queue::~Queue() {

}

// Queue Driver example
int Queue::example() {

	// try the following code
	try {
		Queue q;

		cout << "Insertion of 10 characters in q" << endl;
		for (int i = 0; i < q.getSize(); i++) {
			char x = 32 + rand() % 95;
			cout << x << endl;
			q.enqueue(x);
		}

		cout << endl << "Displaying and deleting elements from q" << endl;
		while (!q.isEmpty()) {
			cout << "Item at the front: " << q.getFront() << endl;
			q.dequeue();
		}

		// default exception - if there are any exceptions, print out the exception
	} catch (...) {
		MyException queueException("Queue Exception Error");
		cout << queueException.getMessage();
		// throw MyException("Queue Error");
	}

	return 0;
}

/*
 * Determines whether the queue is empty.
 *
 * Returns true if the queue is empty, false otherwise.
 */
bool Queue::isEmpty() {
	return count == 0;
}

/*
 * Adds an element to the back of the queue.
 *
 * x: element to be added to the queue.
 */
void Queue::enqueue(char x) {
	back = (back + 1) % SIZE;
	list[back] = x;
	count++;
}

/*
 * Removes the element in the front of the queue.
 */
void Queue::dequeue() {
	front = (front + 1) % SIZE;
	count--;
}

/*
 * Returns the element in the front of the queue. Does not remove it.
 */
char Queue::getFront() {
	return list[front];
}

/*
 * Returns the size of the queue.
 */
int Queue::getSize() {
	return SIZE;
}
