/******************************
 * Week 3 lab:  Stack class   *
 *                            *
 * Class implementing a Stack *
 ******************************/

#include "Stack.h"
#include "../exceptions/MyException.h"

#include <iostream>

using namespace std;

// No argument constructor initializes the stack
Stack::Stack() {
	top = 0;
}

// Destructor. Deallocates the stack
Stack::~Stack() {
}

// Stack example driver
int Stack::example() {

	// try the following code
	try {

		Stack s;

		cout << "Insertion of 10 characters in s" << endl;
		for (int i = 0; i < s.getSize(); i++) {
			char x = 32 + rand() % 95;
			cout << x << endl;
			s.push(x);
		}

		cout << endl << "Displaying and deleting elements from s" << endl;
		while (!s.isEmpty()) {
			cout << "Item at the top: " << s.peek() << endl;

			s.pop();
		}

		// default exception - if there are any exceptions, print out the exception
	} catch (...) {
		MyException stackException("Stack Exception Error");
		cout << stackException.getMessage();
		// throw MyException("Stack Error");
	}

	return 0;
}
/*
 * Determines whether the stack is empty.
 *
 * Returns true if the stack is empty, false otherwise.
 */
bool Stack::isEmpty() {
	return top == 0;
}

/*
 * Adds an element to the top of the stack.
 *
 * x: element to be added to the stack.
 */
void Stack::push(char x) {
	list[top] = x;
	top++;
}

/*
 * Removes the element at the top of the stack.
 */
void Stack::pop() {
	top--;
}

/*
 * Returns the element at the top of the stack. Does not remove it.
 */
char Stack::peek() {
	return list[top - 1];
}

/*
 * Returns the size of the stack.
 */
int Stack::getSize() {
	return SIZE;
}
