// https://www.geeksforgeeks.org/stack-set-4-evaluation-postfix-expression/
/**************************
 * Week 3 lab:  RPN class *
 *                        *
 * Class implementing RPN *
 **************************/

#include "RPN.h"

#include <iostream>

using namespace std;

// No argument constructor initializes the stack
RPN::RPN() {
	top = 0;
}

// Destructor. Deallocates the stack
RPN::~RPN() {
}

// RPN example driver
int RPN::example() {

	char exp[] = "231*+9-";
///	cout << "postfix evaluation: " << evaluatePostfix(exp);
	return 0;
}

// The main function that returns value of a given postfix expression
int evaluatePostfix(char *exp) {
	// Create a stack of capacity equal to expression size
///   struct Stack* stack = createStack(strlen(exp));
	int i;

	// See if stack was created successfully
	///   if (!stack) return -1;

	// Scan all characters one by one
	for (i = 0; exp[i]; ++i) {
		// If the scanned character is an operand (number here),
		// push it to the stack.
		if (isdigit(exp[i])) {
			///          push(stack, exp[i] - '0');
		}

		// If the scanned character is an operator, pop two
		// elements from stack apply the operator
		else {
			///           int val1 = pop(stack);
			///           int val2 = pop(stack);
			switch (exp[i]) {
			///           case '+': push(stack, val2 + val1); break;
			///           case '-': push(stack, val2 - val1); break;
			///           case '*': push(stack, val2 * val1); break;
			///           case '/': push(stack, val2/val1); break;
			}
		}
	}
///    return pop(stack);
	return 0;
}

/*
 * Determines whether the stack is empty.
 *
 * Returns true if the stack is empty, false otherwise.
 */
bool RPN::isEmpty() {
	return top == 0;
}

/*
 * Adds an element to the top of the stack.
 *
 * x: element to be added to the stack.
 */
void RPN::push(char x) {
	list[top] = x;
	top++;
}

/*
 * Removes the element at the top of the stack.
 */
void RPN::pop() {
	top--;
}

/*
 * Returns the element at the top of the stack. Does not remove it.
 */
char RPN::peek() {
	return list[top - 1];
}

/*
 * Returns the size of the stack.
 */
int RPN::getSize() {
	return SIZE;
}
