/**************************
 * Week 3 lab:  RPN class *
 *                        *
 * Class implementing RPN *
 **************************/
class RPN {

public:
	RPN(); // Constructor
	~RPN();  // Destructor
	int example(); // example
	int evaluatePostfix(char* exp);
	bool isEmpty();
	void push(char);
	void pop();
	char peek();
	int getSize();

private:
	static const int SIZE = 10;	//size of the queue array
	char list[SIZE];			//array to store the stack items
	int top;					//amount of elements in the array
};
