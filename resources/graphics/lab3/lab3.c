/*  Robert Estey 											  */
/*  Email:  estey@mosvax.sps.mot.com  Phone:  (602) 655-2391  Fax:  (602) 655-3569						  */
/*  UMASS ECE660 / NTU ST740A	Program 3											  */
/*																  */
/*  Developed and Ran on VAX/VMS V5.5-2 & DEC/Motif V1.1									  */
/*																  */
/*  cc/standard=portable program			- C Compiler V3.2 on VAX/VMS V5.5-2					  */
/*  link program, sys$library:dxm_link.opt/opt		- Link in the X11 - DEC/Motif V1.1 libraries				  */
/*  set  display/create/transport=decnet/node=cv64 	- Set the display							  */
/*  define sys$input sys$command			- Set up program to take input from screen not Xterminal (scanf)	  */
/*  run  program					- Execute the program							  */
/*																  */
/*  sys$library:dxm_link.opt (contents in file are below)									  */
/*																  */
/*  ! Linker options to link with DEC/Motif 1.1 shareable images								  */
/*  psect_attr=XMQMOTIF,noshr,lcl												  */
/*  SYS$SHARE:DECW$DXMLIBSHR/SHAREABLE												  */
/*  SYS$SHARE:DECW$XMLIBSHR/SHAREABLE												  */
/*  SYS$SHARE:DECW$XLIBSHR/SHAREABLE												  */
/*  SYS$SHARE:VAXCRTL/SHAREABLE													  */
/*																  */
/*  This program also has a postscript device driver which was provided by Professor F. S. Hill and I made slight modifications.  */
/*																  */
/**********************************************************************************************************************************/
/*																  */
/*  Program Description:  3D_README.TXT												  */
/*																  */
/**************************************************** INITIALIZE & START PROGRAM **************************************************/

#include 	<stdio.h>									  /* include standard i/o library */
#include	<stdlib.h>									  /* include standard lib library */
#include	<string.h>										/* include string library */
#include        <X11/Xlib.h> 		                                                                   /* include x11 library */
#include        <math.h>                                                                                  /* include math library */
#include	<processes.h>								    /* include processes library (system) */

#define		MAXNUM			500							     		/* maximum number */
#define		MAXX			1000								/* maximum number of rows */
#define		MAXY			750							     /* maximum number of columns */

/******************************************************** POSTSCRIPT DEFAULTS *****************************************************/
									
#define         NumBeforeStroke         100                                                                     /* i have no idea */
#define         margin                  54                                             /* number of points for the outside margin */
#define         pageheight              792                                                                 /* 11 inch x 72 point */
#define         pagewidth               612                                                               /* 8.5 inch x 72 points */

/************************************************************* TYPEDEFS ***********************************************************/

typedef		struct {int l, t, r, b;} tInt4;							     /* structure integer 4 tuple */
typedef		struct {double x, y;} tReal2;						              /* structure double 2 tuple */
typedef		struct {double x, y, z;} tReal3;						      /* structure double 3 tuple */
typedef         struct {double l, t, r, b;} tReal4;                             		      /* structure double 4 tuple */
typedef         struct {double A, B, dx, dy;} tMap;                  		      /* structure mapping from world to viewport */
typedef         struct {int numrows, numcols; double Rs;} tDisplay;                      		     /* structure display */

typedef		struct {tReal3 eye, lookat, ref, u, v, n; 						      /* structure camera */
			double E; tReal4 W; double viewAngle; double cameraAspect;} tCamera;

/************************************************************ PROTOTYPES **********************************************************/

void 		x11graphics();							      		                  /* X11 graphics */
void 		get_GC(GC *gc1, GC *gc2);  							          /* get graphics context */

void            setInt4(int left, int top, int right, int bottom, tInt4 *V);   	                		 /* int rectangle */
tInt4           matchW2V(tReal4 *W);			    	                             /* match world size to viewport size */
tMap            W2V(tReal4 *W, tInt4 *V);   	                                                             /* world to viewport */
void            moveto(double x, double y);                                                         /* move to screen coordinates */
void            lineto(double x, double y);                                                  /* draw a line on screen coordinates */

void		optionsMenu();										/* draws the options menu */
void		menuScreen();							 /* menu or screen area program, mainline program */
void 		locateMB(tReal2 *point, int *bp);				     /* locateMBs screen point and button pressed */
void		loadArray();								/* load the array with vertices and faces */
void 		loadFile();										  /* load the output file */
void            initCamera();										     /* initialize camera */
void		eyeAdjust();											/* eye adjustment */
void		windowAdjust();										     /* window adjustment */
void	        lookatAdjust();									       /* lookat point adjustment */
void		drawObject(int fill, int bfr);								       /* draw the object */
void		computeUVN();									       /* compute the UVN vectors */
void 		initArrays();										     /* initialize arrays */
void 		drawPolygon();											/* draw a polygon */
void 		tiltCamera();							   /* tilt camera on u & v axis, n axis no change */
void 		stereoView();								     /* create a stereo view of an object */
void 		fillFace(int i, double x, double y);						     /* fills a face of an object */
int 		backfaceRemoval(int point);							  /* remove backface from drawing */
double 		dot3D(tReal3 *vector1, tReal3 *vector2);				     /* dot product on 3 dimensional rays */
double 		hypotenuse(tReal2 *point1, tReal2 *point2);						/* compute the hypotenuse */
tReal2 		perspective(tReal3 *P);					     /* implement the perspective projection on viewplane */
tReal3 		cross(tReal3 *vector1, tReal3 *vector2);					       /* 3D vector cross product */

void            Init_Post(int vportWid, int vportHt);                                                    /* initialize postscript */
void            Write_Post(int x, int y, int which);                                                          /* write postscript */

/********************************************************* GLOBAL VARIABLES  ******************************************************/

Display         *display;                                       /* X Terminal address, pointer to display structure on X terminal */
int             screen;      		                      /* X Terminal screen, root window to screen structure on X terminal */
Window          win;                                                                                             /* Window widget */
GC              gc1, gc2; 									        /* ID of graphics context */
XEvent		event;									       /* Structure for event information */

FILE            *input_file;                                                                                /* declare input file */
FILE		*output_file;										   /* declare output file */
FILE            *postscript_file;                                                                      /* declare postscript file */
char            input_filename[20];									    /* name of input file */
char            output_filename[20];									   /* name of output file */
char            postscript_filename[20];	 	                                               /* name of postscript file */

int             vWid                    = 1006;                                                /* choose postscript viewport size */
int             vHt                     = 734;                                                 /* choose postscript viewport size */
int		fill			= 0;							    /* fill polygon toggle switch */
int		bfr			= 0;							/* backface removal toggle switch */
int		stereoViewPS		= 0;								     /* stereoView switch */

int		infinite		= 1;							      /* set up for infinite loop */
int		status 			= 0;									 /* return status */

int		menu_left		= 0;						       /* left coordinate of options menu */
int		menu_top		= 0;						        /* top coordinate of options menu */
int		menu_right		= 150;						      /* right coordinate of options menu */
int		menu_bottom		= 700;						     /* bottom coordinate of options menu */
int             menu_contents           = 20;                                                               /* menu item contents */
int             menu_seperator          = 30;                                                              /* menu item seperator */
int             menu_line               = 50;                                                      	    /* menu line - x axis */

double		radiansperdegree	= 0.0174532925199;						    /* radians per degree */
double		epsilon			= 0.0872661462595;							     /* 5 degrees */
double		epsilon10		= 1.0471975511966;							    /* 60 degrees */

tInt4           V;                                                                                                    /* viewport */
tMap            Map;                                                                                 /* world to viewport mapping */

tCamera		tC;										   /* define the synthetic camera */
tReal3		realCP;									    		     /* point coordinates */

XPoint 		fill_vertex[MAXNUM];							 /* structure of vertex list of fillfaces */
tReal3 		vertices[MAXNUM];						   		      /* structure of vertex list */
int 		faces[MAXNUM][MAXNUM];									/* structure of face list */

/*********************************************************** MAIN PROGRAM *********************************************************/

int main(int argc, char **argv)										    /* main program start */
{
 x11graphics();												  /* execute X11 graphics */
}														   /* end program */

/**********************************************************  X11 GRAPHICS *********************************************************/

void x11graphics()		
{
 int    window_x                = 0;  		                                		       /* Window position, X axis */
 int    window_y                = 0;                                            		       /* Window position, Y axis */

 int	window_width		= 1000;									      /* X11 window width */
 int	window_height		= 750;									     /* X11 window height */

 char   *window_name            = "Window Program";                                      /* Window name that appears on Title Bar */
 char   *display_name   = NULL;   /* Server to connect to; NULL means connect to server specified in environment variable DISPLAY */
 int    window_depth            = 0;                                   /* Window window_depth, window_depth = 0 taken from parent */
 int    window_border_width     = 4;                                                                              /* Border width */
 int    window_class            = 0;                                                                              /* Window class */
 long   valuemask               = 0;                                             /* Specifies which window attributes are defined */
 int    onoff                   = 0;                             /* 0 - disable synchronization, nonzero - enable synchronization */
 int    mode                    = 0;     	                                                               /* coordinate mode */

 Visual                         *visual;                                                                         /* Visual widget */
 Colormap                       cmap;                                                                          /* Colormap widget */
 XGCValues                      values;                                                                  /* X GC values structure */
 XSetWindowAttributes           setwinattr;                                                      /* X window attributes structure */

 if((display=XOpenDisplay(display_name)) == NULL )                                                         /* Connect to X server */
  {
   (void) fprintf(stderr, "Window Program: cannot connect to X server %s\n", XDisplayName(display_name));
   exit(0);
  }

 XSynchronize(display, onoff);                               /* Synchronous mode for debugging, events are reported as they occur */

 screen                         = DefaultScreen(display);                               /* Get screen size from display structure */
 window_depth                   = DisplayPlanes(display, screen);                 /* Get window_depth size from display structure */
 visual                         = DefaultVisual(display, screen);                       /* Get visual size from display structure */
 cmap                           = DefaultColormap(display, screen);                       /* Get color map from display structure */

 valuemask                      = CWBackPixel | CWBorderPixel;                                    /* Setting up window attributes */
 setwinattr.background_pixel    = BlackPixel(display, screen);
 setwinattr.border_pixel        = WhitePixel(display, screen);

 win = XCreateWindow(display, RootWindow(display, screen), window_x, window_y, window_width, window_height,      /* Create window */
                     window_border_width, window_depth, window_class, visual, valuemask, &setwinattr);

/*  									     Set the minimum set of properties for window manager */

 XSetStandardProperties(display, win, window_name, NULL, NULL, NULL, NULL, NULL);

/*          Select all event types wanted (ExposureMask | ButtonPressMask | ButtonReleaseMask | KeyPressMask | PointerMotionMask) */

 XSelectInput(display, win, ExposureMask | ButtonPressMask | ButtonReleaseMask | KeyPressMask | PointerMotionMask);

 get_GC(&gc1, &gc2); 									   /* Create GC for graphics applications */
 XMapWindow(display, win);                                                                                          /* Map window */

/********************************************************* BEGIN GRAPHICS *********************************************************/

 do {XNextEvent(display, &event);} while(event.type != Expose);				     /* do until you get the expose event */
 optionsMenu();		 									       /* writes the options menu */
 menuScreen();									 /* menu or screen area program, mainline program */
}

/********************************************************  GRAPHIC CONTEXT ********************************************************/

void get_GC(GC *gc1, GC *gc2)
{
 long valuemask = 0;	 								     /* ignore XGCvalues and use defaults */
 XGCValues values;
 int line_width = 1;												/* set line width */
 int line_style = LineSolid;										   /* set for solid lines */
 int cap_style = CapButt;									      /* cap all line connections */
 int join_style = JoinBevel;											   /* bevel style */

 *gc1 = XCreateGC(display, win, valuemask, &values);					       /* Create default graphics context */
 XSetForeground(display, *gc1, WhitePixel(display,screen));   					/* Specify foreground pixel color */
 XSetBackground(display, *gc1, BlackPixel(display,screen));   					/* Specify background pixel color */
 XSetLineAttributes(display, *gc1, line_width, line_style, cap_style, join_style); 			   /* Set line attributes */

 *gc2 = XCreateGC(display, win, valuemask, &values);					       /* Create default graphics context */
 XSetForeground(display, *gc2, WhitePixel(display,screen));   					/* Specify foreground pixel color */
 XSetBackground(display, *gc2, BlackPixel(display,screen));   					/* Specify background pixel color */
 XSetLineAttributes(display, *gc2, line_width, line_style, cap_style, join_style); 			   /* Set line attributes */
}

/********************************************************** OPTIONS MENU **********************************************************/

void optionsMenu()										       /* writes the options menu */
{
 char	string[30];										   /* maximum string length of 30 */
 int	i = 0;													       /* counter */

 strcpy(string, "Get Mesh"); 		XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));	
 strcpy(string, "Polygon");		XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Save Mesh");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "Polygon");		XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Create");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "Postscript");		XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Draw Object");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "Current Camera");	XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Face        Back");	XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "Fill        Face");	XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Stereo View");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "");			XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Default");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "Camera");		XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Tilt Camera");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "CW           CCW");	XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Look at Point");	XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "Adjustment");		XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Eye Adjustment");	XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "Forward Backward");	XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Left       Right");	XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "Up          Down");	XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "WindowAdjustment");	XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "");			XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "-     Width    +");	XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "-     Height   +");	XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "");			XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "Quit");		XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));

 for(i = menu_top; i <= menu_bottom; i += menu_line)
  {
   if((i == 500) || (i == 600))	continue;								 /* just for this program */
   XDrawLine(display, win, gc1, menu_left, i, menu_right, i);						  /* draw horizonal lines */
  }
 XDrawLine(display, win, gc1, menu_right, menu_top, menu_right, menu_bottom);				    /* draw vertical line */
}

/********************************************************* MENU OR SCREEN *********************************************************/

void menuScreen()
{
 int		i, j;												 /* loop counters */
 int		item;									      /* number of item that was selected */
 int		bp;										 /* mouse button that was pressed */
 tReal2		mp;								  /* coordinates of mouse button that was pressed */

 initCamera();										/* initialize the camera when starting up */
 V = matchW2V(&tC.W);		                                            /* match window aspect ratio to viewport aspect ratio */
 Map = W2V(&tC.W,&V);     										/* map window to viewport */

 while(infinite) 						 	     		/* keep running until quit button pressed */
  {
   locateMB(&mp, &bp);											         /* get menu item */

/*********************************************************** MENU ITEM  ***********************************************************/

   if(mp.x < menu_right) 									       /* make sure its an option */
    {
     item = (mp.y / menu_line) + 1;									     /* get the menu item */
     switch(item)
      {
       case 1:											    	   	 /* Open datafile */
	system("dir *.dat");								   /* lists the name of all the datafiles */
	printf("\n\nEnter the datafile you want to read\n\n");			      /* inquire user what file they want to read */
        scanf("%s", input_filename); 										/* input filename */
        if((input_file = fopen(input_filename,"r")) == NULL)    					       /* open input file */
         {printf("\ncould not open input file\n"); exit(0);}

	loadArray();									/* load the array with vertices and faces */

	fclose(input_file);										  /* close the input file */
        computeUVN();											  /* compute the UVN axis */
       break;

       case 2:												       /* Create datafile */
	printf("\n\nEnter the name of the datafile you want to write\n\n");
        scanf("%s", output_filename);
        if((output_file = fopen(output_filename,"w")) == NULL)   					      /* open output file */
           {printf("\ncould not open output file\n"); exit(0);} 

        drawPolygon();												/* draw a polygon */
	loadFile();									 /* load the file with vertices and faces */

	fclose(output_file);										 /* close the output file */
       break;

       case 3:													    /* postscript */
        printf("\n\nEnter the name of the postscript file you want to write\n\n");
        scanf("%s", postscript_filename);
        if((postscript_file = fopen(postscript_filename,"w")) == NULL) 					  /* open postscript file */
           {printf("\ncould not open postscript file\n"); exit(0);}

        Init_Post(vWid,vHt);                                                                             /* initialize postscript */
        if(stereoViewPS == 0) drawObject(fill, bfr); else stereoView();		       /* create postscript file of either option */

        fprintf(postscript_file,"\nstroke showpage\n");                      /* write stroke showpage to postscript file to print */
        fclose(postscript_file);                                                                         /* close postscript file */
       break;

       case 4:													   /* draw object */
        XClearWindow(display,win);                                                                  /* clears the entire X window */
        optionsMenu();
        drawObject(fill, bfr);											/* draw an object */
        stereoViewPS = 0;									  /* set stereoView PS switch off */
       break;

       case 5:
        if(mp.x < (menu_right / 2)) 
         {if(fill == 0) fill = 1; else fill = 0;}		 				    /* fill object, toggle switch */
        else
         {if(bfr == 0) bfr = 1; else bfr = 0;}						      /* back face removal, toggle switch */
       break;

       case 6:
        XClearWindow(display,win);                                                                  /* clears the entire X window */
        optionsMenu();
	stereoView();									     /* create a stereo view of an object */
        stereoViewPS = 1;									   /* set stereoView PS switch on */
       break;

       case 7:												     /* initialize camera */
        initCamera();
       break;

       case 8:													   /* tilt camera */
 	tiltCamera();
       break;
	
       case 9:
        lookatAdjust();										       /* lookat point adjustment */
       break;

       case 10:												        /* eye adjustment */
        eyeAdjust();
       break;

       case 12:												/* window size adjustment */
        windowAdjust();
       break;

       case 14:													   /* QUIT Option */
        XFreeGC(display, gc1);                                                   /* Frees all memory associated with GC on server */
        XFreeGC(display, gc2);                                                   /* Frees all memory associated with GC on server */
        XCloseDisplay(display);                                            /* Disconnect client program from X server and display */
        exit(0);                                                                                                  /* Exit program */
       break;
      }														    /* End switch */
    }													    /* End if screen item */
  }													    /* End while infinite */
}													          /* End function */

/******************************************************** POSTSCRIPT DRIVERS ******************************************************/

/****************************************************** INITIALIZE POSTSCRIPT *****************************************************/

void Init_Post(int vportWid,int vportHt)
 { 
/* 

1. Puts proper stuff in PS file to print title, if any.
2. Defines m for moveto, l for lineto, s for stroke for brevity.
3. Sets PS coord system with origin near upper left of landscape page, x-axis to right, y-axis downward, leaving margin around page.
4. Uses vportWid,vportHt (the width and height of viewport in pixels), to find viewport's aspect ratio. Then compares it with aspect
   ratio of PS drawing area, and sets PS scaleFactor so viewport maps to largest region possible having same aspect ratio as 
   viewport, tucked in upper left corner of PS page.  */

  int H, W;
  double vportAspect, scaleFactor;
  char fmt[200];
  char fname[30], picname[80];

  strcpy(fmt, "/Helvetica findfont 12 scalefont setfont 144 36 m ( Robert Estey - UMASS ECE660 / NTU ST740A - Program 3 ");
  strcat(fmt, postscript_filename);
  strcat(fmt, " ) show\n");

  if (vportWid == 0 || vportHt == 0){puts("degenerate viewport !! I'm outa here.."); return;}
      vportAspect = (double)vportHt / (double)vportWid;

  fprintf(postscript_file," %% fsh, file = lab 3 \n"); 							/* put comment in PS file */
  fprintf(postscript_file,"/m {moveto}def /l {lineto}def /s {stroke} def\n");
  fprintf(postscript_file,"%d 0 translate 90 rotate\n",pagewidth);	  	    /* landscape, origin at bottom left for title */

  fprintf(postscript_file,fmt,"lab 3");	 								      	      /* print it */

                                                          /* shift and invert coord syst so origin is at top, and y-axis downward */
								     /* 3/4 inch border * 72 is 54 points, to be left around page */

  fprintf(postscript_file," %d %d translate 1 1 neg scale\n",margin,pagewidth-margin);

									  	     /* set proper scale for largest drawing area */
  H = pagewidth - 2 * margin;  								     /* drawable height of page in points */
  W = pageheight - 2 * margin; 								      /* drawable width of page in points */

  scaleFactor = (vportAspect > (double)H / W ? 	H / (double)vportHt : W / (double)vportWid);
  fprintf(postscript_file," %8.4lf %8.4lf scale\n", scaleFactor, scaleFactor);
} 													      /* end of Init_Post */

/******************************************************** WRITE POSTSCRIPT ********************************************************/

void Write_Post(int x, int y, int which)
 			           /* if which = 1 then write m for moveto(); else write lineto(), and handle count before stroke */
{
 static int countForStroke = 0; 									/* init'd first time only */
 static int countOnLine = 0;
 									 /* countForStroke: # of lineto's so far since last stroke*/
 								    /* countOnLine: # of entries so far on current line of PS file*/
 countForStroke++;
 countOnLine++;
 if(countOnLine > 8)
  {
   countOnLine = 0;
   fprintf(postscript_file,"\n"); 										    /* write <CR> */
  };

 if(which == 1) 
  { 													  /* about to do moveto() */
   if(countForStroke > NumBeforeStroke) 							 /* put stroke before next moveto */
    {
     countForStroke = 0;
     fprintf(postscript_file," s %d %d m ", x, y);
    }
   else 												      /* no stroke needed */
    fprintf(postscript_file,"%d %d m ", x, y);
  }
 else 												    /* which <> 1, so do a lineto */
  fprintf(postscript_file,"%d %d l ", x, y);
} 													   /* end of Write_Post() */

/*******************************************************INITIALIZE ARRAYS *********************************************************/

void initArrays()
{
 int	i, j;													      /* counters */
 for(i = 0; i < MAXNUM; i++)
  {
   vertices[i].x = 0.0; vertices[i].y = 0.0; vertices[i].z = 0.0; 				       /* zero out vertices array */
   for(j = 0; j < MAXNUM; j++)
    faces[i][j] = 0;											  /* zero out faces array */
  }
}

/***************************************************** LOAD VIEWPORT PARAMETERS ***************************************************/

void setInt4(int left, int top, int right, int bottom, tInt4 *V)
{
 V->l = left; V->t = top; V->r = right; V->b = bottom;                         /* load V (viewport) datastructure with parameters */
}

/***************************************************** MATCH WORLD TO VIEWPORT ****************************************************/

tInt4 matchW2V(tReal4 *W) 		                        	   /* matches world aspect ratio to viewport aspect ratio */
{
 tDisplay        gDisplay;                                                        /* window aspect ratio to viewport asepct ratio */
 double          physicalHeight          = 21.25;                                /* height of screen in cm (1024 x 768 Xterminal) */
 double          physicalWidth           = 29.10;                                 /* width of screen in cm (1024 x 768 Xterminal) */

 tInt4 V;
 double Rw, Wwidth, Wheight;
 Wwidth = W->r - W->l; Wheight = W->t - W->b;  							    /* compute widths and heights */
 gDisplay.numrows = MAXY + 1; gDisplay.numcols = MAXX + 1;                     /* load display structure with numrows and numcols */

 if(Wwidth == 0.0 || Wheight == 0.0)                                                                            /* invalid window */
  {printf("\n invalid window part 1 \n"); exit(0);}

 gDisplay.Rs = physicalHeight / physicalWidth; Rw = Wheight / Wwidth;                /* get the aspect ratio of display and world */

 if(Rw > gDisplay.Rs)                                                                                        /* landscape display */
  {
   V.l = 0; V.t = 0; V.b = gDisplay.numrows - 1; V.r = (int)(Rw * gDisplay.numrows / gDisplay.Rs);
  }
 else                                                                                                         /* portrait display */
  {
   V.l = 0; V.t = 0; V.b = (int)(gDisplay.Rs * gDisplay.numcols / Rw) ; V.r = gDisplay.numrows - 1;
  }
 return(V);
}

/******************************************************* MAP WORLD TO VIEWPORT ****************************************************/

tMap W2V(tReal4 *W, tInt4 *V)   						 /* map world coordinates to viewport coordinates */
 {
  double Wwidth, Wheight, Vwidth, Vheight;

  Wwidth = W->r - W->l; Wheight = W->t - W->b; Vwidth = V->r - V->l; Vheight = V->t - V->b; /* compute widths and heights,V is neg*/

  if(Wwidth == 0.0 || Wheight == 0.0)                                                                           /* invalid window */
   {printf("\n invalid window part 2 \n"); exit(0);}

  Map.A = Vwidth / Wwidth; Map.B = Vheight / Wheight;    		                               /* scales being calculated */
  Map.dx = V->l - (W->l * Map.A); Map.dy = V->b - (W->b * Map.B);       	                 /* translations being calculated */
  return(Map);
 }

/************************************************************* MOVE TO ************************************************************/

                           /* this routine doesn't do much compared to other graphics programs due to how Xwindows is implemented */

void moveto(double x, double y)                                                             /* compute coordinates but don't draw */
{
 realCP.x = x; realCP.y = y;                                                                    /* update screen current position */
}

/************************************************************* LINE TO ************************************************************/

void lineto(double x, double y)                                                                              /* draw line segment */
{
 int x1, y1, x2, y2;                                                                                        /* screen coordinates */

 x1 = (int)(((Map.A * realCP.x) + Map.dx) + 0.5);                              /* compute world coordinates to screen coordinates */
 y1 = (int)(((Map.B * realCP.y) + Map.dy) + 0.5);
 x2 = (int)(((Map.A * x) + Map.dx) + 0.5);                                     /* compute world coordinates to screen coordinates */
 y2 = (int)(((Map.B * y) + Map.dy) + 0.5);
 XDrawLine(display, win, gc2, x1, y1, x2, y2);                                                 /* draw line in screen coordinates */
 realCP.x = x; realCP.y = y;                                                                    /* update screen current position */

				   /* use the following line to remove all the queued events, this is used for debugging purposes */

 while(XCheckMaskEvent(display, ButtonPressMask | ButtonReleaseMask | PointerMotionMask, &event));

 Write_Post(x1,y1,1); 											/* moveto, put in PS file */
 Write_Post(x2,y2,2); 										        /* lineto, put in PS file */
}

/**************************************************** LOCATEMB CURRENT POSITION ***************************************************/

void locateMB(tReal2 *point, int *bp)				           /* locateMB mouse button pressed and pixel point value */
{
 do
  {
   XWindowEvent(display, win, ButtonPressMask, &event); 				   /* wait for mouse button to be pressed */
  } while (event.type != ButtonPress);

 *bp = event.xbutton.button;							 	 /* get the mouse button that was pressed */
 point->x = event.xbutton.x;							      /* get the x screen coordinate of the mouse */
 point->y = event.xbutton.y;							      /* get the y screen coordinate of the mouse */
}

/****************************************************** INITIALIZE THE CAMERA *****************************************************/

void initCamera()
{
 tC.eye.x = 1.0; tC.eye.y = 0.0; tC.eye.z = 0.0;				   		   /* set eye position to (1,0,0) */
 tC.lookat.x = 0.0; tC.lookat.y = 0.0; tC.lookat.z = 0.0;			  		  /* set look at point to (0,0,0) */
 tC.W.l = -1.0; tC.W.t = 1.0; tC.W.r = 1.0; tC.W.b = -1.0;					     /* set window to (-1,1,1,-1) */
 tC.E = 1.0;								  /* eye distance, between viewplane and eye along n axis */
 tC.cameraAspect = 0.75;										   /* camera aspect ratio */
}

/********************************************************* COMPUTE U, V, N ********************************************************/

void computeUVN()
{
 double	magnitude;								       /* compute the vectors, UVN not normalized */

 tC.n.x = tC.lookat.x - tC.eye.x; tC.n.y = tC.lookat.y - tC.eye.y; tC.n.z = tC.lookat.z - tC.eye.z;

 tC.u.x = tC.n.y; tC.u.y = -tC.n.x; tC.u.z = 0.0;				            /* u.z = 0, horizontal axis on camera */

 tC.v.x = -tC.n.x * tC.n.z; tC.v.y = -tC.n.y * tC.n.z; tC.v.z = pow(tC.n.x,2) + pow(tC.n.y,2);    /* v.z if positive, pointing up */

 if((tC.n.x == 0) && (tC.n.y == 0))	       /* force the camera on the user, if level camera (straight up or down on z axis ); */
  {
   tC.n.z = tC.n.z / abs(tC.n.z); tC.u.x = 1.0; tC.u.y = 0.0; tC.u.z = 0.0; tC.v.x = 0.0; tC.v.y = 1.0; tC.v.z = 0.0;
  }

 magnitude = sqrt(pow(tC.n.x,2) + pow(tC.n.y,2) + pow(tC.n.z,2));	  			 /* get the magnitude of vector n */
 tC.n.x = tC.n.x / magnitude; tC.n.y = tC.n.y / magnitude;  tC.n.z = tC.n.z / magnitude; 		    /* normalize vector n */
 magnitude = sqrt(pow(tC.u.x,2) + pow(tC.u.y,2) + pow(tC.u.z,2));	  			 /* get the magnitude of vector u */
 tC.u.x = tC.u.x / magnitude; tC.u.y = tC.u.y / magnitude;  tC.u.z = tC.u.z / magnitude;		    /* normalize vector u */
 magnitude = sqrt(pow(tC.v.x,2) + pow(tC.v.y,2) + pow(tC.v.z,2));	  			 /* get the magnitude of vector v */
 tC.v.x = tC.v.x / magnitude; tC.v.y = tC.v.y / magnitude;  tC.v.z = tC.v.z / magnitude;		    /* normalize vector v */

												       /* compute reference point */
 tC.ref.x = tC.eye.x + (tC.E * tC.n.x); tC.ref.y = tC.eye.y + (tC.E * tC.n.y); tC.ref.z = tC.eye.z + (tC.E * tC.n.z);
}

/******************************************************** LOOKAT ADJUSTMENT *******************************************************/

void lookatAdjust()											 /* get lookat parameters */
{
 printf("\n\n Enter the lookat coordinates x, y & z with spaces between each variable. \n\n");
 scanf("%lf %lf %lf", &tC.lookat.x, &tC.lookat.y, &tC.lookat.z);
}

/********************************************************* DOT PRODUCT, 3D ********************************************************/

double dot3D(tReal3 *vector1, tReal3 *vector2)						     /* dot product on 3 dimensional rays */
{
 double		result;											       /* result returned */
 result = (vector1->x * vector2->x) + (vector1->y * vector2->y) + (vector1->z * vector2->z);
 return(result);
}

/***************************************************** PERSPECTIVE PROJECTION *****************************************************/

tReal2 perspective(tReal3 *P)						     /* implement the perspective projection on viewplane */
{
 tReal2	Q;												   /* viewplane point, 2D */
 tReal3 ray;										   /* ray = point in world - eye position */
 double denominator;

 ray.x = P->x - tC.eye.x; ray.y = P->y - tC.eye.y; ray.z = P->z - tC.eye.z;	 			       /* compute the ray */
 denominator = dot3D(&ray, &tC.n);

 if(denominator < 0.00001) denominator = 0.0001;	        /* prevent division by zero.  point is to the eyes side or behind */

 Q.x = tC.E * dot3D(&ray, &tC.u) / denominator; 						   /* get u position on viewplane */
 Q.y = tC.E * dot3D(&ray, &tC.v) / denominator;							   /* get v position on viewplane */
 return(Q);
}

/******************************************************** WINDOW ADJUSTMENT *******************************************************/

void windowAdjust()
{
 int		bp;										 /* mouse button that was pressed */
 tReal2		mp;								  /* coordinates of mouse button that was pressed */
 double		W, H;											    /* segment parameters */

 W = tC.E * tan(epsilon); H = tC.cameraAspect * W;				     /* compute default width and height segments */

 do
  {
   locateMB(&mp, &bp);								 /* get current mouse position and button pressed */
   if(bp == 2) break;											  /* break out of do loop */
   if((mp.y > 600 && mp.y < 625) && (mp.x < (menu_right / 2))) {tC.W.l = tC.W.l + W; tC.W.r = tC.W.r - W;}	/* width increase */
   if((mp.y > 600 && mp.y < 625) && (mp.x > (menu_right / 2))) {tC.W.l = tC.W.l - W; tC.W.r = tC.W.r + W;}      /* width decrease */
   if((mp.y > 625 && mp.y < 650) && (mp.x < (menu_right / 2))) {tC.W.t = tC.W.t - H; tC.W.b = tC.W.b + H;}     /* height increase */
   if((mp.y > 625 && mp.y < 650) && (mp.x > (menu_right / 2))) {tC.W.t = tC.W.t + H; tC.W.b = tC.W.b - H;}     /* height decrease */
  } while(bp != 2);									         /* do while button 2 not pressed */
}

/********************************************************* EYE ADJUSTMENT *********************************************************/

void eyeAdjust()
{
 int		bp;										 /* mouse button that was pressed */
 tReal2		mp;								  /* coordinates of mouse button that was pressed */
 double		radial;								      /* radial from origin, distance from origin */
 double		theta;										       /* azimuth ccw from x axis */
 double		phi;									   /* latitude from the x,y up the z axis */

 radial = sqrt(pow(tC.eye.x, 2) + pow(tC.eye.y, 2) + pow(tC.eye.z, 2));							/* radial */
 theta = atan2(tC.eye.y, tC.eye.x);										       /* azimuth */
 if(radial != 0) phi = asin(tC.eye.z / radial); else phi = 0;							      /* latitude */

 do
  {
   locateMB(&mp, &bp);								 /* get current mouse position and button pressed */
   if(bp == 2) break;											  /* break out of do loop */

   if((mp.y > 475) && (mp.y < 500))								     /* move forward and backward */
    {
     if(mp.x < (menu_right / 2))				 						/* forward n axis */
      {
       tC.eye.x = tC.eye.x + tC.n.x * epsilon; tC.eye.x = tC.eye.x + tC.n.x * epsilon; tC.eye.x = tC.eye.x + tC.n.x * epsilon;
       tC.ref.x = tC.ref.x + tC.n.x * epsilon; tC.ref.x = tC.ref.x + tC.n.x * epsilon; tC.ref.x = tC.eye.x + tC.n.x * epsilon;
      }
     else												       /* backward n axis */
      {
       tC.eye.x = tC.eye.x - tC.n.x * epsilon; tC.eye.x = tC.eye.x - tC.n.x * epsilon; tC.eye.x = tC.eye.x - tC.n.x * epsilon;
       tC.ref.x = tC.ref.x - tC.n.x * epsilon; tC.ref.x = tC.ref.x - tC.n.x * epsilon; tC.ref.x = tC.eye.x - tC.n.x * epsilon;
      }
    }

   if(mp.y > 500 && mp.y < 525)							   /* along the origin not u axis, azimuth, theta */
    {
     if(mp.x < (menu_right / 2)) theta = theta - epsilon; else theta = theta + epsilon;        /* left - epsilon, right + epsilon */
    }

   if(mp.y > 525 && mp.y < 550)							    /* along the origin not v axis, latitude, phi */
    {
     if(mp.x < (menu_right / 2)) phi = phi + epsilon; else phi = phi - epsilon;     		  /* up + epsilon, down - epsilon */
    }

   if(mp.y > 500 && mp.y < 550)									    /* recompute the eye position */
    {
     tC.eye.x = radial * cos(phi) * cos(theta);	tC.eye.y = radial * cos(phi) * sin(theta); tC.eye.z = radial * sin(phi);
    }
  } while(bp != 2);									         /* do while button 2 not pressed */
}

/******************************************************** HYPOTENUSE **************************************************************/

double hypotenuse(tReal2 *point1, tReal2 *point2)							/* compute the hypotenuse */
{
 double dx, dy;
 dx = pow((abs(point1->x - point2->x)),2); dy = pow((abs(point1->y - point2->y)),2);
 return(sqrt(dx + dy));
}

/************************************************************ STEREOVIEW **********************************************************/

void stereoView()									     /* create a stereo view of an object */
{
 tReal3	eye_center;

 eye_center.x = tC.eye.x; eye_center.y = tC.eye.y; eye_center.z = tC.eye.z;			       /* save old eye parameters */

 tC.eye.x = eye_center.x + (epsilon10 * tC.u.x);							      /* set up right eye */
 tC.eye.y = eye_center.y + (epsilon10 * tC.u.y);
 tC.eye.z = eye_center.z + (epsilon10 * tC.u.z);
 drawObject(fill, bfr);												/* draw an object */

 tC.eye.x = eye_center.x - (epsilon10 * tC.u.x);							       /* set up left eye */
 tC.eye.y = eye_center.y - (epsilon10 * tC.u.y);
 tC.eye.z = eye_center.z - (epsilon10 * tC.u.z);
 drawObject(fill, bfr);												/* draw an object */

 tC.eye.x = eye_center.x; tC.eye.y = eye_center.y; tC.eye.z = eye_center.z;		    /* reset eye with original parameters */
}

/*********************************************************** TILT CAMERA **********************************************************/

void tiltCamera()								   /* tilt camera on u & v axis, n axis no change */
{
 int		bp;										 /* mouse button that was pressed */
 tReal2		mp;								  /* coordinates of mouse button that was pressed */

 do
  {
   locateMB(&mp, &bp);								 /* get current mouse position and button pressed */
   if(bp == 2) break;											  /* break out of do loop */

   if((mp.y > 350) && (mp.y < 400))								     /* move forward and backward */
    {
     if(mp.x < (menu_right / 2))				 					/* turn counter clockwise */
      {
       tC.u.x = cos(epsilon) * tC.u.x - sin(epsilon) * tC.v.x;
       tC.u.y = cos(epsilon) * tC.u.y - sin(epsilon) * tC.v.y;
       tC.u.z = cos(epsilon) * tC.u.z - sin(epsilon) * tC.v.z;

       tC.v.x = sin(epsilon) * tC.u.x + cos(epsilon) * tC.v.x;
       tC.v.y = sin(epsilon) * tC.u.y + cos(epsilon) * tC.v.y;
       tC.v.z = sin(epsilon) * tC.u.z + cos(epsilon) * tC.v.z;
      }
     else													/* turn clockwise */
      {
       tC.u.x = cos(epsilon) * tC.u.x + sin(epsilon) * tC.v.x;
       tC.u.y = cos(epsilon) * tC.u.y + sin(epsilon) * tC.v.y;
       tC.u.z = cos(epsilon) * tC.u.z + sin(epsilon) * tC.v.z;

       tC.v.x = -sin(epsilon) * tC.u.x + cos(epsilon) * tC.v.x;
       tC.v.y = -sin(epsilon) * tC.u.y + cos(epsilon) * tC.v.y;
       tC.v.z = -sin(epsilon) * tC.u.z + cos(epsilon) * tC.v.z;
      }
    }
  } while(bp != 2);									         /* do while button 2 not pressed */
}

/********************************************************** CROSS PRODUCT *********************************************************/

tReal3 cross(tReal3 *vector1, tReal3 *vector2)							       /* 3D vector cross product */
{
 tReal3 vector;
 vector.x = vector1->y * vector2->z - vector1->z * vector2->y;
 vector.y = vector1->z * vector2->x - vector1->x * vector2->z;
 vector.z = vector1->x * vector2->y - vector1->y * vector2->x;
 return(vector);
}

/************************************************************ LOAD ARRAY **********************************************************/

void loadArray()
{
 char		buffer[80];										 /* character buffer area */
 char		*temp;											   /* temporary character */
 int		i, j;												      /* counters */
 int		total_vertices;									      /* total number of vertices */
 int            total_faces;                                                                             /* total number of faces */
 int		polygon_faces;								   /* total number of faces for a polygon */

 initArrays();												     /* initialize arrays */

 do {fgets(buffer, 78, input_file);} while(buffer[0] == '%' || buffer[0] == '
');
 sscanf(buffer, "%d", &total_vertices);						      	      /* get the total number of vertices */
 vertices[0].x = total_vertices;							  /* load the total vertices in the array */

 do {fgets(buffer, 78, input_file);} while(buffer[0] == '%' || buffer[0] == '
');
 sscanf(buffer, "%d", &total_faces);							 	 /* get the total number of faces */
 faces[0][0] = total_faces;								     /* load the total faces in the array */

 do {fgets(buffer, 78, input_file);} while(buffer[0] == '%' || buffer[0] == '
');
 sscanf(buffer, "%lf %lf %lf", &vertices[1].x, &vertices[1].y, &vertices[1].z);		 	/* load the vertices in the array */
 for(i = 2; i <= total_vertices; i++) 
 fscanf(input_file, "%lf %lf %lf", &vertices[i].x, &vertices[i].y, &vertices[i].z);   	      /* load the vertices into the array */

 for(i = 1; i <= total_faces; i++)								 /* load the faces into the array */
  {
   do {fgets(buffer, 78, input_file);} while(buffer[0] == '%' || buffer[0] == '
');
   temp = strtok(buffer, " \t");							   /* set up delimeter as spaces and tabs */
   sscanf(temp, "%d", &polygon_faces);
   faces[i][0] = polygon_faces;

   for(j = 1; j <= polygon_faces; j++)								 /* load the faces into the array */
    {
     temp = strtok((char *)0, " \t");						       /* (char *) 0, casting 0 to a null pointer */
     sscanf(temp, "%d", &faces[i][j]);
    }
  }
}

/********************************************************** FILL FACE *************************************************************/

void fillFace(int i, double x, double y)    							     /* fills a face of an object */
{
 fill_vertex[i].x = (int)(((Map.A * x) + Map.dx) + 0.5);    		       /* compute world coordinates to screen coordinates */
 fill_vertex[i].y = (int)(((Map.B * y) + Map.dy) + 0.5);
}

/********************************************************* DRAW POLYGON ***********************************************************/

void drawPolygon()									    	  /* draw a polygon on the screen */
{
 int		i, j, num;							 				      /* counters */
 tReal2		oldpt, firstpt;							  /* coordinates of mouse button that was pressed */
 int		bp;										 /* mouse button that was pressed */
 tReal2		mp;								  /* coordinates of mouse button that was pressed */
 tReal2		vertex2D;										  /* 2 dimensional vertex */

 initArrays(); 												     /* initialize arrays */
 locateMB(&mp, &bp);								 /* get current mouse position and button pressed */
 oldpt = mp; firstpt = mp;							   			      /* set up old point */
 i = 1; vertices[i].x = (oldpt.x - Map.dx) / Map.A; vertices[i].y = (oldpt.y - Map.dy) / Map.B;	 /* store vertices into the array */

 while(infinite)
  {
   locateMB(&mp, &bp); 		 	 								    /* get 1st coordinate */
   if(bp != 1) break;								   /* stay in loop until button 2 or 3 is pressed */

   XDrawLine(display, win, gc2, (int)oldpt.x, (int)oldpt.y, (int)mp.x, (int)mp.y);			     /* draw line segment */
   oldpt = mp;												       /* reset old point */
   i++; vertices[i].z = 0;									 /* store vertices into the array */
   vertices[i].x = (oldpt.x - Map.dx) / Map.A; vertices[i].y = (oldpt.y - Map.dy) / Map.B;
  } 

 XDrawLine(display, win, gc2, (int)oldpt.x, (int)oldpt.y, (int)firstpt.x, (int)firstpt.y);	     /* draw closing line segment */

 vertices[0].x = i * 2; num = i; faces[0][0] = i;				     /* loading total information into the arrays */

 locateMB(&mp, &bp);
 vertex2D.x = vertices[1].x; vertex2D.y = vertices[1].y;
 vertices[0].z = (hypotenuse(&mp, &vertex2D) - Map.dy) / Map.B;

 for(i = 1; i <= num; i++)									     /* load the cap of the prism */
  {
   vertices[i+num].z = vertices[0].z;
   vertices[i+num].x = vertices[i].x; vertices[i+num].y = vertices[i].y;
  }

 for(i = 1; i < num; i++)										       /* build face list */
  {
   faces[i][0] = 4;										   /* number of vertices per face */
   faces[i][1] = i;											    /* start vertex, base */
   faces[i][2] = i + num;										     /* start vertex, cap */
   faces[i][3] = i + num + 1;										       /* end vertex, cap */
   faces[i][4] = i + 1;											      /* end vertex, base */
  }
									   /* write the last face list, wrap around to first face */
 faces[i][0] = 4;										   /* number of vertices per face */
 faces[i][1] = i;											    /* start vertex, base */
 faces[i][2] = i + num;											     /* start vertex, cap */
 faces[i][3] = 1 + num;											      /* end vertex, base */
 faces[i][4] = 1;											       /* end vertex, cap */
}

/************************************************************ LOAD FILE ***********************************************************/

void loadFile()												  /* load the output file */
{
 int		i, j;												      /* counters */

 fprintf(output_file, "%d\n", (int)vertices[0].x);					      /* put the total number of vertices */

 fprintf(output_file, "%d\n", faces[0][0]);						 	 /* put the total number of faces */

 for(i = 1; i <= vertices[0].x; i++) 
  fprintf(output_file, "%lf %lf %lf\n", vertices[i].x, vertices[i].y, vertices[i].z);		 	      /* put the vertices */

 for(i = 1; i <= faces[0][0]; i++)
  {
   fprintf(output_file, "%d", faces[i][0]);					 /* put the total number of faces for the polygon */
   for(j = 1; j <= faces[i][0]; j++)
    {
     fprintf(output_file, "	%d", faces[i][j]);								 /* put the faces */
    }
   fprintf(output_file, "\n");								     /* start new line for next face list */
  }
}

/********************************************************* BACKFACE REMOVAL *******************************************************/

int backfaceRemoval(int point)									  /* remove backface from drawing */
{
 tReal3 m, f1, f2, ray;
 double visible;

 ray.x = vertices[faces[point][2]].x - tC.eye.x; 				   /* compute a vector from the object to the eye */
 ray.y = vertices[faces[point][2]].y - tC.eye.y; 
 ray.z = vertices[faces[point][2]].z - tC.eye.z;

 f1.x  = vertices[faces[point][1]].x - vertices[faces[point][2]].x; 			      /* compute a vector from the object */
 f1.y  = vertices[faces[point][1]].y - vertices[faces[point][2]].y;
 f1.z  = vertices[faces[point][1]].z - vertices[faces[point][2]].z;

 f2.x  = vertices[faces[point][3]].x - vertices[faces[point][2]].x;			      /* compute a vector from the object */
 f2.y  = vertices[faces[point][3]].y - vertices[faces[point][2]].y;
 f2.z  = vertices[faces[point][3]].z - vertices[faces[point][2]].z;

 m = cross(&f1, &f2);									   /* get the normal vector of the object */
 visible = dot3D(&m, &ray);
 if(visible < 0) visible = 1;	  /* if the dot product of the normal vector and ray is less than zero then the object is visible */
 return(visible);
}

/*********************************************************** DRAW OBJECT **********************************************************/

void drawObject(int fill, int bfr)										/* draw an object */
{
 int i, j;													       /* counter */
 int visible;											    /* visible switch on backface */
 int shape, mode;											  /* face fill parameters */
 tReal2 Q, vertex1;											     /* screen coordinate */

 V = matchW2V(&tC.W);		                                            /* match window aspect ratio to viewport aspect ratio */
 Map = W2V(&tC.W,&V);     										/* map window to viewport */
 
 for(i = 1; i <= faces[0][0]; i++)						 /* set up for total number of faces, faces[0][0] */
  {
   for(j = 1; j <= faces[i][0]; j++)				 /* set up for total number of vertices in a polygon, faces[n][0] */
    {
     visible = backfaceRemoval(i);
     if(bfr) if(!visible) break;						   /* remove backface from drawing if not visible */

     Q = perspective(&vertices[faces[i][j]]);						   /* get the point in screen coordinates */

     if(j == 1)
      {
       moveto(Q.x, Q.y);									       /* move to new coordinates */
       fillFace(j, Q.x, Q.y);										  /* store fill face data */
       vertex1.x = Q.x; vertex1.y = Q.y;							     /* save 1st point of polygon */
      }
     else
      {
       lineto(Q.x, Q.y);										  /* draw the coordinates */
       fillFace(j, Q.x, Q.y);										  /* store fill face data */
      }
    }
   if(j != 1) lineto(vertex1.x, vertex1.y);									     /* close the polygon */
   if(fill) XFillPolygon(display, win, gc2, &fill_vertex[1], (j - 1), Convex, CoordModeOrigin); 	      /* fill the polygon */
  }
}
