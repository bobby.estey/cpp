




The title of the project was 3D Mesh Magic.  This is the last project of 
ECE660 / ST740A.

MB - Mouse Button.

The user can run this program almost totally with a mouse with the exception
of lookat point coordinates and datafile names.  This programs does all that
was required in addition to stereoview and tilt the camera.

Option 1:  Get Mesh Polygon

This option will get a mesh file loaded into the programs array structure.
This option when pressed asks for key input of the datafile.

Option 2:  Save Mesh Polygon

The user can draw a polygon using MB1.  Everytime the user presses MB1 is 
connects the vertex segments.  To end the polygon press MB2 and the polyline 
closes to a polygon.  The user then presses MB1 again to get the Z axis 
(height).  Once this is done the program writes all the information to a mesh 
file to be displayed later.

Option 3:  Create Postscript

The program will write the keystrokes required for a postscript file.  This 
option also asks the user what they would like to name the file.

Option 4:  Draw Object, Current Camera

The program will display the object with the current synthetic camera.

Option 5A:  Face Fill

Fills the polygon with a solid color.

Option 5B:  Back Face

Removes the faces that are not visible.

Option 6:  Stereo View

Draw the left and right eyes.

Option 7:  Resets the synthetic camera back to default settings which is

eye=(1,0,0) lookat=(0.0.0) window=(-1,1,1,-1) E=1

Option 8:  Tilt Camera

Tilts the synthetic camera either clockwise or counterclockwise.

Option 9:  Look at Point Adjustment

Prompts the user for lookat parameters.  The input must have a space between
all values.  1.0 1.0 1.0 would be a good example for coordinates x, y and z.










Option 10:  Eye Adjustment

Forward - moves the eye forward towards the camera.
Backward - moves the eye backward from the camera.
Left - moves the eye left.
Right - moves the eye right.
Up - moves the eye up.
Down - moves the eye down.

Option 11:  Window Adjustment

Adjusts the window width and/or height.

Option 12:  Quit

Ends the program.
