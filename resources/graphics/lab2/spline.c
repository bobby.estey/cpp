/**********************************************************************************************************************************/
/* Revised (once again) for ECE 660, f.s.hill, 11/9/94										  */
/* Spline curves are based on:													  */
/* 1). The array P[0] ...P[L] of real control points.										  */
/* 2). The order of the spline : e.g.  4 for a cubic spline									  */
/* 3). kind = 0 for open spline curve based on 'standard knot vector'.								  */
/*     kind = 1 for closed periodic spline curve.										  */
/* Note:  The order must not exceed (L + 1).  If order == L + 1 the spline curve is in fact a Bezier curve.			  */
/**********************************************************************************************************************************/

#define MaxNumKnots 50    	 						/* (L + order + 1) of them are needed at run time */
typedef struct {double x, y;} tRealPoint;
double knot[MaxNumKnots];										/* global for convenience */
tRealPoint P[40];  								     /* the control points... as many as you want */
int L;				 								/* global: last index in polyline */

/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< N  - the Spline Basis function >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/

double N(int k, int order, double t)
{
 double denom1, denom2, sum;

 if (order == 1) return (((t < knot[k]) || (t >= knot[k+1])) ? 0.0 : 1.0);
 else {
       sum = (((denom1 = knot[k + order - 1] - knot[k])) > 0.00001) ?
       ((t - knot[k]) * N(k, order - 1, t) / denom1) : 0.0 ;
       sum += (((denom2 = knot[k + order] - knot[k + 1])) > 0.00001) ?
       ((knot[k + order] - t) * N(k + 1, order - 1, t) / denom2) : 0.0 ;
       return sum;
      }
}
/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< drawSpline>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/

void drawSpline(int kind, int order, int L)
{
 double t, tmod, t_max, spln, t_inc = 0.04;	 						  /* set t_inc according to taste */
 tRealPoint p;
 int i;
											  	   /* first build the knot vector */
 if(kind == 0)
  { 		 												 /* open B-spline */
   for (i = 0; i <= L + order; i++) 
        if (i < order)   knot[i] = 0.0;
        else if (i <= L ) knot[i] = knot[i-1] + 1;
      	else knot[i] = L - order + 2;
	t_max= L - order + 2;
  }
 else
  {												        /* closed periodic spline */
   for (i = 0; i <= order; i++)  knot[i] = i;
    	t_max = L + 1;
  }
									 	      /* now generate and draw the B-spline curve */
 for(t = 0; t <= t_max; t += t_inc)
  {
   p.x = 0.0; p.y = 0.0;
   for (i = 0; i <= L; i++)
    {   										      /* form the linear combo. of points */
     if(kind == 0)  spln = N(i, order, t);
     else
      {
       tmod = t - i;
       while (tmod < 0) tmod += (L+1); 									       /* modulo it (L+1) */
       spln = N(0, order, tmod);
      }
     p.x += P[i].x * spln; p.y += P[i].y * spln;
    }    									        		      /* end of inner for */
   if (t == 0.0)  moveto((int)p.x,(int) p.y); else lineto((int)p.x,(int)p.y);
  }    														    /* end of for */
} 													   /* end of drawSpline() */

/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< main -- tester >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/

main()
{   										   /* use mouse to get polyline, then draw spline */
int Gd= DETECT ,Gm; 										 		  /* Turbo C only */
int x,y,butt,i;
initmouse();  initgraph(&Gd,&Gm,""); setcolor(15);
setmousevis(1);
L = -1;	       			  										 /* no points yet */
do{								           			    /* lay down polyline by mouse */
   while(!(butt=GetMouse(&x,&y))); 									   /* wait for mouse down */
   if(butt == 1)
    {
     setmousevis(0);
     L++;
     if(L==0) moveto(x,y); else lineto(x,y);
     P[L].x = x; P[L].y = y;											/* get next point */
     setmousevis(1);
     while(GetMouse(&x,&y)); 										     /* wait for mouse up */
    }
  } while(butt == 1);
if(butt == 2) drawSpline(1,4,L);	 							 /* closed spline if right button */
else drawSpline(0,4,L);            	 							   /* open spline if both buttons */
getch(); 				 								       /* pause to admire */
closegraph();
}
