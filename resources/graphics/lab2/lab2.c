/*  Robert Estey - 											  */
/*  Email:  estey@mosvax.sps.mot.com  Phone:  (602) 655-2391  Fax:  (602) 655-3569						  */
/*  UMASS ECE660 / NTU ST740A	Program 2											  */
/*																  */
/*  Developed and Ran on VAX/VMS V5.5-2 & DEC/Motif V1.1									  */
/*																  */
/*  cc/standard=portable program			- C Compiler V3.2 on VAX/VMS V5.5-2					  */
/*  link program, sys$library:dxm_link.opt/opt		- Link in the X11 - DEC/Motif V1.1 libraries				  */
/*  set  display/create/transport=decnet/node=cv64 	- Set the display							  */
/*  run  program					- Execute the program							  */
/*																  */
/*  sys$library:dxm_link.opt (contents in file are below)									  */
/*																  */
/*  ! Linker options to link with DEC/Motif 1.1 shareable images								  */
/*  psect_attr=XMQMOTIF,noshr,lcl												  */
/*  SYS$SHARE:DECW$DXMLIBSHR/SHAREABLE												  */
/*  SYS$SHARE:DECW$XMLIBSHR/SHAREABLE												  */
/*  SYS$SHARE:DECW$XLIBSHR/SHAREABLE												  */
/*  SYS$SHARE:VAXCRTL/SHAREABLE													  */
/*																  */
/**********************************************************************************************************************************/
/*																  */
/* Revised (once again) for ECE 660, f.s.hill, 11/9/94										  */
/* Spline curves are based on:													  */
/* 1). The array P[0] ...P[L] of real control points.										  */
/* 2). The order of the spline : e.g.  4 for a cubic spline									  */
/* 3). kind = 0 for open spline curve based on 'standard knot vector'.								  */
/*     kind = 1 for closed periodic spline curve.										  */
/* Note:  The order must not exceed (L + 1).  If order == L + 1 the spline curve is in fact a Bezier curve.			  */
/*																  */
/**********************************************************************************************************************************/
/*																  */
/*  Program Description:  SUPERDRAW_README.TXT											  */
/*																  */
/**************************************************** INITIALIZE & START PROGRAM **************************************************/

#include 	<stdio.h>									  /* include standard i/o library */
#include	<stdlib.h>									  /* include standard lib library */
#include	<string.h>										/* include string library */
#include        <X11/Xlib.h> 		                                                                   /* include x11 library */
#include        <math.h>                                                                                  /* include math library */

#define		MAXNUM			50							     /* maximum number of objects */
#define		MAXX			1000
#define		MAXY			750
/*#define	MAXX			1254				        maximum number of columns (1280 x 1024 Xterminal) */
/*#define	MAXY			988					   maximum number of rows (1280 x 1024 Xterminal) */
/*#define       MAXX                    998                                      maximum number of columns (1024 x 768 Xterminal) */
/*#define       MAXY                    732                                         maximum number of rows (1024 x 768 Xterminal) */
/*#define	MAXX			619				        maximum number of columns (640 x 480 VGA Monitor) */
/*#define	MAXY			446				 	   maximum number of rows (640 x 480 VGA Monitor) */

#define		maxHotspots		8						   /* total number of hotspots per object */
#define 	maxNumKnots 		50    	 				/* (L + order + 1) of them are needed at run time */

#define		superellipse		1							  /* define the superellipse type */
#define		polyline		2							      /* define the polyline type */

/************************************************************* TYPEDEFS ***********************************************************/

typedef		struct	{double x, y;} tRealPoint;						       /* structure double points */
typedef         struct  {double l, t, r, b;} tRealRect;                             		    /* structure double rectangle */
struct  	vertex_list {tRealPoint p; struct vertex_list *next;} *t_ptr, *p_ptr;			   /* structure of vertex */

typedef		struct  {int type; tRealPoint handles[9]; double angle; int selected;
			 tRealPoint rectp1; tRealPoint rectp2;
			 union
			  {double bulge;
			   struct vertex_list *vertex;} item;
			  } tObject;						    /* type of object (super ellipse or polyline) */

tObject 	*objects[MAXNUM];										 /* objects array */

/************************************************************ PROTOTYPES **********************************************************/

void 		x11graphics();							      		                  /* X11 graphics */
void 		get_GC(GC *gc1, GC *gc2);  							          /* get graphics context */

int 		locateCP(tRealPoint *point, int *bp);				       /* locates screen point and button pressed */
int 		locateCPv(tRealPoint *point, int *bp);				       /* locates screen point and button pressed */
int 		get_table_entry();									   /* get the table entry */
int 		selectedObj(int *objectNum);								     /* select the object */
int 		closestObj(int *objectNum, tRealPoint *objectVertex, tRealPoint *mp);			/* get the closest object */

double 		distsq(tRealPoint *point1, tRealPoint *point2);				   /* get the hypotenuse between 2 points */
double 		N(int k, int order, double t);									      /* B-Spline */

void		deselectAll();								 /* deselects all objects in object table */
void 		locate(tRealPoint *point, int *bp);				       /* locates screen point and button pressed */
void		optionsMenu();									       /* writes the options menu */
void		menuScreen();										   /* menu or screen area */
void		storeObj(int item, int objectNum, tRealPoint *mp, tRealPoint *pivot);	    /* store the object in data structure */
void 		rubberRect(int item, int *objectNum, tRealPoint *mp, tRealPoint *pivot);		      /* rubber rectangle */
void 		rubberLine(int item, int *objectNum, tRealPoint *mp, tRealPoint *pivot);			   /* rubber line */
void		ellipse(int objectNum);   								       /* draw an ellipse */
void 		hotSpots(int objectNum);						    /* draws the hotspots of bounding box */
void 		drawSpline(int kind, int order, int L);							     /* draw the B-Spline */
void 		drawObj();										       /* draw the object */
void 		rotateObj(int objectNum);								      /* rotate an object */
void 		moveVertex(int objectNum);							  /* move the vertex of an object */
void	        drawTheObj(int objectNum);								       /* draw new object */
void		moveObject(int objectNum);							      		/* move an object */
void 		refresh(int objectNum);						     /* refresh screen and draw from object table */

/********************************************************* GLOBAL VARIABLES  ******************************************************/

Display         *display;                                       /* X Terminal address, pointer to display structure on X terminal */
int             screen;      		                      /* X Terminal screen, root window to screen structure on X terminal */
Window          win;                                                                                             /* Window widget */
GC              gc1, gc2; 									        /* ID of graphics context */
XEvent		event;									       /* Structure for event information */

int		infinite		= 1;							      /* set up for infinite loop */
int		status 			= 0;									 /* return status */

int		menu_left		= 0;						       /* left coordinate of options menu */
int		menu_top		= 0;						        /* top coordinate of options menu */
int		menu_right		= 120;						      /* right coordinate of options menu */
int		menu_bottom		= 750;						     /* bottom coordinate of options menu */
int             menu_contents           = 20;                                                               /* menu item contents */
int             menu_seperator          = 30;                                                              /* menu item seperator */
int             menu_line               = 50;                                                      	    /* menu line - x axis */
double		radiansperdegree	= 0.0174532925199;						    /* radians per degree */

tRealPoint	RealCP, SplineCP, RealPoint;							    /* points, screen coordinates */
double 		knot[maxNumKnots];									/* global for convenience */
tRealPoint 	P[MAXNUM];  							     /* the control points... as many as you want */
int 		L;				 						/* global: last index in polyline */

/*********************************************************** MAIN PROGRAM *********************************************************/

int main(int argc, char **argv)										    /* main program start */
{
 x11graphics();												  /* execute X11 graphics */
}														   /* end program */

/**********************************************************  X11 GRAPHICS *********************************************************/

void x11graphics()		
{
 int    window_x                = 0;  		                                		       /* Window position, X axis */
 int    window_y                = 0;                                            		       /* Window position, Y axis */

 int	window_width		= 1000;
 int	window_height		= 750;

/*int   window_width		= 1260;		        Window size, width - Including the window manager (1280 x 1024 Xterminal) */
/*int	window_height		= 990;		       Window size, height - Including the window manager (1280 x 1024 Xterminal) */
/*int   window_width            = 1006;                  Window size, width - Including the window manager (1024 x 768 Xterminal) */
/*int   window_height           = 734;                  Window size, height - Including the window manager (1024 x 768 Xterminal) */
/*int  window_width            = 619;                   Window size, width - Including the window manager (640 x 480 VGA monitor) */
/*int  window_height           = 446;                  Window size, height - Including the window manager (640 x 480 VGA monitor) */

 char   *window_name            = "Window Program";                                      /* Window name that appears on Title Bar */
 char   *display_name   = NULL;   /* Server to connect to; NULL means connect to server specified in environment variable DISPLAY */
 int    window_depth            = 0;                                   /* Window window_depth, window_depth = 0 taken from parent */
 int    window_border_width     = 4;                                                                              /* Border width */
 int    window_class            = 0;                                                                              /* Window class */
 long   valuemask               = 0;                                             /* Specifies which window attributes are defined */
 int    onoff                   = 0;                             /* 0 - disable synchronization, nonzero - enable synchronization */
 int    mode                    = 0;     	                                                               /* coordinate mode */

 Visual                         *visual;                                                                         /* Visual widget */
 Colormap                       cmap;                                                                          /* Colormap widget */
 XGCValues                      values;                                                                  /* X GC values structure */
 XSetWindowAttributes           setwinattr;                                                      /* X window attributes structure */

 if((display=XOpenDisplay(display_name)) == NULL )                                                         /* Connect to X server */
  {
   (void) fprintf(stderr, "Window Program: cannot connect to X server %s\n", XDisplayName(display_name));
   exit(0);
  }

 XSynchronize(display, onoff);                               /* Synchronous mode for debugging, events are reported as they occur */

 screen                         = DefaultScreen(display);                               /* Get screen size from display structure */
 window_depth                   = DisplayPlanes(display, screen);                 /* Get window_depth size from display structure */
 visual                         = DefaultVisual(display, screen);                       /* Get visual size from display structure */
 cmap                           = DefaultColormap(display, screen);                       /* Get color map from display structure */

 valuemask                      = CWBackPixel | CWBorderPixel;                                    /* Setting up window attributes */
 setwinattr.background_pixel    = BlackPixel(display, screen);
 setwinattr.border_pixel        = WhitePixel(display, screen);

 win = XCreateWindow(display, RootWindow(display, screen), window_x, window_y, window_width, window_height,      /* Create window */
                     window_border_width, window_depth, window_class, visual, valuemask, &setwinattr);

/*  									     Set the minimum set of properties for window manager */

 XSetStandardProperties(display, win, window_name, NULL, NULL, NULL, NULL, NULL);

/*          Select all event types wanted (ExposureMask | ButtonPressMask | ButtonReleaseMask | KeyPressMask | PointerMotionMask) */

 XSelectInput(display, win, ExposureMask | ButtonPressMask | ButtonReleaseMask | KeyPressMask | PointerMotionMask);

 get_GC(&gc1, &gc2); 									   /* Create GC for graphics applications */
 XMapWindow(display, win);                                                                                          /* Map window */

/********************************************************* BEGIN GRAPHICS *********************************************************/

 do {XNextEvent(display, &event);} while(event.type != Expose);				     /* do until you get the expose event */
 optionsMenu();		 									       /* writes the options menu */
 menuScreen();									 /* menu or screen area program, mainline program */
}

/********************************************************  GRAPHIC CONTEXT ********************************************************/

void get_GC(GC *gc1, GC *gc2)
{
 long valuemask = 0;	 								     /* ignore XGCvalues and use defaults */
 XGCValues values;
 int line_width = 1;												/* set line width */
 int line_style = LineSolid;										   /* set for solid lines */
 int cap_style = CapButt;									      /* cap all line connections */
 int join_style = JoinBevel;											   /* bevel style */

 *gc1 = XCreateGC(display, win, valuemask, &values);					       /* Create default graphics context */
 XSetForeground(display, *gc1, WhitePixel(display,screen));   					/* Specify foreground pixel color */
 XSetBackground(display, *gc1, BlackPixel(display,screen));   					/* Specify background pixel color */
 XSetLineAttributes(display, *gc1, line_width, line_style, cap_style, join_style); 			   /* Set line attributes */
 XSetFunction(display, *gc1, GXcopy);							  /* Set drawing for copy (default) state */

 *gc2 = XCreateGC(display, win, valuemask, &values);					       /* Create default graphics context */
 XSetForeground(display, *gc2, BlackPixel(display,screen));   					/* Specify foreground pixel color */
 XSetBackground(display, *gc2, WhitePixel(display,screen));   					/* Specify background pixel color */
 XSetLineAttributes(display, *gc2, line_width, line_style, cap_style, join_style); 			   /* Set line attributes */
 XSetFunction(display, *gc2, GXxor);								     /* Set drawing for XOR state */
}

/************************************************************* LOCATE *************************************************************/

void locate(tRealPoint *point, int *bp)					     /* locate mouse button pressed and pixel point value */
{
 do
  {
   XWindowEvent(display, win, ButtonPressMask, &event); 				   /* wait for mouse button to be pressed */
  } while (event.type != ButtonPress);

 *bp = event.xbutton.button;							 	 /* get the mouse button that was pressed */
 point->x = event.xbutton.x;							      /* get the x screen coordinate of the mouse */
 point->y = event.xbutton.y;							      /* get the y screen coordinate of the mouse */
}

/***************************************************** LOCATE CURRENT POSITION ****************************************************/

int locateCP(tRealPoint *point, int *bp)				     /* locate mouse button pressed and pixel point value */
{
 do									/* stay in loop until pointer movement or button released */
  {							
   XWindowEvent(display, win, PointerMotionMask|ButtonReleaseMask, &event); 
  } while (event.type != MotionNotify && event.type != ButtonRelease);

 *bp = event.xbutton.button;							 	 /* get the mouse button that was pressed */
 point->x = event.xbutton.x;							      /* get the x screen coordinate of the mouse */
 point->y = event.xbutton.y;							      /* get the y screen coordinate of the mouse */

 if(event.type == MotionNotify) return(1);						            /* return 1 if pointer motion */
 else return(0);
}

/***************************************************** LOCATE CURRENT POSITION ****************************************************/

int locateCPv(tRealPoint *point, int *bp)				     /* locate mouse button pressed and pixel point value */
{
 do									 /* stay in loop until pointer movement or button pressed */
  {
   XWindowEvent(display, win, PointerMotionMask|ButtonPressMask, &event);
  } while (event.type != MotionNotify && event.type != ButtonPress);

 *bp = event.xbutton.button;							 	 /* get the mouse button that was pressed */
 point->x = event.xbutton.x;							      /* get the x screen coordinate of the mouse */
 point->y = event.xbutton.y;							      /* get the y screen coordinate of the mouse */

 if     (*bp == 1) return(1);								      /* return 1 if button 1 was pressed */
 else if(*bp == 2) return(2);								      /* return 2 if button 2 was pressed */
 else if(*bp == 3) return(3);								      /* return 3 if button 3 was pressed */
 else return(0);
}

/********************************************************** OPTIONS MENU **********************************************************/

void optionsMenu()										       /* writes the options menu */
{
 char	string[30];										   /* maximum string length of 30 */
 int	i = 0;													       /* counter */

 strcpy(string, "Create"); 		XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));	
 strcpy(string, "SuperEllipse");	XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Adjust");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "Bulge");		XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Create");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "Polyline");		XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Close");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "Polyline");		XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Move");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "Vertex");		XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Spline");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "Polyline");		XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Refresh");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "Screen");		XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Move");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "Object");		XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Rotate");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "Object");		XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Quit");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "");			XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "Enter Degrees");	XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "or Bulge Below");	XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, " .1   |  .5");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "");			XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "  1   |   2");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "");			XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, "  5   |  10");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));
 strcpy(string, "");			XDrawString(display, win, gc1, menu_left, i += menu_contents, string, strlen(string));
 strcpy(string, " 30   |  90");		XDrawString(display, win, gc1, menu_left, i += menu_seperator, string, strlen(string));

 for(i = menu_top; i <= menu_bottom; i += menu_line)
  {
   XDrawLine(display, win, gc1, menu_left, i, menu_right, i);
  }
 XDrawLine(display, win, gc1, menu_right, menu_top, menu_right, menu_bottom);
}

/********************************************************* MENU OR SCREEN *********************************************************/

void menuScreen()
{
 int		i, bp;										 /* mouse button that was pressed */
 int		objectNum;								 			 /* object number */
 tRealPoint	c, mp, pivot, oldpt, objectVertex;				  /* coordinates of mouse button that was pressed */
 int		item, sub_item;									      	     /* menu item numbers */

 while(infinite) 						 	     		/* keep running until quit button pressed */
  {
   locate(&mp, &bp);											         /* get menu item */

/*********************************************************** MENU ITEM  ***********************************************************/

   if(mp.x < menu_right) 									       /* make sure its an option */
    {
     item = (mp.y / menu_line) + 1;									     /* get the menu item */
     switch(item)
      {
       case 1:											    	   /* Create Superellipse */
        rubberRect(item, &objectNum, &mp, &pivot);							 /* draw rubber rectangle */
        storeObj(item, objectNum, &mp, &pivot);								/* store the ellipse data */
        ellipse(objectNum);										      /* draw the ellipse */
        hotSpots(objectNum); 										     /* draw the hotspots */
       break;

       case 2:
        if(!selectedObj(&objectNum)) continue;								/* get the closest object */
        if(objects[objectNum]->type == polyline) continue;					        /* don't bulge a polyline */
        do
         {
          locate(&mp, &bp);										      /* get bulge number */
          sub_item = (mp.y / menu_line) + 1;

          if(bp == 2) break;

          if	  ((sub_item == 12) && (mp.x < (menu_right / 2))) objects[objectNum]->item.bulge = 0.1;	     /* load bulge number */
	  else if  (sub_item == 12) 				  objects[objectNum]->item.bulge = 0.5;
          else if ((sub_item == 13) && (mp.x < (menu_right / 2))) objects[objectNum]->item.bulge = 1.0;
          else if  (sub_item == 13) 				  objects[objectNum]->item.bulge = 2.0;
          else if ((sub_item == 14) && (mp.x < (menu_right / 2))) objects[objectNum]->item.bulge = 5.0;
          else if  (sub_item == 14)				  objects[objectNum]->item.bulge = 10.0;
          else if ((sub_item == 15) && (mp.x < (menu_right / 2))) objects[objectNum]->item.bulge = 30.0;
          else     						  objects[objectNum]->item.bulge = 90.0;

          ellipse(objectNum);								       /* draw the ellipse with new bulge */
          refresh(objectNum);										        /* refresh screen */
         } while(bp != 2);
       break;

       case 3:											 	       /* Create Polyline */
        rubberLine(item, &objectNum, &mp, &pivot);							     /* draw the polyline */
        storeObj(item, objectNum, &mp, &pivot);							       /* store the polyline data */
        hotSpots(objectNum); 										     /* draw the hotspots */
       break;

       case 4:													/* close polyline */
        if(!selectedObj(&objectNum)) continue;								/* get the closest object */
        if(objects[objectNum]->type == superellipse) continue;					    /* don't close a superellipse */
        t_ptr = objects[objectNum]->item.vertex;
        if(t_ptr != NULL) 
         {
          oldpt = t_ptr->p;
          while(t_ptr->next != NULL)
           {
            t_ptr = t_ptr->next;
            mp = t_ptr->p;
           }
          XDrawLine(display, win, gc2, (int)oldpt.x, (int)oldpt.y, (int)mp.x,  (int)mp.y);		     /* draw closing line */
          t_ptr->next = calloc(1, sizeof(struct vertex_list));
          t_ptr = t_ptr->next;
          t_ptr->p = oldpt;        
         } 
       break;

       case 5:												  /* Move Polyline Vertex */
        if(!selectedObj(&objectNum)) continue;								/* get the closest object */
        if(objects[objectNum]->type == superellipse) continue;				      /* don't move a superellipse vertex */
	moveVertex(objectNum);										       /* move the vertex */
        hotSpots(objectNum); 										     /* draw the hotspots */
        refresh(objectNum);										        /* refresh screen */
       break;

       case 6:												       /* Spline Polyline */
        if(!selectedObj(&objectNum)) continue;								/* get the closest object */
        if(objects[objectNum]->type == superellipse) continue;					 /* don't B-Spline a superellipse */
        t_ptr = objects[objectNum]->item.vertex;
        L = 0;
        while(t_ptr != NULL)
         {
	  P[L++] = t_ptr->p;								       /* load the array with vertex data */
          t_ptr = t_ptr->next;
	 }
        drawSpline(0, 4, --L);										     /* draw the B-Spline */
       break;

       case 7:												  	/* Refresh Screen */
        refresh(objectNum);										        /* refresh screen */
       break;

       case 8:
        if(!selectedObj(&objectNum)) continue;								/* get the closest object */
        moveObject(objectNum);										       /* move the object */
        refresh(objectNum);										        /* refresh screen */
       break;

       case 9:												 	 /* Rotate Object */
        if(!selectedObj(&objectNum)) continue;								/* get the closest object */
        do
         {
          locate(&mp, &bp);										      /* get angle number */
          sub_item = (mp.y / menu_line) + 1;

          if(bp == 2) break;

          if	  ((sub_item == 12) && (mp.x < (menu_right / 2))) objects[objectNum]->angle = 0.1;	     /* load angle amount */
	  else if  (sub_item == 12) 				  objects[objectNum]->angle = 0.5;
          else if ((sub_item == 13) && (mp.x < (menu_right / 2))) objects[objectNum]->angle = 1.0;
          else if  (sub_item == 13) 				  objects[objectNum]->angle = 2.0;
          else if ((sub_item == 14) && (mp.x < (menu_right / 2))) objects[objectNum]->angle = 5.0;
          else if  (sub_item == 14)				  objects[objectNum]->angle = 10.0;
          else if ((sub_item == 15) && (mp.x < (menu_right / 2))) objects[objectNum]->angle = 30.0;
          else     						  objects[objectNum]->angle = 90.0;

          rotateObj(objectNum);										     /* rotate the object */
          refresh(objectNum);										        /* refresh screen */
         } while(bp != 2);
       break;

       case 10:													   /* QUIT Option */
        XFreeGC(display, gc1);                                                   /* Frees all memory associated with GC on server */
        XFreeGC(display, gc2);                                                   /* Frees all memory associated with GC on server */
        XCloseDisplay(display);                                            /* Disconnect client program from X server and display */
        exit(0);                                                                                                  /* Exit program */
       break;
      }														    /* End switch */
    }													    /* End if screen item */
   else
    {
     if(!closestObj(&objectNum, &objectVertex, &mp)) continue;						/* get the closest object */

     if(bp == 1) continue;
     else if(bp == 2) continue;
     else if(bp == 3) 										/* if button 3 then delete object */
      {
       objects[objectNum] = NULL;
       XClearWindow(display,win);   								    /* clears the entire X window */
       optionsMenu();		 								       /* writes the options menu */
       drawObj();											     /* draws the objects */
      }
    }
  }													    /* End while infinite */
}													          /* End function */

/******************************************************* RUBBER RECTANGLE *********************************************************/

void rubberRect(int item, int *objectNum, tRealPoint *mp, tRealPoint *pivot)
{
 int		i, bp;										 /* mouse button that was pressed */
 tRealPoint	oldpt;								  /* coordinates of mouse button that was pressed */

 sleep(1);										   /* slow program down to execute X call */
 while(XCheckMaskEvent(display, ButtonPressMask | ButtonReleaseMask | PointerMotionMask, &event)); /* removes all the MASK events */
 do{locate(mp, &bp);} while (mp->x < menu_right); 	 						   /* get 1st coordinates */
 *pivot = *mp; oldpt = *mp;  									   /* set up pivot and old points */
 
 while(locateCP(mp, &bp))										      /* get new location */
  {
   if(memcmp(mp, &oldpt, sizeof(oldpt)))							    /* check if pointer has moved */
    {
     XDrawLine(display, win, gc2, (int)pivot->x, (int)pivot->y, (int)oldpt.x,  (int)pivot->y);		 /* draw rubber rectangle */
     XDrawLine(display, win, gc2, (int)oldpt.x,  (int)pivot->y, (int)oldpt.x,  (int)oldpt.y);	       /* clear it off the screen */
     XDrawLine(display, win, gc2, (int)oldpt.x,  (int)oldpt.y,  (int)pivot->x, (int)oldpt.y);
     XDrawLine(display, win, gc2, (int)pivot->x, (int)oldpt.y,  (int)pivot->x, (int)pivot->y);

     XDrawLine(display, win, gc2, (int)pivot->x, (int)pivot->y, (int)mp->x,    (int)pivot->y);		 /* draw rubber rectangle */
     XDrawLine(display, win, gc2, (int)mp->x,    (int)pivot->y, (int)mp->x,    (int)mp->y);
     XDrawLine(display, win, gc2, (int)mp->x,    (int)mp->y,    (int)pivot->x, (int)mp->y);
     XDrawLine(display, win, gc2, (int)pivot->x, (int)mp->y,    (int)pivot->x, (int)pivot->y);
     oldpt = *mp;											       /* reset old point */
    }
  }
 i = get_table_entry(); if(i < 0) exit(0); objects[i] = calloc(1, sizeof(tObject));    		       	       /* allocate object */
 objects[i]->item.bulge = 2.0;
 *objectNum = i;

 XDrawLine(display, win, gc2, (int)pivot->x, (int)pivot->y, (int)oldpt.x,  (int)pivot->y);		 /* draw rubber rectangle */
 XDrawLine(display, win, gc2, (int)oldpt.x,  (int)pivot->y, (int)oldpt.x,  (int)oldpt.y);	       /* clear it off the screen */
 XDrawLine(display, win, gc2, (int)oldpt.x,  (int)oldpt.y,  (int)pivot->x, (int)oldpt.y);
 XDrawLine(display, win, gc2, (int)pivot->x, (int)oldpt.y,  (int)pivot->x, (int)pivot->y);
 deselectAll();
 objects[i]->selected = 1;											 /* select object */
}

/********************************************************* RUBBER LINE ************************************************************/

void rubberLine(int item, int *objectNum, tRealPoint *mp, tRealPoint *pivot)
{
 int		button, bp, i, j = 0;								 /* mouse button that was pressed */
 tRealPoint	oldpt;								  /* coordinates of mouse button that was pressed */
 int 		left, top, right, bottom;								 /* polyline bounding box */

 left = 0; top = 0; right = MAXX; bottom = MAXY;							   /* initialize hotspots */

 i = get_table_entry(); 	if(i < 0) exit(0);	objects[i] = calloc(1, sizeof(tObject));

 while(XCheckMaskEvent(display, ButtonPressMask | ButtonReleaseMask | PointerMotionMask, &event)); /* removes all the MASK events */
 locate(mp, &bp); 		 	 								   /* get 1st coordinates */
 *pivot = *mp; oldpt = *mp;  									   /* set up pivot and old points */

 while(infinite)
  {
   button = locateCPv(mp, &bp);
   if(mp->x > menu_right)
    {
     if(button == 0)										/* rubber line and follow pointer */
      {
       XDrawLine(display, win, gc2, (int)pivot->x, (int)pivot->y, (int)oldpt.x, (int)oldpt.y);
       XDrawLine(display, win, gc2, (int)pivot->x, (int)pivot->y, (int)mp->x, (int)mp->y);
       oldpt = *mp;											       /* reset old point */
      }
     else if(button == 1)								 /* rubber line and follow button 1 press */
      {
       if(objects[i]->item.vertex == NULL)
        {
         objects[i]->item.vertex = calloc(1, sizeof(struct vertex_list));
         t_ptr = objects[i]->item.vertex;
         left = pivot->x; top = pivot->y; right = pivot->x; bottom = pivot->y;
        }
       else
        {
         t_ptr = objects[i]->item.vertex;
         while(t_ptr->next != NULL) t_ptr = t_ptr->next;
         t_ptr->next = calloc(1, sizeof(struct vertex_list));         
         t_ptr = t_ptr->next;
        }

       t_ptr->p.x = pivot->x; t_ptr->p.y = pivot->y; t_ptr->next = NULL;
       if(pivot->x < left)  left  = pivot->x; if(pivot->y < top)    top    = pivot->y; 
       if(pivot->x > right) right = pivot->x; if(pivot->y > bottom) bottom = pivot->y;
       *pivot = *mp; 
       j++;
      }
     else if(button == 2) 										  /* end polyline drawing */
      {
       if(j <= 1) {fprintf(stderr, "Number of Points must be greater than 1"); exit(0);}

       XDrawLine(display, win, gc2, (int)pivot->x, (int)pivot->y, (int)oldpt.x, (int)oldpt.y);

       t_ptr = objects[i]->item.vertex;
       while(t_ptr->next != NULL) t_ptr = t_ptr->next;
       t_ptr->next = calloc(1, sizeof(struct vertex_list));         
       t_ptr = t_ptr->next;

       t_ptr->p.x = pivot->x; t_ptr->p.y = pivot->y; t_ptr->next = NULL;
       if(pivot->x < left)  left  = pivot->x; if(pivot->y < top)    top    = pivot->y; 
       if(pivot->x > right) right = pivot->x; if(pivot->y > bottom) bottom = pivot->y;
       break;
      }
    }
  }
 mp->x = right; mp->y = bottom; pivot->x = left; pivot->y = top;
 *objectNum = i;
 deselectAll();
 objects[i]->selected = 1;											 /* select object */
}

/********************************************************* STORE OBJECT ***********************************************************/

void storeObj(int item, int objectNum, tRealPoint *mp, tRealPoint *pivot)
{
 tRealPoint c;

 objects[objectNum]->rectp1.x = pivot->x; objects[objectNum]->rectp1.y = pivot->y;			     /* store pivot point */
 objects[objectNum]->rectp2.x = mp->x;    objects[objectNum]->rectp2.y = mp->y;				     /* store mouse point */

 c.x = (((mp->x - pivot->x) / 2) + pivot->x); 	c.y = (((mp->y - pivot->y) / 2) + pivot->y);	      /* compute the center point */

 objects[objectNum]->type = item; objects[objectNum]->angle = 0.0; 					     /* default angle = 0 */

 objects[objectNum]->handles[0].x = c.x;	objects[objectNum]->handles[0].y = c.y;			  	  /* center point */
 objects[objectNum]->handles[1].x = pivot->x;	objects[objectNum]->handles[1].y = pivot->y;			     /* hot spots */
 objects[objectNum]->handles[2].x = c.x;	objects[objectNum]->handles[2].y = pivot->y;
 objects[objectNum]->handles[3].x = mp->x;	objects[objectNum]->handles[3].y = pivot->y;
 objects[objectNum]->handles[4].x = mp->x;	objects[objectNum]->handles[4].y = c.y;
 objects[objectNum]->handles[5].x = mp->x;	objects[objectNum]->handles[5].y = mp->y;
 objects[objectNum]->handles[6].x = c.x;	objects[objectNum]->handles[6].y = mp->y;
 objects[objectNum]->handles[7].x = pivot->x;	objects[objectNum]->handles[7].y = mp->y;
 objects[objectNum]->handles[8].x = pivot->x;	objects[objectNum]->handles[8].y = c.y;
}

/********************************************************* DRAW OBJECT ************************************************************/

void drawObj()
{
 int i = 0;
 tRealPoint c, oldpt;

 for(i = 0; i < MAXNUM; i++) 
  {
   if(objects[i] == NULL) continue;

   if(objects[i]->type == superellipse)
    {
     ellipse(i);										        /* draw all superellipses */
    }
   else													
    {
     t_ptr = objects[i]->item.vertex;
     if(t_ptr == NULL) continue;
     while(infinite)
      {   
       oldpt.x = t_ptr->p.x; oldpt.y = t_ptr->p.y;			
       t_ptr = t_ptr->next; if(t_ptr == NULL) break;
       XDrawLine(display, win, gc2, (int)oldpt.x, (int)oldpt.y, (int)t_ptr->p.x, (int)t_ptr->p.y);  	    /* draw all polylines */
      }
    }
  }
}

/******************************************************* DRAW THE OBJECT **********************************************************/

void drawTheObj(int objectNum)
{
 int i = 0;
 tRealPoint c, oldpt;

 if(objects[objectNum] == NULL) return;

 if(objects[objectNum]->type == superellipse)
  {
   ellipse(objectNum);										    /* draw a single superellipse */
  }
 else
  {
   t_ptr = objects[objectNum]->item.vertex;
   if(t_ptr == NULL) return;
   while(infinite)
    {   
     oldpt.x = t_ptr->p.x; oldpt.y = t_ptr->p.y;			
     t_ptr = t_ptr->next; if(t_ptr == NULL) break;
     XDrawLine(display, win, gc2, (int)oldpt.x, (int)oldpt.y, (int)t_ptr->p.x, (int)t_ptr->p.y);        /* draw a single polyline */
    }
  }
}

/******************************************************* GET TABLE ENTRY **********************************************************/

int get_table_entry()
{
 int i;

 for(i = 0; i < MAXNUM; i++)
  {
   if(objects[i] == NULL) return(i);					      /* find first available object area to store object */
  }
 return(-1);
}

/******************************************************** DIST SQUARE *************************************************************/

double distsq(tRealPoint *point1, tRealPoint *point2)
{
 double dx, dy;

 dx = pow((abs(point1->x - point2->x)),2); dy = pow((abs(point1->y - point2->y)),2);
 return(sqrt(dx + dy));								 				    /* hypotenuse */
}

/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< N  - the Spline Basis function >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/

double N(int k, int order, double t)								/* code from Professor F. S. Hill */
{
 double denom1, denom2, sum;

 if (order == 1) return (((t < knot[k]) || (t >= knot[k+1])) ? 0.0 : 1.0);
 else {
       sum = (((denom1 = knot[k + order - 1] - knot[k])) > 0.00001) ?
       ((t - knot[k]) * N(k, order - 1, t) / denom1) : 0.0 ;
       sum += (((denom2 = knot[k + order] - knot[k + 1])) > 0.00001) ?
       ((knot[k + order] - t) * N(k + 1, order - 1, t) / denom2) : 0.0 ;
       return sum;
      }
}
/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< drawSpline>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/

void drawSpline(int kind, int order, int L)						 	/* code from Professor F. S. Hill */
{
 double t, tmod, t_max, spln, t_inc = 0.04;	 						  /* set t_inc according to taste */
 tRealPoint p;
 int i;
											  	   /* first build the knot vector */
 if(kind == 0)
  { 		 												 /* open B-spline */
   for (i = 0; i <= L + order; i++) 
        if (i < order)   knot[i] = 0.0;
        else if (i <= L ) knot[i] = knot[i-1] + 1;
      	else knot[i] = L - order + 2;
	t_max= L - order + 2;
  }
 else
  {												        /* closed periodic spline */
   for (i = 0; i <= order; i++)  knot[i] = i;
    	t_max = L + 1;
  }
									 	      /* now generate and draw the B-spline curve */
 for(t = 0; t <= t_max; t += t_inc)
  {
   p.x = 0.0; p.y = 0.0;
   for (i = 0; i <= L; i++)
    {   										      /* form the linear combo. of points */
     if(kind == 0)  spln = N(i, order, t);
     else
      {
       tmod = t - i;
       while (tmod < 0) tmod += (L+1); 									       /* modulo it (L+1) */
       spln = N(0, order, tmod);
      }
     p.x += P[i].x * spln; p.y += P[i].y * spln;
    }    									        		      /* end of inner for */
   if (t == 0.0)
    {
     SplineCP.x = p.x; SplineCP.y = p.y;
    }
   else								   /* moveto((int)p.x,(int) p.y); else lineto((int)p.x,(int)p.y); */
    {
     XDrawLine(display, win, gc2, (int)SplineCP.x, (int)SplineCP.y, (int)p.x, (int)p.y);
     SplineCP.x = p.x; SplineCP.y = p.y;
    }
  }    														    /* end of for */
} 													   /* end of drawSpline() */

/******************************************************** DRAW HOTSPOTS ***********************************************************/

void hotSpots(int objectNum)
{
 int i, j, k, xa, ya, xb, yb;
 int sizepoint = 5;

 for(i = 1; i <= maxHotspots; i++)
  {
   xa = (int)objects[objectNum]->handles[i].x; ya = (int)objects[objectNum]->handles[i].y;

   xb = xa+sizepoint; XDrawLine(display, win, gc2, xa, ya, xb, ya); xa = xb;
   yb = ya+sizepoint; XDrawLine(display, win, gc2, xa, ya, xb, yb); ya = yb;
   xb = xa-sizepoint; XDrawLine(display, win, gc2, xa, ya, xb, yb); xa = xb;
   yb = ya-sizepoint; XDrawLine(display, win, gc2, xa, ya, xb, yb); ya = yb;
  }
}

/******************************************************* CLOSEST OBJECT ***********************************************************/

int closestObj(int *objectNum, tRealPoint *objectVertex, tRealPoint *mp)
{
 int i, j, oldobjectNum;
 double dsq, bestdsq, bestobj;
 tRealPoint bestVertex, p;
 int found = 0;

 bestdsq = 10000;								   /* set the farthest object, should be infinite */
 bestobj = -1;												      /* set object index */
 oldobjectNum = *objectNum;
 deselectAll();

 for(i = 0; i < MAXNUM; i++)
  {
   if(objects[i] != NULL)
    {
     found = 1;
     for(j = 1; j <= maxHotspots; j++)
      {
       if((dsq = distsq(mp, &objects[i]->handles[j])) < bestdsq)
        {
         bestdsq = dsq;										  	      /* reduced distance */
         bestobj = i;										     /* closest object to pointer */
        }
      }

    }
  }
 *objectNum = bestobj;
 *objectVertex = bestVertex;

 if(found)
  {
   objects[*objectNum]->selected = 1;
   hotSpots(*objectNum);
  }
 return(found);
}

/****************************************************** DESELECT OBJECT ***********************************************************/

void deselectAll()
{
 int i;

 for(i = 0; i < MAXNUM; i++)
  {
   if(objects[i] != NULL)
    {
     if(objects[i]->selected == 1) hotSpots(i);
     objects[i]->selected = 0;										  /* deselect all objects */
    }   
  }
}

/****************************************************** SELECT THE OBJECT *********************************************************/

int selectedObj(int *objectNum)
{
 int i;

 for(i = 0; i < MAXNUM; i++)
  {
   if(objects[i] != NULL)
    {
     if(objects[i]->selected == 1)
     *objectNum = i;      										      /* select an object */
     return(1);     
    }   
  }
return(0);
}

/********************************************************* MOVE VERTEX ************************************************************/

void moveVertex(int objectNum)
{
 int		button, bp, i;	
 tRealPoint	cp, np, pp, mp, oldpt;
 int 		left, top, right, bottom;
 double 	dsq, bestdsq, bestobj;


 struct vertex_list 	*bestVertex, *c_ptr, *p_ptr, *n_ptr;

 bestdsq = 10000;								   /* set the farthest object, should be infinite */

 left  = objects[objectNum]->rectp1.x; top    = objects[objectNum]->rectp1.y;
 right = objects[objectNum]->rectp2.x; bottom = objects[objectNum]->rectp2.y;

 sleep(1);										   /* slow program down to execute X call */
 while(XCheckMaskEvent(display, ButtonPressMask | ButtonReleaseMask | PointerMotionMask, &event)); /* removes all the MASK events */
 do{locate(&mp, &bp);} while (mp.x < menu_right); 	 		 				   /* get 1st coordinates */

 c_ptr = objects[objectNum]->item.vertex;
 while(c_ptr != NULL)
  {
   if((dsq = distsq(&mp, &c_ptr->p)) < bestdsq)
    {
     bestVertex = c_ptr;									     /* located the closest point */
     bestdsq = dsq;
   }
   c_ptr = c_ptr->next;
  }
 cp = bestVertex->p;											     /* best vertex found */

 if(bestVertex == objects[objectNum]->item.vertex)							 /* first vertex selected */
  {
   np = bestVertex->next->p;
   n_ptr = bestVertex->next;
   pp = np;
   p_ptr = n_ptr;
   XDrawLine(display, win, gc2, (int)np.x, (int)np.y, (int)cp.x, (int)cp.y);
   XDrawLine(display, win, gc2, (int)np.x, (int)np.y, (int)mp.x, (int)mp.y);
  }
 else if(bestVertex->next == NULL)									  /* last vertex selected */
  {
   c_ptr = objects[objectNum]->item.vertex;
   while(c_ptr != NULL)
    {
     if(c_ptr->next == bestVertex)
      {
       pp = c_ptr->p;
       p_ptr = c_ptr;
       np = pp;
       n_ptr = p_ptr;
       break;
      }
     c_ptr = c_ptr->next;
    }
   XDrawLine(display, win, gc2, (int)np.x, (int)np.y, (int)cp.x, (int)cp.y);
   XDrawLine(display, win, gc2, (int)np.x, (int)np.y, (int)mp.x, (int)mp.y);
  }
 else													/* middle vertex selected */
  {
   c_ptr = objects[objectNum]->item.vertex;
   while(c_ptr != NULL)
    {
     if(c_ptr->next == bestVertex)
      {
       pp = c_ptr->p;
       p_ptr = c_ptr;
       break;
      }
     c_ptr = c_ptr->next;
    }
   np = bestVertex->next->p;
   n_ptr = bestVertex->next;
   XDrawLine(display, win, gc2, (int)pp.x, (int)pp.y, (int)cp.x, (int)cp.y);
   XDrawLine(display, win, gc2, (int)np.x, (int)np.y, (int)cp.x, (int)cp.y);
   XDrawLine(display, win, gc2, (int)pp.x, (int)pp.y, (int)mp.x, (int)mp.y);
   XDrawLine(display, win, gc2, (int)np.x, (int)np.y, (int)mp.x, (int)mp.y);
  }
 oldpt = mp;												       /* reset old point */
 while(locateCP(&mp, &bp))										      /* get new location */
  {
   if(memcmp(&mp, &oldpt, sizeof(oldpt)))							    /* check if pointer has moved */
    {
     XDrawLine(display, win, gc2, (int)pp.x, (int)pp.y, (int)oldpt.x, (int)oldpt.y);
     XDrawLine(display, win, gc2, (int)np.x, (int)np.y, (int)oldpt.x, (int)oldpt.y);
     XDrawLine(display, win, gc2, (int)pp.x, (int)pp.y, (int)mp.x, (int)mp.y);
     XDrawLine(display, win, gc2, (int)np.x, (int)np.y, (int)mp.x, (int)mp.y);
     oldpt = mp;											       /* reset old point */
    }
  }
 bestVertex->p = mp;

 if(mp.x < left)   objects[objectNum]->rectp1.x = mp.x;
 if(mp.y < top)    objects[objectNum]->rectp1.y = mp.y;
 if(mp.x > right)  objects[objectNum]->rectp2.x = mp.x;
 if(mp.y > bottom) objects[objectNum]->rectp2.y = mp.y;
}

/******************************************************* ROTATE OBJECT ************************************************************/

void rotateObj(int objectNum)
{
 int		i;
 tRealPoint	c, p, q;
 double		sn, cs;
	
 drawTheObj(objectNum);										    /* erase the object on screen */
 hotSpots(objectNum); 

 cs = cos(objects[objectNum]->angle * radiansperdegree); sn = sin(objects[objectNum]->angle * radiansperdegree);

 c.x = objects[objectNum]->handles[0].x; c.y = objects[objectNum]->handles[0].y;				  /* center point */

 for(i = 1; i <= maxHotspots; i++)
  {
   p.x = objects[objectNum]->handles[i].x; p.y = objects[objectNum]->handles[i].y;				     /* hot spots */
   q.x = ((p.x - c.x) * cs) - ((p.y - c.y) * sn) + c.x;						 /* compute new rotated hot spots */
   q.y = ((p.x - c.x) * sn) + ((p.y - c.y) * cs) + c.y;
   objects[objectNum]->handles[i].x = q.x; objects[objectNum]->handles[i].y = q.y;				     /* hot spots */
  }

 if(objects[objectNum]->type == superellipse)
  {
   ellipse(objectNum);												  /* call ellipse */
  }
 else		
  {
   t_ptr = objects[objectNum]->item.vertex;
   while(t_ptr != NULL)
    {
     p.x = t_ptr->p.x; p.y = t_ptr->p.y;
     q.x = ((p.x - c.x) * cs) - ((p.y - c.y) * sn) + c.x;					  /* compute new rotated vertexes */
     q.y = ((p.x - c.x) * sn) + ((p.y - c.y) * cs) + c.y;
     t_ptr->p.x = q.x; t_ptr->p.y = q.y;
     t_ptr = t_ptr->next;
    }
  }
 drawTheObj(objectNum);											       /* draw new object */
 hotSpots(objectNum); 
}

/********************************************************* MOVE OBJECT ************************************************************/

void moveObject(int objectNum)
{
 int		button, bp, i;	
 tRealPoint	cp, np, pp, mp, oldpt;
 int 		left, top, right, bottom;
 double 	dx, dy, dsq, bestdsq, bestobj;

 struct vertex_list 	*bestVertex, *c_ptr, *p_ptr, *n_ptr;

 bestdsq = 10000;								   /* set the farthest object, should be infinite */
 left = 0; top = 0; right = MAXX; bottom = MAXY;

 sleep(1);										   /* slow program down to execute X call */
 while(XCheckMaskEvent(display, ButtonPressMask | ButtonReleaseMask | PointerMotionMask, &event)); /* removes all the MASK events */
 do{locate(&mp, &bp);} while (mp.x < menu_right); 	 		 								   /* get 1st coordinates */

 oldpt = objects[objectNum]->handles[0];
 while(locateCP(&mp, &bp))										      /* get new location */
  {
   if(memcmp(&mp, &oldpt, sizeof(oldpt)))							    /* check if pointer has moved */
    {
     dx = mp.x - oldpt.x; dy = mp.y - oldpt.y;
     hotSpots(objectNum);
     drawTheObj(objectNum);

     for(i = 0; i <= maxHotspots; i++)
      {
       objects[objectNum]->handles[i].x = objects[objectNum]->handles[i].x + dx;		          /* compute new location */
       objects[objectNum]->handles[i].y = objects[objectNum]->handles[i].y + dy;
      }

     if(objects[objectNum]->type != superellipse)
      {
       t_ptr = objects[objectNum]->item.vertex;
       while(t_ptr != NULL)
        {
         t_ptr->p.x = t_ptr->p.x + dx; t_ptr->p.y = t_ptr->p.y + dy;
         t_ptr = t_ptr->next;
        }
      }
    }
   drawTheObj(objectNum);
   hotSpots(objectNum);
   oldpt = mp;
  }
 drawTheObj(objectNum);
 hotSpots(objectNum);
}

/******************************************************** REFRESH SCREEN **********************************************************/

void refresh(int objectNum)
{
 XClearWindow(display,win);  	 								    /* clears the entire X window */
 optionsMenu();		 									       /* writes the options menu */
 drawObj();									      /* draws the objects from the options table */
 hotSpots(objectNum); 									/* draws the hot spots of selected object */
}

/******************************************************** DRAW ELLIPSE ************************************************************/

void ellipse(int objectNum)						/* x(t) = A * cos(t)**2/bulge, x(t) = B * sin(t)**2/bulge */
{
 int 		i;
 int		num = 50;
 tRealPoint	c, p, q;
 double 	sweep = 360;
 double 	delta, cs, csa, csp, sn, sna, snp, S, T;
 double 	angle, bulge, radiusa, radiusb;

 c.x   = objects[objectNum]->handles[0].x; 	c.y = objects[objectNum]->handles[0].y;
 angle = objects[objectNum]->angle; 	 	bulge = objects[objectNum]->item.bulge;
 radiusa  = abs(objects[objectNum]->handles[4].x - objects[objectNum]->handles[8].x) / 2;
 radiusb  = abs(objects[objectNum]->handles[2].y - objects[objectNum]->handles[6].y) / 2;

 delta = radiansperdegree * sweep / num; T = tan(delta / 2); S = sin(delta); 
 sn = sin(radiansperdegree  * angle); cs = cos(radiansperdegree * angle);
 RealCP.x = c.x + radiusa * cs; RealCP.y = c.y + radiusb * sn;

 for(i = 1; i <= num; i++)
  {
   sn = T * cs + sn;
   cs = cs - S * sn;
   sn = T * cs + sn;
   if(sn < 0) snp = (-1.0) * (pow((fabs(sn)), (2 / bulge))); else snp = pow(fabs(sn), (2 / bulge));
   if(cs < 0) csp = (-1.0) * (pow((fabs(cs)), (2 / bulge))); else csp = pow(fabs(cs), (2 / bulge));
   p.x = c.x + radiusa * csp; p.y = c.y + radiusb * snp;

   csa = cos(objects[objectNum]->angle * radiansperdegree); sna = sin(objects[objectNum]->angle * radiansperdegree);
   q.x = ((p.x - c.x) * csa) - ((p.y - c.y) * sna) + c.x; q.y = ((p.x - c.x) * sna) + ((p.y - c.y) * csa) + c.y;

   XDrawLine(display, win, gc2, (int)RealCP.x, (int)RealCP.y, (int)q.x, (int)q.y);
   RealCP.x = q.x; RealCP.y = q.y;
  }
}
