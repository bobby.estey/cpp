Robert Estey 
Email:  estey@mosvax.sps.mot.com  Phone:  (602) 655-2391  Fax:  (602) 655-3569
UMASS ECE660 / NTU ST740A   Program 2                                         

SUPERDRAW PROGRAM - README TEXT FILE

Key:  	MB1 - Mouse Button 1 (Left)
	MB2 - Mouse Button 2 (Center)	
	MB3 - Mouse Button 3 (Right)



General X Note (options 1, 5 and 8):

I couldn't find the appropriate X call to clear the X server buffer so I used 
the sleep command along with the XCheckMaskEvent call.  This call clears all 
buffered X calls but this takes a while to perform.  It seems that there is a
spawned process when clearing the server and the computer keeps computing.

PLEASE WAIT 5 SECONDS BEFORE DOING OPTIONS 1 (Create SuperEllipse), OPTION 5
(Move Vertex) AND 8 (Move Object).

THERE IS NO KEYPAD USED IN THE PROGRAM.  ALL USER FUNCTIONALITY IS PERFORMED 
TOTALLY BY A MOUSE.



GENERAL INFORMATION:

Options Menu:	The options menu is on the left side of the screen.  All
		object creations and specific functions are performed at this
		location.

Drawing Area:	This is to the right of the Options Menu.  All drawings and
		MB (Mouse Button) coordination is performed at this location.

Degrees & Bulge 
Submenu:	This menu has 8 values you can use for bulge or degree 
		parameters.  MB controlled.  NOTE:  The submenu is an infinite
		loop until MB2 is pressed.

Bounding Box:	The bounding box is shown by the hotspots.  The hotspots also 
		show which object is selected in the drawing area.

Select Object:	Move the mouse to the drawing area and press MB1.

Delete Object:	Select an object with MB1 in the drawing area.  Press MB3 and 
		the object will be removed from the object list and screen.



OPTIONS MENU:

Create Superellipse (option 1):

Please read the General X Note.  Select the Create Superellipse option.  Move
to the drawing area and press MB1 down (pivot).  Drag mouse to final destination
then let go.  This will write the superellipse and store it in the object table.

Adjust Bulge (option 2):

Select the Superellipse in the drawing area with MB1.  Then press the Adjust 
Bulge button with MB1.  Then press the desired bulge factor.  This option 
leaves you in an infinite loop making bulge changes until you get a desired 
result.  Press MB2 to leave the bulge menu.

Create Polyline (option 3):

Select the Create Polyline option.  Move to the drawing area and press MB1
down (pivot).  Drag mouse to next vertex destination in the drawing area and
release the mouse button.  This will put the vertex in a linklist.  Press
MB1 down again and drag to another location.  Repeat steps until finished with
polyline.  When finished press MB2.  This will end the polyline and write the 
object into the object table.

Close Polyline (option 4):

Select a Polyline in the drawing area by pressing MB1.  Then press Close 
Polyline option.  This will close the selected polyline and update the objects
table.

Move Vertex (option 5):

Please read the General X Note.  Select a polyline with MB1 in the drawing area.
Select the Move Vertex option.  Hold MB1 down until appropriate position is 
found and release.  This will update the objects table.

Spline Polyline (option 6):

Select a polyline with MB1 in the drawing area.  Select Spline Polyline.
This will draw the B-Spline of the selected polyline.

Refresh Screen (option 7):

This option clears the screen and reads objects from the options table and
redraws them.

Rotate Object:

Select an object with MB1 in the drawing area.  Select Rotate Object.  Then 
press the desired degree of rotation in the submenu.  This call puts you in an 
infinite loop until MB2 is pressed.  This will rotate the object according to 
the degree input you selected.

Quit:  Exit the program.
