								   /* ECE661 program to test boolean list merging, fsh, 4/26/94 . */

/* Combine takes two arrays of hit times (each terminated with a sentinel -1), and returns the array of hit times (-2 terminated) */
												/* for the given boolean operator */
#include 	<stdio.h>
#include 	<stdlib.h>
#define 	NUM 		20
#define 	DBL 		double

#define 	fmt 		"\n******* operator = %d"

#define 	OPEN(fname,fp) 	{if(!(fp = fopen(fname,"w")))\
        			{puts("\n can't open it!"); exit(-1);}}

typedef 	enum 		{UNION,INTERSECTION,DIFFERENCE} OPS;

void		Combine(DBL A[], DBL B[], DBL D[], OPS opr); 							     /* prototype */

void 		makeList( DBL A[]);
void 		printList(char ch, DBL a[]);

FILE 		*fp;

/************************************************************ COMBINE *************************************************************/

void Combine(DBL A[], DBL B[], DBL D[],OPS opr)
{
 DBL C[2 * NUM];
 int Cin,S[2 * NUM];
 int i = 0, j = 0, k = -1;
 int Ain = 0, Bin = 0; 											    /* both initially out */
 while(A[i] > 0 && B[j] > 0)  										     /* take 'next' event */
  {
   if(A[i] <= B[j])
    {
     C[++k] = A[i++]; 											      /* take A's t-value */
     Ain = ! Ain;
    }
   else
    {
     C[++k] = B[j++];
     Bin = !Bin;
    }
   if(opr == UNION)    S[k] = Ain | Bin;
   else if(opr == INTERSECTION) S[k] = Ain & Bin;
   else if (opr == DIFFERENCE)  S[k] = Ain & !Bin;
  }										       /* Now either A[] or B[] has been consumed */

 if(opr == UNION || opr == DIFFERENCE)
  while(A[i] > 0)     												/* finish off A[] */
   {
    if(Bin !=0) puts("\n Bin bad!");
    C[++k]   = A[i++]; Ain = !Ain;
    S[k] = Ain;     											    /* since Bin = 0 here */
   }

  else if(opr == UNION)
   while(B[j] > 0)  												/* finish off B[] */
    {
     if(Ain != 0) puts("\n Ain bad!");
     C[++k]   = B[j++]; Bin  = !Bin;
     S[k] = Bin; 											    /* since Ain = 0 here */
    }

									       /*  Scan S[] array to eliminate abutting intervals */

   S[++k] = -2; 												 /* terminate S[] */
   for(i=0, j = -1, Cin = 0; S[i] > -1; i++)
    if(S[i] != Cin)
     {
      D[++j] = C[i];  Cin = !Cin;
     }
    D[++j]=-2; /* terminate it */
}														       /* Combine */

/************************************************************ PRINT LIST **********************************************************/

void printList(char ch,DBL a[])
{								     /* print result hit list, and put in file for later scrutiny */
 int i= 0;
 for(i= 0; a[i] > 0; i++)
  {
   fprintf(fp, "\n %c[%2d] = %6.2lf", ch,i, a[i]);
   printf("\n %c[%2d] = %6.2lf", ch,i, a[i]);
  }
}

/************************************************************ MAKE LIST ***********************************************************/

void makeList(DBL A[])
{
 int last,i; 											  /* make t-list, even # of times */
 do{last = rand()% NUM;} while(last %2 !=0);
 A[0] = rand()/10000.0;  										     /* some initial time */
 for(i = 1; i <last; i++)
 A[i]= A[i-1] + rand()/8000.0;
 A[last] = -2;  										    /* make last one a terminator */
}

/************************************************************ MAIN ****************************************************************/

main()  									      /* test program for combining boolean lists */
{								       /* user gives a seed; random lists generated and processed */
 OPS opr;
 int m,i, last, seed;
 DBL A[NUM], B[NUM], D[NUM];					
 
 puts("\n give seed");  scanf("%d",&seed);  						       /* generate sample (random) tlists */
 if(seed < 0) {fclose(fp); exit(0);}
 srand(seed);
 OPEN("bool.dat",fp);
 makeList(A);
 printList('A',A);
 makeList(B);
 printList('B',B);
 for(opr = 0; opr < 3; opr++) /* test all 3 operators */
  {
   fprintf(fp, fmt, opr);
   printf(fmt , opr);
   Combine(A,B,D,opr);
   printList('D',D);
  }
}
