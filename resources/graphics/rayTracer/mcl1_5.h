/*  mcl1_5.h  last revision 5/15/95 by Noel to fix up plane 
lists.
MCL2.0.h   in progress 4/22/95
 *  MCL11.h version 1.1, released 3/13/95
 *  replaces version 1.0, released  March 9, 1995
 * Written by Philip H. Cramer, Steven R. Morin, Noel Llopis, and F.S.Hill.

 * MCL was developed for use in ECE 661 (Advanced Computer Graphics).
 * Permission to use and distribute MCL is granted as long as MCL is not
 * altered in any way.
 *
 * For more information about MCL, contact:
 * Professor Francis S. Hill, ECE Department
 * University of Massachusetts, Amherst, MA  01003
 * Email:  hill@ecs.umass.edu
 *
 */

 /* --------------- changes made since version 1.01 -------
2/26: added enum type tObjTypes to associate names of object types with values.
2/26: added portable type names: BYTE, SHORT, USHORT, LONG, ULONG
2/26: changed tWindow type to use USHORT's for the rectangle
3/2: added inter and stuff fields to tInstance;
3/2: changed named AffineMatrix to tAffine3D: more descriptive.
3/7: added dataFile field in tInstance, to hook filename.
3/7: changed type of color in light to tColor
3/7: added getColor3();same as get3Tup() only returns tColor.
3/7: began conversion to no printf()'s, no exit()'s
 ------------------ end of changes ---------------------*/

#ifndef CRAMER_GRAPH_LANG
/*<<<<<<<<<<<<<<<<< macros >>>>>>>>>>>>>>>>>>>*/
#define CRAMER_GRAPH_LANG
#define IDENTLENGTH     32
#define FILENAMELENGTH  50 /* Watch out for long paths and filenames! */
#define DBL double


#define NEW(type,ptr) { \
	ptr = (struct type *) malloc (sizeof(struct type)); \
	if (!ptr) { \
		strcat(errMsg, "in NEW(), out of memory!!..."); \
		return -1; \
	}}

#define NEWtInstance(ptr) { \
	ptr = (tInstance *) malloc (sizeof(tInstance)); \
	if (!ptr) { \
		strcat(errMsg, "in NEW(), out of memory making a new tInstance!!..."); \
		return -1;} \
	ptr->left = ptr->right = ptr->next = NULL;\
	ptr->planes = NULL; ptr->stuff = NULL; ptr->dataFile = NULL;\
	}


#define MIN(a,b) (((a)<(b))? (a): (b))
#define MAX(a,b) (((a)>(b))? (a): (b))
#define DOT3(a,b) ((a).x * (b).x + (a).y * (b).y + (a).z * (b).z)

#define transformPoint(p,m,q)\
  {q.x = p.x * m[0][0] + p.y * m[1][0] + p.z * m[2][0];\
	q.y = p.x * m[0][1] + p.y * m[1][1] + p.z * m[2][1];\
	q.z = p.x * m[0][2] + p.y * m[1][2] + p.z * m[2][2];}

#define BOOL_UNION   '+'
#define BOOL_INTER   '*'
#define BOOL_DIFF    '-'
#define BOOL_REPLACE '/'

/*<<<<<<<<<<<<<<<<<<<< typedefs >>>>>>>>>>>>>>>>>*/
typedef unsigned short USHORT;  /* must be two-byte integer */
typedef short SHORT;            /* must be two-byte integer */
typedef unsigned long ULONG;    /* must be 4-byte integer */
#ifndef LONG
typedef long int LONG;          /* must be 4-byte integer */
#endif

typedef struct  { DBL x, y, z; } threeTup;  /* store 3-tuples */
typedef struct  { DBL a, b, c, d; } fourTup;
typedef struct  { DBL a, b, c, d, e, f, g; } sevenTup;
typedef struct tColor {DBL r, g, b;} tColor;
typedef struct tColor4 {DBL r, g, b, n;} tColor4;
typedef struct tWindow {SHORT l, r, t, b;} tWindow;
typedef struct cuboid {DBL l,t,r,b,front,back;} tCuboid;
typedef struct sph {DBL cx, cy, cz, rad;} tSphere;
typedef	struct {   /* store a 3x3 matrix, and a 3 tuple translation */
	DBL mat[3][3];
	threeTup tr;
	} tAffine3D;

typedef struct tLight {    /* holds light information */
  tColor color;
  threeTup position;
  struct tLight *next;
  int    index;
} tLight;

typedef struct tCamera {   /*holds all camera information */
  threeTup eye, lookAt, ref, u, v, n;
  DBL eyeDist, beta, tilt, aspect, W, H, viewFar, viewNear;
  short int nRows, nCols;
} tCamera;


typedef struct face{
	threeTup pt;
	} tFacePlane;

typedef struct{
	char name[15];
	int num;
	tFacePlane * face;
	} tPlaneList;

typedef struct tInstance { /* holds info. about instances of objects */
  char name[IDENTLENGTH];					          /* name string */
  short int type;                	 /* shape type */
  char boolOp;                   	 /*   +,*,-,/ or ' ' in no operation   */
  struct tInstance *next,        	 /* pointer to next instance */
  *left, *right;                 	 /* bool tree pointers */
  tColor ambient, color, diffuse;
  tColor4 specular;
  fourTup params;
  sevenTup texture;
  DBL reflection, transparent, speed;
  tAffine3D transf;
  tAffine3D inver;
  tWindow screenExtent;					 /* screen extent rect. */
  tCuboid boxExtent;						 /* box Extent */
  tSphere sphereExtent;				    /* sphee extent */
  tPlaneList *planes;                /* hook for plane list */
  void * stuff;						    /* for future use */
  char * dataFile;	                /* filename */

} tInstance;

typedef struct tSceneDescript {
  tLight *lightptr;              	 /* light pointer */
  tCamera *cameraptr;            	 /* camera pointer */
  tInstance *objectptr;          	 /* object pointer */
  int maxRecursionDepth;
  DBL minShinyness, minTransparency;
  tColor ambient, background;
  tPlaneList * polyList;			    /* array of lists of planes */
} tSceneDescript;

typedef enum objectType{
  tetrahedronObj,cubeObj, octahedronObj, dodecahedronObj,
  icosahedronObj, buckyballObj, diamondObj, polyhedronObj,
  userDefinedObj, squareObj, planeObj, sphereObj, coneObj,
  cylinderObj, taperedCylObj, torusObj, boolObj
} tObjTypes;

/*<<<<<<<<<<<<<< prototypes >>>>>>>>>>>>>>>>>*/
SHORT interpretSceneFile(char *inputFilename, tSceneDescript *scene, char *msg);
SHORT MCLfree (tSceneDescript * scene);
#endif

