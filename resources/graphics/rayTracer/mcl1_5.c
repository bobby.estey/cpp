/* mcl1_5.c.. last revision 5/14/95 by Noel to fix up plane lists
 * Noel's MCL1_3.c on ftp site 4/16/95
 * MCL2.C: Version 2.0 in prep.. 4/6/95
 * to replace MCL1_1, released 3/13/95
 * replaces MCL.C version 1.01 from  March 7, 1995
 * Programmed by Philip H. Cramer, Steven R. Morin, Noel Llopis,and F.S.Hill.

 * MCL was developed for use in ECE 661 (Advanced Computer Graphics).
 * Permission to use and distribute MCL is granted as long as MCL is not
 * altered in any way.
 *
 * For more information about MCL, contact:
 * Professor Francis S. Hill, ECE Department
 * University of Massachusetts, Amherst, MA  01003
 * Email:  hill@ecs.umass.edu  */

 /*<<<<<<<<<<<<<<<<<< updates, changes >>>>>>>>>>>>>>>>>>>>
 MCL v1.1:
 2/9: added CYLINDER entry in initScan(): addToTable(CYLINDER,"CYLINDER");
 2/9: in parseAttributes(): changed two messages from "Non-ambigous" to "ambiguous"
 2/26: fix line 588: if(isdigit(..)): add ( and )
 2/26: line 509: nextChar():change return type from char to signed char
 2/26: line 100: change type of global inputFileChar to signed char: same reason
 2/26: line 842: cam->aspect = 480/(DBL)640, for consistency
 2/26: line 945: add fabs() around camera->n.z;
 2/26: line 1473: add "== True" in while() conditional
 2/26: delete four redundant free()'s in scanCleanUp.
 2/26: added tObjTypes to mcl.h,and changed type 'values'
		in writeObjectToOutput() to these names
 2/26: added DIAMOND type to list of possible types
 2/26: changed internal names (but not user string names):
			NEAR to NEARplane, FAR to FARplane, TRANSPARENT to TRANSPARENTatt,
			so no conflict with Windows built-in terms.
 2/28: in initScan() removed unnecessary shapeList[2] = ...
 2/28: rearranged order of names in tObjTypes (declaredin mcl.h) so
			polyhedralonescome first in order of increasing complexity. This
			lets us read in an array of plane lists, and associate position in array
			with object in question.
 2/28: simplified openInputFile() and moved it to end.
 Also moved outputData() to end.
 2/28: added plane list reader that builds plane lists for polyhedral
		objects.
 3/2: eliminated addToTable(): init'd two arrays to hold symbol
		info,and pushed each pair onto table in turn. (This puts
		symbols into table in reverse order, but order doesn't matter.)
		Also saved space by malloc-ing only as much
		space as each item in list needs for name of symbol.
 3/2: added inter field in tInstance to hold intersection info.
 3/2: added protection in doScale() against illegalfactor of 0.
 3/2: added tapered cylinder object: "TAPEREDCYL", taperedCylObj.
 3/7: interpretSceneFile() scans object list and loads screen extent
		in eachobject. Note: CAMERA must be defined before DISPLAY.
 3/7: interpretSceneFile() scans object list and if an object is a
		polyhedral type it hooks appropriate plane list to object.
 3/7: fixed shear to general form. Removed other shear verbs.
 3/7: began conversion to no printf()'s, no exit()'s, etc.
 3/8: added Noel's preprocessor
 3/9: defined tSymbolType: abbreviation of enumsymbolType.
 3/9: changed param type of expect to tSymbolType so it matches.
 3/9: moved shapeList into parseAttributes().
 3/9: changed expect() to macro EXPECT(): checks whether
		current global Symbol is of required type. If so, it gets
		next symbol and goes on. If not, it writes an error message
		and returns -1 from the function that invokes it.
 3/9: changed syntaxError() to a macro that writes an error message,
		and returns -1 from the function that invokes it.
 3/9: eliminated newObject(): incorporated it into defineObject().

 MCL v1.2:
 4/6: fixed pointer p to polyList[i].face in buildPlaneList()
 4/6: added boxExtent and spherExtent fields in tInstance
 4/6: change name token to inclToken in IncludeFile()
 4/6: eliminated macro CALLOC - not needed.
 4/6: eliminated nextChar() ..changed it to macro (thanks to
		Craig Schomp: nextChar is fgetc(MCLfile)
 4/6: removed 't' from fopen(TEMP_FILENAME,"wt");
 4/6: added: else p->planes = NULL in buildPlaneList().
 4/6: #define'd NEWtInstance macro that sets all 5 pointers to NULL.
 4/6: fixed fscanf("%hd",&numPlanes) in BuildPlaneList().
 4/6: changed ERROR to preERROR so no confilct with Windows built-ins
 4/10: added index field to the tLight structure
 4/10: fixed duplication of last line in each input file
 4/10: fixed duplication of first object in each group (and reworked
		 displayObj()--now objects are entered in backwards order!)
 4/10: error propagation corrected (now only the real error reported).
 4/10: fixed problem with empty #define

 MCL v1.3:
 4/12: added: correct line number and filename when reporting errors
 4/13: added: getString() to parse a string. nextSymbol was changed
		 accordingly.
 4/13: added: reserved word FILE followed by a string, is read and
		 stored in the dataFile field of tInstance (warning: this field
		 is NOT propagated through the tree or group... yet)
 4/13: changed TEXTURE so it takes a 7 tuple instead of a 4 tuple
 4/22: removed global PolyList[] array; put it into tSceneDescript so it can be freed
 4/22: changed size of name array in tPlaneList to only 15
 4/22: changed size of tmpMsg[] from 800 to 300;
 4/22: deleted buildPlaneLists(): incorporated code into interpretSceneFile()
 4/26: fix screen extents for plane and square.
 4/26: in buildBool() fixed init7Tup for texture field
 4/26: cleaned up groupAttributeCopy flow, and 7tup for texture

 MCL v1.4:
 5/8:  Added definitions of boolean operators in MCL.H
 5/8:  Boolean objects now have type boolObj and not -1. All the
       nodes in the boolean tree that are not leaves will have
       the boolObj type.
 5/8:  Transformations are propagated down the boolean tree all the
       way to the leaves.
 5/8:  Added MCLfree() that frees up all memory dynamically allocated by MCL.
 5/11: Modified displayObj() so that boolean objects can be added inside
       groups.

 MCL v1.41:
 5/13: Fixed problem filling the planes pointer in interpretSceneFile()
 5/13: The array of planes (scene->polyList) is now dynamically allocated
 5/13: The planes pointer is now also filled in the case of a boolean object.

 <<<<<<<<<<<<<<<<<<<<<<<<< endof updates, changes >>>>>>>>>>>>>>>>>*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include "mcl1_5.h"
#include <assert.h>
/*#include <conio.h>*/
#define NL '\n'
#define IDENTLENGTH 32
#define ODDNUM -787.3773
#define PI 3.141592654

#define nextChar fgetc(MCLfile)

/* Noel's for preprocessor */
#define preERROR  -1
#define preOK      0
#define SYMT_MAXENTRIES   500
#define STRING_LENGTH     200
#define TEMP_FILENAME     "mcl.tmp"

/* Write an error message, and return
from whichever function they reside in */

#define SYNTAXERR(msg)\
 {sprintf(errMsg,"\nMCL: Syntax error.\n");\
  GetLineFile (MCLlineNumber, errMsg);\
  sprintf (tmpMsg,"\nWanted %s but got token = %s", msg, token);\
  strcat (errMsg,tmpMsg); return -1;}

/* this one checks type of already received symbol, and if ok gets
another one and its calling function proceeds. If previous symbol
is not of ok type, macro returns from whichever function it resides in */

#define EXPECT(symtype, msg)\
  {if(Symbol == symtype)\
		{if(nextSymbol())\
		  return -1;\
  }\
  else{ sprintf(errMsg,"\nMCL: Expect error.\n");\
        GetLineFile (MCLlineNumber, errMsg);\
		  sprintf(tmpMsg,"\nExpected %s but got token = %s", msg, token);\
		strcat(errMsg,tmpMsg); return -1;}}


/*<<<<<<<<<<<<<<<<<< typedefs >>>>>>>>>>>>>>>>>>>*/
typedef enum boolean {False = 0, True = 1} tBool;

typedef enum symbolType{
	DEFINE, GROUP, SCALE,
	TRANSLATE, COLOR, DIFFUSE, DATAFILE, SPECULAR,
	PARAMS, AMBIENT, REFLECT, ROTATE, SHEAR, DISPLAY,
	TRANSPARENTatt, TYPE, TEXTURE, SPEED,

	SPHERE,CONE,CYLINDER,TAPEREDCYL,PLANE,
	SQUARE, TORUS, TETRAHEDRON, CUBE, OCTAHEDRON, DODECAHEDRON,
	ICOSAHEDRON, POLYHEDRON, BUCKYBALL, DIAMOND, USERTYPE, BOOLOBJ,

	BOOLEAN, UNION, INTERSECT, DIFFERENCE,
	REPLACE, LIGHT, AT, BACKGROUND, CAMERA,
	EYE, LOOKAT, EYEDISTANCE, BETA, TILT,
	NCOLS, NROWS, FARplane, NEARplane, ASPECT, LASTSYMBOL,
	USERDEFINED, NUMERAL, OPENCURLY, CLOSECURLY, COMMA, OPENBRACKET,
	CLOSEBRACKET,NOOP, ENDFILE, TEXTSTRING } tSymbolType;

typedef enum typeNode
	{SIMPLEDEF, GROUPDEF, BOOLDEF, DONTKNOW } tNodeType;

/* for Noel's preprocessor */
typedef struct {	
  char * symbol;
  char * value;
} SYMTentry;

typedef struct {
  int       total;
  SYMTentry entry[SYMT_MAXENTRIES];
} SYMT;

typedef struct inclnode {
  LONG   tmpstart, tmpend;          /* Starting and ending tmp file line #s */
  LONG   realstart, realend;        /* Starting and ending real file line #s */
  char   filename[FILENAMELENGTH]; /* Read file name */
  struct inclnode * next;
} INCLNODE;


/* Stores intermediate objects before displaying them */
typedef struct tObject {
	 char name[IDENTLENGTH];       /* object's name */
	 enum symbolType type;         /* sphere,cone, cube...*/
	 enum symbolType boolOp;       /* Boolean operation */
	 struct tObject *next,       /* pointer to next object  */
			  *left,  *right, /* pointer to boolean objects */
			  *group;        /* pointer to group object, if NULL this is
								  a simple object; else it points to the object list. */
	tColor ambient, color, diffuse;
	tColor4 specular;
	fourTup params;
    sevenTup texture;
	DBL reflection, transparent, speed;
	tAffine3D transf;         /* matrix */
	tAffine3D inver;          /* inverse matrix */
    char * dataFile;          /* filename */
} tObject;

typedef struct tSymbolNode {
			tSymbolType symbolVal;
			char *tokenStr; 	/* place for name string */
			struct tSymbolNode *next;
			 } tSymbolNode;

/*<<<<<<<<<<<<<<<<<<<<<< GLOBALS >>>>>>>>>>>>>>>>>>>>>>>*/

SYMT * symt;	         /* noel's: for preprocessor */
INCLNODE * globalincl;   /* noel's: for preprocessor */
INCLNODE * lastincl;     /* noel's: for preprocessor */

tNodeType nodeType, structType;

static FILE *MCLfile;          /* input mcl file */

char *errMsg;  /* where to append error messages */
char tmpMsg[300];	/* to build new part of error message */
tSceneDescript *sceneptr;	/* hook for scene */

static unsigned int MCLlineNumber; /* line number that we are currently scanning */
static signed char inputFileChar = 0; /* stores input character from MCL input file.*/
static DBL number;     /* global variable to hold string/number conversion. */
static char token[80];    /* global variable to store untokenized symbol  */
tSymbolType Symbol;		/* holds type of most recent token gobbled */

/* some global object pointers */
static tObject *objectRoot=NULL, *lastObj=NULL, *globalObjectptr;
static short int numNamelessObjects = 0;      /* used to give [   ] objects a name */
static tSymbolNode *symbolTableptr = NULL;
static tLight *lightList;
static tCamera *camptr, *camera;

static tAffine3D initMatrix = {{{1, 0, 0}, {0, 1, 0},
										  {0, 0, 1}},{0, 0, 0}};
threeTup init3Tup  = {0,0,0};
tColor initColor3  = {0,0,0};
fourTup init4Tup   = {0,0,0,0};
sevenTup init7Tup  = {0,0,0,0,0,0,0};
tColor4 initColor4 = {0,0,0,0};

threeTup odd3Tup  = {ODDNUM, ODDNUM, ODDNUM};
tColor oddColor3  = {ODDNUM, ODDNUM, ODDNUM};
fourTup odd4Tup   = {ODDNUM, ODDNUM, ODDNUM, ODDNUM};
sevenTup odd7Tup  = {ODDNUM, ODDNUM, ODDNUM, ODDNUM, ODDNUM, ODDNUM, ODDNUM};
tColor4 oddColor4 = {ODDNUM, ODDNUM, ODDNUM, ODDNUM};
tBool endOfFile;
short numPlanes;

/* <<<<<<<<<<<<< PROTOTYPES >>>>>>>>>>>>>>>>>>*/

SHORT  MCLPreprocess (char * inFilename);
SHORT  PreprocessorCleanUp (void);
SHORT  IncludeFile (char * filename, FILE * fout);
void   SubstSymbols (SYMT * symt, char * txt);
void   SYMTAddSymbol (SYMT * symt, char * symbol, char * value);
SYMT * SYMTCreate (void);
void   SYMTDestroy (SYMT * symt);
SHORT  GetLineFile (LONG tmpline, char * txt);
SHORT  CreateInclNode (LONG start, LONG end, char * filename);

tAffine3D MatrixMultiply(tAffine3D m1, tAffine3D m2);
void MatrixCopy(tAffine3D *dest, tAffine3D *src);
tAffine3D MatrixRotate(tAffine3D src, int axis, double angle);
SHORT initScan( void );

void doRotate(tObject *objptr, threeTup tuple);
void doScale(struct tObject *objptr, threeTup tuple);
void doShear(struct tObject *objptr, sevenTup args);
void doTranslate(struct tObject *objptr, threeTup tuple);
void doGroupMatrixMultiply(struct tObject *src, struct tObject *dest);
void skipWhiteSpace( void );
SHORT searchSymTable(char *token,tSymbolType * typ);
SHORT nextSymbol( void );
tBool symbolInSet( short int setSize, short int set[]);
tBool searchObjList(char *objname);
SHORT letThereBeLights(void);
SHORT initCamera(void);
SHORT parseCameraAttributes(void);
SHORT cameraDefs(void);
void initObject(tObject *objptr);
SHORT getReal(DBL *);
SHORT get4Tuple(fourTup *attrib);
SHORT get4Color(tColor4 *attrib);
SHORT get3Tuple(threeTup *attrib);
SHORT get3Color(tColor *attrib);
SHORT get7Tup(sevenTup *attrib);
SHORT getString (char * txt);
void copyFields(tObject *newobj,  tObject *ptr);
SHORT copyObjToGroup(tObject *newobj, char *objName);
SHORT createGroup(tObject *groupRoot);
void groupAttributeCopy(tObject *obj);
void doTreeCopy(tObject *root, tObject *node);
void booleanAttributeCopy(tObject *root, tObject *copyobj);
tNodeType whichStructure(tObject *node);
SHORT buildTree(tObject *treenode, tObject *copyobj);
SHORT buildBool(tObject *objptr);
SHORT parseAttributes( tObject *objptr );
SHORT defineObject( void );
void freeBoolTree(tObject *treenode);
SHORT scanCleanUp(void);
SHORT copyObject(tInstance *newobj,  tObject *oldobj);
SHORT buildInstanceTree(tInstance *parentnode, tInstance *treenode, tObject *copyobj);
SHORT displayObj(void);
void writeObjectToOutput(FILE *out, tInstance *optr);
void writeTreeToOutput(FILE *out, tInstance *tree);
void normalizeVector(threeTup *vector);
SHORT doCameraCalcs(tCamera *cam);
SHORT parseMCLfile( void );
SHORT makeScreenExtent(tInstance *p, tCamera *cam);
SHORT buildPlaneLists(tPlaneList **list, short *pNum);
void  FillPlanePtr (tInstance * obj, tSceneDescript * scene);
SHORT FreeObject (tInstance ** obj);

/*<<<<<<<<<<<<<<<<<<<< InitScan >>>>>>>>>>>>>>>*/
/* Builds Symbol table, init. pointers et. al. */
SHORT initScan( void )
{
	int i;
	tColor initColor = {0,0,0};
	tSymbolNode *newsymbol;

#define TABLElength 55
#define maxStrLen 14

	tSymbolType sym[TABLElength] =
	{AMBIENT,ASPECT,AT, BACKGROUND,BETA,BOOLEAN,
	BUCKYBALL,CAMERA,COLOR,	CONE,CUBE,CYLINDER,
	DEFINE,DIAMOND,DIFFERENCE, DIFFUSE,DISPLAY,
	DODECAHEDRON,EYE,EYEDISTANCE, DATAFILE, FARplane, GROUP,
	ICOSAHEDRON,INTERSECT,LIGHT,LOOKAT,	NEARplane,
	NCOLS,NROWS,OCTAHEDRON,PARAMS, PLANE,POLYHEDRON,
	REFLECT,REPLACE,ROTATE,SCALE, SHEAR,SPECULAR,
	SPEED,SPHERE,SQUARE,TAPEREDCYL,TETRAHEDRON,
	TEXTURE,TILT,TORUS,TRANSLATE,TRANSPARENTatt,
	TYPE,UNION,USERTYPE};

	char str[TABLElength][maxStrLen] =
	{"AMBIENT","ASPECT","AT","BACKGROUND","BETA","BOOLEAN",
	"BUCKYBALL","CAMERA","COLOR","CONE","CUBE","CYLINDER",
	"DEFINE","DIAMOND","DIFFERENCE", "DIFFUSE","DISPLAY",
	"DODECAHEDRON","EYE","EYEDIST", "FILE", "FAR","GROUP",
	"ICOSAHEDRON", "INTERSECT","LIGHT","LOOKAT","NEAR",
	"NCOLS","NROWS","OCTAHEDRON","PARAMS", "PLANE","POLYHEDRON",
	"REFLECT","REPLACE","ROTATE","SCALE", "SHEAR","SPECULAR",
	"SPEED","SPHERE","SQUARE","TAPEREDCYL","TETRAHEDRON",
	"TEXTURE","TILT","TORUS", "TRANSLATE","TRANSPARENT",
	"TYPE","UNION","USERTYPE"};

	symbolTableptr = NULL;  /* global ptr to table */

	for(i = 0; i < TABLElength; i++)
	{
		NEW(tSymbolNode, newsymbol);
		newsymbol->symbolVal = sym[i];  /* load up new node */

		newsymbol->tokenStr = (char *)calloc(strlen(str[i])+1, sizeof(char));
		if(newsymbol->tokenStr==NULL)
		{
			strcat(errMsg,"\n out of memory!");
			return -1;
		}
		strcpy(newsymbol->tokenStr,str[i]);

		newsymbol->next = symbolTableptr; /* push it onto list */
		symbolTableptr = newsymbol;
	}

	/*	NEW(tSceneDescript, sceneptr);*/
	sceneptr->objectptr = NULL;
	sceneptr->lightptr  = NULL;
	sceneptr->cameraptr = NULL;
	sceneptr->ambient =  initColor;
	sceneptr->background = initColor;
	objectRoot = NULL;
	lightList = NULL;
	if(initCamera())return -1;
	MCLlineNumber = 1;
	return 0;
}/* initScan */

/*<<<<<<<<<<<<<<<<< MatrixMultiply >>>>>>>>>>>>>>>>*/
tAffine3D MatrixMultiply(tAffine3D m1, tAffine3D m2)
{
	tAffine3D result;
	short matrixcount1, matrixcount2, matrixcount3;
	DBL  sum;

for (matrixcount1 = 0; matrixcount1<3; matrixcount1++)
  for (matrixcount2 = 0; matrixcount2<3; matrixcount2++) {
	 sum = 0.0000;
	 for (matrixcount3 = 0; matrixcount3<3; matrixcount3++) {
		sum += m1.mat[matrixcount1][matrixcount3] *
				 m2.mat[matrixcount3][matrixcount2];
	 }
	 result.mat[matrixcount1][matrixcount2] = sum;
  }
/*  Calculate the new tr fields of the resulting matrix. */
result.tr.x = (m1.tr.x * m2.mat[0][0]) + (m1.tr.y * m2.mat[1][0]) +
				  (m1.tr.z * m2.mat[2][0]) + m2.tr.x;
result.tr.y = (m1.tr.x * m2.mat[0][1]) + (m1.tr.y * m2.mat[1][1]) +
              (m1.tr.z * m2.mat[2][1]) + m2.tr.y;
result.tr.z = (m1.tr.x * m2.mat[0][2]) + (m1.tr.y * m2.mat[1][2]) +
              (m1.tr.z * m2.mat[2][2]) + m2.tr.z;  
return(result); 
}/* MatrixMultiply */
/*<<<<<<<<<<<<<<<<<<<< MatrixCopy >>>>>>>>>>>>>>>*/
/* Copy the tAffine3D field from one memory location to another */
void MatrixCopy(tAffine3D *dest, tAffine3D *src)
{
	short matrixcount1, matrixcount2;
	for (matrixcount1 = 0; matrixcount1<3; matrixcount1++)
	 for (matrixcount2 = 0; matrixcount2<3; matrixcount2++)
	 dest->mat[matrixcount1][matrixcount2] =
							src->mat[matrixcount1][matrixcount2];
	dest->tr.x = src->tr.x;
	dest->tr.y = src->tr.y;
	dest->tr.z = src->tr.z;
} /* MatrixCopy*/

/*<<<<<<<<<<<<<<<<< MatrixRotate >>>>>>>>>>>>>>>>>*/
/* Construct the Matrix used for Rotating Objects  */
tAffine3D MatrixRotate(tAffine3D src, int axis, double angle)
{
tAffine3D temp = initMatrix, result;
double anglec, angles, angled;
angled = angle * ( acos(0.0) / 90.0 );
angles = sin(angled);
anglec = cos(angled);

switch (axis) {
	 case 0: {
		temp.mat[1][1] =  anglec;   temp.mat[1][2] = angles;
	 temp.mat[2][1] = -angles;   temp.mat[2][2] = anglec;
	 break;
	}
	case 1: {
	 temp.mat[0][0] =  anglec;   temp.mat[0][2] = -angles;
	 temp.mat[2][0] =  angles;   temp.mat[2][2] =  anglec;
		break;
	}
	 case 2: {
	 temp.mat[0][0] =  anglec;   temp.mat[0][1] = angles;
	 temp.mat[1][0] = -angles;   temp.mat[1][1] = anglec;
	 break;
	}
 }
 result = MatrixMultiply(src, temp);
 return(result);
} /* MatrixRotate */

/*<<<<<<<<<<<<<<< doRotate >>>>>>>>>>>>>>>>>*/
void doRotate(tObject *objptr, threeTup tuple)
{
	tAffine3D result, temp=initMatrix, m_temp=initMatrix;
/* Process the X Rotation First, Y rotation second, Z rotation third.*/
temp = MatrixRotate(temp, 0, tuple.x);
temp = MatrixRotate(temp, 1, tuple.y);
temp = MatrixRotate(temp, 2, tuple.z);
/* Process the Z Rotation First, Y rotation second, Z rotation third
 for the inverse matrix. */
m_temp = MatrixRotate(m_temp, 2, -tuple.z);
m_temp = MatrixRotate(m_temp, 1, -tuple.y);
m_temp = MatrixRotate(m_temp, 0, -tuple.x);
/* transformation matrix first.*/
result = MatrixMultiply(objptr->transf, temp);
MatrixCopy(&(objptr->transf), &result);
/* Inverse Matrix Second.*/
result = MatrixMultiply(m_temp, objptr->inver);
MatrixCopy(&(objptr->inver), &result);
} /* doRotate */

/* <<<<<<<<<<<<<<< doScale>>>>>>>>>>>>>>>>>>>>*/
 /* Procedure for calculating the transf and iverse matrixes for scales. */
void doScale(struct tObject *objptr, threeTup tuple)
{
tAffine3D P=initMatrix, result;

if(tuple.x == 0.0) tuple.x = 0.0001; /* avoid illegal factor of 0 */
if(tuple.y == 0.0) tuple.y = 0.0001;
if(tuple.z == 0.0) tuple.z = 0.0001;

P.mat[0][0] = tuple.x;
P.mat[1][1] = tuple.y;
P.mat[2][2] = tuple.z;
result = MatrixMultiply(objptr->transf, P);
MatrixCopy(&(objptr->transf), &result);
P.mat[0][0] = 1/tuple.x;  P.mat[0][1] = 0.000;      P.mat[0][2] = 0.000;
P.mat[1][0] = 0.000;      P.mat[1][1] = 1/tuple.y;  P.mat[1][2] = 0.000;
P.mat[2][0] = 0.000;      P.mat[2][1] = 0.000;      P.mat[2][2] = 1/tuple.z;
result = MatrixMultiply(P, objptr->inver);
MatrixCopy(&(objptr->inver), &result);
} /* doScale */

/* <<<<<<<<<<<<<<<<<<<<<< doShear >>>>>>>>>>>>>>>>>>>*/
 /* Procedure for calculating the transf and inverse matrixes
 for a general shear. */

void doShear(struct tObject *objptr, sevenTup v)
{ /* concatenate affine for a general shear onto object's affine.
	norm is the normal (need not be unit) to a plane through the origin;
	vec is a vector (need not be unit) lying in that plane.
	Shears all points a' la Ron Goldman, GRAPHICS GEMS II ,p.339, 1991.
	Aangle angle (in degrees) must be less than 90 in size;
	(otherwise it is set to 89 degrees.) */

	tAffine3D A = initMatrix,B = initMatrix, result;
	DBL f, tmp, angle;
	threeTup norm,vec;
	norm.x = v.a;
	norm.y = v.b;
	norm.z = v.c;
	vec.x  = v.d;
	vec.y  = v.e;
	vec.z  = v.f;
	angle  = v.g;

	normalizeVector(&norm); /* if not already norm's, do it now */
	normalizeVector(&vec);
	if(fabs(angle > 89)) angle = 89.0;
	f = tan(angle * 3.14159265/180);
	norm.x *= f;  /* scale them by tan(angle .. ) */
	norm.y *= f;
	norm.z *= f;

	tmp = norm.x * vec.x; A.mat[0][0] += tmp; B.mat[0][0] += -tmp;
	tmp = norm.x * vec.y; A.mat[0][1] += tmp; B.mat[0][1] += -tmp;
	tmp = norm.x * vec.z; A.mat[0][2] += tmp; B.mat[0][2] += -tmp;
	tmp = norm.y * vec.x; A.mat[1][0] += tmp; B.mat[1][0] += -tmp;
	tmp = norm.y * vec.y; A.mat[1][1] += tmp; B.mat[1][1] += -tmp;
	tmp = norm.y * vec.z; A.mat[1][2] += tmp; B.mat[1][2] += -tmp;
	tmp = norm.z * vec.x; A.mat[2][0] += tmp; B.mat[2][0] += -tmp;
	tmp = norm.z * vec.y; A.mat[2][1] += tmp; B.mat[2][1] += -tmp;
	tmp = norm.z * vec.z; A.mat[2][2] += tmp; B.mat[2][2] += -tmp;

	result = MatrixMultiply(objptr->transf, A);
	MatrixCopy(&(objptr->transf), &result); /* put result back into object */

	result = MatrixMultiply(objptr->inver, B);
	MatrixCopy(&(objptr->inver), &result);
} /* doShear */

/*<<<<<<<<<<<<<<< do Translate Thang >>>>>>>>>>>>>>>>>*/
void doTranslate(struct tObject *objptr, threeTup tuple)
{	/* use TRANSLATE to translate an object through a given vector amount */

	tAffine3D temp = initMatrix, result;
	temp.tr.x = tuple.x; temp.tr.y = tuple.y; temp.tr.z = tuple.z;
	result = MatrixMultiply(objptr->transf, temp);
	MatrixCopy(&(objptr->transf), &result);
	temp = initMatrix;
	temp.tr.x = -tuple.x;
	temp.tr.y = -tuple.y;
	temp.tr.z = -tuple.z;
	result = MatrixMultiply(temp, objptr->inver);
	MatrixCopy(&(objptr->inver), &result);
}/* doTranslate */

/*<<<<<<<<<<<<<<<<< doGroupMatrixMultiply >>>>>>>>>>>*/
void doGroupMatrixMultiply(struct tObject *src, struct tObject *dest) 
{
  tAffine3D result;

  result = MatrixMultiply(src->transf,  dest->transf);
  MatrixCopy(&(src->transf), &result);
  result = MatrixMultiply(dest->inver, src->inver);
  MatrixCopy(&(src->inver), &result);

} /* doGroupMatrixMultiply */

/* <<<<<<<<<<<<<<<< skipWhiteSpace >>>>>>>>>>>>>>>>>>>>>>*/
 /*  read in the next non-whitespace character. */
void skipWhiteSpace( void )
{
		do {
	/* if character is a carrage return, start parsing next line.  */
	if (inputFileChar == NL)
		MCLlineNumber++;
		inputFileChar = nextChar;
	} while (isspace(inputFileChar));
/* put the character back, so it can be read again */
	ungetc(inputFileChar, MCLfile);
} /* skipWhiteSpace */

/* <<<<<<<<<<<<<< nextSymbol >>>>>>>>>>>>>>>>>>>*/
/* This function builds strings by reading in one character at a time
 * from the input file.  It will see if the string is a word in the language
 * and "tokenize" it, it will check to see if it is a number,
 * an operator (-,+,*,/), a language terminal ( {,}, [, ]) or a
 * userdefined word. It will return end of file as a token if end of file is reached
 * All comments are read in and discarded. */
SHORT nextSymbol( void )
{
	tBool digitParsed = False;
		/* initialize new token  */

	tSymbolNode *p = symbolTableptr;

	int tokenLength = 0;
	strcpy(token,"                   ");
		/* get first character in new token  */
	skipWhiteSpace();
	inputFileChar = nextChar;
		/* check if inputFileChar is part of a word */
	if (isalpha(inputFileChar))
	{
		while (isalnum(inputFileChar))
		{
			inputFileChar = toupper(inputFileChar);
			token[tokenLength++] = inputFileChar;
			inputFileChar = nextChar;
		}
			/* see if word is in Symbol table  */
		ungetc(inputFileChar, MCLfile);
		inputFileChar = 0;
		token[tokenLength] = '\0';

	  /* Have a token. See if it's in the Symbol table.
	  If so, set Symbol to corresponding value.
	  If not, set Symbol to USERDEFINED.*/

	 for(p = symbolTableptr; p != NULL; p = p->next) {
		if(strcmp(token, p->tokenStr) == 0) /* foundit */
		{
			Symbol = p->symbolVal;
			return 0;
		}
     }
		/* not found */
		Symbol = USERDEFINED;
		return 0;
		/* check: is inputFileChar a number, or the boolean (-)? */
	}
	else if (isdigit(inputFileChar) || inputFileChar == '.' ||
				inputFileChar == '-')
	{
		/* check: is it part of a number?  */
		while (isdigit(inputFileChar) ||	 inputFileChar == '.' ||
				 inputFileChar == '-')
		{
			/* if it's (0-9), Symbol can't be diff operator */
			if (isdigit(inputFileChar))
				digitParsed = True;
			if (digitParsed == True && inputFileChar=='-')
			{
				token[tokenLength]='\0';
				SYNTAXERR("valid number (saw '-' in middle of a number)");
			}
			token[tokenLength++] = inputFileChar;
			inputFileChar = nextChar;
		}
			/* convert string to a DBL */
		if (!isspace(inputFileChar) &&
			 !strchr("{}[]",inputFileChar) && !isalpha(inputFileChar))
		{
			token[tokenLength++] = '\0';
			SYNTAXERR("valid number");
		}
		else
		{
			ungetc(inputFileChar, MCLfile);
			inputFileChar = 0;
			  /* is token a boolean difference */
			if (tokenLength == 1  &&  token[0] == '-' && !digitParsed )
			{
				Symbol = DIFFERENCE;
				return 0;
			}
			if (digitParsed)
			{
				number = atof( token );
				Symbol= NUMERAL;
				return 0;
			}
			else
			{
				token[tokenLength]='\0';
				SYNTAXERR("valid number");
			}
		}
	}   /* else if isidigit()..... */
	else switch(inputFileChar)
	{
	 case '{': Symbol = OPENCURLY;    return 0;
	 case '}': Symbol = CLOSECURLY;   return 0;
	 case '[': Symbol = OPENBRACKET;  return 0;
	 case ']': Symbol = CLOSEBRACKET; return 0;
	 case BOOL_UNION  : Symbol = UNION;        return 0;
 	 case BOOL_INTER  : Symbol = INTERSECT;    return 0;
 	 case BOOL_DIFF   : Symbol = DIFFERENCE;   return 0;
 	 case BOOL_REPLACE: Symbol = REPLACE;      return 0;
	 case EOF: Symbol = ENDFILE;
							  endOfFile = True; return 0;
	 case ',': Symbol = COMMA;        return 0;
	 case '!': /* comment */
		do
		{
			inputFileChar = nextChar;
			if (inputFileChar==EOF)
			{
				endOfFile=True;
				Symbol = ENDFILE;
			}
		} while (inputFileChar!= NL);
		MCLlineNumber++;
		inputFileChar = 0;
		if(nextSymbol())return -1;
		return 0;
     case '"':  /* string */
       inputFileChar = nextChar;
       while (inputFileChar != EOF && inputFileChar != '"' &&
              tokenLength < STRING_LENGTH) {
         token[tokenLength++] = inputFileChar;
         inputFileChar = nextChar;        
       }
       if (inputFileChar == EOF) {
         endOfFile = True;
         Symbol = ENDFILE;
         return 0;
       }
       if (inputFileChar != '"') SYNTAXERR("string too long");
       token[tokenLength] = '\0';
       Symbol = TEXTSTRING;
       return 0;

	 default: token[tokenLength] = '\0'; /* terminate it !*/
				 SYNTAXERR("not recognized symbol in nextSymbol()");
	} /* end switch */

} /* nextSymbol */

/*<<<<<<<<<<<<< symbolInSet >>>>>>>>>>>>>>>>>>>>>*/
/* Checks to see if a Symbol is a member of a certain set. */
tBool symbolInSet( short int setSize, short int set[])
{
	short int index;
	for ( index = setSize-1; index >= 0; index--)
		if (set[index] == Symbol)
			return True;
	return False;
}/* symbolInSet */

/*<<<<<<<<<<<<<<< searchObjList >>>>>>>>>>>>>>>>>>>>*/
/* search the object list to see if objname has already been defined.*/
tBool searchObjList(char *objname)
{
   tObject *ptr;
	if (objectRoot == NULL)
		return False;   /* first object defined. */
	ptr = objectRoot;
/* compare the name at each node. */
	do
		if (strcmp(ptr->name,  objname) == 0) {
			globalObjectptr = ptr;
			return True;
		}
		else
			ptr = ptr->next;
	while (ptr != NULL );
	return False;  /* objname is a uniqe object name. */
} /*searchObjList */

/*<<<<<<<<<<<<<<<<<<<< getReal >>>>>>>>>>>>>>>>>*/
 /* Read the next Symbol from the input file, it better be a number.
 * parses an attribute of the form <attribute> <real> */
SHORT getReal(DBL *v)
{
	if(nextSymbol())return -1;
	if (Symbol == NUMERAL)
	{
		if(nextSymbol())return -1;
		*v = number;
		return 0;
	}
	else
		SYNTAXERR("a number");
} /* getReal */

/*<<<<<<<<<<<<<<<<<<<< get3Tuple >>>>>>>>>>>>>>>>>>*/
/* parses and returns a three tuple */
SHORT get3Tuple(threeTup *attrib)
{
	if(nextSymbol())return -1;
	attrib->x = number;
	EXPECT(NUMERAL, "a number");
	attrib->y = number;
	EXPECT(NUMERAL, "a number");
	attrib->z = number;
	EXPECT(NUMERAL, "a number");
	return 0;
} /* get3TupleChar */

/*<<<<<<<<<<<<<<<<<<<< get3Color >>>>>>>>>>>>>>>>>>*/
/* parses and returns a color three tuple */
SHORT get3Color(tColor *attrib)
{
	if(nextSymbol())return -1;
	attrib->r = number;
	EXPECT(NUMERAL, "a number");
	attrib->g = number;
	EXPECT(NUMERAL, "a number");
	attrib->b = number;
	EXPECT(NUMERAL, "a number");
	return 0;
} /* get3Color */

/*<<<<<<<<<<<<<<<<<< get4Tuple >>>>>>>>>>>>>>>>>>*/
 /* parses and returns a fourTuple. */
SHORT get4Tuple(fourTup *attrib)
{
	if(nextSymbol())return -1;
	attrib->a = number;
	EXPECT(NUMERAL, "a number");
	attrib->b = number;
	EXPECT(NUMERAL, "a number");
	attrib->c = number;
	EXPECT(NUMERAL, "a number");
	EXPECT(NUMERAL, "a number");
	attrib->d = number;
	return 0;
} /* get4Tuple */

/*<<<<<<<<<<<<<<<<<< get7Tuple >>>>>>>>>>>>>>>>>>*/
 /* parses and returns a sevenTuple. used by doShear() */
SHORT get7Tuple(sevenTup * attrib)
{
	if(nextSymbol())return -1;
	attrib->a = number;
	EXPECT(NUMERAL, "a number");
	attrib->b = number;
	EXPECT(NUMERAL, "a number");
	attrib->c = number;
	EXPECT(NUMERAL, "a number");
	attrib->d = number;
	EXPECT(NUMERAL, "a number");
	attrib->e = number;
	EXPECT(NUMERAL, "a number");
	attrib->f = number;
	EXPECT(NUMERAL, "a number");
	EXPECT(NUMERAL, "a number");
	attrib->g = number;
	return 0;
} /* get7Tuple */

/*<<<<<<<<<<<<<<<<<< get4Color >>>>>>>>>>>>>>>>>>*/
 /* parses and returns a color 4-tuple. */
SHORT get4Color(tColor4 *attrib)
{
	if(nextSymbol())return -1;
	attrib->r = number;
	EXPECT(NUMERAL, "a number");
	attrib->g = number;
	EXPECT(NUMERAL, "a number");
	attrib->b = number;
	EXPECT(NUMERAL, "a number");
	EXPECT(NUMERAL, "a number");
	attrib->n = number;
	return 0;
} /* get4Color */

/*<<<<<<<<<<<<<<<<< getString >>>>>>>>>>>>>>>>>>>*/
/* gets a string from the input file. String is defined as a
   series of characters limited by a set of " in both sides.
   For the moment a string CANNOT span over more than one line.
   Example string: "duhh" */
SHORT getString (char * txt)
{
  if(nextSymbol())return -1;
  strcpy (txt, token);
  EXPECT(TEXTSTRING, "a string");
  return 0;
}

/*<<<<<<<<<<<<<<<< letThereBeLights >>>>>>>>>>>>>>>>>>>>*/
/* letThereBeLights parses the LIGHT statement in MCL.*/
SHORT letThereBeLights(void)
{
	tLight *newlight, *ptr;
	if(nextSymbol())return -1;
	EXPECT(OPENCURLY, "'{' (The light list must be enclose by { } )");
	while(Symbol != CLOSECURLY)
{
		if (Symbol == BACKGROUND) {
  		  if(get3Color(&sceneptr->background))return -1;
		}
		else if (Symbol == AMBIENT) {
			if(get3Color(&sceneptr->ambient)) return -1;
		}
	/* add a new light to the light list */
		else {
			NEW(tLight, newlight);
		/* take into account the first light */
			if (lightList == NULL) {
				lightList = newlight;
				ptr = newlight;
			/* hook up with global scene ptr */
				sceneptr->lightptr = lightList;
			}
			else {
			/* read down to the end of the light list  (we do this if user
			has more than one light definition block in MCL file. )*/
				ptr = lightList;
				while(ptr->next)
					ptr=ptr->next;
				ptr->next = newlight;
				ptr = ptr->next;
			}
			newlight->next = NULL;
			if (Symbol != COLOR)
				EXPECT(COLOR,"COLOR (Parsing LIGHT definitions)");
			if(get3Color(&newlight->color))return -1;
			if (Symbol != AT)
				EXPECT(AT, "AT (Parsing LIGHT definitions)");
			if(get3Tuple(&newlight->position))return -1;

		} /* while */
	} /* else */
/* The next line is to make syntax errors easier for students to decipher */
	if (Symbol != CLOSECURLY) {
		SYNTAXERR("valid light attribute");
	}
	EXPECT(CLOSECURLY,"'}' (needed at end of light definition)");
	return 0;
} /* letThereBeLights*/

/*<<<<<<<<<<<<<<<<<< initCamera >>>>>>>>>>>>>>>>>>>>>>>*/
 /* initialize camera with default values, and make proper pointer connections
 with the sceneptr */
SHORT initCamera(void)
{
threeTup initEye = {1,0,0}, initU = {0,1,0}, initV = {0,0,1},
			initN = {-1,0,0};
threeTup init3Tup = {0,0,0};
/* make connections */
	NEW(tCamera, camera);
	camptr = camera;
/* set default values */
	camera->eye = initEye;
	camera->lookAt = init3Tup;
	camera->ref = init3Tup;
	camera->u = initU;
	camera->v = initV;
	camera->n = initN;
	camera->eyeDist = 1;
	camera->aspect = 480/(DBL)640;
	camera->W = 1;
	camera->H = 1;
	camera->beta = 45;
	camera->tilt = 0;
	camera->viewNear = .9;
	camera->viewFar = 100000.0;
	camera->nRows = 480;
	camera->nCols = 640;
	return 0;
} /* initCamera*/

/*<<<<<<<<<<<<<<<<<<< parseCameraAttributes >>>>>>>>>>>>>*/
/* parse camera attributes - eye, lookAt, beta, aspect, eyedistance, far,
near, tilt, nRows, nCols */
SHORT parseCameraAttributes(void)
{
	short int cameraSize = 10;
	short int cameraList[] ={ EYE, EYEDISTANCE, LOOKAT,
			BETA, TILT, NEARplane, NROWS, NCOLS, FARplane, ASPECT};
	DBL tmp;
	while (symbolInSet(cameraSize, cameraList))
	{ /* each getReal() etc. reads number, AND gets next symbol */
	  switch(Symbol)
	  {
		case EYE:         if(get3Tuple(&camptr->eye))return -1; break;
		case LOOKAT:      if(get3Tuple(&camptr->lookAt))return -1; break;
		case BETA:        if(getReal(&camptr->beta))return -1; break;
		case EYEDISTANCE: if(getReal(&camptr->eyeDist))return -1; break;
		case FARplane:    if(getReal(&camptr->viewFar))return -1; break;
		case NCOLS:       if(getReal(&tmp)) return -1;
								else camptr->nCols  = (short)tmp; break;
		case NROWS:       if(getReal(&tmp)) return -1;
								else camptr->nRows = (short)tmp; break;
		case NEARplane:   if(getReal(&camptr->viewNear))return -1; break;
		case TILT:        if(getReal(&camptr->tilt))return -1; break;
		case ASPECT:      if(getReal(&camptr->aspect))return -1; break;
	  } /* switch */
	} /* while symbolInSet */
	return 0;
} /* parseCameraAttributes*/

/*  <<<<<<<<<<<<<<<<<<< cameraDefs  >>>>>>>>>>>>>>>>>>*/
/* Define camera attributes */
SHORT cameraDefs(void)
{
	sceneptr->cameraptr = camptr;
	if(nextSymbol())return -1;
	EXPECT(OPENCURLY, "'{' (parsing camera defs)");
	if (parseCameraAttributes()) return -1;
	if (Symbol != CLOSECURLY)
		SYNTAXERR("valid camera attribute");
	EXPECT(CLOSECURLY,"'}' (needed at the end of camera defs).");
	return 0;
} /* cameraDefs */

/*<<<<<<<<<<<<<<< normalizeVector >>>>>>>>>>>>>>>>>>>>*/
void normalizeVector(threeTup *vector) 
{
	float length;
	length = sqrt(vector->x * vector->x +
                  vector->y * vector->y +
						vector->z * vector->z);
	if (length != 0) {
		vector->x = vector->x / length;
		vector->y = vector->y / length;
		vector->z = vector->z / length;
	}
	else
		vector->x = vector->y = vector->z = 0;
} /* normalizeVector */

/*<<<<<<<<<<<<<<<<<< doCameraCalcs  >>>>>>>>>>>*/
SHORT doCameraCalcs(tCamera *cam)
{
  DBL ang,sine,cosine;
  threeTup unew,vnew;
  cam->n.x = cam->lookAt.x - cam->eye.x;   /* n = lookat - eye */
  cam->n.y = cam->lookAt.y - cam->eye.y;
  cam->n.z = cam->lookAt.z - cam->eye.z;
/* If cam->n.x, cam->n.y, cam->n.z all zero, use default camera */
if (( cam->n.x == 0.000) && (cam->n.y == 0.000) && (cam->n.z == 0.000))
{
  cam->eye.x = 1.000; cam->eye.y = 0.000; cam->eye.z = 0.000;
  cam->ref.x = 0.000; cam->ref.y = 0.000; cam->ref.z = 0.000;
  cam->n.x = -1.000; cam->n.y = 0.000; cam->n.z = 0.000;
  cam->u.x =  0.000; cam->u.y = 1.000; cam->u.z = 0.000;
  cam->v.x =  0.000; cam->v.y = 0.000; cam->v.z = 1.000;
  cam->eyeDist = 1.0;
  cam->W       =  1.000;
  cam->H       =  1.000;
  cam->tilt    =  0.000;
  cam->beta    = 45.000;
  return 0;
}
/* If cam->n.x, cam->n.y = 0, do straight up or down cam */
if ((cam->n.x == 0.000) && (cam->n.y == 0.000)) {
  cam->n.z /=  fabs(camera->n.z);  /* keep sign, normalize */
  cam->u.x  = -camera->n.z;
  cam->u.y  = 0.000000;
  cam->u.z  = 0.000000;
  cam->v.x  = 0.000000;
  cam->v.y  = 1.000000;
  cam->v.z  = 0.000000;
}
else  /* regular camera */
{
  cam->u.x =  cam->n.y; /* u = (ny, -nx, 0) */
  cam->u.y = -cam->n.x;
  cam->u.z = 0;
  cam->v.x = -cam->n.x * cam->n.z; /* v=nx^2 + ny^2 */
  cam->v.y = -cam->n.y * cam->n.z;
  cam->v.z = cam->n.x * cam->n.x + cam->n.y * cam->n.y;
  normalizeVector(&cam->u);
  normalizeVector(&cam->v);
  normalizeVector(&cam->n);
}
/* tilt axes as needed */
if(cam->tilt != 0.0){
	ang = cam->tilt * PI / 180.0;
	sine = sin(ang); cosine = cos(ang);
	unew.x = cosine * cam->u.x - sine * cam->v.x;
	unew.y = cosine * cam->u.y - sine * cam->v.y;
	unew.z = cosine * cam->u.z - sine * cam->v.z;
	vnew.x = sine * cam->u.x + cosine * cam->v.x;
	vnew.y = sine * cam->u.y + cosine * cam->v.y;
	vnew.z = sine * cam->u.z + cosine * cam->v.z;
	cam->u = unew;
	cam->v = vnew;
}/* end of tilt adjustment */
  cam->ref.x = cam->eye.x + cam->eyeDist * cam->n.x;   /* ref=eye+E*n */
  cam->ref.y = cam->eye.y + cam->eyeDist * cam->n.y;
  cam->ref.z = cam->eye.z + cam->eyeDist * cam->n.z;
		/* W = eyeDistance * tan(Beta * PI / 180) */
  cam->W = cam->eyeDist * tan((PI * cam->beta) / 180);
		/* H = W * window aspect ratio */
  cam->H = cam->W * cam->aspect;
  return 0;
}/* doCameraCalcs */


/*<<<<<<<<<<<<<<<<<<<<<<<<<< initObject >>>>>>>>>>>>>>>>>*/
/* initialize an object. */
void initObject(tObject *objptr) {
	objptr->next = NULL;
	objptr->group = NULL;
	objptr->left = NULL;
	objptr->right = NULL;
	objptr->boolOp = NOOP;
	objptr->type = SPHERE;
	objptr->ambient = initColor3;
	objptr->color = initColor3;
	objptr->diffuse = initColor3;
	objptr->params = init4Tup;
	objptr->specular = initColor4;
	objptr->transparent = 0;
	objptr->reflection = 0;
	objptr->texture = init7Tup;
    objptr->speed = 1;
	objptr->inver = initMatrix;
	objptr->transf = initMatrix;
    objptr->dataFile = NULL;
} /* initObject */

/*<<<<<<<<<<<<<<<<<<<< copyFields >>>>>>>>>>>>>>>>>*/
/* copies info from ptr to newobj*/
void copyFields(tObject *newobj,  tObject *ptr) 
{
	strcpy(newobj->name, ptr->name);
	newobj->type = ptr->type;
	newobj->group = NULL;
	newobj->next = NULL;
	newobj->left = ptr->left;
	newobj->right = ptr->right;
	newobj->boolOp = ptr->boolOp;
	newobj->specular = ptr->specular;
   	newobj->params = ptr->params;
	newobj->diffuse = ptr->diffuse;
	newobj->ambient = ptr->ambient;
	newobj->color = ptr->color;
	newobj->transparent = ptr->transparent;
	newobj->reflection = ptr->reflection;
	newobj->speed = ptr->speed;
	newobj->texture = ptr->texture;
	newobj->inver = ptr->inver;
	newobj->transf = ptr->transf;
    if (ptr->dataFile == NULL)
      newobj->dataFile = NULL;
    else
      strcpy (newobj->dataFile, ptr->dataFile);
} /* copyFields */


/*<<<<<<<<<<<<<< copyObjToGroup >>>>>>>>>>>>>>*/
 /* copies a node from the objectRoot list to a group.*/
SHORT copyObjToGroup(tObject *newobj, char *objName)
{
	 tObject *ptr = objectRoot, *newobj2;
/* find the node to copy */
	while (strcmp(ptr->name, objName))
		ptr=ptr->next;
	if(ptr->group == NULL) {/* not copying a group*/
	/* Copy all info from ptr to newobj  */
		copyFields(newobj, ptr);
	/* attach the objcpy pointer from the new node,
		to the node to copy */
	} else {
	/* make copy of entire group structure */
		ptr = ptr->group;
		copyFields(newobj,ptr);
		ptr = ptr->next;
		while(ptr) {     /* while there are still group objects to copy */
			NEW(tObject, newobj2);
			newobj->next = newobj2;     /* make the link */
			copyFields(newobj2,ptr);
			ptr = ptr->next;
			newobj = newobj2;
		} /* while */
	}
	return 0;
} /* copyObjToGroup */


/*<<<<<<<<<<<<<<<<<<<< createGroup >>>>>>>>>>>*/
/*   This function creates a group structure from the production rule
GROUP <already definded object> {, <already defined object>} */
SHORT createGroup(tObject *groupRoot)
{
	char objName[IDENTLENGTH];
	tObject *newobj, *newobj2;
	tBool doneParsing = False;
/* initialize group attributes with an odd (strange) number.  If the value
 * has changed after parsing all the attributes, we know that the user
 * has changed the values.  We will know which attributes to copy to all
 * the objects in the group */
	groupRoot->ambient = oddColor3;
	groupRoot->color = oddColor3;
	groupRoot->diffuse = oddColor3;
	groupRoot->params = odd4Tup;
	groupRoot->specular = oddColor4;
	groupRoot->reflection = ODDNUM;
	groupRoot->transparent = ODDNUM;
	groupRoot->speed = ODDNUM;
	groupRoot->texture = odd7Tup;
/* get name of first group object */
	if(nextSymbol())return -1;
	strcpy(objName, token);
/* check to see if object exists, or if this is the object we are building */
	if (!searchObjList(objName) || !globalObjectptr->next)
		SYNTAXERR("defined object (object name is  undefined)");
/* create new node */
	NEW(tObject, newobj);
	groupRoot->group = newobj;
	if (copyObjToGroup(newobj, objName)) return -1;
	do {
		if(nextSymbol())return -1;
		if (Symbol != COMMA)
			doneParsing = True;
		else {
		/* add another node to the group */
			if(nextSymbol())return -1;
			strcpy(objName, token);
			if (!searchObjList(objName) || !globalObjectptr->next)
				SYNTAXERR("defined object (object name is undefined)");
			NEW(tObject, newobj2);
			newobj->next = newobj2;
			if (copyObjToGroup(newobj2, objName)) return -1;

		/* Move newobj to end of the list */
			newobj = groupRoot->group;
			while(newobj->next) newobj=newobj->next;

		}
	} while (!doneParsing);
	return 0;
} /* createGroup */


/*<<<<<<<<<<<<<<<<< groupAttributeCopy >>>>>>>>>>>>>>>>>>*/
 /* Copies all the attributes that the user specified for groups to all the
 *  objects in that group.  If the attribute doesn't equal ODDNUM, we know
 *  the user has changed the attribute, copy it to all objects in group.*/
void groupAttributeCopy(tObject *obj)
{
	tObject *p;

/* check if ambient attribute has been changed */
	if (obj->ambient.r != ODDNUM || obj->ambient.g !=ODDNUM ||
		 obj->ambient.b != ODDNUM)
		 /* copy ambient attributes to all objects in group */
		 for(p = obj->group;p != NULL; p= p->next)
			p->ambient = obj->ambient;

/* check if user changed group color attribute */
	if (obj->color.r != ODDNUM || obj->color.g != ODDNUM ||
		 obj->color.b != ODDNUM)
		 /* copy color attributes to all objects in group */
		 for(p = obj->group; p!= NULL; p= p->next)
			p->color = obj->color;

/* check if diffuse attribute has been changed */
	if (obj->diffuse.r != ODDNUM || obj->diffuse.g != ODDNUM ||
		 obj->diffuse.b != ODDNUM )
		 /* copy diffuse attributes to all objects in group */
		 for(p = obj->group; p!= NULL; p=p->next)
			p->diffuse = obj->diffuse;

/* check if params attribute has been changed */
	if (obj->params.a != ODDNUM || obj->params.b != ODDNUM ||
		 obj->params.c != ODDNUM || obj->params.d != ODDNUM)
		 /* copy params attributes to all objects in group */
		 for(p = obj->group; p!= NULL; p = p->next)
			p->params = obj->params;

/* check if texture attribute has been changed */
	if (obj->texture.a != ODDNUM || obj->texture.b != ODDNUM ||
		 obj->texture.c != ODDNUM || obj->texture.d != ODDNUM ||
		 obj->texture.e != ODDNUM || obj->texture.f != ODDNUM ||
		 obj->texture.g != ODDNUM)
		 for(p = obj->group;p != NULL; p = p->next) /* copy attr's to all objects in group */
			p->texture = obj->texture;

/* check if specular attribute has been changed */
	if (obj->specular.r != ODDNUM || obj->specular.g != ODDNUM ||
		 obj->specular.b != ODDNUM || obj->specular.n != ODDNUM)
		 /* copy specular attributes to all objects in group */
		 for(p = obj->group; p != NULL; p = p-> next)
			p->specular = obj->specular;

/* check if reflection attribute has been changed */
	if (obj->reflection != ODDNUM)
		 /* copy relection attributes to all objects in group */
		 for(p = obj->group; p != NULL; p = p-> next)
			p->reflection = obj->reflection;

/* check if transparent attribute has been changed */
	if (obj->transparent != ODDNUM)
		 /* copy transparent attributes to all objects in group */
		 for(p = obj->group; p != NULL; p = p->next)
			p->transparent = obj->transparent;

/* check if speed attribute has been changed */
	if (obj->speed != ODDNUM)
		 /* copy speed attributes to all objects in group */
		 for(p = obj->group; p != NULL; p = p->next)
			p->speed = obj->speed;

} /* groupAttributeCopy */


/*<<<<<<<<<<<<<<<<<< doTreeCopy >>>>>>>>>>>>>*/
 /* copy changed attributes from the top of the tree to all nodes under
 * the root node (treetop)  */
void doTreeCopy(tObject *root, tObject *node) 
{
	if (root->ambient.r != oddColor3.r)
		node->ambient = root->ambient;
	if (root->color.r != oddColor3.r)
		node->color = root->color;
	if (root->diffuse.r != oddColor3.r)
		node->diffuse = root->diffuse;
	if (root->params.a != odd4Tup.a)
		node->params = root->params;
	if (root->specular.r != oddColor4.r)
		node->specular = root->specular;
	if (root->reflection != ODDNUM)
		node->reflection = root->reflection;
	if (root->transparent != ODDNUM)
		node->transparent = root->transparent;
	if (root->speed != ODDNUM)
   	node->speed = root->speed;
	if (root->texture.a != ODDNUM)
		node->texture = root->texture;
} /* doTreeCopy */

/*<<<<<<<<<<<<<<<<<<< booleanAttributeCopy >>>>>>>>>>>>>>>>>>>*/
 /*  copy all changed attributes from the root node of the tree to the leaf nodes */
void booleanAttributeCopy(tObject *root, tObject *copyobj) 
{
	if (copyobj->left)
		booleanAttributeCopy(root, copyobj->left);
	if (copyobj->right)
		booleanAttributeCopy(root, copyobj->right);
/* found a leaf node, copy root to copyobj */
	if (!copyobj->left && !copyobj->right)
		doTreeCopy(root, copyobj);
} /* booleanattributeCopy*/

/*<<<<<<<<<<<<<<<<<<< whichStructure >>>>>>>>>>>>>>>>>*/
 /* function to return the type of node an object is. will return simple type, 
group type, or boolean type */
tNodeType whichStructure(tObject *node)
{
/* check for group type */
	if (node->group != NULL)
	/* see if group node has only one object in it */
		if (node->group->next == NULL)
			return SIMPLEDEF;
		else
			return GROUPDEF;
/* check for boolean type */
	if (node->left != NULL && node->right != NULL)
		return BOOLDEF;
	else
		return SIMPLEDEF;
}


/*<<<<<<<<<<<<<<<<<<< buildTree >>>>>>>>>>>>>>>>>>>>*/
 /*  a procedure to build one side of a binary tree.
 *  treenode is a node on the tree that objects will be copied to.
 *  copyobj is the object that will be copied to treeRoot. */
SHORT buildTree(tObject *treenode, tObject *copyobj)
{
	tObject *newnode, *newleaf, *newnode2;

	if (structType == BOOLDEF) {
		if (!treenode->left && !treenode->right)
			copyFields(treenode, copyobj);
		if (copyobj->left || copyobj->right) {
			NEW(tObject,newnode);
			initObject(newnode);
		/* attach newnode to tree */
			if (treenode->left) {
				treenode->right = newnode;
			}
			else {
				treenode->left = newnode;
			}
			if (copyobj->left)
				if (buildTree(newnode, copyobj->left)) return -1;
			NEW(tObject, newnode2);
			initObject(newnode2);
			treenode->right = newnode2;
			if (copyobj->right)
				if (buildTree(newnode2, copyobj->right)) return -1;
		}
	} /* structType = BOOLDEF*/
				/* if it is only one node to be copied, put it into treenode. */
	else if (structType == SIMPLEDEF)
	{
	/* check if this is a group object w/ one node,
		if it is, move the pointer down to the next object */
		if(copyobj->group)
			copyobj = copyobj -> group;
		copyFields(treenode, copyobj);
		treenode -> boolOp = NOOP;
	}
	/* copy a group copy group into a binary tree linked with unions */
	else if (structType == GROUPDEF) {
		if (copyobj->group)
			copyobj = copyobj -> group;
	/* make treenode a union */
		treenode->boolOp = UNION;
	/* create a new leaf, and copy the group object to
		the new leaf */
		NEW(tObject, newleaf);
		copyFields(newleaf, copyobj);
	/* create a new node for another union */
		NEW(tObject, newnode);
		initObject(newnode);
	/* make all connections */
		treenode->left = newleaf;
		treenode->right = newnode;
	/* check to see if next group object is last object.
		If it is not the last, keep building recursivly.
		If it is, stop recursive call and add the last object
		to the right side of the last treenode. */
		copyobj = copyobj -> next;
		if (copyobj->next) {
			 /* continue to build tree recursivly*/
			if (buildTree(newnode, copyobj)) return -1;
		  }
		else /* last node */
			copyFields(newnode, copyobj);
	} /* if structType = GROUPDEF */
	return 0;
} /* buildTree */

/*<<<<<<<<<<<<<<< buildBool >>>>>>>>>>>>>>>>>>*/
/*  Builds a binary tree to parse a boolean object. */
SHORT buildBool(tObject *objptr)
{
  short int boolOpList[] = {DIFFERENCE, UNION, INTERSECT, REPLACE};
  short int boolOpSize = 4;
tObject *boolobj1, *boolobj2;
	if (nodeType != DONTKNOW)
		SYNTAXERR(
			"non-Ambiguous object (tried to redefine as Boolean object");
	nodeType = BOOLDEF;
/* initialize boolean node fields to ODDNUM so we will be able
 * to tell which nodes have been changed.  The user might have already
 * changed some nodes before the BOOLEAN command, so we must first
 * check to see if the nodes have been already changed.  If they have
 * already been changed, leave them alone. */
	if (objptr->ambient.r == initColor3.r && objptr->ambient.g == initColor3.g &&
		objptr->ambient.b == initColor3.b )
		objptr->ambient = oddColor3;

	if(objptr->color.r == initColor3.r && objptr->color.g == initColor3.g &&
		objptr->color.b == initColor3.b )
		objptr->color = oddColor3;

	if(objptr->diffuse.r == initColor3.r && objptr->diffuse.g == initColor3.g &&
		objptr->diffuse.b == initColor3.b)
		objptr->diffuse = oddColor3;

	if(objptr->params.a == init4Tup.a && objptr->params.b == init4Tup.b &&
		objptr->params.c == init4Tup.c && objptr->params.d == init4Tup.d)
		objptr->params = odd4Tup;

	if(objptr->texture.a == init7Tup.a && objptr->texture.b == init7Tup.b &&
		objptr->texture.c == init7Tup.c && objptr->texture.d == init7Tup.d &&
		objptr->texture.e == init7Tup.e && objptr->texture.f == init7Tup.f &&
		objptr->texture.g == init7Tup.g)
		objptr->texture = odd7Tup;

	if(objptr->specular.r == initColor4.r && objptr->specular.g == initColor4.g &&
		objptr->specular.b == initColor4.b && objptr->specular.n == initColor4.n)
		objptr->specular = oddColor4;

	if(objptr->reflection == 0)
		objptr->reflection = ODDNUM;
	if(objptr->transparent == 0)
		objptr->transparent = ODDNUM;
	if (objptr->speed == 1)
		objptr->speed = ODDNUM;

	/* get first bool object */
	if(nextSymbol())return -1;
	if (!searchObjList(token) || !globalObjectptr->next)
		SYNTAXERR("defined object (object name is undefined)");
	structType = whichStructure(globalObjectptr);
	NEW(tObject, boolobj1);
	initObject(boolobj1);
    objptr->type = BOOLOBJ;
	objptr->left = boolobj1;

	/* build the left half of the tree */
	if (buildTree(boolobj1, globalObjectptr)) return -1;

	/* get boolean operator */
	if(nextSymbol())return -1;
	if (!symbolInSet(boolOpSize, boolOpList))
		SYNTAXERR("boolean operator");
	objptr->boolOp = Symbol;

	/* get next object name */
	if(nextSymbol())return -1;
	if (!searchObjList(token)|| !globalObjectptr->next)
		SYNTAXERR("Defined object (object name is undefined)");
	structType = whichStructure(globalObjectptr);
	NEW(tObject, boolobj2);
	initObject(boolobj2);
	objptr->right = boolobj2;

	/* build right side of tree */
	if (buildTree(boolobj2, globalObjectptr)) return -1;
	if(nextSymbol())return -1;
	return 0;
}/* buildBool */


/*<<<<<<<<<<<<<<<<< parseAttributes >>>>>>>>>>>>>>>>>>*/
/* Scans attributes such as: AMBIENT, COLOR, DIFFUSE,...
	See entire list about 6 lines below */
SHORT parseAttributes( tObject *objptr )
{
  tObject *bracketobj, *ptr;
  char objName[IDENTLENGTH];
  /* attributes */
  short int attribSize = 17;
  static short attribList[] = { AMBIENT, COLOR, DIFFUSE, DATAFILE,
	 TEXTURE, SPEED, REFLECT, TRANSPARENTatt, ROTATE, SCALE,
	 SPECULAR, PARAMS,TRANSLATE, TYPE, GROUP, SHEAR,OPENBRACKET, BOOLEAN };
  /* shapes */
  static short shapeList[16]= {SPHERE,CONE,CYLINDER,TAPEREDCYL,PLANE,
    SQUARE, TORUS, TETRAHEDRON, CUBE, OCTAHEDRON, DODECAHEDRON,
    ICOSAHEDRON, POLYHEDRON, BUCKYBALL, DIAMOND, USERDEFINED};
  short int shapeListSize = 16;
  threeTup tempTup;
  sevenTup args;
  char tmpstr[STRING_LENGTH];

  while (symbolInSet(attribSize, attribList)== True || Symbol == BOOLEAN)
	{
		if (Symbol == TYPE)
		{
			 /* check to see if node has been redefined */
			 if (nodeType != DONTKNOW)
				 SYNTAXERR("bad object, type unknown");
			if(nextSymbol())return -1;
			/* Symbol should be a shape Symbol */

			if ( symbolInSet(shapeListSize, shapeList))
				objptr->type = Symbol;
			else
				SYNTAXERR("shape type (CUBE, SPHERE, CONE...)");
			nodeType = SIMPLEDEF;
			if(nextSymbol())return -1;
		} 		/* Symbol == type */

			  /* parse [ <objname> <attributes> ] */
		 else if (Symbol == OPENBRACKET)
		 {
			 if (nodeType == DONTKNOW)
				SYNTAXERR("Use the command GROUP before defining [] objects");
			 if(nextSymbol())return -1;
				strcpy(objName, token);
				/* make sure that objName has already been defined */
			 if(!searchObjList(objName) || !globalObjectptr->next)
				SYNTAXERR("defined object (object name is undefined)");
				/* create and initialize new object */
			NEW(tObject, bracketobj);
			initObject(bracketobj);
			  /* give the new bracket object a number for a name */
			sprintf(objName,"%s%d", token, numNamelessObjects++);
			copyFields(bracketobj, globalObjectptr);
			sprintf(bracketobj->name, objName);

			bracketobj -> next = NULL;
			bracketobj -> group = NULL;
			bracketobj -> left = NULL;
			bracketobj -> right = NULL;
			bracketobj -> boolOp = NOOP;
			 /* attach new object to end of group list */
			ptr = objptr->group;
			while(ptr->next) ptr = ptr->next;
			ptr->next = bracketobj;
			if(nextSymbol())return -1;
			if (parseAttributes(bracketobj)) return -1;
			if(Symbol == CLOSEBRACKET)
				EXPECT(CLOSEBRACKET, "']'") /* don't put ';' here! */
			else if (Symbol == CLOSECURLY)
			{
				strcpy(token,"}");
				SYNTAXERR("']' (']' omitted in the define statement");
			}
		 } /*OPENBRACKET*/

		else switch(Symbol)
		{
			case BOOLEAN: if (buildBool(objptr)) return -1; break;
			case AMBIENT: if(get3Color(&objptr->ambient))return -1; break;
			case COLOR:   if(get3Color(&objptr->color))return -1;   break;
			case DIFFUSE: if(get3Color(&objptr->diffuse))return -1; break;
			case REFLECT: if(getReal(&objptr->reflection))return -1;break;
			case TRANSPARENTatt: if(getReal(&objptr->transparent))return -1; break;
			case PARAMS: if(get4Tuple(&objptr->params)) return -1; break;
			case SPECULAR: if(get4Color(&objptr->specular)) return -1;break;
			case SPEED: if(getReal(&objptr->speed)) return -1; break;
			case TEXTURE: if(get7Tuple(&objptr->texture)) return -1; break;

			case TRANSLATE: if(get3Tuple(&tempTup)) return -1;
									 doTranslate(objptr, tempTup); break;
			case ROTATE:    if(get3Tuple(&tempTup))return -1;
									 doRotate(objptr, tempTup); break;
			case SCALE:     if(get3Tuple(&tempTup)) return -1;
									 doScale(objptr, tempTup); break;
			case SHEAR:     if(get7Tuple(&args)) return -1;
									 doShear(objptr, args); break;

			case GROUP:	 /* check for a redefined node */
								if (nodeType != DONTKNOW)
								SYNTAXERR("bad object, (redefined as group)");
								nodeType = GROUPDEF;
								if (createGroup(objptr)) return -1; break;

            case DATAFILE: 
              if (getString(tmpstr))return -1;
              objptr->dataFile = malloc (strlen(tmpstr)+1);
              strcpy (objptr->dataFile, tmpstr);
              break;

		} /* switch */
	} /* while */

	if (Symbol!=CLOSECURLY && Symbol != CLOSEBRACKET)
				SYNTAXERR("object attribute");

		return 0;
} /* parseAttributes */


/* <<<<<<<<<<<<<<<<< defineObject  >>>>>>>>>>>>>>>>*/
/* parse a DEFINE statement */
SHORT defineObject( void )
{
	char newObjName[IDENTLENGTH];
	tObject *objptr;
	/* the next Symbol should be a user defined group or obj. name */
	if(nextSymbol())return -1;
	strcpy(newObjName, token);

	/* check if this is a unique object name. */
	if(searchObjList(newObjName))
		SYNTAXERR("unique object name (two objects with the same name)");
	if(nextSymbol())return -1;
	 /* create structure for the new object */
	NEW(tObject, objptr);
	strcpy(objptr->name, newObjName); /* load name into thisobject */

	initObject(objptr); /* put initial values in attributes */

	/* put it last on the list */
	if (objectRoot == NULL) objectRoot = objptr;
	else lastObj->next = objptr;
	lastObj = objptr;
	nodeType = DONTKNOW;
	EXPECT(OPENCURLY,"{");
	if (parseAttributes(objptr)) return -1;
	EXPECT(CLOSECURLY,"}");
	if (nodeType == DONTKNOW)
		 SYNTAXERR("DONTKNOW type symbol found");
/* copy group attributes to all the objects in the group */
	if (nodeType == GROUPDEF)
		groupAttributeCopy(objptr);
/* copy all boolean attributes to all the objects in the tree */
	else if (nodeType == BOOLDEF) {
		booleanAttributeCopy(objptr, objptr);
		if (objptr->ambient.r == ODDNUM)
			objptr->ambient = initColor3;
		if (objptr->color.r == ODDNUM)
			objptr->color = initColor3;
		if (objptr->diffuse.r == ODDNUM)
			objptr->diffuse = initColor3;
		if (objptr->params.a == ODDNUM)
			objptr->params = init4Tup;
		if (objptr->specular.r == ODDNUM)
			objptr->specular = initColor4;
		if (objptr->transparent == ODDNUM)
			objptr->transparent = 0;
		if (objptr->reflection == ODDNUM)
			objptr->reflection = 0;
		if (objptr->speed == ODDNUM)
			objptr->speed = 1;
		if (objptr->texture.a == ODDNUM)
			objptr->texture = init7Tup;
	}
	return 0;
} /* define object */

/*<<<<<<<<<<<<<<< freeBoolTree >>>>>>>>>>>>>>>>>>> */
 /* return boolean tree memory back to system */
void freeBoolTree(tObject *treenode)
{
	if (treenode->left || treenode->right) {
		if(treenode->left)
			freeBoolTree(treenode->left);
		if(treenode->right)
			freeBoolTree(treenode->right);
		free(treenode);
	 }
	 else
		 free(treenode);
}/* freeBoolTree */
/*<<<<<<<<<<<<<<<<<<<< scanCleanUp >>>>>>>>>>>>>>>>>>>*/
/* returns memory back to system */
SHORT scanCleanUp(void)
{
	tObject *p, *q;
	 tSymbolNode *m, *n;
	do {
		p = objectRoot;
		structType = whichStructure(objectRoot);
		if (structType == SIMPLEDEF) {
			objectRoot = p->next;
			free(p);
		}
		else if (structType == GROUPDEF) {
			p = objectRoot->group;
			do {
				q=p;
				p = p->next;
				free(q);
			} while (p);
			objectRoot = objectRoot->next;
			free(p);
		} /* else GROUPDEF */
		else /* boolean tree */ {
			objectRoot = objectRoot->next;
			freeBoolTree(p);
		}
	} while (objectRoot);
/* delete Symbol table */
	m = symbolTableptr;
	n = m->next;
	while (m) {
		free(m);
		m = n;
		if (n->next)
			n = n->next;
		else
			m = NULL;
	}
  /* delete four redundant free()'s:*/
	/* free(symbolTableptr); free(lastObj);*/
	/* free(globalObjectptr); free(objectRoot);*/
	return 0;
} /* scanCleanUp */

/*<<<<<<<<<<<<<<<< copyObject >>>>>>>>>>>>>>>>>>*/
/* copies information from the data structure to the object list.*/
SHORT copyObject(tInstance *newobj,  tObject *oldobj)
{
	short int type;

/* copy fields from the old object to the new one */
	strcpy(newobj->name, oldobj->name);
	type = oldobj->type;
	if (type == BUCKYBALL) type = buckyballObj;
	else if (type == CONE) type = coneObj;
	else if (type == CUBE) type = cubeObj;
	else if (type == CYLINDER) type = cylinderObj;
	else if (type == TAPEREDCYL) type = taperedCylObj;
	else if (type == DODECAHEDRON) type = dodecahedronObj;
	else if (type == ICOSAHEDRON) type = icosahedronObj;
	else if (type == OCTAHEDRON) type = octahedronObj;
	else if (type == PLANE) type = planeObj;
	else if (type == POLYHEDRON) type = polyhedronObj;
	else if (type == SPHERE) type = sphereObj;
	else if (type == SQUARE) type = squareObj;
	else if (type == TETRAHEDRON) type = tetrahedronObj;
	else if (type == TORUS) type = torusObj;
	else if (type == USERDEFINED) type = userDefinedObj;
	else if (type == DIAMOND) type = diamondObj;
    else if (type == BOOLOBJ) type = boolObj;

	newobj->type = type;
	if (oldobj->boolOp == NOOP)
		newobj->boolOp = ' ';
	else if (oldobj->boolOp == UNION)
		newobj->boolOp = BOOL_UNION;
	else if (oldobj->boolOp == DIFFERENCE)
		newobj->boolOp = BOOL_DIFF;
	else if (oldobj->boolOp == INTERSECT)
		newobj->boolOp = BOOL_INTER;
	else
		newobj->boolOp = BOOL_REPLACE;
	newobj->ambient.r = oldobj->ambient.r;
	newobj->ambient.g = oldobj->ambient.g;
	newobj->ambient.b = oldobj->ambient.b;
	newobj->color.r = oldobj->color.r;
	newobj->color.g = oldobj->color.g;
	newobj->color.b = oldobj->color.b;
	newobj->diffuse.r = oldobj->diffuse.r;
	newobj->diffuse.g = oldobj->diffuse.g;
	newobj->diffuse.b = oldobj->diffuse.b;
	newobj->params.a = oldobj->params.a;
	newobj->params.b = oldobj->params.b;
	newobj->params.c = oldobj->params.c;
	newobj->params.d = oldobj->params.d;
	newobj->specular.r = oldobj->specular.r;
	newobj->specular.g = oldobj->specular.g;
	newobj->specular.b = oldobj->specular.b;
	newobj->specular.n = oldobj->specular.n;
	newobj->reflection = oldobj->reflection;
	newobj->transparent = oldobj->transparent;
		newobj->speed = oldobj->speed;
	newobj->texture = oldobj->texture;
	newobj->transf = oldobj->transf;
	newobj->inver = oldobj->inver;
    newobj->dataFile = oldobj->dataFile;

    /* initialize pointers */
	newobj->next = NULL;
    newobj->left = NULL;
    newobj->right = NULL;

    return 0;
} /* copyObject */

/*<<<<<<<<<<<<<<<< buildInstanceTree >>>>>>>>>>>>>>>>>>*/
 /* Copies a tree from the intermediate data structure to the output data structure */
SHORT buildInstanceTree(tInstance *parentnode, tInstance *treenode, tObject *copyobj)
{
  tInstance *newnode, *newnode2;
  tAffine3D result;

  assert (parentnode && treenode && copyobj);

  /* First time it's called with this treenode */
  if (!treenode->left && !treenode->right ) {
    if (copyObject(treenode, copyobj)) return -1;

    /* Propagate transformations down */
    result = MatrixMultiply(treenode->transf, parentnode->transf);
    MatrixCopy(&(treenode->transf), &result);
    result = MatrixMultiply(parentnode->inver, treenode->inver);
    MatrixCopy(&(treenode->inver), &result);
  }

	if (copyobj->left || copyobj->right) {
		NEWtInstance(newnode);
	/* attach new node to the tree */
		if (treenode->left)
			treenode->right = newnode;
		else
			treenode->left = newnode;
		if (copyobj->left)
			if (buildInstanceTree(treenode, newnode, copyobj->left)) return -1;
		NEWtInstance(newnode2);
		treenode->right = newnode2;
		if (copyobj->right)
			if (buildInstanceTree(treenode, newnode2, copyobj->right)) return -1;
	}
	if (treenode->left && treenode->right) {
		strcpy(treenode->name, "treenode");
		treenode->type = BOOLOBJ;
	}
	return 0;
} /* buildInstanceTree */


/* <<<<<<<<<<<<<<<<<< displayObj  >>>>>>>>>>>>>>>*/
/* Parse the display list*/
SHORT displayObj(void)
{
	tInstance *newobj, *newobj2, *newobj3;
	tObject *saveObjectptr;

	/* get name of object */
	if(nextSymbol())return -1;
			 /* check if name has been defined, if it is defined globalObjectptr will
	point to the node */
	if (!searchObjList(token))
		SYNTAXERR("defined object (obj in display statement not defined)");
	/* Keep a reference to the current globalObjectptr.*/
    saveObjectptr = globalObjectptr;

	structType = whichStructure(globalObjectptr);

	/* Note: whichStructure(p) returns:
		 BOOLDEF if p->left and p->right both non-NULL;
		 GROUPDEF if p->group is not NULL and there's more than
		 one object in the group (p->group->next is not NULL ;
		 SIMPLEDEF otherwise */

    switch (structType) {

      /* Simple object */
      case SIMPLEDEF:
	    /* check to see if it is really a group structure w/ one node */
		if (globalObjectptr->group) {
			globalObjectptr = globalObjectptr->group;
	        /* Matrix multiply the group transformations with each
               object in the group */
			doGroupMatrixMultiply(globalObjectptr, saveObjectptr);
		}
	    /* copy info from intermediate structure to the output structure */
        NEWtInstance(newobj);
		if (copyObject(newobj, globalObjectptr)) return -1;
        /* Now add the object to the beginning of the output list */
        newobj->next = sceneptr->objectptr;
        sceneptr->objectptr = newobj;
		break;

      /* Group object */
	  case GROUPDEF:
		globalObjectptr = globalObjectptr -> group;

	    while (globalObjectptr) {
	      /* Matrix multiply the group transformations with
		     each object in the group */
          doGroupMatrixMultiply(globalObjectptr, saveObjectptr);

          /* create new display node */
		  NEWtInstance(newobj);
		  if (copyObject(newobj, globalObjectptr)) return -1;
          /* Now add the object to the beginning of the output list */
          newobj->next = sceneptr->objectptr;
          sceneptr->objectptr = newobj;

          /* now copy the boolean tree */
          if (newobj->type == boolObj) {
 		    NEWtInstance(newobj2);
		    newobj->left = newobj2;
		    if (buildInstanceTree(newobj,newobj2,globalObjectptr->left)) return -1;
		    NEWtInstance(newobj3);
		    newobj->right = newobj3;
		    if (buildInstanceTree(newobj,newobj3,globalObjectptr->right)) return -1;
          }

          /* Get next object in group */
		  globalObjectptr = globalObjectptr -> next;
		} /* while */
        break;

      /* Boolean object */
	  case BOOLDEF:
        /* Create output object */
        NEWtInstance(newobj);
		if (copyObject(newobj, globalObjectptr)) return -1;
        /* Now add the object to the beginning of the output list */
        newobj->next = sceneptr->objectptr;
        sceneptr->objectptr = newobj;

		NEWtInstance(newobj2);
		newobj->left = newobj2;
		if (buildInstanceTree(newobj,newobj2,globalObjectptr->left)) return -1;
		NEWtInstance(newobj3);
		newobj->right = newobj3;
		if (buildInstanceTree(newobj,newobj3,globalObjectptr->right)) return -1;
        break;

      default:
        sprintf (errMsg, "MCL internal error: Unknown object type");
        return -1;
	}

    /* check to see if there is another object to add
	to the list. */
	if(nextSymbol())return -1;
	if (Symbol == COMMA)
		if (displayObj()) return -1;
	return 0;
} /* displayObjs */


/*<<<<<<<<<<<<<<<<< writeObjectToOutput >>>>>>>>>>>>>>>>>*/
/* write tInstance object to an outfile */
void writeObjectToOutput(FILE *out, tInstance *optr)
{
	short int i,type;
	fprintf(out,"{\n");
	type = optr->type;
	if (type == BUCKYBALL) type = buckyballObj;
	else if (type == CONE) type = coneObj;
	else if (type == CUBE) type = cubeObj;
	else if (type == CYLINDER) type = cylinderObj;
	else if (type == TAPEREDCYL) type = taperedCylObj;
	else if (type == DODECAHEDRON) type = dodecahedronObj;
	else if (type == ICOSAHEDRON) type = icosahedronObj;
	else if (type == OCTAHEDRON) type = octahedronObj;
	else if (type == PLANE) type = planeObj;
	else if (type == POLYHEDRON) type = polyhedronObj;
	else if (type == SPHERE) type = sphereObj;
	else if (type == SQUARE) type = squareObj;
	else if (type == TETRAHEDRON) type = tetrahedronObj;
	else if (type == TORUS) type = torusObj;
	else if (type == USERDEFINED) type = userDefinedObj;
	else if (type == DIAMOND) type = diamondObj;

	fprintf(out, "%s \n%d %c\n",  optr->name, type, optr->boolOp);
	fprintf(out, "%f\t%f\t%f\n",optr->ambient.r,
			  optr->ambient.g,optr->ambient.b);
	fprintf(out, "%f\t%f\t%f\n", optr->color.r, optr->color.g,
			  optr->color.b);
	fprintf(out, "%f\t%f\t%f\n", optr->diffuse.r,
			  optr->diffuse.g,optr->diffuse.b);
	fprintf(out, "%f\t%f\t%f\t%f\n", optr->specular.r,
			  optr->specular.g,optr->specular.b, optr->specular.n);
	fprintf(out,"%f\t%f\t%f\n",optr->reflection, optr->transparent,
									  optr->speed);
	fprintf(out, "%f\t%f\t%f\t%f\n", optr->texture.a, optr->texture.b,
	optr->texture.c, optr->texture.d);
	fprintf(out, "%f\t%f\t%f\t%f\n\n", optr->params.a,
			  optr->params.b,optr->params.c, optr->params.d);
	/* print out matricies */
	for(i=0; i<3; i++)
		fprintf(out,"%f\t%f\t%f\n", optr->transf.mat[i][0],
				  optr->transf.mat[i][1], optr->transf.mat[i][2]);
	fprintf(out,"%f\t%f\t%f\n", optr->transf.tr.x,
			  optr->transf.tr.y, optr->transf.tr.z);
	fprintf(out,"\n");
	for(i=0; i<3; i++)
		fprintf(out,"%f\t%f\t%f\n", optr->inver.mat[i][0],
				  optr->inver.mat[i][1], optr->inver.mat[i][2]);
	fprintf(out,"%f\t%f\t%f\n", optr->inver.tr.x,
			  optr->inver.tr.y, optr->inver.tr.z);
	fprintf(out,"}\n");
}/* writeObjectToOutput */


/*<<<<<<<<<<<<<<<< writeTreeToOutput >>>>>>>>>>>*/
/* Write a boolean tree to the output file */
void writeTreeToOutput(FILE *out, tInstance *tree)
{
	writeObjectToOutput(out, tree);
	if (tree->left)
		writeTreeToOutput(out, tree->left);
	if (tree->right)
		writeTreeToOutput(out, tree->right);

	if (!tree->right && !tree->left)
		fprintf(out,"NULL\n");
} /* writeTreeToOutput*/


/*<<<<<<<<<<<<<<<<<<<<< parseMCLfile >>>>>>>>>>>>>>>>>>>>*/
/* parse file */
SHORT parseMCLfile( void )
{
	inputFileChar = '\0';
	if(nextSymbol())return -1;
	endOfFile = False;
	while (!endOfFile) {
		if (Symbol == DEFINE) {
			if (defineObject()) return -1;
        }
		else if (Symbol == LIGHT) {
			if (letThereBeLights()) return -1;
        }
		else if (Symbol == CAMERA) {
			if (cameraDefs()) return -1;
        }
		else if (Symbol == DISPLAY) {
			if (displayObj()) return -1;
		  }
		else {
			SYNTAXERR("DEFINE, LIGHT, CAMERA, DISPLAY)\n");
		  }
	} /* while Symbol != ENDFILE */
	return 0;
} /* parseMCLfile */

/*<<<<<<<<<<<<<<< makeScreenExtents >>>>>>>>>>>>>>>>*/
SHORT makeScreenExtent(tInstance *p, tCamera *cam)
{ /* build a screen extent for object p */

	threeTup pt,Q;
	DBL umin,umax,vmin,vmax;
	DBL ut,vt,nt,u,v;
	tWindow w;

	/* set up default extent: whole screen */
	w.l = 0; w.r = (SHORT)(cam->nCols - 1);
	w.t = 0; w.b = (SHORT)(cam->nRows - 1);
	p->screenExtent= w;

	if(p->type == boolObj) return 0; /* boolean.. no extent for now */
	if(p->type == planeObj)	return 0; /* max extent if a plane */

	umin = vmin =  100000;
	umax = vmax = -100000;

	/* map the 8 corners of the generic cube */
	for(pt.x = -1.0; pt.x <= 1.0; pt.x += 2.0)
		for(pt.y = -1.0; pt.y <= 1.0; pt.y += 2.0)
			for(pt.z = -1.0; pt.z <= 1.0; pt.z += 2.0)
			{
			  if(p->type == squareObj) pt.z = 0.0; /* square is its
			  own bounding box */

			  Q.x = 	pt.x * p->transf.mat[0][0] +
						pt.y * p->transf.mat[1][0] +
						pt.z * p->transf.mat[2][0] + p->transf.tr.x;
			  Q.y =	pt.x * p->transf.mat[0][1] +
						pt.y * p->transf.mat[1][1] +
						pt.z * p->transf.mat[2][1] + p->transf.tr.y;
			  Q.z =  pt.x * p->transf.mat[0][2] +
						pt.y * p->transf.mat[1][2] +
						pt.z * p->transf.mat[2][2] + p->transf.tr.z;

			  /* project this transformed corner */

			  Q.x -= cam->eye.x;   /* Q = Q - eye */
			  Q.y -= cam->eye.y;
			  Q.z -= cam->eye.z;

			  ut = DOT3(Q,cam->u); /* note: pt is cam. coord's*/
			  vt = DOT3(Q,cam->v); /* is (ut,vt,nt) */
			  nt = DOT3(Q,cam->n);

			  /* pt is behind eye, use default extent */
			  if(nt <= -cam->eyeDist) return 0;

				u = cam->eyeDist * ut / nt;
				v = cam->eyeDist * vt / nt;

			  umin = MIN(umin,u);
			  umax = MAX(umax,u);
			  vmin = MIN(vmin,v);
			  vmax = MAX(vmax,v);
	} 											/* the 3 for loops */

	/* map box in viewplane onto pixel locations */
	 w.l = (SHORT)(0.5 * (DBL)cam->nCols * (1.0 + umin/cam->W));
	 w.r = (SHORT)(0.5 * (DBL)cam->nCols * (1.0 + umax/cam->W));
	 w.t = (SHORT)(0.5 * (DBL)cam->nRows * (1.0 - vmax/cam->H));
	 w.b = (SHORT)(0.5 * (DBL)cam->nRows * (1.0 - vmin/cam->H));

	 p->screenExtent = w;
	 return 0;
} /* makeScreenExtent*/




/*************************************************************/
/* PREPROCESSOR FUNCTIONS

/********************** MCLPreprocess ************************/

SHORT MCLPreprocess (char * inFilename)
/* Function called by MCL. It opens inFilename, and saves the final file
  as MCL.TMP and passes its name back to MCL.*/
{
  FILE * fout;
  int result;

  if (!(fout = fopen (TEMP_FILENAME, "w"))) {
	 sprintf (errMsg, "MCL preprocessor: Could not open temp file");
	 return preERROR;
  }

  globalincl = NULL;
  lastincl   = NULL;
  symt = SYMTCreate();
  result = IncludeFile (inFilename, fout);
  SYMTDestroy (symt);

  fclose (fout);

  return result;
}


/********************* PreprocessorCleanUp *************************/
SHORT PreprocessorCleanUp (void)
{
  INCLNODE * tmp;

  /* Removes file from disk */
  if (remove(TEMP_FILENAME)) {
    sprintf (errMsg, "MCL preprocessor: Problems with temp file");
	return preERROR;
  }

  /* Free up list of include-nodes */
  while (globalincl) {
    tmp = globalincl;
    globalincl = globalincl->next;
    free (tmp);
  }
  lastincl = NULL;

  return preOK;
}

/******************** IncludeFile *****************************/

SHORT IncludeFile (char * filename, FILE * fout)
/* Recursive function that adds lines to the output file,
	 keeps track of the symbol table, and substitutes all constants.
   The include node list is created when:
       - A new include file starts
       - A file ends
 */
{
  FILE * fin;
  char   line[STRING_LENGTH], tmp[STRING_LENGTH];
  char * inclToken, * value;
  int    result = preOK;            /* Default is no error */
  char   NLstr[2] = {10,0};      /* New Line string */
  LONG   startline, currentline;

  if(!(fin = fopen(filename,"r"))) {
	 /* This is the global error string. Call it whatever is defined in MCL */
	 sprintf (errMsg, "MCL preprocessor: Could not open file %s", filename);
	 return preERROR;
  }

  currentline = 0;
  startline   = 1;
  /* Start getting lines */
  while (fgets (line, STRING_LENGTH-1, fin) != NULL) {
    currentline ++;

    strcpy (tmp, line);
	inclToken = strtok(tmp ," ");
    /* Include a file */
	if (!strcmp (inclToken, "#include")) {
	  inclToken = strtok (NULL, NLstr);
      CreateInclNode (startline, currentline - 1, inclToken);
	  if (IncludeFile (inclToken, fout) < 0) {     /* Error if file not found */
	    result = preERROR;
	    break;
	  }
      startline = currentline;
      strcpy (line, NLstr);
	}
	 /* Process a normal line by first substituting anything and then
       checking if there are any defines */
	else {
	  SubstSymbols (symt, line);
      if (!strcmp (inclToken, "#define")) {
	    inclToken = strtok (NULL, " ");
	    value = strtok (NULL, NLstr);        
        if (value)
          /* If not empty define send string */
  	      SYMTAddSymbol (symt, inclToken, value);
        else
			 /* If emtpy define send null string--not just NULL! */
          SYMTAddSymbol (symt, inclToken, "");
        strcpy (line, "\n");
	  }
    }
    fputs (line, fout);

  } /* while */
  fclose (fin);
  if (line[strlen(line)-1] != 10) {
    fputs (NLstr, fout);
  }

  /* Now create a include node and add it to the list */
  if (result == preOK) 
    CreateInclNode (startline, currentline, filename);    

  return result;
}

/************ SubstSymbols ****************************/

void SubstSymbols (SYMT * symt, char * txt)
 /* Checks the text for any symbols found in the symbol table,
	and if so it substitutes them into the text. */
{
  int i, change;
  char * substr;
  char   tmp[STRING_LENGTH];

  assert (symt);
  for (i = 0; i < symt->total; i++) {
	 do {
		change = 0;
		substr = strstr (txt, symt->entry[i].symbol);
		if (substr) {
		  change = 1;
		  strcpy (tmp, substr);
		  strcpy (substr, symt->entry[i].value);
		  strcat (txt, &tmp[strlen(symt->entry[i].symbol)]);
		} /* if */
	 } while (change);
  } /* for */
} /* SubstSymbols */

/**************** SYMTAddSymbol *******************/

void SYMTAddSymbol (SYMT * symt, char * symbol, char * value)
{
  int i;
  assert (symt);
  i = symt->total;

  assert (symt && symbol);

  /* Remove leading white spaces */
  while (value[0] == ' ')
    value++;

  /* Add symbol to the table */
  symt->entry[i].symbol = (char *) malloc (strlen(symbol)+1);
  assert (symt->entry[i].symbol);
  strcpy (symt->entry[i].symbol, symbol);

  symt->entry[i].value  = (char *)malloc (strlen(value)+1);
  assert (symt->entry[i].value);
  strcpy (symt->entry[i].value, value);

  symt->total++;
}

/*<<<<<<<<<<<<<<<<<< SYMTCreate >>>>>>>>>>>>>>>>>>>>>>>>*/

SYMT * SYMTCreate (void)
{
  SYMT * symt;

  symt = (SYMT *) malloc (sizeof (SYMT));
  assert (symt);
  symt->total = 0;
  return symt;
}

/*<<<<<<<<<<<<<<<<<<<<< SYMTDestroy >>>>>>>>>>>>>>>>>>>>>>>*/

void SYMTDestroy (SYMT * symt)
{
  int i;

  assert (symt);
  for (i = 0; i < symt->total; i++) {
	 free (symt->entry[i].symbol);
	 free (symt->entry[i].value);
  }
  free(symt);
} /* SYMTAddSymbol */


/*********************** GetLineFile ************************/

SHORT GetLineFile (LONG tmpline, char * txt)
/* It translates a temporary line number into a real file number, and
   appends this number and the file to which it belongs to the string
   at txt */
{
  INCLNODE * incl;
  LONG       realline;
  char       tmp[100];

  /* There has to be something in the list */
  if (globalincl == NULL) {
    sprintf (errMsg, "MCL internal error: Include nodes list empty!");
    return preERROR;
  }

  incl = globalincl;
  while (incl && tmpline > incl->tmpend)
    incl = incl->next;
  if (incl == NULL) {
    sprintf (errMsg, "MCL internal error: Include nodes list too short!");
    return preERROR;
  }

  realline = tmpline - incl->tmpstart + incl->realstart;
  sprintf (tmp, "Line: %li File: %s", realline, incl->filename);
  strcat (txt, tmp);

  return preOK;
}

/*********************** CreateInclNode ************************/

SHORT CreateInclNode (LONG start, LONG end, char * filename)
{
  INCLNODE * incl;

  NEW (inclnode, incl);
  incl->realstart = start;
  incl->realend   = end;
  if (lastincl)
    incl->tmpstart = lastincl->tmpend + 1;
  else
    incl->tmpstart = 1;
  incl->tmpend    = incl->tmpstart + (end - start);
  strcpy (incl->filename, filename);

  incl->next = NULL;
  if (lastincl)
    lastincl->next = incl;
  else
    globalincl = incl;
  lastincl = incl;

  return preOK;
}


/* <<<<<<<<<<<<<<<< interpretSceneFile  >>>>>>>>>>>>>>>>>*/

/* This function is used only with choice 2 discussed below. It opens the
MCL scene file, reads and interprets it, writes scene to an output file,
and returns a tSceneDescript struct for use in your raytracer */

SHORT interpretSceneFile(char *rawFileName,tSceneDescript *scene, char *msg)
{
  tFacePlane *thisFace;
  tInstance *p;
  char buff[81]; /* to read a line from file */
  FILE *fp;
  int i, j,numFaces;

  sceneptr = scene; /* hook up global ptr. sceneptr here with right address */
  errMsg = msg;  	/* ditto for global error message string ptr. */
  strcpy (errMsg, "");  /* Initialize error to null string */

  if (MCLPreprocess (rawFileName) < 0) return -1;
  if(initScan())return -1;
  if(!(MCLfile = fopen(TEMP_FILENAME,"r")))
  {
	strcat(errMsg,"Can't open mcl.tmp");
	return(-1);
  }
  if (parseMCLfile()) {
	 return -1;
  }
  fclose(MCLfile);
  if (PreprocessorCleanUp ()) return -1;

	/* if you also want to write an actual output file, replace the
	following if..else statement with: outputData(); the if..else clause
	 fills in the dependent camera attributes; */

	if (scene->cameraptr == NULL)
		scene->cameraptr = camera; /* default camera */
	else
		if(doCameraCalcs(scene->cameraptr))return -1; /* fill in camera values */

	if(scanCleanUp())return -1;

	/* scan through objects and put screen extent
			in each (except for booleans) */

  for(p = sceneptr->objectptr; p != NULL; p = p->next)
  {
		if(makeScreenExtent(p, scene->cameraptr))return -1; /* put screen extent into object */
		/* printf("\n screen extent = (%4d,%4d,%4d,%4d)",
			p->screenExtent.l, p->screenExtent.t,
			p->screenExtent.r,p->screenExtent.b); */
	}

	/* read planes in planes.dat and build each plane list.*/
	if(!(fp=fopen("planes.dat","r"))) return -1;
	while(fgets(buff,80,fp) && buff[0] != '#'); /* skip comments */

	fscanf(fp,"%hd",&numPlanes);  /* is currently 7 */
    scene->polyList = (tPlaneList *)malloc (numPlanes * sizeof(tPlaneList)); 
    if (!scene->polyList) {
	  strcat(errMsg,"\n in buildPlaneList: out of memory");
	  return -1;
	}

	for(i = 0;i < numPlanes;i++)
	{
		fscanf(fp,"%s",scene->polyList[i].name); /* read, but not used */

		fscanf(fp,"%d",&numFaces);  /* num faces in this polyhedron */
		scene->polyList[i].num = numFaces;

		thisFace = (tFacePlane *)malloc(numFaces * sizeof(tFacePlane));
		if(!thisFace)
		{
			strcat(errMsg,"\n in buildPlaneList: out of memory");
			return -1;
		}
		scene->polyList[i].face = thisFace;

		for(j = 0; j < numFaces; j++)  /* read the plane */
		  fscanf(fp,"%lf %lf %lf", &thisFace[j].pt.x,
					&thisFace[j].pt.y,&thisFace[j].pt.z);

	} /* for i */
	fclose(fp);

    /* Go through all the objects, and update the planes pointer for the
       polyhedra */
	for (p = scene->objectptr; p != NULL; p = p->next) {
      FillPlanePtr (p, scene);
    }

	return 0;

} /* interpretSceneFile */


/*********************** FillPlanePtr ************************/
void FillPlanePtr (tInstance * obj, tSceneDescript * scene)
/* It fills the plane field in the object with a pointer to the
   plane list if it's a polyhedron, or NULL if it isn't.
   If it's a boolean, we recursively fill the children. */
{
  if (!obj) return;

  if (obj->type < numPlanes)
    obj->planes = &scene->polyList[obj->type];
  else
    obj->planes = NULL;

  if (obj->type == boolObj) {
    FillPlanePtr (obj->left, scene);
    FillPlanePtr (obj->right, scene);
  }
}



/*********************** MCLfree ************************/

SHORT MCLfree (tSceneDescript * scene)
/* This function frees up all the memory dynamically allocated
   by MCL while interpreting the scene and creating the data
   structures.
   WARNING: if there is anything attached to the "stuff" field,
            it's the programmer's responsability to free it
            before calling this function.
*/
{
  void * ptr, * tmp;
  tInstance * obj;
  int i;

  /* First free the camera */
  free (scene->cameraptr);

  /* Now the lights */
  ptr = scene->lightptr;
  while (ptr) {
    tmp = ptr;
    ptr = ((tLight *)ptr)->next;
    free (tmp);    
  }

  /* Now the objects */
  obj = scene->objectptr;
  while (obj) {
    tmp = obj;
    obj = obj->next;
    if (FreeObject ((tInstance **)&tmp)) return -1;
  }

  /* And finally the plane list */
  assert (scene->polyList);
  for (i = 0; i < numPlanes; i++) {
    if (scene->polyList[i].face == NULL) {
      sprintf (errMsg, "MCL internal error: Null pointer in plane list");
      return -1;
    }
    free (scene->polyList[i].face);
  }
  free (scene->polyList);
  scene->polyList = NULL;

  return 0;
}

/*********************** FreeObject ************************/

SHORT FreeObject (tInstance ** obj)
/* Recursive function the frees up all the memory taken up by
   an object instance */ 
{
  if (obj == NULL || *obj == NULL) { 
    sprintf (errMsg, "MCL internal error: Null pointer at FreeObject()");
    return -1;
  }

  /* Free file name */
  if ((*obj)->dataFile != NULL)
    free ((*obj)->dataFile);

  /* Free boolean tree */
  if ((*obj)->left)
    FreeObject (&(*obj)->left);
  if ((*obj)->right)
    FreeObject (&(*obj)->right);

  /* Get rid of object itself */
  free (*obj);
  obj = NULL;

  return 0;
}


/*<<<<<<<<<<<<<<<< end of MCL2.C >>>>>>>>>>>>>>>>>>>>>>>*/

