/************************** gifboth.c *********************/
/* GIFBOTH.C, version 1.0,  February 21, 1995 */
/* F.S.Hill, , for ECE661 */
/* A Portable GIF reader and writer
 based largely on Craig Lindley's gif.c Code from
		"Practical Ray Tracing in C" book,
		J. Wiley, New York, 1992
*/
/* GIF is a trademark of Compuserve */
/*
The two main routines have usage:

1). DisplayGIFFile(fname, left, top, right, bottom);
  which reads the GIF file named fname, starts up graphics, 
  and draws the GIF image in the rectangle 
  R = (left, top, right, bottom) on the display. 
  Any part of the image lying outside R is not drawn; 
  Any part of R not covered by the image is not altered.

2). WriteGIFFile(fname, left, top, right, bottom);
  which creates the GIF file named fname, and writes
  to it the portion of the displayed image on the screen
  lying in the rectangle R = (left, top, right, bottom).
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define fatal(msg) {puts(msg);exit(-1);}
#define MIN(a,b)  (((a)<(b)) ? (a) : (b))
#define MAX(a,b)  (((a)>(b)) ? (a) : (b))

/* #define tellGIF 2 */
/* #define _IBMDOS_ */
#ifdef _IBMDOS_
#include <process.h>
#include <conio.h>
#include <alloc.h>
#include <graphics.h>
#include <dos.h>     /* declares REGPACK for assembly routines */
#define FAR far
#define HUGE huge
#define SETPIXEL(col,row,ind) putpixel(col,row,ind)
#define GETPIXEL(col,row) getpixel(col,row)
#define CALLOC(num,size) farcalloc(num,size)
#define FREE(x) farfree(x)
#else
/* for other systems */
/* Here my fake is to have setpixel do nothing, and
	to have getpixel just evaluate to some value, here 17. */

extern  unsigned short getPixel(short col, short row); 									/* re */
extern  void putPixel(short col, short row, short index);								/* re */

#define FAR    /* empty */
#define HUGE   /* empty */
#define SETPIXEL(col,row,ind) putPixel(col,row,ind)	/* put yours here */
#define GETPIXEL(col, row)  getPixel(col, row)
#define CALLOC(num,size) calloc(num,size)
#define FREE(x) free(x)
#endif

/* Global Definitions */
#define BITSPERPIXEL               8
#define MINBITS     (BITSPERPIXEL+1)
#define MAXBITS                   12   /* 9 - 12 bits used for 8 bit images */
#define TABLESIZE               5003   /* Prime # greater than 2^MAXBITS */
#define HASHING_SHIFT              5
#define CLEARCODE                256   /* Code to flush the string table */
#define EOICODE                  257   /* End of Information code */
#define FIRSTCODE                258   /* First code_value */
#define EXPBUFSIZE               255   /* Size of GIF data block buffer */
#define EXPBUFBITS   (EXPBUFSIZE<<3)   /* Size of above in bits */
#define COMPBUFSIZE              255   /* Size of GIF data block buffer */
#define COMPBUFBITS (COMPBUFSIZE<<3)   /* Size of above in bits */
#define MAXCODEVAL(n)       (1<<(n))   /* Max code value macro */
#define FLUSHCODE     (MAXCODEVAL(MAXBITS+1)) /* signals time to flush */

/* Error Bit Definitions */
#define NoError           0
#define EBadParms        -1
#define EFileNotFound    -2
#define EOpeningFile     -3
#define EReadFileHdr     -4
#define ENotPCXFile      -5
#define ECorrupt         -6
#define EWrtFileHdr      -7
#define EWrtOutFile      -8
#define EReadScanLine    -9
#define EWrtScanLine    -10
#define EPCCFile        -11
#define EGraphics       -12
#define ENoMemory       -13
#define EWrtExtPal      -14
#define ENotGIFFile     -15
#define EReadRowNum     -16
#define EReadData       -17

#define BITSPERCOLOR          6
#define GLOBALCOLORTBL     0x80
#define LOCALINTERLACEFLAG 0x40
#define GLOBALCOLORTBLSIZE    8

/*<<<<<<<<<<<<<<<< typedefs >>>>>>>>>>>>>>>>*/

/* ALERT!!! adjust next four typedef's so that on your system BYTE,
USHORT, SHORT, and LONG are of lengths 1, 2, 2, and 4 bytes */

typedef unsigned char BYTE;		/* must be one-byteinteger */
typedef unsigned short USHORT;  /* must be two-byte integer */
typedef short SHORT;  			/* must be two-byte integer */
typedef unsigned long ULONG;	/* must be four-byte integer */

typedef BYTE LUTpalette[256][3];
typedef USHORT CompletionCode;
enum { red=0,grn=1,blu=2 }; /* index values for R,G,B */

/*<<<<<<<<<<<<<<<<< global variables >>>>>>>>>>*/

struct LogicalScreenDescriptor
{
	USHORT LogicalScreenWidth;   /* logical screen width */
	USHORT LogicalScreenHeight;
	BYTE     PackedField;
	BYTE     BackGroundIndex;
	BYTE     AspectRatio;
}ScnDesc;

struct ImageDescriptor   {
	char     ImageSeparator;
	USHORT ImageLeftPosition;
	USHORT ImageTopPosition;
	USHORT ImageWidth;
	USHORT ImageHeight;
	BYTE     PackedField;
}ImgDesc;

LUTpalette Pal; /* create array for LUT values */
static FILE *GIFFile;
int Verbose = 1;  			 /* give data on files */
USHORT TheImageWidth;       /* width  in pixels of image */
USHORT TheImageHeight;      /* height in pixels of image */
USHORT TheImageTop;         /* top offset of image on screen */
USHORT TheImageLeft;        /* left offset of image */
USHORT gleft,gtop,gright,gbottom; /* global border of image */

/* Variable used for the LZW compression and expansion */
static USHORT NumOfBits;             /* current # of bits in code */
static USHORT newCode;
static SHORT  newCharacter;
static USHORT newIndex;
static USHORT NextCode;
static USHORT MaxCode;               /* current max code value */
static USHORT NextBitPos;            /* next free bit position */
static BYTE CompressionBuf[COMPBUFSIZE+3];
static BYTE ExpandBuf[EXPBUFSIZE];     /* for expansion for disk read */
static SHORT  BytesLeftInBlock;          /* bytes left in block just read */
static USHORT ScanLine;              /* scan line of raster image */
static USHORT Pixel;                 /* pixel in scan line */
/* global pointers to arrays used in compression and expansion */


#ifdef _IBMDOS_
static union REGS regs;
#endif
static USHORT FAR *prefix_code;
static BYTE   FAR *append_character;
static SHORT  FAR *code_value;       /* hash table of code values */
static BYTE   FAR *decode_stack;     /* array holds decoded string */

/*<<<<<<<<<<<<<<< GIF Prototypes >>>>>>>>>>>>>>>>> */

USHORT getShort(FILE * f);
ULONG getLong(FILE *f);
void putShort(USHORT s, FILE *fp);
void putLong(ULONG i, FILE *fp);

static USHORT AllocMem(void);
static void DeAllocMem(void);
static void InitTable(void);
static USHORT GetByte(void);
static void OutPixel(USHORT Color);
static USHORT OutCode(USHORT Code);
static SHORT FindMatch(USHORT Code, USHORT Character);
static BYTE FAR *DecodeCodes(BYTE FAR *Buffer, USHORT Code);
static USHORT InCode(void);
static void ExpandFile (void);
static void CompressFile(void);

void DisplayGIFFile (char *FileName,
							USHORT left,  USHORT top,
							USHORT right, USHORT bottom);

void WriteGIFFile (char *FileName,
						 USHORT left,  USHORT top,
						 USHORT right, USHORT bottom);

/*<<<<<<<<<<<<<<< graphics prototypes >>>>>>>>>>>*/
/* SetPixel() and GetPixel() are macros here */
void setPalette(LUTpalette PalBuf);
extern void getPalette(LUTpalette PalBuf);			/* re */

void startGraphics(void);
void endGraphics( void );
int HUGE DetectVGA(void);

/*<<<<<<<<<<<<<<<<<<<<< getShort >>>>>>>>>>>>>>>>>>>>>>>>>*/
USHORT getShort(FILE * f)
{
/* get a 2-byte value from little-endian file */
	USHORT x,y;
	y = (USHORT)fgetc(f);
	x = (USHORT)fgetc(f);
	return (y | (x << 8));
}
/*<<<<<<<<<<<<<<<<<<< getLong >>>>>>>>>>>>>>>>>>>>>>>>*/
ULONG getLong(FILE *f)
{
/* get 4-byte value from little-endian file */

	ULONG ip,ic;
	ip = (ULONG)fgetc(f);
	ic = (ULONG)fgetc(f);
	ip = ip | (ic << 8);
	ic = (ULONG)fgetc(f);
	ip = ip | (ic << 16);
	ic = (ULONG)fgetc(f);
	ip = ip | (ic << 24);
	return ip;
}

/*<<<<<<<<<<<<<<<<<<<<< putShort >>>>>>>>>>>>>>>>>>>>*/
void putShort(USHORT s, FILE *fp)
{
/* write to file fp a 2-byte short s in little endian order */
	BYTE c;
	c = s & 0xff; 				putc(c,fp);
	c = ( s >> 8) & 0xff; 	putc(c, fp);
}

/*<<<<<<<<<<<<<<<<<<<<< putLong  >>>>>>>>>>>>>>>>>>>>>>>>>*/
void putLong(ULONG i, FILE *fp)
{
/* write to file a 4-byte long i in little endian order */

	BYTE c;
	c  = i & 0xff;			  putc(c,fp);
	c = (i >> 8)  & 0xff;  putc(c,fp);
	c = (i >> 16) & 0xff;  putc(c,fp);
	c = (i >> 24) & 0xff;  putc(c,fp);
}

/*<<<<<<<<<<<<<<<<< DeAllocMem>>>>>>>>>>>>>>>>>>>>*/
/* This function deallocs all memory from the far heap that has
currently been allocated. If the memory pointers are NULL, no
memory has been allocated for the corresponding array.*/
static void DeAllocMem(void)  {

  if (prefix_code != NULL)       FREE(prefix_code);
  if (append_character != NULL)  FREE(append_character);
  if (code_value != NULL)        FREE(code_value);
  if (decode_stack != NULL)      FREE(decode_stack);
}

/*<<<<<<<<<<<<<<<<<< AllocMem >>>>>>>>>>>>>>>>>>>>>>>*/
/* This function allocs the memory for the required arrays from the
far heap. The same memory is allocated for compression and expansion
although not actually necessary. */

static USHORT AllocMem(void)
{
  /* NULL all pointers so you can tell if alloc succeeded */
  prefix_code = NULL;
  append_character = NULL;
  code_value = NULL;
  decode_stack = NULL;

  prefix_code = (USHORT FAR *)
	 CALLOC((ULONG) TABLESIZE, (ULONG) sizeof(USHORT));
  append_character = (BYTE FAR *)
	 CALLOC((ULONG) TABLESIZE, (ULONG) sizeof(BYTE));
  code_value = (SHORT FAR *)
	 CALLOC((ULONG) TABLESIZE, (ULONG) sizeof(int));
  decode_stack = (BYTE FAR *)
	 CALLOC((ULONG) TABLESIZE, (ULONG) sizeof(BYTE));

  /* Make sure all allocs were successful */
  if (!prefix_code || !append_character ||
		!code_value  || !decode_stack)  {
	 DeAllocMem();
	 fatal("Error not enough memory for GIF arrays\n");
  }
  return(1);              /* everything is ok */
}/* AllocMem */

/*<<<<<<<<<<<<<<<<<<<<<< InCode >>>>>>>>>>>>>>>>>>>>>>*/
/* This function is used in LZW expansion to return the next NumOfBits code
from the input file. It handles the blocking utilized in the GIF
spec. When the expansion buffer is depleted of data, this function
reads in the next block from the disk. */

static USHORT InCode(void)
{
	static USHORT Mask[4] =   /* for masking 9, 10, 11 and 12 bit codes */
					 { 0x01FF, 0x03FF, 0x07FF, 0x0FFF };
	register USHORT BitPos, BytesToMove, Index;
	register USHORT BytePos;
	register ULONG ExtractWord;
	register int TheChar;

	BitPos = NextBitPos & 7;       /* bits used in current byte of buffer */
	BytePos= NextBitPos >> 3;      /* byte in buffer with free bits */

	if ((NextBitPos + NumOfBits) >= EXPBUFBITS)   {  /* at end of buf ? */
	  BytesToMove = EXPBUFSIZE - BytePos;            /* if so move last */
	  for (Index = 0; Index < BytesToMove; Index++)  /* bytes to beg of next */
		  ExpandBuf[Index] = ExpandBuf[Index+BytePos];/* buffer. */
	  while (Index < EXPBUFSIZE)   {                 /* fill rest of buf */
		  if (BytesLeftInBlock == 0)   {              /* from disk */
			 BytesLeftInBlock = getc(GIFFile);         /* read block count */
			 if (BytesLeftInBlock == EOF)              /* if EOF read - abort */
				return(EOICODE);
			 if (BytesLeftInBlock == 0)                /* remember to process */
				break;                                  /* last few bytes in */
		  }                                           /* ExpandBuf */
		  TheChar = getc(GIFFile);                    /* read data from block */
		  if (TheChar == EOF)
				return(EOICODE);
		  ExpandBuf[Index++] = TheChar;
		  BytesLeftInBlock--;
	  }
	  BitPos = NextBitPos = NextBitPos & 7;
	  BytePos= 0;
	}
	/* When we get here, we have a code to return */
	ExtractWord = ((ULONG) ExpandBuf[BytePos]) |
					  ((ULONG) ExpandBuf[BytePos+1] << 8) |
					  ((ULONG) ExpandBuf[BytePos+2] << 16);
	ExtractWord >>= BitPos;
	NextBitPos += NumOfBits;
	return(ExtractWord & Mask[NumOfBits-MINBITS]);
}/* InCode */

/*<<<<<<<<<<<<<<<<<<<< OutPixel>>>>>>>>>>>>>>>>>>>>>>*/
/* Sets one pixel of the raster image each time
it's called. Remember its location in the image
each time it is called. Only draw those lying
in the given rectangle */

static void OutPixel(USHORT Color)
{

  if((Pixel+ gleft) <= gright && (ScanLine + gtop) <= gbottom)
	  SETPIXEL(Pixel+gleft, ScanLine+gtop, Color);
#ifdef tellGIF
	printf("\n %4X %u ",Color, NumOfBits);
#endif
  Pixel++;
  if (Pixel == ImgDesc.ImageWidth)  {
	 Pixel = 0;
	 ScanLine++;
  }
}/* OutPixel */

/*<<<<<<<<<<<<<<<<<<<<< InitTable >>>>>>>>>>>>>>>>>>>>>>*/
/* This function initializes the code_value array and various variables
necessary for the LZW compressor and expander to work. This is called
initially and then whenever the CLEARCODE is output or received.*/

static void InitTable(void)
{
  register USHORT Index;

  /* Initialize the string table first */
  for (Index = 0; Index < TABLESIZE; Index++)
	 code_value[Index] = -1;

  NumOfBits= MINBITS;
  NextCode = FIRSTCODE;
  MaxCode  = MAXCODEVAL(MINBITS);
}/* InitTable */

/*<<<<<<<<<<<<<<<< DecodeCodes >>>>>>>>>>>>>>>>>*/
/* Decode a string from the string table, storing it in a buffer.
The buffer can then be output in reverse order by the expansion
program.*/

static BYTE FAR *DecodeCodes(BYTE FAR *Buffer, USHORT Code)
{
	int Count = 0;

	while(Code >= FIRSTCODE )  {
		*Buffer++ = append_character[Code];
		Code = prefix_code[Code];
		if (Count++ >= TABLESIZE )
			fatal("Error during code expansion\n");
	}
	*Buffer = Code;
	return(Buffer);
}/* DecodeCodes */

/*<<<<<<<<<<<<<< ExpandFile >>>>>>>>>>>>>>>*/
static void ExpandFile (void)
{
	USHORT NewCode;
	USHORT OldCode;
	USHORT Character;
	BYTE FAR *Sp;                  /* decode stack stack pointer */

	ScanLine = 0;                  /* initialize where in raster image */
	Pixel = 0;                     /* to store expanded pixel data */
	memset(ExpandBuf,'\0',sizeof(ExpandBuf)); /* clear buffer */
	BytesLeftInBlock = 0;          /* bytes left in block just read */
	NextBitPos = EXPBUFBITS;       /* prime the pump for disk read */
	Character = getc(GIFFile);     /* read bits per pixel then throw away */
	InitTable();

	while ((NewCode = InCode()) != EOICODE)  {

		if (NewCode == CLEARCODE)  {/* clear string table */
			InitTable();
			OldCode = InCode();
			if (OldCode == EOICODE)
			  break;
			Character = OldCode;
			OutPixel(OldCode);
			continue;
		}
		if (NewCode >= NextCode)  { /* check for special case */
			*decode_stack=Character;
			Sp=DecodeCodes(decode_stack+1,OldCode);
#ifdef tellGIF
		printf("\007"); /* beep */
#endif
		}
		else
			Sp=DecodeCodes(decode_stack,NewCode);

		Character = *Sp;            /* output decoded string in reverse */
		while (Sp >= decode_stack)
		  OutPixel(*Sp--);

		if (NextCode <= MaxCode) {  /* add to string table if not full */
											 /* if full do not add anything */
			prefix_code[NextCode] = OldCode;
			append_character[NextCode++] = Character;
			if ((NextCode == MaxCode) && (NumOfBits < MAXBITS)) {
				MaxCode = MAXCODEVAL(++NumOfBits);
			}
		}
		OldCode=NewCode;
	}
}
/* ExpandFile */

/* <<<<<<<<<<<<<<<<<<<<< DisplayGIFFile >>>>>>>>>>>>>>>>*/
/* Read and display the 256 color GIF file.*/

void DisplayGIFFile (char *FileName,
							USHORT left,  USHORT top,
							USHORT right, USHORT bottom)
{
	int AChar;
	register USHORT Index;
	char str[7];
	USHORT ind;

	gtop = top;    /* make global copies of rectangle */
	gleft = left;
	gright = right;
	gbottom = bottom;

	if (!AllocMem())               /* alloc the required arrays */
	  fatal("out ofmemory!!");


	/* try to open the GIF file */
	if ((GIFFile = fopen(FileName,"rb")) == NULL)
	{
		printf("GIF file: %s not found\n",FileName);
		exit(-1);
	}

	/* read 6 char's in file, and see if they are: "GIF87a" */

	for(ind = 0; ind < 6; ind++)
		str[ind] = fgetc(GIFFile);
	str[6] = '\0';

	if (strcmp(str,"GIF87a") != 0)
			fatal("Error not a GIF file\n");

	/* Read in the Logical Screen Descriptor */

	ScnDesc.LogicalScreenWidth  =	getShort(GIFFile);
	ScnDesc.LogicalScreenHeight = getShort(GIFFile);
	ScnDesc.PackedField         =	getc(GIFFile);
	ScnDesc.BackGroundIndex 	 =	getc(GIFFile);
	ScnDesc.AspectRatio         =	getc(GIFFile);

	/* OK, we have a GIF file. Display info if requested */
	if (Verbose)
	{
		printf("GIF Image Information for file: %s\n\n",FileName);
		printf("\tLogical Screen Width:  %d\n", ScnDesc.LogicalScreenWidth);
		printf("\tLogical Screen Height: %d\n", ScnDesc.LogicalScreenHeight);
		printf("\tPacked Field: 0x%Xd\n",ScnDesc.PackedField);
		printf("\tBackground Color Index: %d\n",ScnDesc.BackGroundIndex);
		printf("\tAspect Ratio: %d\n",ScnDesc.AspectRatio);
	}
	if ((ScnDesc.PackedField & GLOBALCOLORTBL) == 0)
			fatal("Error no global color table in file\n");

	if ((ScnDesc.PackedField & 7) != 7)
			fatal("Error only 256 color images supported\n");

	/* read and scale LUT values from GIF file */
	/* scale 0..255 to 0.63 */
	for(ind = 0; ind < 256; ind++)
	{
		Pal[ind][red] =	getc(GIFFile);
		Pal[ind][grn] = 	getc(GIFFile);
		Pal[ind][blu] = 	getc(GIFFile);

/*		Pal[ind][red] >>= 2; */
/*		Pal[ind][grn] >>= 2; */
/*		Pal[ind][blu] >>= 2; */
	}
	/* Header has been read, now read image info */
	ImgDesc.ImageSeparator = getc(GIFFile);
	ImgDesc.ImageLeftPosition = getShort(GIFFile);
	ImgDesc.ImageTopPosition = getShort(GIFFile);
	ImgDesc.ImageWidth = getShort(GIFFile);
	ImgDesc.ImageHeight = getShort(GIFFile);
	ImgDesc.PackedField = getc(GIFFile);

	if (Verbose)
	{
		printf("\nImage Descriptor Information for file: %s\n\n",FileName);
		printf("\tImage Separator: %c\n",ImgDesc.ImageSeparator);
		printf("\tImage Left Position: %d\n", ImgDesc.ImageLeftPosition);
		printf("\tImage Top  Position: %d\n", ImgDesc.ImageTopPosition);
		printf("\tImage  Width: %d\n", ImgDesc.ImageWidth);
		printf("\tImage Height: %d\n", ImgDesc.ImageHeight);
		printf("\tPacked Field: 0x%Xd\n",ImgDesc.PackedField);
		printf("\nHit <Enter> to proceed - ^C to abort\n");
	}

	if (ImgDesc.ImageSeparator != ',')
	{
		endGraphics();
		fatal("Image separator expected\n");
	}
	if ((ImgDesc.PackedField & LOCALINTERLACEFLAG) != 0)
	{
		endGraphics();
		fatal("Only non-interlaced images supported\n");
	}

	startGraphics(); /* choose video mode and start */
	setPalette(Pal);
	ExpandFile();    /* unpack and display GIF file */

	AChar = getc(GIFFile); /* read trailer */
	if ((AChar != ';') && (AChar != 0x21))  {
		endGraphics();
		printf("Error reading trailer or multiple images in GIF file\n");
		fclose(GIFFile);
		exit(ECorrupt);
	}
	fclose(GIFFile);
	DeAllocMem();
}/* DisplayGIFFile */

/*################## Make a GIFfile ######################*/
/* The following routines create a GIF file from the */
/* displayed raster image and write it to disk.*/
/*########################################################*/

/*<<<<<<<<<<<<<<<<<<<<<<<< OutCode >>>>>>>>>>>>>>>>>>>>>>>>*/
/* Output a variable length code. This code packs the Code value into
the current number of bits in the CompressionBuf array. When a full
block (COMPBUFSIZE) has been accumulated it will be written to disk.
When FLUSHCODE is given, the data in CompressionBuf is written to the
disk in a somewhat smaller block.*/

static CompletionCode OutCode(USHORT Code)
{
	register USHORT BitPos;
	register USHORT BytePos;
	register ULONG MergeWord;

	BitPos = NextBitPos & 7;       /* bits used in current byte of buffer */
	BytePos= NextBitPos >> 3;      /* byte in buffer with free bits */

	if (Code != FLUSHCODE)   {     /* no flush, just normal operation */
	  if (BitPos == 0)   {         /* if buffer happens to be byte aligned */
		 CompressionBuf[BytePos]   = Code;      /* store lower 8 bits of Code */
		 CompressionBuf[BytePos+1] = Code >> 8; /* store upper bits of Code */
	  }
	  else   {                       /* not so lucky no byte alignment */
		 MergeWord = (((ULONG) Code) << BitPos) |
						 CompressionBuf[BytePos];
		 CompressionBuf[BytePos]   = MergeWord;      /* store lower  bits */
		 CompressionBuf[BytePos+1] = MergeWord >> 8; /* store middle bits */
		 CompressionBuf[BytePos+2] = MergeWord >> 16;/* store upper  bits */
	  }
	  NextBitPos += NumOfBits;          /* add in current code bit size */
	  /*
	  Now check to see if we have filled the buffer and must write it to
	  the disk.
	  */
	  if (NextBitPos >= COMPBUFBITS)  { /* is the buffer full ? */
		 putc(COMPBUFSIZE,GIFFile);      /* write the block size */
		 if (fwrite(CompressionBuf,COMPBUFSIZE,1,GIFFile) != 1)
			return(EWrtScanLine);
		 BytePos= NextBitPos >> 3;       /* byte in buffer with free bits */
		 BytePos -= COMPBUFSIZE;         /* calc # of bytes over COMPBUFSIZE */
		 NextBitPos = (BytePos << 3) +   /* calc # of bits to carry over */
						  (NextBitPos & 7);
		 /* Copy end of buffer to beginning */
		 CompressionBuf[0] = CompressionBuf[COMPBUFSIZE];
		 CompressionBuf[1] = CompressionBuf[COMPBUFSIZE+1];
		 CompressionBuf[2] = CompressionBuf[COMPBUFSIZE+2];
	  }
	}
	else   {                            /* flush code received */
	  if (BitPos != 0)                  /* if we have started using new byte */
		 BytePos++;                      /* must include it in final block */
	  putc(BytePos,GIFFile);            /* write the block size */
	  if (fwrite(CompressionBuf,BytePos,1,GIFFile) != 1)
			return(EWrtScanLine);         /* and the block */
	}
	return(NoError);
} /* OutCode */

/*<<<<<<<<<<<<<<<<<<<<< GetByte >>>>>>>>>>>>>>>>>>>>*/
/* This function gets one pixel of information from the raster image
each time it is called. It remember its location in the image each
time it is called.*/

static USHORT GetByte(void)
{

  USHORT RetChar;

  RetChar = GETPIXEL(Pixel,ScanLine);

  Pixel++;
  if (Pixel > gright)
  {
	 Pixel = gleft;
	 ScanLine++;

	 if (ScanLine > gbottom)
		RetChar = EOF;
  }
/*	printf("RetChar=%d\n", (int)RetChar); */
  return(RetChar);
}/* GetByte */

/*<<<<<<<<<<<<<<<<<<< Findmatch >>>>>>>>>>>>>>>>>>>>>*/
/* This is the hashing routine. This code probes with the hash at
Index to see if either the entry is empty or if it is filled with
the data we are looking for. If the initial hash Index is
occupied, we will try again for an empty Index by incrementing.*/

static SHORT FindMatch(USHORT Code, USHORT Character)
{
	register USHORT Index, Offset = 1;

	Index = (Code ^ (Character << HASHING_SHIFT)) % TABLESIZE;
	while(1)
	{
		if (code_value[Index] == -1)     /* if entry is empty return Index */
			return(Index);
		if (prefix_code[Index] == Code &&/* if entry is our data return also */
			 append_character[Index] == Character)
			return(Index);
		Index += Offset;                 /* not empty nor our data so we */
		Offset += 2;                     /* must probe again */
		if (Index >= TABLESIZE)          /* must keep Index within bounds */
			Index -= TABLESIZE;
	}
}/* FindMatch */

/* <<<<<<<<<<<<<<<<<<<<<<CompressFile >>>>>>>>>>>>>>*/
/* Do the LZW compression of the raster data. */

static void CompressFile(void)
{

	register USHORT Code, Index;
	register int Character;

	ScanLine = gtop;        /* initialize where in raster image */
	Pixel = gleft;          /* to fetch pixel data from */
	InitTable();           /* initialize variables */
	memset(CompressionBuf,'\0',sizeof(CompressionBuf)); /* clear buffer */
	NextBitPos = 0;                     /* initialize to start at buf beg */
	putc(BITSPERPIXEL,GIFFile);         /* output initial # of bits used */
	OutCode(CLEARCODE);                 /* output clear code */

	Code=GetByte();                     /* get first pixel of data */

	/* The main compression loop.*/
	/* When the table is full try to increment the code size. */
	while((unsigned short)(Character = GetByte()) != (unsigned short)EOF)
	{
		Index=FindMatch(Code,Character);
		if (code_value[Index] != -1)
			/*	When we get here, Code/Character combo has been found in
			the table. Fetch the Code value for this combo.	*/
			Code = code_value[Index];
		else
		{
			/* When we get here, the Code/Character combo in not in the
			table so add it. NextCode is assigned to this combo.*/
			OutCode(Code);                          /* send out current code */
			code_value[Index]       = NextCode++;   /* add to table */
			prefix_code[Index]      = Code;
			append_character[Index] = Character;
			/* If the next Code to go out will exceed the number of bits
			currently in use, bump to the next larger number of bits. */
			if ((NextCode-1) == MaxCode) /* next code needs more bits? */
			{
				if (NumOfBits < MAXBITS) /* any more bits to use ? */
				{
					MaxCode = MAXCODEVAL(++NumOfBits);  /* yes, add one more */
				}
				else                       /* no more bits to use */
				{
					OutCode(CLEARCODE);     /* output clear code and flush */
					InitTable();            /* the string table and start */
				}                          /* over with MINBITS again. */
			}
			Code = Character;
		}
	}
	/* Finish up the rest of the file output */
	OutCode(Code);                      /* Output the final code */
	OutCode(EOICODE);                   /* Output the End of Info code */
	OutCode(FLUSHCODE);                 /* Flush the compressed data to disk */
}/* Compressfile */



/*<<<<<<<<<<<<<<<< WriteGIFHdr>>>>>>>>>>>>>>>>>>>>>>>*/
/* This function opens the GIF file for output and writes the required
GIF header, the Logical Screen Descriptor, the palette of 256 colors
which make up the Global Color Table and the image descriptor. The file
is then ready for the LZW compressed image data.*/

/*<<<<<<<<<<<<<<<<<<<<<< WriteGIFFile >>>>>>>>>>>>>>>>>>>>>>*/
/* This function writes a GIF file to disk from the raster
image currently being displayed on the monitor.*/

void WriteGIFFile (char *FileName,
						 USHORT left,  USHORT top,
						 USHORT right, USHORT bottom)
{
	char  str[] = "GIF87a";
	BYTE   PackedF;
	register USHORT Index;

	gleft = left;
	gtop = top;
	gright = right;
	gbottom = bottom;
	TheImageWidth  = right - left + 1;
	TheImageHeight = bottom - top + 1;

	if (!AllocMem())
	  fatal("not enough memory!!");

	/* Attempt to open the file for binary writing */
	if ((GIFFile = fopen(FileName,"wb")) == NULL) {
		endGraphics();
		fatal("Could not open output GIF file\n");
	}
	/* Write the GIF tag to the file */
	for(Index = 0; Index <6 ; Index++)
		fputc(str[Index],GIFFile);

	/* Initialize the logical screen descriptor (LSD) */
	ScnDesc.LogicalScreenWidth  = TheImageWidth;
	ScnDesc.LogicalScreenHeight = TheImageHeight;

	/* Specify a global color table, 8 bit color, 6 bits
	per primary color, and unsorted colors. */
	PackedF = 0;
	PackedF |= GLOBALCOLORTBL;
	PackedF |= ((BITSPERCOLOR-1) << 4);
	PackedF |= (GLOBALCOLORTBLSIZE-1);
	ScnDesc.PackedField = PackedF;
	ScnDesc.BackGroundIndex = 0; /* BackGround index */
	ScnDesc.AspectRatio = 0; /* Aspect */

	putShort(ScnDesc.LogicalScreenWidth,GIFFile);/* write to file */
	putShort(ScnDesc.LogicalScreenHeight,GIFFile);
	putc(ScnDesc.PackedField,GIFFile);
	putc(ScnDesc.BackGroundIndex,GIFFile);
	putc(ScnDesc.AspectRatio,GIFFile);

	getPalette(Pal); /* load Pal from 256 LUT registers */

	for(Index = 0; Index < 256; Index++)
	{
/*		Pal[Index][red] <<= 2; *//* scale LUT values from 0..63 to 0..255*/
/*		Pal[Index][grn] <<= 2; */
/*		Pal[Index][blu] <<= 2; */

		putc(Pal[Index][red],GIFFile); /* write to GIFfile */
		putc(Pal[Index][grn],GIFFile);
		putc(Pal[Index][blu],GIFFile);

/*
printf("Index=%d, r= %d, g=%d, b=%d\n",Index, (int)Pal[Index][red], (int)Pal[Index][grn], (int)Pal[Index][blu]);
*/

	}

	/*	Build the image descriptor, with no local color table.
	Image is written non-interlaced. */

	ImgDesc.ImageSeparator    = ',';
	ImgDesc.ImageLeftPosition = 0; /* ImageLeftPos */
	ImgDesc.ImageTopPosition  = 0; /* ImageTopPos */
	ImgDesc.ImageWidth        = TheImageWidth;
	ImgDesc.ImageHeight       = TheImageHeight;
	ImgDesc.PackedField       = 0;

	/* Write the image descriptor (ID) to the file */
	putc(ImgDesc.ImageSeparator,GIFFile);
	putShort(ImgDesc.ImageLeftPosition,GIFFile);
	putShort(ImgDesc.ImageTopPosition,GIFFile);
	putShort(ImgDesc.ImageWidth,GIFFile);
	putShort(ImgDesc.ImageHeight,GIFFile);
	putc(ImgDesc.PackedField,GIFFile);

	CompressFile();  /* compress the image data */

	putc(0,GIFFile);  /* write trailer to file */
	putc(';',GIFFile);

	fclose(GIFFile);          /* close the completed GIF file */
	DeAllocMem();             /* free up the memory */
} /* WriteGIFFile */

/*############# graphics tools #############*/
/* the following routines work with SVGA256.BGI driver
(use SVGA256M.BGI for mouse action too)
under TURBO C, C++ in DOS. They must be replaced with
appropriate graphics routines in your system.

The tools for writing (displaying) a pixel and reading a pixel are
macros: SETPIXEL(col, row, index), and GETPIXEL(col, row),
and are used only in OutPixel() and GetByte(), respectively. */

/*<<<<<<<<<<<<<<<<< setPalette >>>>>>>>>>>>>>>*/

void setPalette(LUTpalette PalBuf)
{ /* write 256 triples of BYTES into LUT. First part is svga
version on IBM pc. Add stuff in #else portion as necessary
for your environment */

#ifdef _IBMDOS_
	struct REGPACK reg;
	reg.r_ax = 0x1012;  /* Turbo C in DOS */
	reg.r_bx= 0;
	reg.r_cx = 256;
	reg.r_es = FP_SEG(PalBuf);
	reg.r_dx = FP_OFF(PalBuf);
	intr(0x10,&reg);
#else
 /* your routine to set the palette */
#endif
}

#ifdef not_define_in_elsewhere						/* put in program 1 */
/*<<<<<<<<<<<<<<<<< getPalette >>>>>>>>>>>>>>>*/

void getPalette(LUTpalette PalBuf)
{ /* reads 256 LUT BYTE-triples, puts values in PalBuf.
Add stuff in #else part as necessary for your system. */

#ifdef _IBMDOS_
/* This is for svga on IBM pc. Replace it as necessary
for your environment */
	struct REGPACK reg;
	reg.r_ax = 0x1017; /* Turbo C in DOS */
	reg.r_bx= 0;
	reg.r_cx = 256;
	reg.r_es = FP_SEG(PalBuf);
	reg.r_dx = FP_OFF(PalBuf);
	intr(0x10,&reg);
#else
/* The following fakes a LUT. Replace it with real LUT reader
in our system. */
int i;
	for(i=0; i < 256; i++) /* fake palette */
	{
		(PalBuf)[i][red] = i;
		(PalBuf)[i][grn] = i/2;
		(PalBuf)[i][blu] = i/3;
	}
#endif
}
#endif

/*<<<<<<<<<<<<<<<<<<< endGraphics >>>>>>>>>>>>>>>*/

void endGraphics( void )
{
/* leave graphics mode - to text mode.*/

#ifdef _IBMDOS_

  #define VIDEO 0x10         /* Turbo C in DOS */
  regs.h.ah = 0;             /* set video mode function # */
  regs.h.al = 3;             /* 80x25 text 16 color */
  int86(VIDEO,&regs,&regs);
#else
/* your routine for returning to text mode */
	printf("\n on non-ibm system, exiting graphics ");
#endif
}

/* <<<<<<<<<<<<<<<< startGraphics >>>>>>>>>>>>>>*/

void startGraphics(void)
{ /* go to appropriate graphics mode. */

#ifdef _IBMDOS_

  int GD,GM,Gerr;
  int a, x;
  float hue,sat,val;
	installuserdriver("SVGA256m",DetectVGA);
	GD = DETECT;
	initgraph(&GD, &GM, "\\tc\\bgi\\");   /* \tc\bgi is where I */
	Gerr = graphresult();					  /* store svga256.bgi */
	if (Gerr != 0) {
		printf("%s \n",grapherrormsg(Gerr));
		 exit(1);
	 }
#else
	/* put whatever is needed for your system here */
	printf("\n on non-ibm system, entering graphics ");
#endif
}

/* <<<<<<<<<<<<<<< DetectVGA >>>>>>>>>>>>>>>>> */
int HUGE DetectVGA(void)
{
/* called by startGraphics to determine graphics mode.
Turbo C in DOS. Delete this in other environments. */

  int Vid;
  printf("Which video mode would you like to use? \n");
  printf("  2) 640x480x256\n");		/* ok on orchid card */
  printf("  3) 800x600x256\n"); 	   /* ok on orchid */
  printf("  4) 1024x768x256\n\n>"); /* ok on orchid */
  scanf("%d",&Vid);
  return Vid;
}

/*################ Mouse Tools ###############*/
/* Turbo C, C++ IBM mouse routines, usable with
SVGA256M.BGI driver. Replace with suitable tools
in your environment. */

typedef struct{USHORT x,y;} tIntPt;
/*************** prototypes ************************/
void mouse (USHORT *m1, USHORT *m2, USHORT *m3, USHORT *m4);
USHORT resetmouse (void);
void movemouse(tIntPt p);
void setmousevis(USHORT);
void initmouse (void);
USHORT getMouse(tIntPt *p);
tIntPt getPoint(void);

/*<<<<<<<<<<<<<<<<<<< mouse >>>>>>>>>>>>>>*/
void mouse (USHORT *m1, USHORT *m2, USHORT *m3, USHORT *m4)
/* Interface to DOS mouse functions mouse.com and mouse.sys.
INT 33h is accessed thru the int86 system call. */
{
#ifdef _IBMDOS_
	union REGS inregs, outregs; /* declared in dos.h */
	inregs.x.ax = *m1;  inregs.x.bx = *m2;
	inregs.x.cx = *m3;  inregs.x.dx = *m4;
	int86 (0x33, &inregs, &outregs);
	*m1 = outregs.x.ax;  *m2 = outregs.x.bx;
	*m3 = outregs.x.cx;  *m4 = outregs.x.dx;
#endif
}
/*<<<<<<<<<<<<<<<< resetmouse >>>>>>>>>>>>>>>>>*/
USHORT resetmouse (void)
{
/* init's the mouse */
#ifdef _IBMDOS_
	USHORT m1, m2, m3, m4;
	m1 = 0; /* RESET_MOUSE*/
	mouse (&m1, &m2, &m3, &m4);
	return (m1); /* 0 if not installed, -1 if installed */
#else
	return 1;
#endif
}
/*<<<<<<<<<<<<<<< moveMouse >>>>>>>>>>>>>>>>*/
void moveMouse(tIntPt p)
{
	/* Moves the mouse to the location (x,y)  */
#ifdef _IBMDOS_
	USHORT m1, m2;
	m1 = 4;		 /* SET_MOUSE_COORD */
	if (getmaxx() == 319) p.x *= 2;
	/* fixes strange scaling of mouse if 320 pixels across */
	mouse(&m1,&m2,&p.x,&p.y);
#else
	printf("\n moving mouse to %ud %ud",p.x,p.y);
#endif
}
/*<<<<<<<<<<<<<<< setmousevis >>>>>>>>>>>>>>*/
void setmousevis(USHORT vis)
{
	/* vis = 1 shows mouse, else hides mouse */
#ifdef _IBMDOS_

	USHORT m1, m2, m3, m4;
	if (vis == 1) m1 = 1; /* SHOW_MOUS */
		else m1 = 2;  /* HIDE_MOUSE */
	mouse(&m1,&m2,&m3,&m4);
#else
	printf("\n set mouse visibility to %ud",vis);
#endif
}
/*<<<<<<<<<<<<<<< initmouse >>>>>>>>>>>>>>>>>>>*/
void initmouse (void)
{
	/* gets mouse started */
#ifdef _IBMDOS_
	tIntPt p;
	p.x = 80; p.y = 80;
	resetmouse ();
	moveMouse (p);
	setmousevis(1);
#else
	printf("\n init the mouse ");
#endif
}
/*<<<<<<<<<<< getMouse >>>>>>>>>>>>*/
USHORT getMouse(tIntPt *p)
{
	/* return mouse location *p, and which button(s) down:
	 0: neither; 1: left; 2: right; 3: both  */
	USHORT m1, butt;
#ifdef _IBMDOS_

	m1 = 3; /* GET_MOUSE_STATUS */
	mouse(&m1, &butt, &(p->x),&(p->y));
#else
	printf("\n give butt, and mouse location:");
	scanf("%u %u %u",&butt, &(p->x),&(p->y));
#endif
	return butt;
}

/*<<<<<<<<<<<<<<<<< getPoint >>>>>>>>>>>>>>>>*/
tIntPt getPoint(void)
{
	/* wait for user to click button; return mouse location */

  tIntPt pt;
#ifdef _IBMDOS_
  while(getMouse(&pt)==0);  /* wait til button pressed */
  while(getMouse(&pt)!=0);  /* wait til button released */
#else
	puts("\n give mouse location: x y ");
	scanf("%u %u",&(pt.x), &(pt.y));
#endif
  return pt;
}
