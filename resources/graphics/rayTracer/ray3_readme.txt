





This is the README file for the Raytraced Scenes in Full Color (RAY3.C).

The program writes and exercises an application that reads a specification
of the synthetic camera and scene from an MCL file, which the user creates
the scene from any input file, ex:  filename.in

The program was an enhancement of program RAY2.C so that it provides color
renderings of scenes.

The program is invoked using a command-line argument to specify the input
scene file.  This command-line argument is explained as well as program 
compilation and execution in the program RAY3.C.

The program includes the following new features:

1)  Reflection - computes the light intensity of a reflected ray.  The initial
                 ray along with spawned rays.

2)  Refraction - computes the light intensity of a transparent ray.  The initial
		 ray along with spawned rays.

3)  OCTREE     - loads the color LUT with the 256 "best" colors from 24-bit 
		 display values.  The code was provided by Professor F.S. Hill
		 and was incorporated into the RAY3.C program due to an error
		 with the DEC compiler noted in RAY3.C.

ONE REQUIREMENT THAT WAS NOT SUCCESSFULLY FINISHED WAS THE CSG - CONSTRUCTIVE
SOLID GEOMETRY.
