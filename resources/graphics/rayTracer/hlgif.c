/* file hlgif.c, Jan.28,1995 F.S.Hill, Jr. */

#include "mygif.h"

USHORT hlGetImage(SHORT col, SHORT row, USHORT nCols, USHORT nRows);
void hlLoadLUT(ColorRegister LUT[]);

#define SET3(i,r,g,b) {LUT[i].Red =r; LUT[i].Green = g; LUT[i].Blue = b;}
#define SQR(a) ((a)*(a))
#define iBACKGRND 255  /* index of background color */
#define iGRAYSTART 90  /* start of gray colors in LUT */
#define iLINES 254	/* index of line color */
#define iBALLSTART 32 /* start of ball colors in LUT */
#define iCHKRED 0  /* checkerboard colors */
#define iCHKBLK 1

/*<<<<<<<<<<<<<<<<<< hlLoadLUT >>>>>>>>>>>>>>>>>>>*/
/* load up hill's fake values for the test image */

void hlLoadLUT(ColorRegister LUT[])
{
	SHORT i;

	SET3(iCHKRED,255,0,0); /* checkerboard square color */
	SET3(iCHKBLK,0,0,0);
	SET3(iLINES,255,255,255); /* line color */
	SET3(iBACKGRND,160,160,0);    /* background color */
	for(i = 0; i < 32; i++)
	{
		SET3(i + iBALLSTART, 0, 0, i * 8); /* shades of blue */
		SET3(i + iGRAYSTART, i * 8, i * 8, i * 8); /* gray shades */
	}
} /* hlLoadLUT */

/*<<<<<<<<<<<<<<<<<<<< hlGetImage >>>>>>>>>>>>>>>>>>>*/
/* a test routine: 'pretends' to access frame buffer to read pixel
value (LUT index) for (col,row)-th pixel;
Actually generates a value algorithmically */

static USHORT hlGetImage(SHORT col, SHORT row, USHORT nCols, USHORT nRows)
{
	SHORT v;
	SHORT hnCols = nCols / 2; /* half of global image width */
	SHORT hnRows = nRows / 2; /* half the image height */
	double ds, radSq;

	radSq = SQR(0.4 * nRows);

	if(row > hnRows) /* in bottom half of screen */
		v = iGRAYSTART + (SHORT)((nRows - row)/(double)hnRows * 32); /* 32 gray shades in lower half */
	else v = iBACKGRND; /* background */

	if(col == row) v = iLINES; /* diagonal lines */
	if( col == 0 || row == 0 || col == 99 || row == 99)
		v = iLINES; 			/* 100 by 100 box */

	/* do checkerboard */
	if((row < hnRows / 2) && (col > 3 * nCols / 4)) /* in checkerboard */
		v = ((abs(nCols - row) / 20 + abs(nCols - col) / 20) % 2) ? iCHKRED : iCHKBLK;

	/* do the fake sphere on top */
	ds = SQR((double)row - hnRows) + SQR((double)col - hnCols);
	if(ds <= radSq)   /* if pixel is inside the ball */
	{
		v = iBALLSTART + (SHORT)(32 * ( (radSq - ds) / radSq));
		/* printf("\n %d",v);*/
	}
	return v;
} /* hlGetImage */

/*<<<<<<<<<<<<<<<<< main (for testing only) >>>>>>>>>>>>>>>>>>>*/
main()
{
	USHORT row, col;
	char GIFfileName[30];
	ColorRegister myLUT[256];
	USHORT nRows, nCols;
	BYTE pix;

	nCols = 640;
	nRows = 480;

	printf("\n give filename for GIF file: ");
	gets(GIFfileName);
	if(strlen(GIFfileName) == 0) exit(0);

	StartGIF(GIFfileName, nCols, nRows);

	hlLoadLUT(myLUT); /* load it up for test image */

	WriteLUT(myLUT);

	StartLZW();

	for(row = 0; row < nRows; row++)
	{
		for(col = 0 ; col < nCols; col++)
		{
			pix = hlGetImage(col, row, nCols, nRows);
			ProcessPixel(pix);
		}
		if(row % 100 == 0) printf("\n done with row: %u",row);
	}
	FinishGIF();

	puts("\n ok, done building the GIF file, <CR> to exit");

}


