/*  Robert Estey 											  */
/*  Email:  estey@mosvax.sps.mot.com  Phone:  (602) 655-2391  Fax:  (602) 655-3569						  */
/*  UMASS ECE661 / NTU ST741A	Program 3											  */
/*																  */
/*  Developed and Ran on AXP/VMS V6.1 & DEC/Motif V1.2	 									  */
/*																  */
/*  cc program								- C Compiler V4.0 on DEC AXP / VMS system 		  */
/*  link program/opt							- Link in the X11 - DEC/Motif libraries			  */
/*  set  display/create/transport=decnet/node=cv64 			- Set the display					  */
/*  define sys$input sys$command			- Set up program to take input from screen not Xterminal (scanf)	  */
/*																  */
/*  executable_symbol :== $diskdrive:[directory]program inputfile	- set up symbol						  */
/*  executable_symbol 							- Execute the program					  */
/*																  */
/*  program.opt (contents in file are below)											  */
/*																  */
/*  program					- current program being compiled 				        	  */
/*  mcl1_5					- mcl program version 1.5 provided by Professor F.S. Hill of Univ Mass, Amherst   */
/*  gifboth					- gif viewer & writer provided by Professor F.S. Hill of Univ Mass, Amherst       */
/*  sys$share:decw$dxmlibshr12/share		- dec x libraries								  */
/*  sys$share:decw$xmlibshr12/share		- motif libraries								  */
/*  sys$share:decw$xlibshr/share		- x libraries									  */
/*  sys$share:decw$xtlibshrr5/share		- xt libraries									  */
/*																  */
/**********************************************************************************************************************************/
/*																  */
/*  Program Description:  RAY3_README.TXT											  */
/*  																  */
/*  OCTREE.C 					- OCTREE program that converts 24bit images to 256 LUT colors was provided by     */
/*						  Professor F.S. Hill of Univ Mass, Amherst.  The program was modified and        */
/*						  installed internally with the current ray program due to compiler error on      */
/*						  DEC systems.  Call C950418-9644 was logged with DEC.				  */
/*																  */
/**************************************************** INITIALIZE & START PROGRAM **************************************************/

#include 	<stdio.h>									  /* include standard i/o library */
#include	<stdlib.h>									  /* include standard lib library */
#include	<string.h>										/* include string library */
#include        <X11/Xlib.h> 		                                                                   /* include x11 library */
#include	<X11/Xutil.h>									 /* include the X utility library */
#include        <math.h>                                                                                  /* include math library */
#include	<ctype.h>									     /* include character library */

#include	"mcl1_5.h"									   /* include the mcl header file */

extern		SHORT interpretSceneFile(char *inputFilename, tSceneDescript *scene, char *msg);    	     /* scene description */

extern		void DisplayGIFFile(char *FileName, 
	         unsigned short left, unsigned short top, unsigned short right, unsigned short bottom);     /* set up display GIF */

extern		void WriteGIFFile(char *FileName, 
 		 unsigned short left, unsigned short top, unsigned short right, unsigned short bottom);       /* set up write GIF */

typedef 	unsigned char BYTE;             						      /* must be one-byte integer */
typedef 	BYTE LUTpalette[256][3];							       /* typedef for LUT palette */
enum 		{red=0,grn=1,blu=2}; 									/* index values for R,G,B */

extern		LUTpalette Pal; 							    /* palette declaration from GIFBOTH.C */

#define		HLverbose		1
#define		MAXCOL			640				   			     /* maximum number of columns */
#define		MAXROW			480	     			      				/* maximum number of rows */
#define		BACKGROUND		0.0								      /* background color */
#define		maxLevel		5							       /* maximum recursion depth */
#define		checkerSize		0.4							       /* size of checker squares */
#define		minShiny		0.7							      /* is surface shiny enough? */
#define 	minTransparent  	0.7             					 /* is object transparent enough? */
#define		someMinimum		0.9					     /* difference between normal and half vector */
#define 	eps  			0.00001								         /* a tiny amount */
#define 	BIG_FLT 		1.0E25                  					   /* approx. of infinity */

#define 	TRUE 			1
#define 	FALSE 			0
#define 	MAXCOLOR 		256         							  /* max number of colors */
#define 	MAXDEPTH 		7           						       /* max depth of octree - 1 */

typedef struct {BYTE Red; BYTE Green; BYTE Blue;} ColorRegister;   			      /* LUT palette is an array of these */

struct ColorSum {ULONG R; ULONG G; ULONG B;};

typedef struct Node * OCTREE;

struct Node {								                /* a (60 byte) octree node data structure */
 unsigned isLeaf;     								 		      /* 1 if this node is a leaf */
 unsigned Level;       	 									     /* the level this node is at */
 unsigned ColorIndex;  									   /* its LUT index -- assigned in pass 1 */
 unsigned Children;    								   	      /* number of children this node has */
 ULONG ColorCount;  									/* how many times this color has occurred */
 struct ColorSum RGBSum;   								       /* sum of color values accumulated */
 OCTREE NextReduceable;     							    /* link to next reduceable node at this level */
 OCTREE Next[8];       	 										/* the 8 kin of this node */
};

int RANMAX;
struct Node * relic;							      /* OCTREE relic; */ /* relic stack for free'd nodes */

static unsigned 	ImageWidth = MAXCOL;
static unsigned 	ImageHeight = MAXROW;
static unsigned 	Size;
static unsigned 	ReduceLevel;
static unsigned 	LeafLevel;
static ColorRegister 	Palette[MAXCOLOR];   							      /* the LUT palette - global */
static OCTREE 		theTree;        							                    /* the octree */
static OCTREE 		ReduceList[MAXDEPTH + 1];
static unsigned 	NumNodes = 0;							                   /* num.nodes in octree */
static unsigned 	MaxNodes = 0;             						      /* max. num. ever in octree */
static unsigned 	seed;

/********************************************************** MACROS ****************************************************************/

#define PRNT1(x) 		printf("\n "#x" = %6.3lf",x)						     /* test print macros */
#define PRNT3(a) 		printf("\n "#a" = %6.3f %6.3f %6.3f",a.x,a.y,a.z)
#define PRNTC3(a) 		printf("\n "#a" = %6.3f %6.3f %6.3f",a.r,a.g,a.b)                   
#define PRNTC4(a) 		printf("\n "#a" = %6.3f %6.3f %6.3f %6.3f",a.r,a.g,a.b,a.n)                   
#define PRNT4(r) 		printf("\n "#r" = %6.3f %6.3f %6.3f %6.3lf",r.a,r.b,r.c,r.d)
#define PRNTaffine(m) 		{printf("\n "#m" = ");\
				 printf("\n %6.3f %6.3f %6.3f",m.mat[0][0],m.mat[0][1],m.mat[0][2]);\
        			 printf("\n %6.3f %6.3f %6.3f",m.mat[1][0],m.mat[1][1],m.mat[1][2]);\
				 printf("\n %6.3f %6.3f %6.3f",m.mat[2][0],m.mat[2][1],m.mat[2][2]);\
        			 PRNT3(m.tr);}

#define TESTBIT(a,i) 		(((a)&(1<<(i)))>>(i))								 /* testbit macro */

#define min(a,b) 		(a < b) ? a : b									 /* minimum macro */

#define max(a,b) 		(a > b) ? a : b									 /* maximum macro */

#define dot3D(a,b) 		((a.x) * (b.x) + (a.y) * (b.y) + (a.z) * (b.z))				     /* dot product macro */

#define magnitude(a) 		sqrt((a.x * a.x) + (a.y * a.y) + (a.z * a.z))				       /* magnitude macro */

#define normalize(a,b,c) 	{(a.x = b.x / c); (a.y = b.y / c);  (a.z = b.z / c);}			       /* normalize macro */

													/* transpose matrix macro */
#define transpose(q,p,m)	q.x = p.x * m[0][0] + p.y * m[0][1] + p.z * m[0][2]; \
				q.y = p.x * m[1][0] + p.y * m[1][1] + p.z * m[1][2]; \
				q.z = p.x * m[2][0] + p.y * m[2][1] + p.z * m[2][2];
													 /* matrix multiply macro */
#define	transform(q,p,m)	q.x = p.x * m[0][0] + p.y * m[1][0] + p.z * m[2][0]; \
		          	q.y = p.x * m[0][1] + p.y * m[1][1] + p.z * m[2][1]; \
		 	  	q.z = p.x * m[0][2] + p.y * m[1][2] + p.z * m[2][2];

#define		tetrahedron		0								  /* define objects names */
#define         cube			1
#define		octahedron		2
#define		dodecahedron		3
#define		icosahedron		4
#define		buckyball 		5
#define		diamond			6
#define		polyhedron		7
#define		userdefined		8
#define		square			9
#define		plane			10
#define		sphere			11
#define		cone			12
#define         cylinder		13
#define		taperedcyl		14
#define		torus			15
#define		boolobj			16

/************************************************************* TYPEDEFS ***********************************************************/

typedef         struct {double x, y;}       t2Tuple; 						             /* structure 2 tuple */
typedef		struct {double x, y, z;}    t3Tuple;							     /* structure 3 tuple */
typedef		struct {double l, t, r, b;} t4Tuple;							     /* structure 4 tuple */

typedef		struct {short doesHit;								      /* is there an intersection */
                        double hitTime;										  /* the hit time */
			tInstance *hitObj;							     /* the object hit by the ray */
			short entering;								/* ray is entering the object hit */
			t3Tuple hitPoint;						  			 /* the hit point */
			t3Tuple hitNormal;					    			   /* normal at hit point */
			int hitSurface;
		       } tIntersect;									   /* intersect structure */

typedef		struct {t3Tuple start;										  /* start of ray */
			t3Tuple dir;									      /* direction of ray */
			tInstance *inside;				    	      /* speed of object in which the ray travels */
		       } tRay3D;

/************************************************************ PROTOTYPES **********************************************************/

void 		x11graphics();							      		                  /* X11 graphics */
void 		get_GC(GC *gc); 	 							          /* get graphics context */

void 		printCamera(tCamera *theCamera);						     /* test the camera structure */
void 		printLights(tLight *lights);							      /* test the light structure */
void 		printObjects(tInstance *objects);						    /* test the objects structure */

void 		rayTracer(char* input_filename);							  /* ray tracer procedure */
void 		sceneToGeneric(tInstance *obj, tRay3D *sceneRay, tRay3D *genericRay);  	   /* converts the scene to generic scene */
void 		setPixel(t2Tuple *mp1, t2Tuple *mp2);							       /* set pixel value */
int		rayHit(tRay3D *genericRay, tInstance *obj, tIntersect *theIntersect);			     /* ray hit procedure */
int		rayInExtent(tInstance *obj);							       /* ray in extent procedure */
int 		rayBoxExtent(tRay3D *theRay, tInstance *obj);						     /* ray in box extent */
int 		inShadow(t3Tuple startPt, threeTup lightPt);						   /* in shadow procedure */
int 		checkerBoard(tIntersect *theIntersect);			         			  /* checkerboard routine */
int 		wood(tIntersect *theIntersect);							          /* wood texture routine */
double 		texture(tInstance *obj, tIntersect *theIntersect);					   /* texture type switch */
double		imageWriter(tIntersect *theIntersect);							  /* image writer routine */
tColor		colorShade(tRay3D *sceneRay, int level);						 /* color shade procedure */

int 		rayPlane(tRay3D *theRay, tIntersect *theIntersect); 				 /* ray intersect plane procedure */
int 		raySquare(tRay3D *theRay, tIntersect *theIntersect); 				/* ray intersect square procedure */
int 		raySphere(tRay3D *theRay, tIntersect *theIntersect);				/* ray intersect sphere procedure */
int		rayCylinder(tRay3D *theRay, tIntersect *theIntersect);			      /* ray intersect cylinder procedure */
int		rayCone(tRay3D *theRay, tIntersect *theIntersect);			          /* ray intersect cone procedure */
int		rayCube(tRay3D *theRay, tIntersect *theIntersect);				  /* ray intersect cube procedure */
int		rayPolyhedron(tRay3D *theRay, tInstance *obj, tIntersect *theIntersect);    /* ray intersect polyhedron procedure */

void            locateMB(t2Tuple *point, int *bp);                                   /* locateMBs screen point and button pressed */
void            getPalette(LUTpalette PalBuf);							     /* get the palette of server */
unsigned short  getPixel(short col, short row);		      /* get the pixel value associated with the palette into color array */
void		putPixel(short col, short row, short index);  /* put the pixel value associated with the palette into color array */

/******************************************************** OCTREE PROTOTYPES *******************************************************/

unsigned 	Quantize(OCTREE Tree, ColorRegister TheColor);						   /* compute color value */
void 		FillPalette(OCTREE Tree, unsigned *Index);						         /* build palette */
void 		GetReduceable(OCTREE *Node);							      /* control reduceable array */
void 		KillTree(OCTREE *Node);								      /* remove level from octree */
void 		ReduceTree(void);								      /* remove level from octree */
void 		InsertTree(OCTREE *Tree, ColorRegister RGB, unsigned Depth);			       /* add new level to octree */
int 		GenOctree(OCTREE *pTree);								       /* generate octree */
int 		ProcessPass1();										       /* generate octree */

/********************************************************* GLOBAL VARIABLES  ******************************************************/

Display         *display;                                       /* X Terminal address, pointer to display structure on X terminal */
int             screen;      		                      /* X Terminal screen, root window to screen structure on X terminal */
Window          win;                                                                                             /* Window widget */
GC              gc;	 									        /* ID of graphics context */
XEvent		event;									       /* structure for event information */
Colormap        cmap;      		                                                                       /* Colormap widget */

FILE		*input_file, *input_file2, *input_gif, *output_gif;						 /* file pointers */
char		input_filename[25], input_gifname[25], output_gifname[25];     			    /* input and output filenames */
char            input_filename2[25]     = "mandelbrot.dat";   						   /* mandelbrot datafile */

tColor		colorArray[MAXCOL+1][MAXROW+1];								           /* color array */
tColor		GIFArray[MAXCOL][MAXROW];									     /* gif array */
double		imageArray[MAXCOL][MAXROW];									   /* image array */

USHORT		row, col;									  /* screen row and screen column */
int		infinite		= 1;							      /* set up for infinite loop */
int		status 			= 0;									 /* return status */
double		maxRGB			= 0.0;								     /* maximum RGB value */

tSceneDescript  theScene;								/* define theScene as type tSceneDescript */
tCamera		*theCamera;									       /* init the camera pointer */
tLight		*lights;										    /* light list pointer */
tInstance 	*objects;										   /* object list pointer */

/*********************************************************** MAIN PROGRAM *********************************************************/

int main(int argc, char **argv)										    /* main program start */
{
 char		option;											       /* option selected */
 int		i, j;												      /* counters */
 int		bp1, bp2;											/* button pressed */
 t2Tuple	mp1, mp2;											/* mouse position */

 mp1.x = 0.0; mp1.y = 0.0; mp2.x = 639.0; mp2.y = 479.0;						  /* initialize variables */

 if(argc != 2) {printf("\nfailed to put correct number of arguments\n"); exit(0);}	/* exit program if wrong command argument */

 if((input_file2 = fopen(input_filename2,"r")) == NULL)  						     /* open input file 2 */
     {printf("\ncould not open input file #2\n"); exit(0);}

 for(i = 0; i < MAXROW; i++)								      /* load image data into image array */
  {
   for(j = 0; j < MAXCOL; j++)
    {
     fscanf(input_file2, "%lf", &imageArray[i][j]);
    }
  }

 rayTracer(argv[1]);											  /* ray tracer procedure */

 for(row = 0; row < theCamera->nRows; row++)                                                  /* for each row pixel on the screen */
  {
   for(col = 0; col < theCamera->nCols; col++)                                             /* for each column pixel on the screen */
    {
     colorArray[col][row].r /= maxRGB; colorArray[col][row].g /= maxRGB; colorArray[col][row].b /= maxRGB;
    }
  }

 ProcessPass1();											/* build LUT using octree */
							     			 			     
 for (i = 0; i < 256; i++)						    /* coping palette produced by octree into LUT palette */
  {
   Pal[i][red] = Palette[i].Red;
   Pal[i][grn] = Palette[i].Green;
   Pal[i][blu] = Palette[i].Blue;
  }

 printf("\nmaxRGB = %lf", maxRGB);								   /* print out maximum RGB value */
 printf("\ntotal pixels = %ld", ImageWidth * (long)ImageHeight);				  /* print out octree information */
 printf("\nmax. nodes in octree at any time = %u", MaxNodes);
 printf("\nnum. nodes in octree now = %u", NumNodes);
 printf("\nReduceLevel = %u", ReduceLevel);
 printf("\nLeafLevel = %u\n", LeafLevel);

 x11graphics();												  /* execute X11 graphics */

 while(infinite)
  {
   printf("\nEnter the option letter\n"); scanf("%s", &option);						  /* get option from user */
   option = toupper(option);									 /* force all inputs to uppercase */
   switch(option)										    /* switch according to option */
    {
     case 'D':													/* reset defaults */
      {
       mp1.x = 0.0; mp1.y = 0.0; mp2.x = 639.0; mp2.y = 479.0;
      }
     break;

     case 'G':										       			 /* read GIF file */
      {
       printf("\nEnter input GIF filename\n");
       scanf("%s", input_gifname);								   	    /* get input filename */
       if((input_gif = fopen(input_gifname,"r")) == NULL) 				  		       /* open input file */
           printf("\ncould not open input GIF file\n");
       else
        {
         DisplayGIFFile(input_gifname,0,0,MAXCOL-1,MAXROW-1);					      /* call GIF display routine */
         fclose(input_gif);                                       					      /* close input file */
        }
      }
     break;

     case 'W':										       			/* write GIF file */
      {
       printf("\nEnter output GIF filename\n");
       scanf("%s", output_gifname);									   /* get output filename */
       WriteGIFFile(output_gifname,0,0,MAXCOL-1,MAXROW-1);					      /* call GIF writter routine */
      }
     break;

     case 'R':											       /* screen rectangle set up */
      {
       printf("\nEnter the rectangle upper-left and lower-right positions\n");				   /* get mouse positions */
       locateMB(&mp1, &bp1); locateMB(&mp2, &bp2);
      }
     break;

     case 'S':										       /* set pixel value, draw ray trace */
      {
       setPixel(&mp1, &mp2);
      }
     break;

     case 'Q': exit(0); break;											  /* quit program */
    }
  }
}														   /* end program */

/**********************************************************  X11 GRAPHICS *********************************************************/

void x11graphics()		
{
 int    window_x                = 0;  		                                		       /* Window position, X axis */
 int    window_y                = 0;                                            		       /* Window position, Y axis */

 int	window_width		= MAXCOL;	       /* X11 window width - Including the window manager (640 x 480 VGA monitor) */
 int	window_height		= MAXROW;	      /* X11 window height - Including the window manager (640 x 480 VGA monitor) */

 char   *window_name            = "Window Program";                                      /* Window name that appears on Title Bar */
 char   *display_name   = NULL;   /* Server to connect to; NULL means connect to server specified in environment variable DISPLAY */
 int    window_depth            = 0;                                   /* Window window_depth, window_depth = 0 taken from parent */
 int    window_border_width     = 4;                                                                              /* Border width */
 int    window_class            = 0;                                                                              /* Window class */
 long   valuemask               = 0;                                             /* Specifies which window attributes are defined */
 int    onoff                   = 0;                             /* 0 - disable synchronization, nonzero - enable synchronization */
 int    mode                    = 0;     	                                                               /* coordinate mode */
 int	i			= 0;										       /* counter */

 Visual                         *visual;                                                                         /* Visual widget */
 XGCValues                      values;                                                                  /* X GC values structure */
 XSetWindowAttributes           setwinattr;                                                      /* X window attributes structure */
 XColor				colorCells[MAXCOLOR];     					     /* structure for color cells */

 if((display=XOpenDisplay(display_name)) == NULL )                                                         /* Connect to X server */
  {
   (void) fprintf(stderr, "Window Program: cannot connect to X server %s\n", XDisplayName(display_name));
   exit(0);
  }

 XSynchronize(display, onoff);                               /* Synchronous mode for debugging, events are reported as they occur */

 screen                         = DefaultScreen(display);                               /* Get screen size from display structure */
 window_depth                   = DisplayPlanes(display, screen);                 /* Get window_depth size from display structure */
 visual                         = DefaultVisual(display, screen);                       /* Get visual size from display structure */

 valuemask                      = CWBackPixel | CWBorderPixel;                                    /* Setting up window attributes */
 setwinattr.background_pixel    = BlackPixel(display, screen);
 setwinattr.border_pixel        = WhitePixel(display, screen);

 win = XCreateWindow(display, RootWindow(display, screen), window_x, window_y, window_width, window_height,      /* Create window */
                     window_border_width, window_depth, window_class, visual, valuemask, &setwinattr);

 cmap = XCreateColormap(display, win, visual, AllocAll);                               /* Create color map from display structure */

 for(i = 0; i < MAXCOLOR; i++) 							 /* load the color map array from the LUT palette */
  {
   colorCells[i].pixel = i;
   colorCells[i].red   = (int)(255.0 * Pal[i][red]);
   colorCells[i].green = (int)(255.0 * Pal[i][grn]);
   colorCells[i].blue  = (int)(255.0 * Pal[i][blu]);
   colorCells[i].flags = DoRed | DoGreen | DoBlue;
  }

 XStoreColors(display, cmap, colorCells, MAXCOLOR);						/* load color map array into cmap */
                                                                                 
 XSetWindowColormap(display, win, cmap);				 /* whenever you display program window use new color map */

/*  									     Set the minimum set of properties for window manager */

 XSetStandardProperties(display, win, window_name, NULL, (Pixmap)NULL, (char**)NULL, (int)NULL, (XSizeHints*)NULL);

/*          Select all event types wanted (ExposureMask | ButtonPressMask | ButtonReleaseMask | KeyPressMask | PointerMotionMask) */

 XSelectInput(display, win, ExposureMask | ButtonPressMask | ButtonReleaseMask | KeyPressMask | PointerMotionMask);

 get_GC(&gc);	 									   /* Create GC for graphics applications */
 XMapWindow(display, win);                                                                                          /* Map window */

/********************************************************* BEGIN GRAPHICS *********************************************************/

 do {XNextEvent(display, &event);} while(event.type != Expose);				     /* do until you get the expose event */
     XFlush(display);								      /* flush out all X calls that were buffered */
}
                                                                                               
/********************************************************  GRAPHIC CONTEXT ********************************************************/

void get_GC(GC *gc)
{
 long valuemask = 0;	 								     /* ignore XGCvalues and use defaults */
 XGCValues values;
 int line_width = 1;												/* set line width */
 int line_style = LineSolid;										   /* set for solid lines */
 int cap_style = CapButt;									      /* cap all line connections */
 int join_style = JoinBevel;											   /* bevel style */
 int fill_style = FillSolid;										  /* set fill solid style */

 *gc = XCreateGC(display, win, valuemask, &values);					       /* Create default graphics context */
 XSetForeground(display, *gc, WhitePixel(display,screen));   					/* Specify foreground pixel color */
 XSetBackground(display, *gc, BlackPixel(display,screen));   					/* Specify background pixel color */
 XSetLineAttributes(display, *gc, line_width, line_style, cap_style, join_style); 			   /* Set line attributes */
 XSetFillStyle(display, *gc, fill_style);								   /* set fill attributes */
}

/************************************************************ OCTREE **************************************************************/

/*********************************************************** Quantize *************************************************************/

  /* This function returns the color index of the color that most closely matches the color passed to it. This function can only be 
												 called after the Octree is built.*/
unsigned Quantize(OCTREE Tree, ColorRegister TheColor)
{
 int i;

 if(Tree->isLeaf) return(Tree->ColorIndex);
 else
  {
   i = TESTBIT(TheColor.Red,  MAXDEPTH - Tree->Level) * 4 +
       TESTBIT(TheColor.Green,MAXDEPTH - Tree->Level) * 2 +
       TESTBIT(TheColor.Blue, MAXDEPTH - Tree->Level); 

   return(Quantize(Tree->Next[i],TheColor));
  }
}

/*********************************************************** FillPalette **********************************************************/

     /* This function traverses the octree assigning a unique color index value to each leaf node. It also calculates the RGB color 
    components to be used in the leaf nodes by averaging. Finally, it copies the RGB color value for the leaf node into the Palette 
                                                                                                                            array.*/
void FillPalette(OCTREE Tree, unsigned *Index)
{
 unsigned Ind;

 if(Tree != NULL)  
  {
   if(Tree->isLeaf || Tree->Level == LeafLevel)  
    {
     Palette[*Index].Red   = (BYTE)(Tree->RGBSum.R / Tree->ColorCount);
     Palette[*Index].Green = (BYTE)(Tree->RGBSum.G / Tree->ColorCount);
     Palette[*Index].Blue  = (BYTE)(Tree->RGBSum.B / Tree->ColorCount);

     Tree->ColorIndex = *Index;
     Tree->isLeaf = 1;
     *Index = *Index + 1;
    }
   else
    {
     for (Ind=0; Ind < 8; Ind++)
      FillPalette(Tree->Next[Ind],Index);
    }
  }
}

/*********************************************************** GetReduceable ********************************************************/

void GetReduceable(OCTREE *Node)						 /* Get a pointer to the next node to be reduced. */
{
 unsigned NewReduceLevel;

 NewReduceLevel = ReduceLevel;
 while(ReduceList[NewReduceLevel] == NULL) 							      /* find a level with a node */
       NewReduceLevel--;                       									     /* to reduce */
 *Node = ReduceList[NewReduceLevel];        							       /* return ptr to that node */
 ReduceList[NewReduceLevel] = ReduceList[NewReduceLevel]->NextReduceable; 			   /* pop from list of reducibles */
}

/*********************************************************** KillTree *************************************************************/

      /* This function recursively frees memory associated with the node it is pointing at and all nodes under it (its siblings). */

void KillTree(OCTREE *pTree)   
{
 register unsigned Index;

 if (*pTree == NULL) return;
 for (Index=0; Index < 8; Index++) KillTree(&((*pTree)->Next[Index]));

 NumNodes--;
 (*pTree)->NextReduceable = relic;  								      /* push it onto relic stack */
  relic = *pTree;												/*  free(*pTree); */
 *pTree = NULL;
}

/*********************************************************** ReduceTree ***********************************************************/

void ReduceTree(void)							   /* This function performs the reduction of the octree. */
{
 OCTREE Node;
 register unsigned Index, Depth;

 GetReduceable(&Node);                						     /* get ptr to deepest node needing reduction */
 Node->isLeaf = 1;                      					      /* change it from intermediate node to leaf */
 Size = Size - Node->Children + 1;    					    /* reduce number of leaves by the number of children. */
 Depth = Node->Level;

 for(Index=0; Index < 8; Index++)   							      /* free memory occupied by children */
  KillTree(&(Node->Next[Index]));

 if(Depth < ReduceLevel)  
  {          										      /* if depth of reduced node is less */
   ReduceLevel = Depth;			  						     /* than current reduce level, reduce */
   LeafLevel = ReduceLevel + 1;      								/* reduce level and leaf level to */
  }						                                         /* deepest intermediate (non leaf) node. */
}

/*********************************************************** InsertTree ***********************************************************/

void InsertTree(OCTREE *pTree, ColorRegister RGB, unsigned Depth) 
{						     /* This function builds the octree from the pixel color values passed to it. */
 unsigned Branch;
 OCTREE p;  											   /* handy temp. name for *pTree */
 p = *pTree;

 if(p == NULL)											 /* the old NewAndInit() follows: */
  {
   if(relic == NULL)  									       /* no relic's: must do true calloc */
    {
     p = (struct Node *) calloc(1,sizeof(struct Node));
     if (p == NULL)
      {puts("Out of memory!"); exit(-1);}
    }
   else  												  /* reuse a relic'd node */
    {
     p = relic;
     relic = relic->NextReduceable;
     memset(p, 0, sizeof(struct Node)); 									   /* zero it out */
    }
   NumNodes++;
   MaxNodes = MAX(MaxNodes,NumNodes);
   p->Level = Depth;
   p->isLeaf = (Depth >= LeafLevel); 
   if (p->isLeaf) Size++;
  } 											  /* if *pTree... end of old NewAndInit() */

  *pTree = p; 										    /* crucial: hook up ptr in param to p */
  p->ColorCount++;
  p->RGBSum.R += (ULONG) RGB.Red;
  p->RGBSum.G += (ULONG) RGB.Green;
  p->RGBSum.B += (ULONG) RGB.Blue;
 
  if((p->isLeaf == FALSE) && (Depth < LeafLevel))
   {
    Branch = TESTBIT(RGB.Red,  MAXDEPTH - Depth) * 4 +
             TESTBIT(RGB.Green,MAXDEPTH - Depth) * 2 +
	     TESTBIT(RGB.Blue, MAXDEPTH - Depth);

    if (p->Next[Branch] == NULL)
     {
      p->Children++;
      if (p->Children == 2)					 	       /* node is now reducible. Push it onto proper list */
       {
        p->NextReduceable = ReduceList[Depth];
	ReduceList[Depth] = p;
       }
     }
    InsertTree( &(p->Next[Branch]),RGB,Depth + 1);
   } 													      /* if p->isLeaf ... */
}

/*********************************************************** GenOctree ************************************************************/

int GenOctree(OCTREE *pTree) 
{		  /* This function makes a pass over the image data and calls InsertTree which builds the octree from each pixel. */

 register unsigned Row, Col;
 unsigned RowNum;
 int ReturnCode;
 ColorRegister RGB;

 Size = 0;                           									  /* initialize variables */
 ReduceLevel = MAXDEPTH;
 LeafLevel = ReduceLevel + 1;

 for(Row=0; Row < ImageHeight; Row++)
  {
   for(Col=0; Col < ImageWidth; Col++)
    {
     RGB.Red   = (int)(255.0 * colorArray[Col][Row].r);						/* RGB is the pixel's color value */
     RGB.Green = (int)(255.0 * colorArray[Col][Row].g);
     RGB.Blue  = (int)(255.0 * colorArray[Col][Row].b);
     InsertTree(pTree, RGB, 0);       								      /* insert color into octree */
     if (Size > MAXCOLOR - 1)  ReduceTree();  						   /* reduce tree if more than 256 colors */
    }
  }
return 0;
}

/******************************************************** ProcessPass1 ************************************************************/

int ProcessPass1() 							/* Pass one: build the octree and load the color palette. */
{										  /* just pass this color array on to GenOctree() */
 unsigned Index, i;
 int      ReturnCode;

 relic = NULL;												      /* initialize relic */
 for(i=0; i <= MAXDEPTH ; i++)  								   /* init the ReduceList[] array */
  ReduceList[i] = NULL;
  memset(Palette,'\0',sizeof(Palette));						/* Initialize all of the Palette memory to zeros. */

  theTree = NULL;
  if(GenOctree(&theTree)) return -1;									 /* read through the file */

  Index = 1;              										 /* entry 0 is left black */

		   /* scan whole octree, and for each leaf compute average color, assign index to leaf, and load avg intopoalette */

  FillPalette(theTree, &Index);									/* on return Index is # of colors */
  printf("\n\n%d colors used for image.\n",Index);
  return 0;
}

/******************************************************** PRINT CAMERA ************************************************************/

void printCamera(tCamera *theCamera)
{ 												 	 /* print data for camera */
 puts("\n\n the Camera: ");
 PRNT3(theCamera->eye); PRNT3(theCamera->lookAt); PRNT3(theCamera->ref); PRNT3(theCamera->u); PRNT3(theCamera->v); 
 PRNT3(theCamera->n); PRNT1(theCamera->eyeDist); PRNT1(theCamera->beta); PRNT1(theCamera->tilt); PRNT1(theCamera->aspect); 
 PRNT1(theCamera->W); PRNT1(theCamera->H); PRNT1(theCamera->viewFar); PRNT1(theCamera->viewNear);
}

/******************************************************** PRINT LIGHTS ************************************************************/

void printLights(tLight *lights)
{ 												 /* print data for lights in list */
  puts("\n\n the lights : ");
  while(lights != NULL)
  {
   PRNTC3(lights->color); PRNT3(lights->position);
   lights = lights->next;
  }
}

/******************************************************** PRINT OBJECTS ***********************************************************/

void printObjects(struct tInstance *objects)
{											        /* print data for objects in list */
 puts("\n\n the objects : ");
 while(objects != NULL)
  {
   printf("\n %s \n",objects->name); printf("type = %hd",objects->type); printf("\n boolop = %c", objects->boolOp); 
   PRNTC3(objects->ambient); PRNTC3(objects->color); PRNTC3(objects->diffuse); PRNTC4(objects->specular); PRNT4(objects->params); 
   PRNT4(objects->texture); PRNT1(objects->reflection); PRNT1(objects->transparent); PRNT1(objects->speed); 
   PRNTaffine(objects->transf); PRNTaffine(objects->inver);
   objects = objects->next;
  }
}

/**************************************************** LOCATE MOUSE BUTTON CURRENT POSITION ****************************************/

void locateMB(t2Tuple *point, int *bp)                                     /* locateMB mouse button pressed and pixel point value */
{
 do
  {
   XWindowEvent(display, win, ButtonPressMask, &event);                                    /* wait for mouse button to be pressed */
  } while (event.type != ButtonPress);

 *bp = event.xbutton.button;                                                             /* get the mouse button that was pressed */
 point->x = event.xbutton.x;                                                          /* get the x screen coordinate of the mouse */
 point->y = event.xbutton.y;                                                          /* get the y screen coordinate of the mouse */
}

/*********************************************************** SET PIXEL ************************************************************/

void setPixel(t2Tuple *mp1, t2Tuple *mp2)								       /* set pixel value */
{
 XColor	colorCell;										      /* structure for color cell */
 XPoint fill_vertex[4];            									    /* vertex of 4 points */
 int	row, col, row_prime, col_prime, firstCol;								      /* counters */
 int    red, green, blue;									 /* red green blue integer values */

 for(row = mp1->y; row <= mp2->y; row++)						      /* for each row pixel on the screen */
  {
   for(col = mp1->x; col <= mp2->x; col++)						   /* for each column pixel on the screen */
    {
     XSetForeground(display, gc, getPixel(col, row));						/* get pixel color value from LUT */

     firstCol = col;									    /* initialize first column of drawing */

     while((getPixel(col,row) == getPixel(col+1, row)) && col+1 < MAXCOL) col++;	    /* stay in loop until different color */

     XDrawLine(display, win, gc, firstCol, row, col, row);						       /* draw scan lines */
    }
  }
 XFlush(display);										  /* flush out remaining X calls. */
}

/*********************************************************** GET PIXEL ************************************************************/

unsigned short getPixel(short col, short row)
{
 ColorRegister RGB;

 RGB.Red   = (int)(255.0 * colorArray[col][row].r);				/* scales from 0.0 to 1.0 to 0 to 255 color value */
 RGB.Green = (int)(255.0 * colorArray[col][row].g);
 RGB.Blue  = (int)(255.0 * colorArray[col][row].b);
 
 return Quantize(theTree,RGB); 										    /* get best LUT index */
}

/*********************************************************** PUT PIXEL ************************************************************/

void putPixel(short col, short row, short index)						  /* put LUT value into GIF Array */
{
 GIFArray[col][row].r = Pal[index][red] / 255.0;
 GIFArray[col][row].g = Pal[index][grn] / 255.0;
 GIFArray[col][row].b = Pal[index][blu] / 255.0;
}

/*********************************************************** GET PALETTE **********************************************************/

void getPalette(LUTpalette PalBuf)
{
 XColor         colorCell;
 int            i;                                                                                                     /* counter */

 for ( i = 0; i < 256; i++ )
 {
  colorCell.pixel = i;                                                                    /* index into the color map table (LUT) */
  XQueryColor( display, cmap, &colorCell );                                         /* gets corresponding RGB values at the index */

  PalBuf[i][red] = (BYTE)(colorCell.red >> 8);                                           /* scaling from 0 thru 64K to 0 thru 256 */
  PalBuf[i][grn] = (BYTE)(colorCell.green >> 8);
  PalBuf[i][blu] = (BYTE)(colorCell.blue >> 8);
 }
}

/********************************************************** RAY TRACER ************************************************************/

void rayTracer(char* input_filename)									  /* ray tracer procedure */
{
 char		errMessage[200];								 /* string for MCL error messages */
 double		uc, vr;										 /* u vector column, v vector row */
 SHORT		returnCode;										    /* return code status */
 USHORT		index;												  /* index to LUT */
 tRay3D		theRay;												/* the ray tracer */
 tColor		intensity;										    /* intensity of light */

 returnCode = interpretSceneFile(input_filename, &theScene, errMessage);				       /* read input file */
 if(returnCode) {printf("%s \n", errMessage); exit(0);}					      	     /* exit program due to error */

 theCamera = theScene.cameraptr;	   					    /* setting camera pointer to camera structure */
 objects = theScene.objectptr;							   /* settting object pointer to object structure */
 lights = theScene.lightptr;							      /* setting light pointer to light structure */

/******************************************************* PRINT MCL VALUES *********************************************************/

 PRNTC3(theScene.ambient);                                                                                /* ambient scene values */
 PRNTC3(theScene.background);                                                                          /* background scene values */
 printCamera(theScene.cameraptr);                                                                                /* camera values */
 printLights(theScene.lightptr);                                                                                  /* light values */
 printObjects(theScene.objectptr);                                                                               /* object values */

/******************************************************* BUILD THE RAY ************************************************************/

										     /* copy eye coordinate into start coordinate */
 theRay.start.x = theCamera->eye.x; theRay.start.y = theCamera->eye.y; theRay.start.z = theCamera->eye.z;  

 for(row = 0; row < theCamera->nRows; row++)						      /* for each row pixel on the screen */
  {
   for(col = 0; col < theCamera->nCols; col++)						   /* for each column pixel on the screen */
    {
     uc = -(theCamera->W) + (((theCamera->W*2) / theCamera->nCols) * col);		   /* set up u vector column of viewplane */
     vr =   theCamera->H  - (((theCamera->H*2) / theCamera->nRows) * row);	 	      /* set up v vector row of viewplane */

     theRay.dir.x = (theCamera->eyeDist * theCamera->n.x) + (uc * theCamera->u.x) + (vr * theCamera->v.x);   /* direction vectors */
     theRay.dir.y = (theCamera->eyeDist * theCamera->n.y) + (uc * theCamera->u.y) + (vr * theCamera->v.y);
     theRay.dir.z = (theCamera->eyeDist * theCamera->n.z) + (uc * theCamera->u.z) + (vr * theCamera->v.z);

     if((row==240) && (col==320))
      {
       intensity = colorShade(&theRay, 0);			   			 /* returns values of the light intensity */
      }
     else
      {
       intensity = colorShade(&theRay, 0);			   			 /* returns values of the light intensity */
      }

     if(intensity.r == BACKGROUND) 						      /* put background intensity into colorArray */
      {
       colorArray[col][row].r = theScene.background.r;
       colorArray[col][row].g = theScene.background.g; 
       colorArray[col][row].b = theScene.background.b;
      }
     else									           /* put color intensity into colorArray */
      {
       colorArray[col][row].r = intensity.r; colorArray[col][row].g = intensity.g; colorArray[col][row].b = intensity.b;

       if(intensity.r > maxRGB) maxRGB = intensity.r;					/* get the largest intensity scaler value */
       if(intensity.g > maxRGB) maxRGB = intensity.g;
       if(intensity.b > maxRGB) maxRGB = intensity.b;
      }
    }
  }
}

/***************************************************** SCENE TO GENERIC ***********************************************************/

void sceneToGeneric(tInstance *obj, tRay3D *sceneRay, tRay3D *genericRay)	  	   /* converts the scene to generic scene */
{
 int i;														       /* counter */
 double tempx, tempy, tempz, startX, startY, startZ, dirX, dirY, dirZ; 					   /* temporary variables */

 tempx = sceneRay->start.x - obj->transf.tr.x; 
 tempy = sceneRay->start.y - obj->transf.tr.y;
 tempz = sceneRay->start.z - obj->transf.tr.z;

 for (i = 0; i < 3; ++i) 								   /* compute the scene to generic values */
  {
   startX = tempx * obj->inver.mat[0][i]; dirX = sceneRay->dir.x * obj->inver.mat[0][i];
   startY = tempy * obj->inver.mat[1][i]; dirY = sceneRay->dir.y * obj->inver.mat[1][i];
   startZ = tempz * obj->inver.mat[2][i]; dirZ = sceneRay->dir.z * obj->inver.mat[2][i];

   if(i == 0) {genericRay->start.x = startX + startY + startZ; genericRay->dir.x = dirX + dirY + dirZ;}
   if(i == 1) {genericRay->start.y = startX + startY + startZ; genericRay->dir.y = dirX + dirY + dirZ;}
   if(i == 2) {genericRay->start.z = startX + startY + startZ; genericRay->dir.z = dirX + dirY + dirZ;}
  }
}

/****************************************************** RAY HIT SWITCH ************************************************************/

int rayHit(tRay3D *genericRay, tInstance *obj, tIntersect *theIntersect)				     /* ray hit procedure */
{
 switch (obj->type)
  {
   case plane:  	return(rayPlane     (genericRay, theIntersect));
   case square: 	return(raySquare    (genericRay, theIntersect));
   case	sphere: 	return(raySphere    (genericRay, theIntersect));
   case cylinder:	return(rayCylinder  (genericRay, theIntersect));
   case cone:		return(rayCone	    (genericRay, theIntersect));
   case cube:		return(rayPolyhedron(genericRay, obj, theIntersect));
   case tetrahedron:	return(rayPolyhedron(genericRay, obj, theIntersect));
   case octahedron:	return(rayPolyhedron(genericRay, obj, theIntersect));
/* case boolobj: */
  } 
}

/*********************************************************** RAY PLANE ************************************************************/

int rayPlane(tRay3D *theRay, tIntersect *theIntersect)						/* ray intersect square procedure */
{
 theIntersect->doesHit = 0;										    /* clear out does hit */

 if(theRay->dir.z != 0.0)										/* not parrallel to plane */
  {
   theIntersect->hitTime = -theRay->start.z / theRay->dir.z;
   if(theIntersect->hitTime > 0.0) 
    {
     theIntersect->hitNormal.x = 0.0; theIntersect->hitNormal.y = 0.0; theIntersect->hitNormal.z = 1.0; 
     theIntersect->doesHit = 1;
     return(1);
    }
  }
 return(0);
}

/*********************************************************** RAY SQUARE ***********************************************************/

int raySquare(tRay3D *theRay, tIntersect *theIntersect)						/* ray intersect square procedure */
{
 if(theRay->dir.z != 0.0)										/* not parrallel to plane */
  {
   theIntersect->hitTime = -theRay->start.z / theRay->dir.z;
   if(theIntersect->hitTime > 0.0)								  /* does ray hit in front of eye */
    {
     if(((abs(theRay->start.x + (theRay->dir.x * theIntersect->hitTime))) <= 1.0) && 
        ((abs(theRay->start.y + (theRay->dir.y * theIntersect->hitTime))) <= 1.0))
         {
          theIntersect->hitNormal.x = 0.0; theIntersect->hitNormal.y = 0.0; theIntersect->hitNormal.z = 1.0; 
          theIntersect->doesHit = 1;									   /* ray does hit object */
          return(1);
         }
    }
  }
 theIntersect->doesHit = 0;										    /* clear out does hit */
 return(0);
}

/*********************************************************** RAY SPHERE ***********************************************************/

int raySphere(tRay3D *theRay, tIntersect *theIntersect)						/* ray intersect sphere procedure */
{
 double	A, B, C, discrim, disc_root;

 A = dot3D(theRay->dir, theRay->dir);	if(A == 0.0) return(0);				      /* compute A, B, C and discriminant */
 B = dot3D(theRay->start, theRay->dir);	
 C = dot3D(theRay->start, theRay->start) - 1.0;
 discrim = B * B - A * C;

 if(discrim >= 0.0)										 /* TRUE: ray does not hit object */
  {
   disc_root = sqrt(discrim);
   if(-B > disc_root)									/* TRUE: ray entering and does hit object */
    {
     theIntersect->hitTime = (-B - disc_root) / A;
     theIntersect->hitNormal.x = theRay->start.x;
     theIntersect->hitNormal.y = theRay->start.y;
     theIntersect->hitNormal.z = theRay->start.z;
     theIntersect->doesHit = 1;
     theIntersect->entering = 1;
     return(1);
    }
   if(-B > -disc_root)									 /* TRUE: ray exiting and does hit object */
    {
     theIntersect->hitTime = (-B + disc_root) / A;
     theIntersect->hitNormal.x = theRay->start.x;
     theIntersect->hitNormal.y = theRay->start.y;
     theIntersect->hitNormal.z = theRay->start.z;
     theIntersect->doesHit = 1;
     theIntersect->entering = 0;
     return(1);
    }
  }
 return(0);											/* FALSE: ray does not hit object */
}

/********************************************************* RAY CYLINDER ***********************************************************/

int rayCylinder(tRay3D *theRay, tIntersect *theIntersect)				      /* ray intersect cylinder procedure */
{
 double tIn, tOut, tCap, tBase;								  		 	     /* time hits */
 double hitTime, rayzIn, rayzOut;								               /* local variables */
 double A, B, C, discrim, disc_root;

 tIn = -100000.0; tOut = 100000.0;								 /* initialize time hit variables */
 theIntersect->doesHit = 0; 							   /* initialize does hit variable to doesn't hit */

 A = pow(theRay->dir.x,2) + pow(theRay->dir.y,2);
 B = (theRay->start.x * theRay->dir.x) + (theRay->start.y * theRay->dir.y);
 C = (pow(theRay->start.x,2) + pow(theRay->start.y,2)) - 1;
 discrim = B * B - A * C;

 if(A == 0) return(0); 					          /* ray parallel to axis of cylinder does not intersect the wall */
 if(discrim < 0.0) return(0);				             			       /* ray does not intersect the wall */

 disc_root = sqrt(discrim);
 tIn   = ((-B) - disc_root) / A;									     /* ray entering wall */
 tOut  = ((-B) + disc_root) / A;									      /* ray exiting wall */

 rayzIn = theRay->start.z + theRay->dir.z * tIn; rayzOut = theRay->start.z + theRay->dir.z * tOut;            /* set up temp rayz */

 if((rayzIn >= -1) && (rayzIn <= 1) || (rayzOut >= -1) && (rayzOut <= 1))                      /* ray hits cylinder within z-axis */
  {
   if(((pow(theRay->start.x + theRay->dir.x * tIn,2))  +				       /* ray hits cylinder within circle */
       (pow(theRay->start.y + theRay->dir.y * tIn,2)   <= 1)) ||
      ((pow(theRay->start.x + theRay->dir.x * tOut,2)) +
       (pow(theRay->start.y + theRay->dir.y * tOut,2)  <= 1)))
        {
         if(tIn < tOut)                                                                                         /* ray intersects */
          {
           if(tIn > 0.0)                                                                                   /* hit in front of eye */
            {
             theIntersect->doesHit = 1;
             theIntersect->entering = 1;                                                                          /* ray entering */
             theIntersect->hitTime = tIn;
             theIntersect->hitNormal.x = theRay->start.x; theIntersect->hitNormal.y = theRay->start.y;
             theIntersect->hitNormal.z = 0;
             return(1);
            }
           else if(tOut > 0.0)                                                                                     /* ray exiting */
            {
             theIntersect->doesHit = 1;
             theIntersect->entering = 0;
             theIntersect->hitTime = tOut;
             theIntersect->hitNormal.x = theRay->start.x; theIntersect->hitNormal.y = theRay->start.y;
             theIntersect->hitNormal.z = 0;
             return(1);
            }
          }
        }
  }

 if(theRay->dir.z == 0.0) return(0);

 tCap  = (( 1.0 - theRay->start.z) / theRay->dir.z);								       /* cap hit */
 tBase = ((-1.0 - theRay->start.z) / theRay->dir.z);								      /* base hit */

 if((pow(theRay->start.x + theRay->dir.x * tCap,2) + pow(theRay->start.y + theRay->dir.y * tCap,2)) < 1)	       /* cap hit */
  {
   theIntersect->doesHit = 1; 
   theIntersect->entering = 1;
   theIntersect->hitTime = tCap;
   theIntersect->hitNormal.x = 0; theIntersect->hitNormal.y = 0; theIntersect->hitNormal.z = 1; 
   return(1);
  }

 if((pow(theRay->start.x + theRay->dir.x * tBase,2) + pow(theRay->start.y + theRay->dir.y * tBase,2)) < 1)	      /* base hit */
  {
   theIntersect->doesHit = 1; 
   theIntersect->entering = 1;
   theIntersect->hitTime = tBase;
   theIntersect->hitNormal.x = 0; theIntersect->hitNormal.y = 0; theIntersect->hitNormal.z = -1; 
   return(1);
  }
 return(0);                                                                                                         /* ray misses */
}

/********************************************************* RAY CONE ***************************************************************/

int rayCone(tRay3D *theRay, tIntersect *theIntersect)					          /* ray intersect cone procedure */
{
 double tIn, tOut, tCap, tBase;								  		 	     /* time hits */
 double hitTime, rayzIn, rayzOut;								               /* local variables */
 double A, B, C, discrim, disc_root;

 tIn = -100000.0; tOut = 100000.0;								 /* initialize time hit variables */
 theIntersect->doesHit = 0; 							   /* initialize does hit variable to doesn't hit */

 A = pow(theRay->dir.x,2) + pow(theRay->dir.y,2) - (0.25 * pow(theRay->dir.z, 2));
 B = (theRay->start.x * theRay->dir.x) + (theRay->start.y * theRay->dir.y) + (theRay->dir.z * (1 - theRay->start.z) / 4);
 C = (pow(theRay->start.x,2) + pow(theRay->start.y,2)) - (0.25 * (1 - pow(theRay->start.z, 2)));
 discrim = B * B - A * C;

 if(A == 0) return(0); 					          /* ray parallel to axis of cylinder does not intersect the wall */
 if(discrim < 0.0) return(0);				             			       /* ray does not intersect the wall */

 disc_root = sqrt(discrim);
 tIn   = ((-B) - disc_root) / A;									     /* ray entering wall */
 tOut  = ((-B) + disc_root) / A;									      /* ray exiting wall */

 rayzIn = theRay->start.z + theRay->dir.z * tIn; rayzOut = theRay->start.z + theRay->dir.z * tOut;            /* set up temp rayz */

 if((rayzIn >= -1) && (rayzIn <= 1) || (rayzOut >= -1) && (rayzOut <= 1))                      /* ray hits cylinder within z-axis */
  {
   if(((pow(theRay->start.x + theRay->dir.x * tIn,2))  +				       /* ray hits cylinder within circle */
       (pow(theRay->start.y + theRay->dir.y * tIn,2)   <= 1)) ||
      ((pow(theRay->start.x + theRay->dir.x * tOut,2)) +
       (pow(theRay->start.y + theRay->dir.y * tOut,2)  <= 1)))
        {
         if(tIn < tOut)                                                                                         /* ray intersects */
          {
           if(tIn > 0.0)                                                                                   /* hit in front of eye */
            {
             theIntersect->doesHit = 1;
             theIntersect->entering = 1;                                                                          /* ray entering */
             theIntersect->hitTime = tIn;
             theIntersect->hitNormal.x = theRay->start.x * 2; theIntersect->hitNormal.y = theRay->start.y * 2;
             theIntersect->hitNormal.z = (1 - theRay->start.z) / 2;
             return(1);
            }
           else if(tOut > 0.0)                                                                                     /* ray exiting */
            {
             theIntersect->doesHit = 1;
             theIntersect->entering = 0;
             theIntersect->hitTime = tOut;
             theIntersect->hitNormal.x = theRay->start.x * 2; theIntersect->hitNormal.y = theRay->start.y * 2;
             theIntersect->hitNormal.z = (1 - theRay->start.z) / 2;
             return(1);
            }
          }
        }
  }

 if(theRay->dir.z == 0.0) return(0);

 tBase = ((-1.0 - theRay->start.z) / theRay->dir.z);								      /* base hit */

 if((pow(theRay->start.x + theRay->dir.x * tBase,2) + pow(theRay->start.y + theRay->dir.y * tBase,2)) < 1)	      /* base hit */
  {
   theIntersect->doesHit = 1; 
   theIntersect->entering = 1;
   theIntersect->hitTime = tBase;
   theIntersect->hitNormal.x = 0; theIntersect->hitNormal.y = 0; theIntersect->hitNormal.z = -1; 
   return(1);
  }
 return(0);                                                                                                         /* ray misses */
}

/********************************************************* RAY CUBE ***************************************************************/

int rayCube(tRay3D *theRay, tIntersect *theIntersect)				      		  /* ray intersect cube procedure */
{
 int i;														       /* counter */
 double numer, denom;										    /* numerator and denominators */
 double tIn, tOut;										  /* time hit in and time hit out */
 double hitTime;										      /* local variable, time hit */

 tIn = -100000.0; tOut = 100000.0;								 /* initialize time hit variables */

 for(i = 1; i <= 6; i++)										    /* check all 6 planes */
  {
        if(i == 1) {numer = 1.0 + theRay->start.x; denom = -(theRay->dir.x);}
   else if(i == 2) {numer = 1.0 - theRay->start.x; denom =   theRay->dir.x; }
   else if(i == 3) {numer = 1.0 + theRay->start.y; denom = -(theRay->dir.y);}
   else if(i == 4) {numer = 1.0 - theRay->start.y; denom =   theRay->dir.y; }
   else if(i == 5) {numer = 1.0 + theRay->start.z; denom = -(theRay->dir.z);}
   else if(i == 6) {numer = 1.0 - theRay->start.z; denom =   theRay->dir.z; }

   if (denom == 0) 											       /* ray is parallel */
    {
     if(numer < 0) {theIntersect->doesHit = 0;  return(0);}							   /* ray outside */
    }
   else													      /* ray not parallel */
    {
     hitTime = numer / denom;
     if(denom > 0) tOut = min(tOut, hitTime);									     /* ray exits */
     else 	   tIn  = max(tIn,  hitTime);									    /* ray enters */
    }
  }														  /* end for loop */

 if(tIn < tOut) 												/* ray intersects */
  {
   if (tIn > 0.0)											   /* hit in front of eye */
    {
     theIntersect->doesHit = 1; 
     theIntersect->entering = 1; 										  /* ray entering */
     theIntersect->hitTime = tIn;
     return(1);
    }
   else if(tOut > 0.0) 												   /* ray exiting */
    {
     theIntersect->doesHit = 1; 
     theIntersect->entering = 0; 
     theIntersect->hitTime = tOut;
     return(1);
    }
  }  
 theIntersect->doesHit = 0;										       /* ray doesn't hit */
 return(0);
}

/***************************************************** RAY POLYHEDRON *************************************************************/

int rayPolyhedron(tRay3D *theRay, tInstance *obj, tIntersect *theIntersect)		    /* ray intersect polyhedron procedure */
{
 int i, j, k;													       /* counter */
 t3Tuple temp;										   		    /* temporary variable */
 double numer, denom;										    /* numerator and denominators */
 double tIn, tOut;										  /* time hit in and time hit out */
 double hitTime;										      /* local variable, time hit */

 tIn = -100000.0; tOut = 100000.0;								 /* initialize time hit variables */

 for(i = 0; i < obj->planes->num; i++)								    /* check all number of planes */
  {

   temp.x = obj->planes->face[i].pt.x - theRay->start.x;
   temp.y = obj->planes->face[i].pt.y - theRay->start.y;
   temp.z = obj->planes->face[i].pt.z - theRay->start.z;

   numer = dot3D(obj->planes->face[i].pt, temp);
   denom = dot3D(obj->planes->face[i].pt, theRay->dir);

   if (denom == 0) 											       /* ray is parallel */
    {
     if(numer < 0) {theIntersect->doesHit = 0;  return(0);}							   /* ray outside */
    }
   else													      /* ray not parallel */
    {
     hitTime = numer / denom;
     if(denom > 0) {tOut = min(tOut, hitTime); k = i;}								     /* ray exits */
     else 	   {tIn  = max(tIn,  hitTime); j = i;}								    /* ray enters */
    }
  }														  /* end for loop */

 if(tIn < tOut) 												/* ray intersects */
  {
   if (tIn > 0.0)											   /* hit in front of eye */
    {
     theIntersect->doesHit = 1; 
     theIntersect->entering = 1; 										  /* ray entering */
     theIntersect->hitTime = tIn;
     theIntersect->hitNormal.x = obj->planes->face[j].pt.x;
     theIntersect->hitNormal.y = obj->planes->face[j].pt.y;
     theIntersect->hitNormal.z = obj->planes->face[j].pt.z;
     return(1);
    }
   else if(tOut > 0.0) 												   /* ray exiting */
    {
     theIntersect->doesHit = 1; 
     theIntersect->entering = 0; 
     theIntersect->hitTime = tOut;
     theIntersect->hitNormal.x = obj->planes->face[k].pt.x;
     theIntersect->hitNormal.y = obj->planes->face[k].pt.y;
     theIntersect->hitNormal.z = obj->planes->face[k].pt.z;
     return(1);
    }
  }  
 theIntersect->doesHit = 0;										       /* ray doesn't hit */
 return(0);
}

/***************************************************** RAY IN EXTENT **************************************************************/

int rayInExtent(tInstance *obj)									       /* ray in extent procedure */
{
 if((col >= obj->screenExtent.l) && (col <= obj->screenExtent.r) && 
    (row >= obj->screenExtent.t) && (row <= obj->screenExtent.b)) return(1);
 else return(0);
}

/********************************************************* RAY BOX EXTENT *********************************************************/

int rayBoxExtent(tRay3D *theRay, tInstance *obj)							     /* ray in box extent */
{
 t3Tuple P, Q;
 int i;														       /* counter */
 double numer, denom;										    /* numerator and denominators */
 double tIn, tOut;										  /* time hit in and time hit out */
 double hitTime;										      /* local variable, time hit */
 double xmin, xmax, ymin, ymax, zmin, zmax;									      /* 6 planes */

 tIn = -100000.0; tOut = 100000.0;								 	  /* initialize variables */
 xmin = ymin = zmin = 100000;
 xmax = ymax = zmax = -100000;

 for(P.x = -1.0; P.x <= 1.0; P.x += 2.0)			       /* transform generic cube to current object transformation */
  for(P.y = -1.0; P.y <= 1.0; P.y += 2.0)
   for(P.z = -1.0; P.z <= 1.0; P.z += 2.0)
    {
     Q.x = P.x * obj->transf.mat[0][0] + P.y * obj->transf.mat[1][0] + P.z * obj->transf.mat[2][0] + obj->transf.tr.x;
     Q.y = P.x * obj->transf.mat[0][1] + P.y * obj->transf.mat[1][1] + P.z * obj->transf.mat[2][1] + obj->transf.tr.y;
     Q.z = P.x * obj->transf.mat[0][2] + P.y * obj->transf.mat[1][2] + P.z * obj->transf.mat[2][2] + obj->transf.tr.z;

     xmin = min(xmin, Q.x); xmax = max(xmax, Q.x);
     ymin = min(ymin, Q.x); ymax = max(ymax, Q.y);
     zmin = min(zmin, Q.x); zmax = max(zmax, Q.z);
    }

 for(i = 1; i <= 6; i++)										    /* check all 6 planes */
  {
        if(i == 1) {numer = 1.0 + (theRay->start.x - xmin); denom = -(theRay->dir.x);}
   else if(i == 2) {numer = 1.0 - (theRay->start.x - xmax); denom =   theRay->dir.x; }
   else if(i == 3) {numer = 1.0 + (theRay->start.y - ymin); denom = -(theRay->dir.y);}
   else if(i == 4) {numer = 1.0 - (theRay->start.y - ymax); denom =   theRay->dir.y; }
   else if(i == 5) {numer = 1.0 + (theRay->start.z - zmin); denom = -(theRay->dir.z);}
   else if(i == 6) {numer = 1.0 - (theRay->start.z - zmax); denom =   theRay->dir.z; }

   if (denom == 0) 											       /* ray is parallel */
    {
     if(numer < 0) {return(0);}											   /* ray outside */
    }
   else													      /* ray not parallel */
    {
     hitTime = numer / denom;
     if(denom > 0) tOut = min(tOut, hitTime);									     /* ray exits */
     else 	   tIn  = max(tIn,  hitTime);									    /* ray enters */
    }
  }														  /* end for loop */

 if((tIn < tOut) && ((tIn > 0.0) || (tOut > 0.0))) return(1);						     /* ray inside extent */

 return(0);												     /* ray not in extent */
}

/****************************************************** TEXTURE SWITCH ************************************************************/

double texture(tInstance *obj, tIntersect *theIntersect) 					  	   /* texture type switch */
{
 double temp = 0.0;

 switch (toupper(obj->name[0]))								         /* force all inputs to uppercase */
  {
   case 'C': 													  /* checkerboard */
    {temp = (double)(0.5 * checkerBoard(theIntersect)); return(temp);}

   case 'I':													  /* image writer */
    {temp = (0.5 * imageWriter(theIntersect)); return(temp);}

   case 'W':													  /* wood texture */
    {temp = wood(theIntersect); return(temp);}

   case 'X':													    /* no texture */
    {temp = 0.0; return(temp);}
  }
 return(temp);
}

/********************************************************* CHECKERBOARD ***********************************************************/

int checkerBoard(tIntersect *theIntersect)								  /* checkerboard routine */
{
 int 	checkerColor;								    	    	        /* checker color variable */
 int 	tempx, tempy;

 tempx = abs((theIntersect->hitPoint.x + 1) / checkerSize);					/* set up x axis width of checker */
 tempy = abs((theIntersect->hitPoint.y + 1) / checkerSize);					/* set up y axis width of checker */

 checkerColor = floor((tempx + tempy) % 2);						      /* compute the color of the checker */

 return(checkerColor);
}

/************************************************************ WOOD ****************************************************************/

int wood(tIntersect *theIntersect)								  		  /* wood routine */
{
 int	woodColor;								    	    	           /* wood color variable */
 double theta, tempx, tempy, twist, woodTemp;
 int	wables	= 8;									/* the number of wabels in the wood grain */

 theta = atan2(theIntersect->hitPoint.y, theIntersect->hitPoint.x);				   /* 360 degree arc tan of y & x */
 tempx = sqrt(pow(theIntersect->hitPoint.x,2) + pow(theIntersect->hitPoint.y,2)) * 6;
 tempy = 0.4 * sin(wables * theta);
 twist = theIntersect->hitPoint.z / 10;									       /* twist the wable */

 woodTemp = (tempx + tempy + twist);
 woodColor = (int)(woodTemp) % 2;

 return(woodColor);
}

/********************************************************* IMAGE WRITER ***********************************************************/

double imageWriter(tIntersect *theIntersect)								  /* image writer routine */
{
 double imageColor;											  /* image color variable */
 int tempx, tempy;

 tempx = (int)(MAXCOL * (1 + theIntersect->hitPoint.x)) / 2;
 tempy = (int)(MAXCOL * (1 + theIntersect->hitPoint.y)) / 2;

 if((tempx < 0) || (tempy < 0) || (tempx > MAXROW) || (tempy > MAXCOL)) {imageColor = 0.0; return(imageColor);}

 imageColor = (double)imageArray[tempx][tempy];							/* compute the color of the image */

 return(imageColor);
}

/********************************************************* IN SHADOW **************************************************************/

int inShadow(t3Tuple startPt, threeTup lightPt)								   /* in shadow procedure */

    /* return 1 if some object lies between startPt and lightPt. startPt has been chosen to lie "just off" the surface of the hit */
									       /* object, so self-shadowing is not a special case */
{
 tRay3D feeler, genericRay;
 tIntersect theIntersect, extentIntersect;
 tInstance *obj;

 feeler.start = startPt;								    /* compute feller start and direction */
 feeler.dir.x = lightPt.x - startPt.x; feeler.dir.y = lightPt.y - startPt.y;  feeler.dir.z = lightPt.z - startPt.z;

 for(obj = objects; obj != NULL; obj = obj->next)
  {
   if(!rayBoxExtent(&feeler, obj)) continue;						   /* check if feeler ray hits box extent */

   sceneToGeneric(obj, &feeler, &genericRay);				     /* convert screen coordinates to generic coordinates */

   if(obj->type != plane)								   /* do all objects except PLANE objects */
    {
     rayCube(&genericRay, &extentIntersect);				     /* check if generic ray hits the generic cube extent */
     if(!extentIntersect.doesHit) continue;
    }
   if(rayHit(&genericRay, obj, &theIntersect)) return(1);						  /* hit one object, done */
  }
 return(0);  											     /* no object lies in the way */
}

/*********************************************************** colorShade ***********************************************************/

tColor colorShade(tRay3D *sceneRay, int level)								 /* color shade procedure */
{
/* return the light intensity coming "back" along ray from the first object hit uses only the red component of diffuse, specular, */
											     /* etc. for reflection coefficients. */
 double 	mag, dot, textureComp, c1, c2, v1, v2;
 tInstance	*obj, *bestObj;
 tIntersect 	theIntersect, bestIntersect, extentIntersect;
 t3Tuple 	feelerPt, toSource, halfVec, normal, incident;
 tRay3D 	genericRay, spawnedRay;
 tLight 	*source;
 tColor		intensity, RGBIntensity;

 bestIntersect.hitTime = BIG_FLT;							       /* initialize hit time to infinity */
 for(obj = objects; obj != NULL; obj = obj->next)							      /* for every object */
  {
   if((level == 0) && (!rayInExtent(obj)) && (obj->type != plane)) continue; 		  /* check if screen extent is hit by ray */
									  /* if (ray misses world box or sphere extent) continue; */
   sceneToGeneric(obj, sceneRay, &genericRay);				     /* convert screen coordinates to generic coordinates */

   if(obj->type != plane)								   /* do all objects except PLANE objects */
    {
     rayCube(&genericRay, &extentIntersect); 				     /* check if generic ray hits the generic cube extent */
     if(!extentIntersect.doesHit) continue;  	 								    /* ray misses */
    }

   if((rayHit(&genericRay, obj, &theIntersect)) && 							      /* find best object */
      (theIntersect.hitTime < bestIntersect.hitTime)) 
    {
     bestIntersect = theIntersect; 
     bestIntersect.hitObj = obj;
    }
  }														/* end of for obj */

 if(bestIntersect.hitTime == BIG_FLT)						       /* ray misses, return background intensity */
  {
   intensity.r = intensity.g = intensity.b = BACKGROUND;
   return(intensity);
  }

 bestObj = bestIntersect.hitObj; 									    /* get object pointer */

 intensity.r = theScene.ambient.r * bestObj->diffuse.r;						    /* initialize intensity value */
 intensity.g = theScene.ambient.g * bestObj->diffuse.g;
 intensity.b = theScene.ambient.b * bestObj->diffuse.b;

 transpose(normal, bestIntersect.hitNormal, bestObj->inver.mat);		   /* compute vector m (m = m'm-T), normal vector */
 mag = magnitude(normal);										     /* get the magnitude */

 if(mag == 0.0) {printf("\n magnitude normal equals 0.0\n"); exit(0);}				    /* get out of bad calculation */

 normalize(normal, normal, mag);									  /* normalize the vector */

 bestIntersect.hitPoint.x = sceneRay->start.x + (sceneRay->dir.x * bestIntersect.hitTime);		 /* compute the hit point */
 bestIntersect.hitPoint.y = sceneRay->start.y + (sceneRay->dir.y * bestIntersect.hitTime);
 bestIntersect.hitPoint.z = sceneRay->start.z + (sceneRay->dir.z * bestIntersect.hitTime);

							   /* make start point of shadow feeler, back off along ray a tiny amount */
 feelerPt.x = bestIntersect.hitPoint.x - (eps * sceneRay->dir.x);
 feelerPt.y = bestIntersect.hitPoint.y - (eps * sceneRay->dir.y);	
 feelerPt.z = bestIntersect.hitPoint.z - (eps * sceneRay->dir.z);	

 for(source = lights; source != NULL; source = source->next)							/* for each light */
  {
   if(inShadow(feelerPt,source->position)) continue;        		   /* skip to next source, if object in the way of source */
    toSource.x = source->position.x - bestIntersect.hitPoint.x;				       /* compute vector s, source vector */
    toSource.y = source->position.y - bestIntersect.hitPoint.y;
    toSource.z = source->position.z - bestIntersect.hitPoint.z;
    mag = magnitude(toSource);

    if(mag == 0.0) {printf("\n magnitude toSource equals 0.0\n"); exit(0);}			    /* get out of bad calculation */

    normalize(toSource, toSource, mag);

    dot = dot3D(toSource, normal);                   							       /* cosine of angle */

    if(dot > 0)  										      /* get diffuse contribution */
     {
      textureComp = texture(bestObj, &bestIntersect);						   /* compute the type of texture */
      intensity.r += source->color.r * dot * (bestObj->diffuse.r + textureComp);
      intensity.g += source->color.g * dot * (bestObj->diffuse.g + textureComp);
      intensity.b += source->color.b * dot * (bestObj->diffuse.b + textureComp);
     }

    halfVec.x =  toSource.x - sceneRay->dir.x;	       /* get specular contribution; use  Blinn's: H = s + v , with v = -ray->dir */
    halfVec.y =  toSource.y - sceneRay->dir.y;
    halfVec.z =  toSource.z - sceneRay->dir.z;
    mag = magnitude(halfVec);

    if(mag == 0.0) {printf("\n magnitude halfVec equals 0.0\n"); exit(0);}			    /* get out of bad calculation */

    normalize(halfVec, halfVec, mag);

    dot = dot3D(halfVec, normal);

    if(dot > someMinimum)   										/* big enough to bother ? */
     {
      intensity.r += source->color.r * bestObj->specular.r * pow(dot, bestObj->specular.n);
      intensity.g += source->color.g * bestObj->specular.g * pow(dot, bestObj->specular.n);
      intensity.b += source->color.b * bestObj->specular.b * pow(dot, bestObj->specular.n);
     }
  } 													  /* end: for each source */
 if(++level == maxLevel) return(intensity);   							  /* we've recursed deeply enough */

 if(bestObj->reflection >= minShiny)  						      /* add mirror reflected light, r = i + 2c1n */
  {
   dot = dot3D(normal, sceneRay->dir) * 2;   					 /* dot product of normal and scene ray direction */

   spawnedRay.dir.x = (sceneRay->dir.x - dot * normal.x);					       /* get reflected direction */
   spawnedRay.dir.y = (sceneRay->dir.y - dot * normal.y);
   spawnedRay.dir.z = (sceneRay->dir.z - dot * normal.z);

   transform(spawnedRay.start, bestIntersect.hitPoint, bestObj->transf.mat);				/* get new starting point */

   spawnedRay.start.x += bestObj->transf.tr.x;
   spawnedRay.start.y += bestObj->transf.tr.y;
   spawnedRay.start.z += bestObj->transf.tr.z;

   RGBIntensity = colorShade(&spawnedRay, level);  			  /* recursivly enter colorshade until no more reflection */
   intensity.r += bestObj->reflection * RGBIntensity.r;
   intensity.g += bestObj->reflection * RGBIntensity.g;
   intensity.b += bestObj->reflection * RGBIntensity.b;
  }

 if(bestObj->transparent >= minTransparent) 								   /* add refracted light */
  {
   if(level == 1) v1 = 1.0;                                               /* prevent division by zero and initialize speed to air */
   else v1 = sceneRay->inside->speed;                                                  /* initialize previous inside object speed */

   if(v1 <= 0.0) return(intensity);								      /* prevent division by zero */

   v2 = bestObj->speed;											   /* initialize v1 speed */

   mag = magnitude(sceneRay->dir);
   if(mag == 0.0) {printf("\n magnitude transparent incident equals 0.0\n"); exit(0);}		    /* get out of bad calculation */
   normalize(incident, sceneRay->dir, mag);
   c1 = dot3D(normal, -incident);   			      /* dot product of normal and negative incident ray, cosine theta #1 */
   c2 = (pow(v2, 2) / pow(v1, 2)) * (1 - pow(c1, 2));				  			       /* cosine theta #2 */

   if(c2 > 1.0) return(intensity);					/* total internal reflection, there is no transmitted ray */

   c2 = sqrt(1 - c2);
   spawnedRay.dir.x = ((v2 / v1) * incident.x) + ((((v2 / v1) * c1) - c2) * normal.x);		     /* get transparent direction */
   spawnedRay.dir.y = ((v2 / v1) * incident.y) + ((((v2 / v1) * c1) - c2) * normal.y);
   spawnedRay.dir.z = ((v2 / v1) * incident.z) + ((((v2 / v1) * c1) - c2) * normal.z);

   transform(spawnedRay.start, bestIntersect.hitPoint, bestObj->transf.mat);				/* get new starting point */

   spawnedRay.start.x += bestObj->transf.tr.x;
   spawnedRay.start.y += bestObj->transf.tr.y;
   spawnedRay.start.z += bestObj->transf.tr.z;

   spawnedRay.inside = bestObj;

   RGBIntensity = colorShade(&spawnedRay, level);  			  /* recursivly enter colorshade until no more reflection */
   intensity.r += bestObj->transparent * RGBIntensity.r;
   intensity.g += bestObj->transparent * RGBIntensity.g;
   intensity.b += bestObj->transparent * RGBIntensity.b;
  }
 return(intensity);
} 
