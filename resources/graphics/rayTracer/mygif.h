#include <stdio.h>
#include <string.h>
#include <stdlib.h>  /* has prototypes of free() and calloc()*/

/* adjust next four typedef's so that on your system
BYTE, USHORT, SHORT, and LONG are of lengths 1, 2, 2, and 4 bytes */

typedef unsigned char BYTE;		/* must be one-byteinteger */
typedef unsigned short USHORT;  /* must be two-byte integer */
typedef short SHORT;  			/* must be two-byte integer */
typedef unsigned long ULONG;	/* must be four-byte integer */
typedef struct  {
   BYTE Red;                   /* RGB components of color */
   BYTE Green;                 /* register */
   BYTE Blue;
} ColorRegister;

#define fatal(msg) {puts(msg); exit(-1);}

void putShort(USHORT s, FILE *fp);
void ProcessPixel(USHORT pix);
void StartGIF(char * FileName,USHORT ImageWidth, USHORT ImageHeight);
void WriteLUT(ColorRegister LUT[]);
void StartLZW(void);
void FinishGIF(void);






