/*  Robert Estey 											  */
/*  Email:  estey@mosvax.sps.mot.com  Phone:  (602) 655-2391  Fax:  (602) 655-3569						  */
/*  UMASS ECE660 / NTU ST740A	Program 1											  */
/*																  */
/*  Ran on VAX/VMS V5.5-2 & DEC/Motif V1.1											  */
/*																  */
/*  cc   program					- C Compiler V3.2 on VAX/VMS V5.5-2					  */
/*  link program, sys$library:dxm_link.opt/opt		- Link in the X11 - DEC/Motif V1.1 libraries				  */
/*  set  display/create/transport=decnet/node=cv64 	- Set the display							  */
/*  run  program					- Execute the program							  */
/*																  */
/*  sys$library:dxm_link.opt (contents in file are below)									  */
/*																  */
/*  ! Linker options to link with DEC/Motif 1.1 shareable images								  */
/*  psect_attr=XMQMOTIF,noshr,lcl												  */
/*  SYS$SHARE:DECW$DXMLIBSHR/SHAREABLE												  */
/*  SYS$SHARE:DECW$XMLIBSHR/SHAREABLE												  */
/*  SYS$SHARE:DECW$XLIBSHR/SHAREABLE												  */
/*  SYS$SHARE:VAXCRTL/SHAREABLE													  */
/*																  */
/*  Program Description:													  */
/*																  */
/*  This program draws 4 graphics routines.  The Hailstone sequence, Logistic Map, Sierpinski Gasket and the Mandelbrot Set.	  */
/*  All programs inputs are from the input file (lab2.dat) which stores all the integer and double parameters from a user, with   */
/*  the exception of part 3.  To change parameters to the input file please follow the instructions below.			  */
/*																  */
/*      Program Name				Execution				Datafile (lab2.dat)			  */
/*																  */
/*  1)  Hailstone				Exposed Window				Line 1 only				  */
/*																  */
/*      fields: World.left, World.top, World.right, World.bottom, intervals, number (1 thru 100000000)				  */
/*																  */
/*  2)  Logistic Map				Exposed Window				Line 2 only				  */
/*																  */
/*      fields: World.left, World.top, World.right, World.bottom, lambda, x							  */
/*																  */
/*  3)  Sierpinski Gasket			Mouse Button Pressed			n/a					  */
/*																  */
/*  4)  Mandelbrot Set				Keyboard Pressed			Line 3 only				  */
/*																  */
/*      fields:	World.left, World.top, World.right, World.bottom, threshold, numrows, numcols, maxnum				  */
/*																  */
/*  To properly run the program and get the inputs do the following:  Run Program.  The window will expose itself and create an   */
/*  blank display.  Then press the mouse key 3 times.  This will start the Sierpinski Gasket program.  Finally press a key on 	  */
/*  the keyboard and this will execute parts 1 Hailstone program, 2 Logistic Map program and 4 Mandelbrot Set program with 30	  */
/*  second intervals between each of them.											  */
/*																  */
/*  This program has a postscript device driver which was provided by Professor F. S. Hill and I made slight modifications.  	  */
/*  All programs are IFS (Iterated Function Systems).  The title of the project was Chaos Capers.				  */
/*																  */
/*  This program has a Clipping subroutine which was provided by Professor F. S. Hill, test program, Oct 5,1993, fsh. 		  */
/*  I made slight modifications.												  */
/*																  */
/**************************************************** INITIALIZE & START PROGRAM **************************************************/

#include 	<stdio.h>									  /* include standard i/o library */
#include	<stdlib.h>									  /* include standard lib library */
#include	<string.h>										/* include string library */
#include        <X11/Xlib.h> 		                                                                   /* include x11 library */
#include        <math.h>                                                                                  /* include math library */
#include	<signal.h>									        /* include signal library */

#define		MAXX			998				      /* maximum number of columns (1024 x 768 Xterminal) */
#define		MAXY			732				 	 /* maximum number of rows (1024 x 768 Xterminal) */

#define 	NumBeforeStroke 	100									/* i have no idea */
#define 	margin 			54					       /* number of points for the outside margin */
#define 	pageheight 		792 								    /* 11 inch x 72 point */
#define 	pagewidth 		612  								  /* 8.5 inch x 72 points */

#define 	toLeft 			8						/* 1000, point is to the left of viewport */
#define 	Above 			4						     /* 0100, point is above the viewport */
#define 	toRight 		2						  /* 0010, point to the right of viewport */
#define 	Below 			1     						     /* 0001, point is below the viewport */

/************************************************************* TYPEDEFS ***********************************************************/

typedef		struct			{double l, t, r, b;} tRealRect;				      /* structure real rectangle */
typedef		struct			{int    l, t, r, b;} tIntRect;				   /* structure integer rectangle */
typedef		struct			{double A, B, dx, dy;} tMap;		      /* structure mapping from world to viewport */
typedef		struct			{double x, y;} tRealPoint;					 /* structure real points */
typedef		struct			{int    x, y;} tIntPoint;				      /* structure integer points */
typedef		struct			{int 	numrows, numcols; double Rs;} tDisplay;	     		     /* structure display */

/************************************************************ PROTOTYPES **********************************************************/

void 		x11graphics();							      		                  /* X11 graphics */
void 		get_GC(GC *gc);  								          /* get graphics context */

void 		setRealRect(double left, double top, double right, double bottom, tRealRect *W);		/* real rectangle */
void 		setIntRect(int left, int top, int right, int bottom, tIntRect *V);			     /* integer rectangle */
tIntRect 	matchW2V(tRealRect W, tDisplay *gDisplay);			 	     /* match world size to viewport size */
tMap 		W2V(tRealRect W, tIntRect V);								     /* world to viewport */
void 		moveto(double x, double y);							    /* move to screen coordinates */
void 		lineto(double x, double y);						     /* draw a line on screen coordinates */
void		dot(double x, double y);					     /* draws a dot (point) on screen coordinates */
void 		locate(tIntPoint *point, int button_pressed);			       /* locates screen point and button pressed */
int 		clip(tIntPoint *pa, tIntPoint *pb);							      /* clipping routine */
void 		devClipLineTo(int x, int y);								  /* clip line to routine */
void 		Rosette(int n, int radius, int cx, int cy);						  /* draw rosette routine */

void		hailstone();										     /* hailstone program */
void 		logistic_map();										  /* logistic map program */
void		sierpinski_gasket();								     /* sierpinski gasket program */
void		mandelbrot();										/* mandelbrot set program */
int 		mandelcount(tRealPoint c, int maxnum, double thresh);				          /* mandelcount function */

void 		Init_Post(int vportWid, int vportHt);							 /* initialize postscript */
void 		Write_Post(int x, int y, int which);							      /* write postscript */

/********************************************************* GLOBAL VARIABLES  ******************************************************/

Display         *display;                                       /* X Terminal address, pointer to display structure on X terminal */
int             screen;      		                      /* X Terminal screen, root window to screen structure on X terminal */
Window          win;                                                                                             /* Window widget */
GC              gc;                                                                                     /* ID of graphics context */
XEvent		event;									       /* Structure for event information */
double		physicalHeight		= 21.25;				 /* height of screen in cm (1024 x 768 Xterminal) */
double		physicalWidth		= 29.10; 			  	  /* width of screen in cm (1024 x 768 Xterminal) */

FILE            *input_file;                                                                                /* declare input file */
FILE 		*postscript_file;								       /* declare postscript file */
char            input_filename[20]	= "lab2.dat"; 							    /* name of input file */
char		postscript_filename[20] = "lab2.ps";						       /* name of postscript file */
int		infinite		= 1;							      /* set up for infinite loop */
int		status 			= 0;									 /* return status */
int 		vWid 			= 1006;								  /* choose viewport size */
int		vHt 			= 734;								  /* choose viewport size */

tRealRect	W; 													/* window */
tIntRect 	V;												      /* viewport */
tDisplay	gDisplay;							  /* window aspect ratio to viewport asepct ratio */
tMap		Map;										     /* world to viewport mapping */
tIntPoint	IntCP, IntPoint;								     /* point, screen coordinates */
tRealPoint	RealCP, RealPoint;							 	      /* point, world coordinates */

int num, rad, cx, cy;

/*********************************************************** MAIN PROGRAM *********************************************************/

int main(int argc, char **argv)										    /* main program start */
{
 setRealRect(1, 1, 5, 4, &W);											 /* set up window */
 setIntRect(100, 100, 500, 400, &V);
 Map = W2V(W,V);											/* map window to viewport */
 num = 10; rad = 100; cx = 150; cy = 150;

 if((input_file = fopen(input_filename,"r")) == NULL)                                                          /* open input file */
     {printf("\ncould not open input file\n"); exit(0);}

 x11graphics();												  /* execute X11 graphics */

 fclose(input_file); 											      /* close input file */
}														   /* end program */

/*********************************************************** SUBROUTINES **********************************************************/

/**********************************************************  X11 GRAPHICS *********************************************************/

void x11graphics()		
{
 int    window_x                = 0;  		                                /* Window position, X axis (1024 x 768 Xterminal) */
 int    window_y                = 0;                                            /* Window position, Y axis (1024 x 768 Xterminal) */
 int    window_width            = 1006;               /* Window size, width - Including the window manager (1024 x 768 Xterminal) */
 int    window_height           = 734;               /* Window size, height - Including the window manager (1024 x 768 Xterminal) */

 char   *window_name            = "Window Program";                                      /* Window name that appears on Title Bar */
 char   *display_name   = NULL;   /* Server to connect to; NULL means connect to server specified in environment variable DISPLAY */
 int    window_depth            = 0;                                   /* Window window_depth, window_depth = 0 taken from parent */
 int    window_border_width     = 4;                                                                              /* Border width */
 int    window_class            = 0;                                                                              /* Window class */
 long   valuemask               = 0;                                             /* Specifies which window attributes are defined */
 int    onoff                   = 1;                             /* 0 - disable synchronization, nonzero - enable synchronization */
 int    mode                    = 0;     	                                                               /* coordinate mode */

 Visual                         *visual;                                                                         /* Visual widget */
 Colormap                       cmap;                                                                          /* Colormap widget */
 XGCValues                      values;                                                                  /* X GC values structure */
 XSetWindowAttributes           setwinattr;                                                      /* X window attributes structure */

 if((display=XOpenDisplay(display_name)) == NULL )                                                         /* Connect to X server */
  {
   (void) fprintf(stderr, "Window Program: cannot connect to X server %s\n", XDisplayName(display_name));
   exit(0);
  }

 XSynchronize(display, onoff);                               /* Synchronous mode for debugging, events are reported as they occur */

 screen                         = DefaultScreen(display);                               /* Get screen size from display structure */
 window_depth                   = DisplayPlanes(display, screen);                 /* Get window_depth size from display structure */
 visual                         = DefaultVisual(display, screen);                       /* Get visual size from display structure */
 cmap                           = DefaultColormap(display, screen);                       /* Get color map from display structure */

 valuemask                      = CWBackPixel | CWBorderPixel;                                    /* Setting up window attributes */
 setwinattr.background_pixel    = BlackPixel(display, screen);
 setwinattr.border_pixel        = WhitePixel(display, screen);

 win = XCreateWindow(display, RootWindow(display, screen), window_x, window_y, window_width, window_height,      /* Create window */
                     window_border_width, window_depth, window_class, visual, valuemask, &setwinattr);

/*  									     Set the minimum set of properties for window manager */

 XSetStandardProperties(display, win, window_name, NULL, NULL, NULL, NULL, NULL);

/*                              Select all event types wanted (ExposureMask | ButtonPressMask | ButtonReleaseMask | KeyPressMask) */

 XSelectInput(display, win, ExposureMask | ButtonPressMask | ButtonReleaseMask | KeyPressMask);

 get_GC(&gc);                                                                              /* Create GC for graphics applications */
 XMapWindow(display, win);                                                                                          /* Map window */

/********************************************************* BEGIN GRAPHICS *********************************************************/

 while(infinite) 						 	     		     /* Waiting for next X event to occur */
  {
   XNextEvent(display, &event);                                                               /*  Execute on events as they occur */
   switch (event.type)
    {
     case Expose:							      		     /* Waiting for next X event to occur */
      {
       Rosette(num,rad,cx,cy);
       getchar();getchar(); /* pause to admire */
      }
     break;

     case ButtonPress:
      XFlush(display);                                                                 /* Flush the pipeline to update the screen */
      XClearWindow(display,win);								    /* clears the entire X window */
      sierpinski_gasket();									/* call sierpinski gasket program */
     break;

     case KeyPress:
      XClearWindow(display,win);								    /* clears the entire X window */
      if((postscript_file = fopen(postscript_filename,"w")) == NULL)  					  /* open postscript file */
         {printf("\ncould not open input file\n"); exit(0);}
      Init_Post(vWid,vHt);										 /* initialize postscript */

      hailstone();											/* call hailstone program */

      fprintf(postscript_file,"\nstroke showpage\n");			     /* write stroke showpage to postscript file to print */
      fclose(postscript_file); 										 /* close postscript file */

      sleep(30);									 /* wait 30 seconds for the next graphics */
      XClearWindow(display,win);								    /* clears the entire X window */

      if((postscript_file = fopen(postscript_filename,"w")) == NULL)  					  /* open postscript file */
       {printf("\ncould not open input file\n"); exit(0);}
      Init_Post(vWid,vHt);										 /* initialize postscript */

      logistic_map();										     /* call logistic map program */

      fprintf(postscript_file,"\nstroke showpage\n");			     /* write stroke showpage to postscript file to print */
      fclose(postscript_file); 										 /* close postscript file */

      sleep(30);									 /* wait 30 seconds for the next graphics */
      XClearWindow(display,win);								    /* clears the entire X window */

      mandelbrot();										       /* call mandelbrot program */

      sleep(30);									 /* wait 30 seconds for the next graphics */
      XFreeGC(display, gc);                                                      /* Frees all memory associated with GC on server */
      XCloseDisplay(display);                                              /* Disconnect client program from X server and display */
      return;                                                                                                 /* Return to caller */
     break;

     default:
     break;
    }                                                                                                                 /* end case */
  }                                                                                                                  /* end while */
}                                                                                                               /* end subroutine */

/********************************************************  GRAPHIC CONTEXT ********************************************************/

void get_GC(GC *gc)
{
 long valuemask = 0;	 								     /* ignore XGCvalues and use defaults */
 XGCValues values;
 int line_width = 1;												/* set line width */
 int line_style = LineSolid;										   /* set for solid lines */
 int cap_style = CapButt;									      /* cap all line connections */
 int join_style = JoinBevel;											   /* bevel style */

 *gc = XCreateGC(display, win, valuemask, &values);					       /* Create default graphics context */
 XSetForeground(display, *gc, WhitePixel(display,screen));   					/* Specify foreground pixel color */
 XSetBackground(display, *gc, BlackPixel(display,screen));   					/* Specify background pixel color */
 XSetLineAttributes(display, *gc, line_width, line_style, cap_style, join_style); 			   /* Set line attributes */
}

/***************************************************** LOAD WINDOW PARAMETERS *****************************************************/

void setRealRect(double left, double top, double right, double bottom, tRealRect *W)
{
 W->l = left; W->t = top; W->r = right; W->b = bottom;				 /* load W (window) datastructure with parameters */
}

/***************************************************** LOAD VIEWPORT PARAMETERS ***************************************************/

void setIntRect(int left, int top, int right, int bottom, tIntRect *V)
{
 V->l = left; V->t = top; V->r = right; V->b = bottom;			       /* load V (viewport) datastructure with parameters */
}

/***************************************************** MATCH WORLD TO VIEWPORT ****************************************************/

tIntRect matchW2V(tRealRect W, tDisplay *gDisplay)			   /* matches world aspect ratio to viewport aspect ratio */
{
 tIntRect V;
 double Rw, Wwidth, Wheight;
 Wwidth = W.r - W.l; Wheight = W.t - W.b; 							    /* compute widths and heights */
 gDisplay->numrows = MAXY + 1; gDisplay->numcols = MAXX + 1;		       /* load display structure with numrows and numcols */

 if(Wwidth == 0.0 || Wheight == 0.0)									        /* invalid window */
  {printf("\n invalid window part 1 \n"); exit(0);}

 gDisplay->Rs = physicalHeight / physicalWidth; Rw = Wheight / Wwidth;		     /* get the aspect ratio of display and world */

 if(Rw > gDisplay->Rs)											     /* landscape display */
  {
   V.l = 0; V.t = 0; V.b = gDisplay->numrows - 1; V.r = (int)(Rw * gDisplay->numrows / gDisplay->Rs);
  }
 else													      /* portrait display */
  {
   V.l = 0; V.t = 0; V.b = (int)(gDisplay->Rs * gDisplay->numcols / Rw) ; V.r = gDisplay->numrows - 1;
  }
 return(V);
}

/******************************************************* MAP WORLD TO VIEWPORT ****************************************************/

tMap W2V(tRealRect W, tIntRect V)						 /* map world coordinates to viewport coordinates */
 {
  double Wwidth, Wheight, Vwidth, Vheight;

  Wwidth = W.r - W.l; Wheight = W.t - W.b; Vwidth = V.r - V.l; Vheight = V.t - V.b;  /* compute widths and heights, V is negative */

  if(Wwidth == 0.0 || Wheight == 0.0) 										/* invalid window */
   {printf("\n invalid window part 2 \n"); exit(0);}

  Map.A = Vwidth / Wwidth;  		Map.B = Vheight / Wheight;				       /* scales being calculated */
  Map.dx = V.l - (W.l * Map.A); 	Map.dy = V.b - (W.b * Map.B);				 /* translations being calculated */
  return(Map);
 }

/************************************************************* MOVE TO ************************************************************/

			   /* this routine doesn't do much compared to other graphics programs due to how Xwindows is implemented */

void moveto(double x, double y)								    /* compute coordinates but don't draw */
{
 int x1, y1;												     /* world coordinates */
 RealCP.x = x; RealCP.y = y;									 /* update world current position */
}

/************************************************************* LINE TO ************************************************************/

void lineto(double x, double y)										     /* draw line segment */
{
 int x1, y1, x2, y2;											    /* screen coordinates */

 x1 = (int)(((Map.A * RealCP.x) + Map.dx) + 0.5);			       /* compute world coordinates to screen coordinates */
 y1 = (int)(((Map.B * RealCP.y) + Map.dy) + 0.5);
 x2 = (int)(((Map.A * x) + Map.dx) + 0.5);				       /* compute world coordinates to screen coordinates */
 y2 = (int)(((Map.B * y) + Map.dy) + 0.5);
 XDrawLine(display, win, gc, x1, y1, x2, y2);						       /* draw line in screen coordinates */
 RealCP.x = x; RealCP.y = y;									/* update screen current position */
 Write_Post(x1,y1,1); 									    			/* put in PS file */
 Write_Post(x2,y2,2);
}

/************************************************************** DOT ***************************************************************/

void dot(double x, double y)							     /* draws a dot (point) on screen coordinates */
 {
  int x1, y1;

  x1 = (int)(((Map.A * x) + Map.dx) + 0.5);				       /* compute world coordinates to screen coordinates */
  y1 = (int)(((Map.B * y) + Map.dy) + 0.5);
  XDrawPoint(display, win, gc, x1, y1);										/* draw the point */
 }

/************************************************************* LOCATE *************************************************************/

void locate(tIntPoint *point, int button_pressed)			     /* locate mouse button pressed and pixel point value */
 {
  do
   {
    XWindowEvent(display, win, ButtonPressMask|ButtonReleaseMask, &event);		   /* wait for mouse button to be pressed */
   } while (event.type != ButtonRelease);

  button_pressed = event.xbutton.button;						 /* get the mouse button that was pressed */
  point->x = event.xbutton.x;							      /* get the x screen coordinate of the mouse */
  point->y = event.xbutton.y;							      /* get the y screen coordinate of the mouse */
 }

/************************************************************ HAILSTONE ***********************************************************/

void hailstone()											    /* draw the hailstone */
{
 double		i,j;												      /* counters */
 double		left, top, right, bottom;								     /* window parameters */
 double		i_scale, y;										  /* hailstone parameters */

 fscanf(input_file,"%lf %lf %lf %lf %lf %lf \n", &left, &top, &right, &bottom, &i_scale, &y);			    /* get inputs */

 setRealRect(left, top, right, bottom, &W);									 /* set up window */
 V = matchW2V(W, &gDisplay);					            /* match window aspect ratio to viewport aspect ratio */
 Map = W2V(W,V);											/* map window to viewport */

 moveto(W.l,W.b);									 /* start drawing from Window left bottom */
 i = W.l; j = W.b;

 while(y != 1.0)										  /* keep computing until y = 1.0 */
 {
  if(fmod(y,2.0) == 0)
   {
    y = y * 0.5;			       /* even number, y / 2, I learned this trick from ECE660 to multiply and not divide */
   }
  else
   {
    y = (y * 3) + 1;											 /* odd number, y * 3 + 1 */
   }

  i = (i_scale * (W.r - W.l)) + i; j = log(y) / (W.t - W.b);				/* scale line width with iterative factor */
  lineto(i, j);												        /* plot hailstone */
 }
}

/*********************************************************** LOGISTIC MAP *********************************************************/

void logistic_map()											 /* draw the logistic map */
{
 double		i, increment, lambda, x, xk, y;
 double		left, top, right, bottom;								     /* window parameters */

 fscanf(input_file,"%lf %lf %lf %lf %lf %lf \n", &left, &top, &right, &bottom, &lambda, &x);			    /* get inputs */

 setRealRect(left, top, right, bottom, &W);									 /* set up window */
 V = matchW2V(W, &gDisplay);					            /* match window aspect ratio to viewport aspect ratio */
 Map = W2V(W,V);											/* map window to viewport */

										       /* draw box around viewport and x = y line */

 moveto(0.0,0.0); lineto(0.0,1.0);  lineto(1.0,1.0);  lineto(1.0,0.0);  lineto(0.0,0.0);  lineto(1.0,1.0);  moveto(0.0,0.0);

 increment = x;												      /* set up increment */
 for(i = increment; i < 1; i = i + increment)
  {
   y = ((4 * lambda) * x ) * (1 - x); lineto(x,y); x = x + increment;					    /* draws the parabola */
  }
 lineto(x,0);												/* draw last line segment */

 x = increment; xk = increment;									     /* reset x and initialize xk */
 moveto(xk,0);											     /* move to Xk0 (Xk sub zero) */
 for(i = increment; i < 10; i = i + increment)		    /* i < 10 used to get lots of drawings, could be set to 1 as parabola */
  {
   y = ((4 * lambda) * x ) * (1 - x); lineto(xk,y); 								/* draws vertical */
   xk = y; lineto(xk,xk);										      /* draws horizontal */
   x = y;
  }
}

/********************************************************* SIERPINSKI GASKET ******************************************************/

void sierpinski_gasket()									       /* draws sierpinski gasket */
{
 int   		button_pressed[3];    									  /* mouse button pressed */
 int		i, j;
 tIntPoint 	point, p[3];											/* x and y points */

 setRealRect(-1.0, 1.0, 1.0, -1.0, &W);										 /* set up window */
 V = matchW2V(W, &gDisplay);					            /* match window aspect ratio to viewport aspect ratio */
 Map = W2V(W,V);											/* map window to viewport */

 for(i = 0; i < 3; i++) locate(&p[i], &button_pressed[i]);						/* get 3 points on screen */

 i = 0;												     /* set 1st point in triangle */
 for(j = 0; j < 5000; j++)								       /* do 5000 iterations, draw points */
  {
   point.x = (0.5 * (point.x + p[i].x));								 /* get the 1/2 way point */
   point.y = (0.5 * (point.y + p[i].y));
   XDrawPoint(display, win, gc, point.x, point.y);						       /* draws the halfway point */
   i = rand()%3;						      /* gets a random number with modulo 3, returns only (0,1,2) */
  }
}

/*********************************************************** MANDELBROT SET *******************************************************/

void mandelbrot()
{
 int		numrows, numcols, maxnum;			      /* number of rows, columns and maximum number of iterations */
 int		count, i, j;
 double		left, top, right, bottom, thresh;					       /* window parameters and threshold */
 tRealPoint	c; 											       /* point to be IFS */

 fscanf(input_file,"%lf %lf %lf %lf %lf %d %d %d \n", &left, &top, &right, &bottom, &thresh, &numrows, &numcols, &maxnum);

 setRealRect(left, top, right, bottom, &W);									 /* set up window */
 V = matchW2V(W, &gDisplay);					            /* match window aspect ratio to viewport aspect ratio */
 Map = W2V(W,V);											/* map window to viewport */

 for(i = 0; i < numrows; i++)
  {
   for(j = 0; j < numcols; j++)
    {
     c.x = W.l + (W.r - W.l) * (j - 1.0) / (numcols - 1);					/* set up x tuple of c coordinate */
     c.y = W.t + (W.b - W.t) * (i - 1.0) / (numrows - 1);					/* set up y tuple of c coordinate */
     count = mandelcount(c, maxnum, thresh);
     if(count >= maxnum) 
      {
       dot(c.x, c.y);								     /* draws a dot (point) in screen coordinates */
      }
    }
  }
}

/************************************************************ MANDEL COUNT ********************************************************/

int mandelcount(tRealPoint c, int maxnum, double thresh)
{
 double		x, y, tmp, fsq;
 int		count;

 x = c.x; y = c.y; fsq = (x * x) + (y * y); count = 1;			      /* initializing variables from procedure mandelbrot */

 while((count <= maxnum) && (fsq <= thresh))					       /* check if point is in the mandelbrot set */
  {
   tmp = x;												    /* save old real part */
   x = (x * x) - (y * y) + c.x;											 /* new real part */
   y = 2.0 * tmp * y + c.y;										    /* new imaginary part */
   fsq = (x * x) + (y * y);									   /* square of size of new point */
   count++;
  }
 return(count);											     /* number of iterations used */
}

/******************************************************** POSTSCRIPT DRIVERS ******************************************************/

/****************************************************** INITIALIZE POSTSCRIPT *****************************************************/

void Init_Post(int vportWid,int vportHt)
 { 
/* 

1. Puts proper stuff in PS file to print title, if any.
2. Defines m for moveto, l for lineto, s for stroke for brevity.
3. Sets PS coord system with origin near upper left of landscape page, x-axis to right, y-axis downward, leaving margin around page.
4. Uses vportWid,vportHt (the width and height of viewport in pixels), to find viewport's aspect ratio. Then compares it with aspect
   ratio of PS drawing area, and sets PS scaleFactor so viewport maps to largest region possible having same aspect ratio as 
   viewport, tucked in upper left corner of PS page.  */

  int H, W;
  double vportAspect, scaleFactor;
  char fmt[]= "/Helvetica findfont 12 scalefont setfont 144 36 m ( Robert Estey - UMASS ECE660 / NTU ST740A - Program 1 ) show\n";
  char fname[30], picname[80];

  if (vportWid == 0 || vportHt == 0){puts("degenerate viewport !! I'm outa here.."); return;}
      vportAspect = (double)vportHt / (double)vportWid;

  fprintf(postscript_file," %% fsh, file = lab 1 \n"); 							/* put comment in PS file */
  fprintf(postscript_file,"/m {moveto}def /l {lineto}def /s {stroke} def\n");
  fprintf(postscript_file,"%d 0 translate 90 rotate\n",pagewidth);	  	    /* landscape, origin at bottom left for title */

  fprintf(postscript_file,fmt,"lab 1");	 								      	      /* print it */

                                                          /* shift and invert coord syst so origin is at top, and y-axis downward */
								     /* 3/4 inch border * 72 is 54 points, to be left around page */

  fprintf(postscript_file," %d %d translate 1 1 neg scale\n",margin,pagewidth-margin);

									  	     /* set proper scale for largest drawing area */
  H = pagewidth - 2 * margin;  								     /* drawable height of page in points */
  W = pageheight - 2 * margin; 								      /* drawable width of page in points */

  scaleFactor = (vportAspect > (double)H / W ? 	H / (double)vportHt : W / (double)vportWid);
  fprintf(postscript_file," %8.4lf %8.4lf scale\n", scaleFactor, scaleFactor);
} 													      /* end of Init_Post */

/******************************************************** WRITE POSTSCRIPT ********************************************************/

void Write_Post(int x, int y, int which)
			     /* if which = 1 then write m for moveto(); else write l for lineto(), and handle count before stroke */
{
 static int countForStroke = 0; 									/* init'd first time only */
 static int countOnLine = 0;
 									 /* countForStroke: # of lineto's so far since last stroke*/
 								    /* countOnLine: # of entries so far on current line of PS file*/
 countForStroke++;
 countOnLine++;
 if(countOnLine > 8)
  {
   countOnLine = 0;
   fprintf(postscript_file,"\n"); 										    /* write <CR> */
  };

 if(which == 1) 
  { 													  /* about to do moveto() */
   if(countForStroke > NumBeforeStroke) 							 /* put stroke before next moveto */
    {
     countForStroke = 0;
     fprintf(postscript_file," s %d %d m ", x, y);
    }
   else 												      /* no stroke needed */
    fprintf(postscript_file,"%d %d m ", x, y);
  }
 else 												    /* which <> 1, so do a lineto */
  fprintf(postscript_file,"%d %d l ", x, y);
} 													   /* end of Write_Post() */

/************************************************************ CLIPPING ************************************************************/

int clip(tIntPoint *pa, tIntPoint *pb)					    /* return 1 if some segment is visible, else return 0 */
{
 int cA, cB, dx, dy, t = 0, count = 0;
 
 do {
     count++;
     cA = cB = 0;
     if(pa ->x < V.l) 	cA |= toLeft;		if(pb ->x < V.l)   cB |= toLeft;
     if(pa ->y < V.t) 	cA |= Above;		if(pb ->y < V.t)   cB |= Above;
     if(pa ->x > V.r)	cA |= toRight;		if(pb ->x > V.r)   cB |= toRight;
     if(pa ->y > V.b)	cA |= Below;		if(pb ->y > V.b)   cB |= Below;

     if((cA == 0) &&  (cB == 0)) {if(t)printf("\n %0X %0x triv accept",cA,cB); return 1;}      /* trivial accept, both pts inside */

     if((cA & cB) !=0) {if(t)printf("\n%0X %0x %0x %d triv reject", cA,cB, cA &cB, count); return 0;} 		/* trivial reject */

     if(t)printf("\n %0X %0x chop",cA,cB);
     dx = pb->x - pa->x; 									       /* rise and run of segment */
     dy = pb->y - pa->y;
     if(cA !=0) 											  /* chop away at point A */
      {
       if(cA & toLeft){pa->y += (V.l - pa->x) * (double)dy / dx; pa ->x = V.l;}
       else if(cA & toRight){pa->y += (V.r - pa->x) * (double)dy / dx; pa ->x = V.r;}
       else if(cA & Below){pa->x += (V.b - pa->y) * (double)dx / dy; pa ->y = V.b;}
       else if(cA & Above){pa->x += (V.t - pa->y) * (double)dx / dy; pa ->y = V.t;}
      } 												      /* end of if cA !=0 */
				
     else if(cB !=0) /* chop away at point A */
      {
       if(cB & toLeft){pb->y += (V.l - pb->x) * (double)dy / dx; pb ->x = V.l;}
       else if(cB & toRight){pb->y += (V.r - pb->x) * (double)dy / dx; pb ->x = V.r;}
       else if(cB & Below){pb->x += (V.b - pb->y) * (double)dx / dy; pb ->y = V.b;}
       else if(cB & Above){pb->x += (V.t - pb->y) * (double)dx / dy; pb ->y = V.t;}
      } 												      /* end of if cB !=0 */

    } while(count < 100); printf("\n caught in loop!!"); 						 /* should never get here */
}

/******************************************************** DEV CLIP LINE TO ********************************************************/

void devClipLineTo(int x, int y)				       /* draw clipped line from RealCP to (x,y) in screen coords */
{
 tIntPoint first,second;

 first.x = RealCP.x; first.y = RealCP.y; second.x = x; second.y = y; 				  /* get the 2 clipped end points */

 if(clip(&first, &second) == 1) 
  XDrawLine(display, win, gc, first.x, first.y, second.x, second.y);	     			     /* draw clipped line segment */
 moveto(x,y); 												  	 /* update RealCP */
}

/************************************************************* ROSETTE ************************************************************/

void Rosette(int n, int radius, int cx, int cy)						/* sample: draw rosette in screen coord's */
{
 int i,j;
 tIntPoint vert[100];
 double delang;
												     /* make up array of vertices */
 if( n < 3 || n > 100) return; 												 /* bad n */
 delang = 6.2831856/n; 												      /* 2 pi / n */

 for(i = 0; i < n; i++)
  {
   vert[i].x = (int)(radius * cos(i * delang) + cx); vert[i].y = (int)(radius * sin(i * delang) + cy);
  }	
 for(i = 0;i < n;i++) 											  /* now draw the rosette */
  for(j = i+1; j < n; j++)
   {
    moveto(vert[i].x, vert[i].y); 
    devClipLineTo(vert[j].x, vert[j].y);
   }
}
