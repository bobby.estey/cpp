# C RESTful Example

## CURL

curl --location --request GET 'http://localhost:6464/users' \
--header 'Authorization: Basic Y3Y2NDpwYXNzd29yZA==' \
--header 'Cookie: JSESSIONID=CE14DE8A659F14EABC4CE7CC7AA24556'

## Start a REST Service
     
- spring-boot-restful-web-services

## Make Request

- localhost:6464/users
     
## Requests / Results

![Browser Signin](./docs/browserSignin.png)

![Browser Results](./docs/browserResults.png)

![Postman Request & Results](./docs/postmanRequestResults.png)
